#ifndef BINARY_H
#define BINARY_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <cstdint>
#include <string>
#include <vector>

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

void binary_append(vector<uint8_t> &buffer, uint64_t var, uint32_t length);
void binary_append(vector<uint8_t> &buffer, string &var, uint32_t length);

void binary_overwrite(vector<uint8_t> &buffer, size_t pos, uint64_t var, uint32_t length);

uint8_t binary_read8(uint8_t *pData, uint32_t *offset);
uint16_t binary_read16(uint8_t *pData, uint32_t *offset);
uint32_t binary_read32(uint8_t *pData, uint32_t *offset);

#endif