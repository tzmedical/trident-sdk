/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "action.h"

#include <iostream>
#include <iomanip>
#include <vector>

#include "util/binary.h"
#include "util/json.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
_actionData::_actionData(
  string parsedName, actionDataType dataType, uint8_t length, string descriptionText)
{
  parseStr = parsedName;
  type = dataType;
  dataLength = length;
  maxLength = length;
  description = descriptionText;

  setFlag = 0;
  dataVal = 0;
  dataStr = "";
}

//==============================================================================
_actionData::_actionData(
  string parsedName, actionDataType dataType, uint8_t length, uint64_t constantValue)
{
  parseStr = parsedName;
  type = dataType;
  dataLength = length;
  maxLength = length;
  description = "";

  setFlag = 0;
  dataVal = constantValue;
  stringstream dataStream;
  dataStream << dataVal;
  dataStream >> dataStr;
}

//==============================================================================
int _actionData::set(string dS, string actionName, ostream *errStream)
{
  if (ACTION_TYPE_BOOLEAN == type) {
    if (string::npos != dS.find("true")) {
      dataStr.assign("true");
      dataVal = 1;
    }
    else if (string::npos != dS.find("false")) {
      dataStr.assign("false");
      dataVal = 0;
    }
    else {
      stringstream argStream;
      argStream << dS;
      argStream >> dataVal;
      dataStr.assign("");

      if (dataVal) {
        dataStr.assign("true");
      }
      else {
        dataStr.assign("false");
      }
    }
  }
  else if (ACTION_TYPE_NUMBER == type) {
    stringstream argStream;
    argStream << dS;
    argStream >> dataVal;
    dataStr.assign("");

    dataStr.assign(dS);
  }
  else if (ACTION_TYPE_VALIDATION_CONSTANT == type) {
    *errStream << "ERROR! Validation constant shouldn't be set in input file: action." << actionName
               << "." << parseStr << endl;
    return -1;
  }
  else if (ACTION_TYPE_STRING == type) {
    dataStr.assign(dS);
    dataVal = 0;
    dataLength = 0;
    if (dataStr.length() > maxLength) {
      *errStream << "ERROR! Invalid string data: action." << actionName << "." << parseStr << "="
                 << dataStr << endl;
      *errStream << "---Max Length: " << (int)maxLength << endl;
      *errStream << "---Input Length: " << (int)dataStr.length() << endl;
      return -1;
    }
  }
  else {
    *errStream << "ERROR! Unsupported action data type." << endl;
    return -1;
  }

  setFlag = 1;
  return 0;
}

//==============================================================================
int _actionData::setBinary(
  const uint8_t *pData, const uint32_t readLength, string actionName, ostream *errStream)
{
  if ((ACTION_TYPE_BOOLEAN == type) && (0 == maxLength)) {
    dataVal = 1;
    dataStr.assign("true");
  }
  else if ((ACTION_TYPE_VALIDATION_CONSTANT == type)) {
    uint64_t tempData = 0;
    size_t i;

    if (readLength < maxLength) {
      *errStream << "ERROR! Invalid data length: " << readLength << " (" << actionName << "."
                 << parseStr << "?)" << endl;
      return -1;
    }

    for (i = 0; i < maxLength; i++) {
      if (i < sizeof(tempData)) {
        tempData += ((uint64_t)pData[i]) << (8 * i);
      }
    }

    if (tempData != dataVal) {
      *errStream << "ERROR! Invalid validation field: " << tempData << " (" << actionName << "."
                 << parseStr << "?)" << endl;
      return -1;
    }
  }
  else if (ACTION_TYPE_NUMBER == type || ACTION_TYPE_BOOLEAN == type) {
    dataVal = 0;
    size_t i;

    if (readLength < maxLength) {
      *errStream << "ERROR! Invalid data length: " << readLength << " (" << actionName << "."
                 << parseStr << "?)" << endl;
      return -1;
    }

    for (i = 0; i < maxLength; i++) {
      if (i < sizeof(dataVal)) {
        dataVal += ((uint64_t)pData[i]) << (8 * i);
      }
    }

    if (ACTION_TYPE_BOOLEAN == type) {
      if (dataVal) {
        dataStr.assign("true");
      }
      else {
        dataStr.assign("false");
      }
    }
    else {
      stringstream dataStream;
      dataStream << dataVal;
      dataStream >> dataStr;
    }
  }
  else {
    if (!readLength) {
      setFlag = 0;
      return 0;
    }
    dataStr.assign((const char *)pData);
    dataVal = 0;
    dataLength = 0;
  }

  setFlag = 1;

  return 0;
}

//==============================================================================
int _actionData::write(vector<uint8_t> &buffer)
{
  int i = 1;

  if (
    (ACTION_TYPE_NUMBER == type) || (ACTION_TYPE_BOOLEAN == type)
    || (ACTION_TYPE_VALIDATION_CONSTANT == type)) {
    binary_append(buffer, dataVal, dataLength);
    i += dataLength;
  }
  else {
    binary_append(buffer, dataStr, dataStr.length());
    i += dataStr.length();
  }

  return i;
}

//==============================================================================
string _actionData::getParseStr()
{
  if (!parseStr.length()) {
    return "=";
  }
  return "." + parseStr + "=";
}

//==============================================================================
string _actionData::getJsonStr()
{
  return parseStr;
}

//==============================================================================
uint8_t _actionData::getDataLength()
{
  if (
    (ACTION_TYPE_NUMBER == type) || (ACTION_TYPE_BOOLEAN == type)
    || (ACTION_TYPE_VALIDATION_CONSTANT == type)) {
    return dataLength;
  }
  else {
    return dataStr.length();
  }
}

//==============================================================================
actionDataType _actionData::getDataType()
{
  return type;
}

//==============================================================================
int _actionData::isSet()
{
  return setFlag;
}

//==============================================================================
string _actionData::writeActionText()
{
  return getParseStr() + dataStr;
}

//==============================================================================
string _actionData::writeActionJson()
{
  if (!parseStr.length()) {
    return "\"" + dataStr + "\"";
  }
  // Escape risky special characters for JSON
  string tempString = dataStr;
  for (size_t i = 0; i < tempString.length(); i++) {
    if ('"' == tempString[i] || '\\' == tempString[i]) {
      if (i == 0 || '\\' != tempString[i - 1]) {
        tempString.insert(i, "\\");
      }
    }
  }
  return "\"" + parseStr + "\":\"" + tempString + "\"";
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__);
//
//-----------------------------------------------------------------------------

//==============================================================================
_action::_action(
  string parsedName, uint8_t actionTag, string displayName,
  initializer_list<_actionData> actionData, uint8_t minFWVersion, uint8_t maxFWVersion,
  string tooltipText, size_t fileLimit, actionAvailability avail)
{
  parseStr = parsedName;
  tag = actionTag;
  name = displayName;
  dataVector = actionData;

  minFW = minFWVersion;
  maxFW = maxFWVersion;
  tooltip = tooltipText;
  maxPerFile = fileLimit;
  availability = avail;

  actionIndex = 0;
}

//==============================================================================
int _action::parseJson(rapidjson::Value &actionData, ostream *errStream)
{
  int retVal = 0;
  string dS;
  stringstream ss;

  if (actionData.IsString() && dataVector.size() == 1) {
    string dataParseStr = dataVector[0].getJsonStr();
    dS = actionData[dataParseStr.c_str()].GetString();
    retVal = dataVector[0].set(dS, parseStr, errStream);
    if (retVal) {
      return retVal;
    }
  }
  else if (actionData.IsObject()) {
    for (size_t i = 0; i < dataVector.size(); i++) {
      string dataParseStr = dataVector[i].getJsonStr();
      if (actionData.HasMember(dataParseStr.c_str())) {
        if (actionData[dataParseStr.c_str()].IsString()) {
          dS = actionData[dataParseStr.c_str()].GetString();
        }
        else if (actionData[dataParseStr.c_str()].IsBool()) {
          bool temp = actionData[dataParseStr.c_str()].GetBool();
          if (temp) {
            dS.assign("true");
          }
          else {
            dS.assign("false");
          }
        }
        else if (actionData[dataParseStr.c_str()].IsInt()) {
          volatile int temp = actionData[dataParseStr.c_str()].GetInt();
          ss << temp;
          dS.assign(ss.str());
        }

        retVal = dataVector[i].set(dS, parseStr, errStream);
        if (retVal) {
          return retVal;
        }
      }
    }
  }
  else {
    return -1;
  }

  return retVal;
}

//==============================================================================
int _action::parseJson(string actionData, ostream *errStream)
{
  int retVal = 0;

  if (dataVector.size() == 1) {
    retVal = dataVector[0].set(actionData, parseStr, errStream);
    if (retVal) {
      return retVal;
    }
  }
  else {
    return -1;
  }

  return retVal;
}

//==============================================================================
int _action::parseTextIndex(string dS, ostream *errStream)
{
  int retVal = -1;
  if (maxPerFile > 1) {
    string fullParseStr = getParseStr();
    size_t found = dS.find(fullParseStr);
    if (found != string::npos) {
      found = found + fullParseStr.length() + 1;
      if (dS[found] >= '0' && dS[found] <= '9') {
        stringstream indexStream(dS.substr(found));
        indexStream >> retVal;
      }
    }
  }

  return retVal;
}

//==============================================================================
int _action::parseText(string dS, ostream *errStream)
{
  int retVal = 0;

  for (size_t i = 0; i < dataVector.size(); i++) {
    string dataParseStr = dataVector[i].getParseStr();
    size_t found = dS.find(dataParseStr);
    if (found != string::npos || dataParseStr.length() <= 1) {
      actionIndex = parseTextIndex(dS, errStream);
      if (actionIndex < 0) actionIndex = 0;
      retVal = dataVector[i].set(dS.substr(found + dataParseStr.length()), parseStr, errStream);
      break;
    }
  }

  return retVal;
}

//==============================================================================
int _action::parseBinary(const uint8_t *pData, const uint32_t readLength, ostream *errStream)
{
  int retVal = 0;
  size_t i;
  uint32_t j = 0;

  if (actionIndex <= maxPerFile) {
    for (i = 0; i < dataVector.size(); i++) {
      if (j <= readLength) {
        retVal = dataVector[i].setBinary(&pData[j], readLength - j, parseStr, errStream);
        if (retVal) return retVal;
        j += dataVector[i].getDataLength();
      }
    }
    actionIndex++;
  }

  return retVal;
}

//==============================================================================
int _action::writeActionBinary(
  vector<uint8_t> &buffer, bool badAlignment, uint8_t firmwareVersion, ostream *errStream)
{
  int i = 0;
  uint32_t j;

  uint8_t dataLength = 0;
  if (isSet()) {
    i += 3;
    binary_append(buffer, tag, 1);

    for (j = 0; j < dataVector.size(); j++) {
      dataLength += dataVector[j].getDataLength();
    }
    binary_append(buffer, dataLength, 1);

    for (j = 0; j < dataVector.size(); j++) {
      i += dataVector[j].write(buffer);
    }

    if (badAlignment) {
      binary_append(buffer, 0xc4, 1);
    }
    else {
      binary_append(buffer, '\0', 1);
    }
  }

  return i;
}

//==============================================================================
int _action::getTag()
{
  return (int)tag;
}

//==============================================================================
// uint64_t _action::getDataVal()
// {
//   return dataVal;
// }

//==============================================================================
string _action::getParseStr()
{
  return "." + parseStr;
}

//==============================================================================
string _action::getJsonStr()
{
  return parseStr;
}

//==============================================================================
int _action::getActionIndex()
{
  if (maxPerFile <= 1) return -1;
  return actionIndex;
}

//==============================================================================
int _action::writeActionText(ostream &outStream)
{
  int retVal = 0;
  size_t i;
  for (i = 0; i < dataVector.size(); i++) {
    if (dataVector[i].isSet() && ACTION_TYPE_VALIDATION_CONSTANT != dataVector[i].getDataType()) {
      outStream << "action." << parseStr;
      if (maxPerFile > 1) {
        outStream << "." << actionIndex - 1;
      }
      outStream << dataVector[i].writeActionText() << endl;
    }
  }

  return retVal;
}

//==============================================================================
int _action::writeActionJson(ostream &outStream)
{
  int retVal = 0;
  bool needComma = 0;
  outStream << "{\"" << JSON_ACTIONS_TYPE_LABEL << "\":\"" << parseStr << "\",\"";
  outStream << JSON_ACTIONS_DATA_LABEL << "\":";

  if (dataVector.size() == 1 && dataVector[0].getParseStr().length() == 1) {
    outStream << dataVector[0].writeActionJson();
  }
  else {
    outStream << "{";
    size_t i;
    for (i = 0; i < dataVector.size(); i++) {
      if (dataVector[i].isSet() && ACTION_TYPE_VALIDATION_CONSTANT != dataVector[i].getDataType()) {
        if (needComma) {
          outStream << ",";
        }
        outStream << dataVector[i].writeActionJson();
        needComma = 1;
      }
    }

    outStream << "}";
  }
  outStream << "}";

  return retVal;
}

//==============================================================================
// int _action::writeMetaJson(ostream &outStream)
// {
//   int retVal = 0;

//   outStream << "{";
//   outStream << "\"id\":\"" << parseStr << "\",";
//   if (ACTION_TYPE_BOOLEAN == type) {
//     outStream << "\"type\":\"boolean\",";
//     outStream << "\"min\":" << (int)min << ",";
//     outStream << "\"max\":" << (int)max << ",";
//     outStream << "\"units\":\"" << units << "\",";
//   }
//   else if (ACTION_TYPE_NUMBER == type) {
//     outStream << "\"type\":\"number\",";
//     outStream << "\"min\":" << (int)min << ",";
//     outStream << "\"max\":" << (int)max << ",";
//     outStream << "\"units\":\"" << units << "\",";
//   }
//   else if (ACTION_TYPE_STRING == type) {
//     outStream << "\"type\":\"string\",";
//     outStream << "\"maxLength\":" << (int)maxLength << ",";
//   }
//   outStream << "\"minVersion\":" << fixed << setprecision(1) << (float)minFW / 10.0 << ",";
//   outStream << "\"maxVersion\":" << (float)maxFW / 10.0 << ",";
//   outStream << "\"name\":\"" << name << "\",";
//   if (ALL_DEVICES == availability) {
//     outStream << "\"mctOnly\":false,";
//   }
//   else if (MCT_ONLY == availability) {
//     outStream << "\"mctOnly\":true,";
//   }
//   outStream << "\"tooltip\":\"" << tooltip << "\"";
//   outStream << "}";

//   return retVal;
// }

//==============================================================================
int _action::isSet()
{
  int retVal = 0;

  size_t i;
  for (i = 0; i < dataVector.size(); i++) {
    if (dataVector[i].isSet()) {
      retVal++;
    }
  }

  return retVal;
}

//==============================================================================
int _action::checkFirmwareVersion(uint8_t firmware_version, uint8_t print_errs, ostream *errStream)
{
  if (firmware_version < minFW) {
    if (print_errs) {
      *errStream << "ERROR! Action \'" << parseStr << "\' is not supported in versions below v"
                 << (float)minFW / 10.0 << endl;
    }
    return -1;
  }
  else if (firmware_version > maxFW) {
    if (print_errs) {
      *errStream << "ERROR! Action \'" << parseStr << "\' is not supported in versions above v"
                 << (float)maxFW / 10.0 << endl;
    }
    return -1;
  }
  else {
    return 0;
  }
}