
#ifndef SETTING_H
#define SETTING_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <cstdint>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <iostream>
#include "rapidjson/document.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define UNUSED_FIELD 0

/***   Possible TAG values for each entry   ***/
enum settingTags {
  BRADY_BPM = 1,
  TZMR_BRADY_OFFSET_BPM = 2,
  TZMR_BRADY_DURATION = 3,
  TACHY_BPM = 4,
  TZMR_TACHY_OFFSET_BPM = 5,
  TZMR_TACHY_DURATION = 6,
  TZMR_AF_DURATION = 7,
  PAUSE_DURATION = 8,
  BRADY_RATE_CHANGE = 10,
  TACHY_RATE_CHANGE = 11,
  ARRHYTHMIA_MIN_DURATION = 12,
  AF_RATE_CHANGE = 13,
  ARRHYTHMIA_OFFSET_DURATION = 14,
  TZMR_AF_RR_COUNT = 20,
  TZMR_DERIVE_FOR_ANALYSIS = 21,
  TZMR_RPEAK_THRESHOLD = 22,
  TZMR_ANALYSIS_CHANNEL_MASK = 30,
  DETECT_PACEMAKER = 31,
  TZMR_CROSS_CHANNEL_ANALYSIS = 32,
  TZMR_CHANNEL_ENABLE_MASK = 33,
  TZMR_USE_ARRHYTHMIA_STATE_MACHINE = 34,
  TZMR_PIM_ENABLE = 35,
  TZMR_PIM_THRESHOLD = 36,
  TZMR_SAMPLE_CALIBRATION = 37,
  TZMR_LOG_QRS_COMPLEXES = 38,
  TZMR_PIM_HYSTERESIS = 39,
  SUMMARIZE_QRS_DETECTION = 40,
  PEDOMETER_PERIOD = 41,
  STORE_RAW_ACCEL = 42,
  REPORT_BATTERY_TEMPERATURE = 43,
  TZMR_REPORT_START = 51,
  TZMR_REPORT_STOP = 52,
  TZMR_REPORT_BREAK = 53,
  TZMR_REPORT_RESUME = 54,
  TZMR_REPORT_FULL = 55,
  TZMR_REPORT_PACE_DETECT = 56,
  TZMR_REPORT_ANALYSIS_RESUMED = 60,
  TZMR_REPORT_ANALYSIS_PAUSED = 61,
  TZMR_REPORT_LEAD_DISCONNECTED = 62,
  TZMR_REPORT_TACHY_ONSET = 63,
  TZMR_REPORT_TACHY_OFFSET = 64,
  TZMR_REPORT_BRADY_ONSET = 65,
  TZMR_REPORT_BRADY_OFFSET = 66,
  TZMR_REPORT_AF_ONSET = 67,
  TZMR_REPORT_AF_OFFSET = 68,
  TZMR_REPORT_PAUSE_ONSET = 69,
  TZMR_REPORT_PATIENT_EVENT = 70,
  TZMR_REPORT_BATTERY_LOW = 71,
  TZMR_REPORT_CHARGING_STARTED = 72,
  TZMR_REPORT_CHARGING_STOPPED = 73,
  TZMR_REPORT_SERVER_REQUEST = 74,
  TZMR_REPORT_PAUSE_OFFSET = 75,
  TZMR_REPORT_SCP_RETRANSMIT = 76,
  TZMR_REPORT_TZR_REQUEST = 77,
  TZMR_REPORT_PVC_ONSET = 78,
  LENGTH_IS_CALENDAR_DAYS = 95,
  ONE_ECG_FILE = 96,
  STUDY_HOURS = 97,
  TZMR_INVERT_SCP_ECG = 98,
  TZMR_INVERT_ONSCREEN_ECG = 99,
  TZMR_SCP_DIFFERENCE_ENCODING = 100,
  SAMPLE_RATE = 101,
  TZMR_LP_FILTER = 102,
  TZMR_SCP_LENGTH = 103,
  TZMR_LEAD_CONFIG = 104,
  REPORT_PRE_TIME = 105,
  REPORT_POST_TIME = 106,
  TZMR_TTM_ENABLE = 107,
  TZMR_STUDY_LENGTH = 108,
  TZMR_BIT_RESOLUTION = 109,
  TZMR_HUFFMAN_ENCODING = 110,
  DIGITAL_HP_FILTER = 111,
  START_OK_CODE = 112,
  DIGITAL_LP_FILTER = 113,
  DIGITAL_NOTCH_FILTER = 114,
  TZMR_FILL_SQUARE_WAVES = 115,
  TZMR_RECORD_CONTINUOUS = 116,
  TZMR_REPORT_OFFSET_PRE_TIME = 117,
  TZMR_REPORT_OFFSET_POST_TIME = 118,
  TZMR_REPORT_BPM_PERIOD = 119,
  TZMR_TTM_MODE = 120,
  TZMR_TTM_TOP_BYTE = 121,
  TZMR_TTM_FAILOVER_PERIOD = 122,
  TZMR_TTM_QUEUE_LENGTH = 123,
  TZMR_DISPLAY_SIGNAL_BARS = 124,
  MIN_TIME_BETWEEN_EVENTS = 125,
  SUPP_RHYTHM_EVENTS = 126,
  PATIENT_PRE_TIME = 127,
  PATIENT_POST_TIME = 128,
  IGNORE_HAPTICS_ERRORS = 143,
  NAG_ON_REPLACE_ELECTRODE = 144,
  NAG_ON_LOW_BATTERY = 145,
  PATCH_MAX_WEAR_LENGTH = 146,
  ALLOW_LOW_BATTERY_START = 147,
  STUDY_COMPLETE_SLEEP = 148,
  NAG_ON_LEAD_OFF = 149,
  TZMR_MAX_STREAMING_DELAY = 150,
  TZMR_TRIM_EVENTS = 151,
  TZMR_WIRELESS_ENABLE = 152,
  TZMR_AUTO_ANSWER = 153,
  TZMR_SYNC_TIME = 154,
  BULK_UPLOAD = 155,
  TRANSMIT_INTERVAL_REPORTS = 156,
  TZMR_SILENT_MODE = 157,
  TZMR_VIB_CLICKS = 158,
  SCREEN_SLEEP_TIME = 159,
  DEMO_MODE = 160,
  TZMR_SMS_ENABLE = 161,
  TZMR_VOICE_ENABLE = 162,
  TZMR_CHECK_ACTIONS_INTERVAL = 163,
  TZMR_STUDY_END_DATE_TIME = 164,
  SYMPTOM_DIARY_COUNT = 165,
  ACTIVITY_DIARY_COUNT = 166,
  TZMR_STAGGER_INTERVAL_UPLOADS = 167,
  TZMR_LONG_EVENT_THRESHOLD = 168,
  TZMR_LONG_EVENT_VOLTAGE = 169,
  TZMR_MODEM_VOLTAGE = 170,
  TZMR_MODEM_TEMPERATURE = 171,
  TZMR_REMEMBER_LAST_DIARY = 172,
  DIARY_SYMPTOM_1 = 181,
  DIARY_SYMPTOM_2 = 182,
  DIARY_SYMPTOM_3 = 183,
  DIARY_SYMPTOM_4 = 184,
  DIARY_SYMPTOM_5 = 185,
  DIARY_SYMPTOM_6 = 186,
  DIARY_SYMPTOM_7 = 187,
  DIARY_SYMPTOM_8 = 188,
  DIARY_SYMPTOM_9 = 189,
  DIARY_SYMPTOM_10 = 190,
  DIARY_ACTIVITY_1 = 191,
  DIARY_ACTIVITY_2 = 192,
  DIARY_ACTIVITY_3 = 193,
  DIARY_ACTIVITY_4 = 194,
  DIARY_ACTIVITY_5 = 195,
  DIARY_ACTIVITY_6 = 196,
  DIARY_ACTIVITY_7 = 197,
  DIARY_ACTIVITY_8 = 198,
  DIARY_ACTIVITY_9 = 199,
  DIARY_ACTIVITY_10 = 200,
  PATIENT_ID = 201,
  TZMR_PATIENT_NAME = 202,
  TZMR_EVENT_STATIC_PAYLOAD = 203,
  TZMR_INCOMING_NUMBER = 210,
  TOP_BANNER = 211,
  BOTTOM_BANNER = 212,
  SERVER_ADDRESS = 213,
  TLS_IDENTITY = 214,
  TLS_PSK = 215,
  APN_ADDRESS = 216,
  APN_USERNAME = 217,
  APN_PASSWORD = 218,
  HTTP_BEARER_TOKEN = 219,
  TZMR_USE_HTTP = 220,
  TZMR_CHECK_FOR_CLEAN_SERVER = 221,
  TZMR_VERIFY_SERVER_CHANGES = 222,
  TZMR_WAIT_FOR_HTTP_RESPONSE = 223,
  TZMR_REDOWNLOAD_DELAY = 224,
  CONNECTION_TIMEOUT = 225,
  AES_KEY = 230,
  AES_IV = 231,
  WIFI_SSID = 232,
  WIFI_PASSWORD = 233,
  BAD_TAG = 250,
  ERROR_RETRIES = 251,
  ERROR_PERIOD = 252,
  ZYMED_COMPAT = 253,
  TZMR_COMPAT = 254,
  TERM_TAG = 255
};

enum settingType {
  SETTING_TYPE_BOOLEAN = 0,
  SETTING_TYPE_NUMBER = 1,
  SETTING_TYPE_STRING = 3,
  SETTING_TYPE_HEX = 4,
  SETTING_TYPE_DATETIME = 5,
};

enum settingAvailability { ALL_DEVICES = 0, MCT_ONLY = 1 };

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

class _setting {
 private:
  string parseStr;
  uint8_t tag;
  string name;
  settingType type;
  uint64_t min;
  uint64_t max;
  uint8_t dataLength;
  uint8_t maxLength;
  uint8_t minFW;
  uint8_t maxFW;
  string tooltip;
  string units;
  settingAvailability availability;
  vector<uint64_t> options;

  uint8_t setFlag;
  uint64_t dataVal;
  string dataStr;

 public:
  _setting(
    string parsedName, uint8_t settingTag, string displayName, settingType valueType,
    uint64_t minValue, uint64_t maxValue, uint8_t dataLen, uint8_t minFWVersion,
    uint8_t maxFWVersion, string tooltipText, string unitsText,
    settingAvailability avail = ALL_DEVICES, vector<uint64_t> optionsList = {});

  int set(string dS, bool check_range, ostream *errStream = &cerr);
  int setJson(rapidjson::Value &settingData, bool check_range, ostream *errStream = &cerr);
  int setBinary(
    const uint8_t *pData, const uint32_t readLength, bool check_range, ostream *errStream = &cerr);
  int write(vector<uint8_t> &buffer, bool badAlignment, uint8_t firmwareVersion);
  int getTag();

  uint64_t getDataVal();
  string getParseStr();
  string getJsonStr();

  string getSettingText();
  string getSettingJson();
  string getMetaJson();
  string getOptionsJson();

  int isSet();
  int checkFirmwareVersion(uint8_t firmware_version, uint8_t print_errs, ostream *errStream);
};

#endif
