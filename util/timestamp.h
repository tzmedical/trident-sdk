#ifndef TIME_STRING_H
#define TIME_STRING_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <cstdint>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

class timestamp {
 private:
  struct tm t;
  uint32_t milliseconds;
  int16_t timeZone;

#ifndef _WIN32
  time_t mktimeCorrectionOffset;
  time_t calculateSecondsToUTC();
#endif
  void adjust_utc_for_timezone(struct tm *p_t, const int time_zone);
  void convert_to_utc(struct tm *p_t, const int time_zone);

 public:
  timestamp();
  timestamp(const struct tm *t, uint32_t ms, int16_t zone);

  int parseBinary(uint8_t *pRead, int32_t zoneOffset, ostream *errStream);
  int parseText(string timeString);

  int sanityCheck();

  int writeBinary(uint8_t *pBuffer);
  void writeBinary(vector<uint8_t> &buffer);

  string getJson();
  string getText();

  string getTzrName();

  uint64_t getMsEpoch();

  struct tm getRawStruct();
  int16_t getTimeZone();
};

#endif