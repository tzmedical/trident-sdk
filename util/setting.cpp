/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "setting.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include "util/binary.h"
#include "util/json.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__);
//
//-----------------------------------------------------------------------------

_setting::_setting(
  string parsedName, uint8_t settingTag, string displayName, settingType valueType,
  uint64_t minValue, uint64_t maxValue, uint8_t maxLen, uint8_t minFWVersion, uint8_t maxFWVersion,
  string tooltipText, string unitsText, settingAvailability avail, vector<uint64_t> optionsList)
{
  parseStr = parsedName;
  tag = settingTag;
  name = displayName;
  type = valueType;
  min = minValue;
  max = maxValue;
  dataLength = maxLen;
  maxLength = maxLen;
  minFW = minFWVersion;
  maxFW = maxFWVersion;
  tooltip = tooltipText;
  units = unitsText;
  availability = avail;
  options = optionsList;

  setFlag = 0;
  dataVal = 0;
  dataStr = "";
}

//==============================================================================
int _setting::setJson(rapidjson::Value &settingData, bool check_range, ostream *errStream)
{
  int retVal = 0;
  string dS;
  stringstream ss;

  if (settingData[JSON_SETTINGS_DATA_LABEL].IsString()) {
    dS = settingData[JSON_SETTINGS_DATA_LABEL].GetString();
  }
  else if (settingData[JSON_SETTINGS_DATA_LABEL].IsBool()) {
    bool temp = settingData[JSON_SETTINGS_DATA_LABEL].GetBool();
    if (temp) {
      dS.assign("true");
    }
    else {
      dS.assign("false");
    }
  }
  else if (settingData[JSON_SETTINGS_DATA_LABEL].IsInt()) {
    volatile int temp = settingData[JSON_SETTINGS_DATA_LABEL].GetInt();
    ss << temp;
    dS.assign(ss.str());
  }

  retVal = set(dS, check_range, errStream);

  return retVal;
}

//==============================================================================
int _setting::set(string dS, bool check_range, ostream *err_stream)
{
  if (SETTING_TYPE_BOOLEAN == type) {
    if (string::npos != dS.find("true")) {
      dataStr.assign("true");
      dataVal = 1;
    }
    else if (string::npos != dS.find("false")) {
      dataStr.assign("false");
      dataVal = 0;
    }
    else {
      stringstream argStream;
      argStream << dS;
      argStream >> dataVal;
      dataStr.assign("");
      if (check_range && ((dataVal < min) || (dataVal > max))) {
        *err_stream << "ERROR! Invalid boolean data: setting" << getParseStr() << dataVal << endl;
        return -1;
      }

      if (dataVal) {
        dataStr.assign("true");
      }
      else {
        dataStr.assign("false");
      }
    }
  }
  else if (SETTING_TYPE_NUMBER == type) {
    stringstream argStream;
    argStream << dS;
    argStream >> dataVal;
    dataStr.assign("");
    if (check_range) {
      // If a list of options was configured, check that instead of min and max values
      if (options.size()) {
        if (!count(options.begin(), options.end(), dataVal)) {
          *err_stream << "ERROR! setting" << getParseStr() << dataVal
                      << " is not in list of acceptable options: " << getOptionsJson() << endl;
          return -1;
        }
      }
      else {
        if ((dataVal < min) || (dataVal > max)) {
          *err_stream << "ERROR! Invalid binary data: setting" << getParseStr() << dataVal << endl;
          return -1;
        }
      }
    }
    dataStr.assign(dS);
  }
  else if (SETTING_TYPE_HEX == type) {
    stringstream dataStream;
    dataStream << hex << dS;
    dataStream >> dataStr;
  }
  else if (SETTING_TYPE_DATETIME == type) {
    if (dS.length() >= 13) {
      stringstream argStream;
      char tempChar;
      int64_t year = 0, month = 0, day = 0, hour = 0;
      argStream << dS;
      argStream >> year;
      tempChar = argStream.get();
      if (!((tempChar == '-') || (tempChar == '\\') || (tempChar == '/'))) {
        *err_stream << "ERROR! Bad date formatting: setting" << getParseStr() << dataVal << dS
                    << endl;
        return -1;
      }
      argStream >> month;
      tempChar = argStream.get();
      if (!((tempChar == '-') || (tempChar == '\\') || (tempChar == '/'))) {
        *err_stream << "ERROR! Bad date formatting: setting" << getParseStr() << dataVal << dS
                    << endl;
        return -1;
      }
      argStream >> day;
      tempChar = argStream.get();
      if (!(tempChar == ' ')) {
        *err_stream << "ERROR! Bad date formatting: setting" << getParseStr() << dataVal << dS
                    << endl;
        return -1;
      }
      argStream >> hour;

      if (check_range) {
        if ((year < 2010) || (year > 2100)) {
          *err_stream << "ERROR! Invalid study end year: setting" << getParseStr() << dataVal << dS
                      << endl;
          return -1;
        }
        if (!month || (month > 12)) {
          *err_stream << "ERROR! Invalid study end month: setting" << getParseStr() << dataVal << dS
                      << endl;
          return -1;
        }
        uint32_t max_days = 0;
        switch (month) {
          case 1:
          case 3:
          case 5:
          case 7:
          case 8:
          case 10:
          case 12:
            max_days = 31;
            break;
          case 4:
          case 6:
          case 9:
          case 11:
            max_days = 30;
            break;
          case 2:
            if (!(year % 4) && (year % 100)) {
              max_days = 29;
            }
            else {
              max_days = 28;
            }
            break;
        }
        if (!day || (day > max_days)) {
          *err_stream << "ERROR! Invalid study end day: setting" << getParseStr() << dataVal << dS
                      << endl;
          return -1;
        }
        if (hour > 23) {
          *err_stream << "ERROR! Invalid study end hour: setting" << getParseStr() << dataVal << dS
                      << endl;
          return -1;
        }
      }

      dataVal = year | (month << 16) | (day << 24) | (hour << 32);
      dataLength = 5;
      dataStr.assign(dS);
      dataStr = dS;
    }
    else {
      dataVal = 0;
      dataLength = 0;
      dataStr.assign("");
    }
  }
  else {
    dataStr.assign(dS);
    dataVal = 0;
    dataLength = 0;
    if (check_range && (dataStr.length() > maxLength)) {
      *err_stream << "ERROR! Invalid string data: setting" << getParseStr() << dataStr << endl;
      *err_stream << "---Max Length: " << (int)maxLength << endl;
      *err_stream << "---Input Length: " << (int)dataStr.length() << endl;
      return -1;
    }
  }

  setFlag = 1;
  return 0;
}

//==============================================================================
int _setting::setBinary(
  const uint8_t *pData, const uint32_t readLength, bool check_range, ostream *err_stream)
{
  if (SETTING_TYPE_NUMBER == type || SETTING_TYPE_BOOLEAN == type) {
    dataVal = 0;
    size_t i;

    if (readLength > dataLength) {
      *err_stream << "ERROR! Invalid data length: " << readLength << " (setting" << getParseStr()
                  << "?)" << endl;
      return -1;
    }

    for (i = 0; i < readLength; i++) {
      if (i < sizeof(dataVal)) {
        dataVal += ((uint64_t)pData[i]) << (8 * i);
      }
    }

    if (check_range && ((dataVal < min) || (dataVal > max))) {
      *err_stream << "ERROR! Invalid binary data: " << dataVal << " (setting" << parseStr << "?)"
                  << endl;
      return -1;
    }

    if (SETTING_TYPE_BOOLEAN == type) {
      if (dataVal) {
        dataStr.assign("true");
      }
      else {
        dataStr.assign("false");
      }
    }
    else {
      stringstream dataStream;
      dataStream << dataVal;
      dataStream >> dataStr;
    }
  }
  else if (SETTING_TYPE_HEX == type) {
    size_t i;
    stringstream dataStream;
    dataStream << hex << setfill('0');
    for (i = 0; i < readLength; i++) {
      dataStream << setw(2) << (int)pData[i];
    }
    dataStream >> dataStr;
  }
  else if (SETTING_TYPE_DATETIME == type) {
    if (readLength >= 5) {
      stringstream dataStream;
      int64_t year = pData[0] + (pData[0 + 1] << 8);
      int64_t month = pData[0 + 2];
      int64_t day = pData[0 + 3];
      int64_t hour = pData[0 + 4];
      dataStream << setfill('0') << setw(4) << year << "-" << setw(2) << month << "-" << setw(2)
                 << day << " " << setw(2) << hour;
      dataStr.assign(dataStream.str());
      dataVal = year | (month << 16) | (day << 24) | (hour << 32);
      dataLength = 5;
    }
    else {
      dataStr.assign("");
      dataVal = 0;
      dataLength = 0;
    }
  }
  else {
    dataStr.assign((const char *)pData);
    dataVal = 0;
    dataLength = 0;

    if (check_range && (dataStr.length() > maxLength)) {
      *err_stream << "ERROR! Invalid string data: setting" << getParseStr() << dataStr << endl;
      *err_stream << "---Max Length: " << (int)maxLength << endl;
      *err_stream << "---Input Length: " << (int)dataStr.length() << endl;
      return -1;
    }
  }

  setFlag = 1;
  return 0;
}

//==============================================================================
int _setting::write(vector<uint8_t> &buffer, bool badAlignment, uint8_t firmwareVersion)
{
  int i = 0;
  uint32_t j;

  if (setFlag) {
    binary_append(buffer, tag, 1);

    if (
      (SETTING_TYPE_NUMBER == type) || (SETTING_TYPE_BOOLEAN == type)
      || (SETTING_TYPE_DATETIME == type)) {
      binary_append(buffer, dataLength, 1);
      binary_append(buffer, dataVal, dataLength);
    }
    else if (SETTING_TYPE_HEX == type) {
      size_t numPairs = dataStr.length() / 2;
      binary_append(buffer, numPairs, 1);
      string tempStr(dataStr);
      for (j = 0; j < numPairs; j++) {
        tempStr.insert(3 * j + 2, " ");
      }
      stringstream argStream(tempStr);
      for (j = 0; j < numPairs; j++) {
        int temp;
        argStream >> hex >> temp;
        binary_append(buffer, temp, 1);
      }
    }
    else {
      binary_append(buffer, dataStr.length(), 1);
      binary_append(buffer, dataStr, dataStr.length());
    }

    if (badAlignment) {
      binary_append(buffer, 0xc4, 1);
    }
    else {
      binary_append(buffer, '\0', 1);
    }
  }

  return i;
}

//==============================================================================
int _setting::getTag()
{
  return (int)tag;
}

//==============================================================================
uint64_t _setting::getDataVal()
{
  return dataVal;
}

//==============================================================================
string _setting::getParseStr()
{
  return "." + parseStr + "=";
}

//==============================================================================
string _setting::getJsonStr()
{
  return parseStr;
}

//==============================================================================
string _setting::getSettingText()
{
  ostringstream outStream;

  outStream << "setting" << getParseStr();
  outStream << dataStr;

  return outStream.str();
}

//==============================================================================
string _setting::getSettingJson()
{
  ostringstream outStream;

  outStream << "\"" << parseStr << "\":{";
  if (SETTING_TYPE_BOOLEAN == type) {
    outStream << "\"" << JSON_SETTINGS_DATA_LABEL << "\":" << dataStr;
  }
  else if (SETTING_TYPE_NUMBER == type) {
    outStream << "\"" << JSON_SETTINGS_DATA_LABEL << "\":" << dataStr;
  }
  else if (SETTING_TYPE_STRING == type) {
    string tempString = dataStr;
    for (size_t i = 0; i < tempString.length(); i++) {
      // Escape risky special characters for JSON
      if ('"' == tempString[i] || '\\' == tempString[i]) {
        if (i == 0 || '\\' != tempString[i - 1]) {
          tempString.insert(i, "\\");
        }
      }
      // Ignore other ASCII characters we don't support
      else if (tempString[i] < ' ' || tempString[i] > '~') {
        tempString.erase(i);
      }
    }
    outStream << "\"" << JSON_SETTINGS_DATA_LABEL << "\":\"" << tempString << "\"";
  }
  else if (SETTING_TYPE_HEX == type) {
    outStream << "\"" << JSON_SETTINGS_DATA_LABEL << "\":\"" << dataStr << "\"";
  }
  else if (SETTING_TYPE_DATETIME == type) {
    outStream << "\"" << JSON_SETTINGS_DATA_LABEL << "\":\"" << dataStr << "\"";
  }
  outStream << "}";

  return outStream.str();
}

//==============================================================================
string _setting::getMetaJson()
{
  ostringstream outStream;

  outStream << "{";
  outStream << "\"id\":\"" << parseStr << "\",";
  if (SETTING_TYPE_BOOLEAN == type) {
    outStream << "\"type\":\"boolean\",";
    outStream << "\"min\":" << (int)min << ",";
    outStream << "\"max\":" << (int)max << ",";
    outStream << "\"units\":\"" << units << "\",";
  }
  else if (SETTING_TYPE_NUMBER == type) {
    outStream << "\"type\":\"number\",";
    if (options.size()) {
      outStream << "\"options\":" << getOptionsJson() << ",";
    }
    else {
      outStream << "\"min\":" << (int)min << ",";
      outStream << "\"max\":" << (int)max << ",";
    }
    outStream << "\"units\":\"" << units << "\",";
  }
  else if (SETTING_TYPE_STRING == type) {
    outStream << "\"type\":\"string\",";
    outStream << "\"maxLength\":" << (int)maxLength << ",";
  }
  else if (SETTING_TYPE_HEX == type) {
    outStream << "\"type\":\"hex\",";
    outStream << "\"maxLength\":" << (int)maxLength << ",";
  }
  else if (SETTING_TYPE_DATETIME == type) {
    outStream << "\"type\":\"datetime\",";
  }
  outStream << "\"minVersion\":" << fixed << setprecision(1) << (float)minFW / 10.0 << ",";
  outStream << "\"maxVersion\":" << (float)maxFW / 10.0 << ",";
  outStream << "\"name\":\"" << name << "\",";
  if (ALL_DEVICES == availability) {
    outStream << "\"mctOnly\":false,";
  }
  else if (MCT_ONLY == availability) {
    outStream << "\"mctOnly\":true,";
  }
  outStream << "\"tooltip\":\"" << tooltip << "\"";
  outStream << "}";

  return outStream.str();
}

//==============================================================================
string _setting::getOptionsJson()
{
  ostringstream outStream;
  bool first = true;

  outStream << "[";
  for (uint64_t val : options) {
    if (!first) outStream << ",";
    outStream << val;
    first = false;
  }
  outStream << "]";
  return outStream.str();
}

//==============================================================================
int _setting::isSet()
{
  return setFlag;
}

//==============================================================================
int _setting::checkFirmwareVersion(
  uint8_t firmware_version, uint8_t print_errs, ostream *err_stream)
{
  if (firmware_version < minFW) {
    if (print_errs) {
      *err_stream << "ERROR! Setting \'" << parseStr << "\' is not supported in versions below v"
                  << (float)minFW / 10.0 << endl;
    }
    return -1;
  }
  else if (firmware_version > maxFW) {
    if (print_errs) {
      *err_stream << "ERROR! Setting \'" << parseStr << "\' is not supported in versions above v"
                  << (float)minFW / 10.0 << endl;
    }
    return -1;
  }
  else {
    return 0;
  }
}