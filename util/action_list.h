#ifndef ACTION_LIST_H
#define ACTION_LIST_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "action.h"
#include <vector>
#include "rapidjson/document.h"

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__);
//
//-----------------------------------------------------------------------------

class ActionList {
 private:
  vector<_action> actionVector;
  vector<_action> fileActionVector;

  _action* locateAction(actionTags tag, uint8_t firmwareVersion, ostream* errStream);
  _action* locateNewAction(string argument, uint8_t firmwareVersion, ostream* errStream);
  _action* locateJsonAction(string type, uint8_t firmwareVersion, ostream* errStream);
  _action* locateFileAction(string argument, uint8_t firmwareVersion, ostream* errStream);

 public:
  ActionList(string deviceId);

  bool isValid();

  int parseBinary(
    uint8_t* pRead, uint32_t& j, uint8_t firmwareVersion, ostream* outStream, ostream* errStream);
  int writeActionText(ostream* outStream, ostream* errStream);
  int writeActionJson(ostream* outStream, ostream* errStream);

  int parseText(string argument, uint8_t firmwareVersion, ostream* outStream);

  int processJsonArray(rapidjson::Value& actions, uint8_t firmwareVersion, ostream* errStream);

  int writeActionBinary(
    vector<uint8_t>& buffer, uint8_t firmwareVersion, bool badAlignment, int* retVal,
    ostream* errStream);

  int getActionMetaJson(ostream* outStream);
};

#endif