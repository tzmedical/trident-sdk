/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#pragma comment(lib, "Setupapi.lib")
#include "drive_eject.h"

#include <stdio.h>
#include <windows.h>
#include <Setupapi.h>
#include <winioctl.h>
#include <cfgmgr32.h>
#include <string>

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------
// returns the device instance handle of a storage volume or 0 on error
//----------------------------------------------------------------------
DEVINST GetDrivesDevInstByDeviceNumber(long DeviceNumber, UINT DriveType, char* szDosDeviceName)
{
  bool IsFloppy = (strstr(szDosDeviceName, "\\Floppy") != NULL);  // who knows a better way?

  GUID* guid;

  switch (DriveType) {
    case DRIVE_REMOVABLE:
      if (IsFloppy) {
        guid = (GUID*)&GUID_DEVINTERFACE_FLOPPY;
      }
      else {
        guid = (GUID*)&GUID_DEVINTERFACE_DISK;
      }
      break;
    case DRIVE_FIXED:
      guid = (GUID*)&GUID_DEVINTERFACE_DISK;
      break;
    case DRIVE_CDROM:
      guid = (GUID*)&GUID_DEVINTERFACE_CDROM;
      break;
    default:
      return 0;
  }

  // Get device interface info set handle for all devices attached to system
  HDEVINFO hDevInfo = SetupDiGetClassDevs(guid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

  if (hDevInfo == INVALID_HANDLE_VALUE) {
    return 0;
  }

  // Retrieve a context structure for a device interface of a device information set
  DWORD dwIndex = 0;
  long res;

  BYTE Buf[1024];
  PSP_DEVICE_INTERFACE_DETAIL_DATA pspdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)Buf;
  SP_DEVICE_INTERFACE_DATA spdid;
  SP_DEVINFO_DATA spdd;
  DWORD dwSize;

  spdid.cbSize = sizeof(spdid);

  while (true) {
    res = SetupDiEnumDeviceInterfaces(hDevInfo, NULL, guid, dwIndex, &spdid);
    if (!res) {
      break;
    }

    dwSize = 0;
    SetupDiGetDeviceInterfaceDetail(
      hDevInfo, &spdid, NULL, 0, &dwSize,
      NULL);  // check the buffer size

    if (dwSize != 0 && dwSize <= sizeof(Buf)) {
      pspdidd->cbSize = sizeof(*pspdidd);  // 5 Bytes!

      ZeroMemory(&spdd, sizeof(spdd));
      spdd.cbSize = sizeof(spdd);

      long res = SetupDiGetDeviceInterfaceDetail(hDevInfo, &spdid, pspdidd, dwSize, &dwSize, &spdd);
      if (res) {
        // open the disk or cdrom or floppy
        HANDLE hDrive = CreateFile(
          pspdidd->DevicePath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
        if (hDrive != INVALID_HANDLE_VALUE) {
          // get its device number
          STORAGE_DEVICE_NUMBER sdn;
          DWORD dwBytesReturned = 0;
          res = DeviceIoControl(
            hDrive, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &sdn, sizeof(sdn), &dwBytesReturned,
            NULL);
          if (res) {
            if (DeviceNumber == (long)sdn.DeviceNumber) {  // match the given device number with the
                                                           // one of the current device
              CloseHandle(hDrive);
              SetupDiDestroyDeviceInfoList(hDevInfo);
              return spdd.DevInst;
            }
          }
          CloseHandle(hDrive);
        }
      }
    }
    dwIndex++;
  }

  SetupDiDestroyDeviceInfoList(hDevInfo);

  return 0;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//! Eject a drive given a drive letter.
//! \return 0 if successful, -1 otherwise
int EjectDrive(
  std::istringstream* inStream, std::ostringstream* outStream, std::ostringstream* errStream,
  std::ostringstream* logStream)
{
  char driveLetter = inStream->get();

  if (driveLetter >= 'a' && driveLetter <= 'z') {
    driveLetter += 'A' - 'a';
  }

  if (driveLetter < 'A' || driveLetter > 'Z') {
    *errStream << "Invalid Drive Identifier: " << driveLetter << std::endl;
    return -1;
  }

  // pre-build the strings for volume access
  char szRootPath[] = "_:\\";
  szRootPath[0] = driveLetter;

  char szDevicePath[] = "_:";
  szDevicePath[0] = driveLetter;

  char szVolumeAccessPath[] = "\\\\.\\_:";
  szVolumeAccessPath[4] = driveLetter;

  long DeviceNumber = -1;

  // open the storage volume
  HANDLE hVolume = CreateFileA(
    szVolumeAccessPath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);

  if (hVolume == INVALID_HANDLE_VALUE) {
    *errStream << "Error getting volume handle" << std::endl;
    return -1;
  }

  // get the volume's device number
  STORAGE_DEVICE_NUMBER sdn;
  DWORD dwBytesReturned = 0;
  long res = DeviceIoControl(
    hVolume, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &sdn, sizeof(sdn), &dwBytesReturned, NULL);

  if (res) {
    DeviceNumber = sdn.DeviceNumber;
  }
  CloseHandle(hVolume);

  if (DeviceNumber == -1) {
    *errStream << "Error getting device number" << std::endl;
    return -1;
  }

  // get the drive type which is required to match the device numbers correctely
  UINT DriveType = GetDriveTypeA(szRootPath);

  // get the dos device name (like \device\floppy0) to decide if it's a floppy or not - who
  // knows a better way?
  char szDosDeviceName[MAX_PATH];
  res = QueryDosDeviceA(szDevicePath, szDosDeviceName, MAX_PATH);
  if (!res) {
    *errStream << "Error getting device name" << std::endl;
    return -1;
  }

  // get the device instance handle of the storage volume by means of a SetupDi enum and
  // matching the device number
  DEVINST DevInst = GetDrivesDevInstByDeviceNumber(DeviceNumber, DriveType, szDosDeviceName);

  if (DevInst == 0) {
    *errStream << "Invalid device instance" << std::endl;
    return -1;
  }

  PNP_VETO_TYPE VetoType = PNP_VetoTypeUnknown;
  WCHAR VetoNameW[MAX_PATH];
  VetoNameW[0] = 0;
  bool bSuccess = false;

  // get drives's parent, e.g. the USB bridge, the SATA port, an IDE channel with two drives!
  DEVINST DevInstParent = 0;
  res = CM_Get_Parent(&DevInstParent, DevInst, 0);

  for (long tries = 0; tries < 3; tries++) {  // sometimes we need some tries...
    VetoNameW[0] = 0;
    res = CM_Request_Device_EjectW(DevInstParent, &VetoType, VetoNameW, MAX_PATH, 0);

    bSuccess = (res == CR_SUCCESS && VetoType == PNP_VetoTypeUnknown);
    if (bSuccess) {
      break;
    }

    Sleep(500);  // required to give the next tries a chance!
  }

  if (bSuccess) {
    return 0;
  }

  *errStream << "Failed with result: " << res << std::endl;
  return -1;
}
