#ifndef SETTING_LIST_H
#define SETTING_LIST_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "setting.h"
#include <vector>
#include "rapidjson/document.h"

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__);
//
//-----------------------------------------------------------------------------

class SettingList {
 private:
  vector<_setting> settingVector;

 public:
  SettingList(string deviceId);

  bool isValid();

  _setting* locateSetting(string argument, uint8_t firmwareVersion, ostream* errStream);
  _setting* locateSetting(
    settingTags tag, uint8_t firmwareVersion, bool isTzmr, bool isTzmrCompat, ostream* errStream);

  int processJsonObject(
    rapidjson::Value& settings, uint8_t firmwareVersion, bool checkInputs, ostream* errStream);

  int writeBinary(
    vector<uint8_t>& buffer, uint8_t firmwareVersion, bool badAlignment, int* retVal,
    ostream* errStream);

  int getSettingMetaJson(ostream* outStream);
};

#endif