
#ifndef EVENTS_H
#define EVENTS_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <cstdint>
#include <string>
#include <sstream>
#include <map>

#include "util/timestamp.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   Possible TAG values for each entry   ***/
enum eventTags {
  FIND_ALL = 0,
  // --- TAGS ---            --- VALUE ---         --- DATA ---
  START_TAG = 1,                       // none
  STOP_TAG = 2,                        // none
  BREAK_TAG = 3,                       // none
  RESUME_TAG = 4,                      // none
  FULL_TAG = 5,                        // none
  PACEMAKER_DETECTION_TAG = 11,        // pulse width (microseconds)
  QRS_DETECTION_TAG = 12,              // 0xMMTT, M == channel mask, TT == beat type
  RHYTHM_LABEL_TAG = 13,               // ASCII label
  QRS_COUNT_TAG = 14,                  // Number of QRS beats in a time frame
  PVC_COUNT_TAG = 15,                  // Number of PVC beats in a time frame
  SCP_EOF_TAG = 16,                    // none
  TZMR_BPM_COUNT_TAG = 17,             // Samples
  TZMR_BPM_STDEV_TAG = 18,             // BPM
  TZMR_BPM_AVERAGE_TAG = 19,           // BPM
  TZMR_BPM_MIN_TAG = 20,               // BPM
  TZMR_BPM_MAX_TAG = 21,               // BPM
  TZMR_TACHY_ENTRY_TAG = 22,           // channel mask
  TZMR_TACHY_EXIT_TAG = 23,            // channel mask
  TZMR_BRADY_ENTRY_TAG = 24,           // channel mask
  TZMR_BRADY_EXIT_TAG = 25,            // channel mask
  TZMR_PAUSE_ENTRY_TAG = 26,           // channel mask
  TZMR_AFIB_ENTRY_TAG = 27,            // channel mask
  TZMR_AFIB_EXIT_TAG = 28,             // channel mask
  TZMR_PAUSE_EXIT_TAG = 29,            // channel mask
  TACHY_RATE_CHANGE_TAG = 30,          // BPM
  BRADY_RATE_CHANGE_TAG = 31,          // BPM
  TZMR_PVC_ENTRY_TAG = 32,             // channel mask
  AF_RATE_CHANGE_TAG = 33,             // BPM
  TZMR_ANALYSIS_RESUMED_TAG = 49,      // channel mask
  TZMR_ANALYSIS_PAUSED_TAG = 50,       // channel mask
  LEAD_DISCONNECTED = 51,              // Bitmap of leads (1=disconnected): 0b000EASIG
  TZMR_MISSING_SCP_TAG = 81,           // none
  TZMR_MISSING_EVENT_TAG = 82,         // Event count (0 - 999)
  PATIENT_TAG = 101,                   // none
  PEDOMETER_TAG = 102,                 // Step Count
  BATTERY_SOC_TAG = 150,               // Battery state of charge (% * 256)
  BATTERY_VALUE_TAG = 151,             // Battery Voltage (mV)
  BATTERY_LOW_TAG = 152,               // Battery Voltage (mV)
  CHARGING_STARTED_TAG = 153,          // none
  CHARGING_STOPPED_TAG = 154,          // none
  BATTERY_TEMP_TAG = 155,              // Battery Temperature (C)
  TZMR_TIME_ZONE_CHANGE_TAG = 175,     // Offset from UTC in MINUTES (MAX +/- 780)
  TZR_REQUEST_TAG = 190,               // Day and hour of TZR requested
  BULK_UPLOAD_TAG = 198,               // Number of files
  SCP_RETRANSMIT_TAG = 199,            // Error Code (ACTIONS_ERR code)
  SCP_REQUEST_TAG = 200,               // Number of files requested
  SETTINGS_SUCCESS_TAG = 201,          // Setting File ID #
  TZMR_SMS_COMMAND_ERROR_TAG = 202,    // none
  MSG_RECEIVED_TAG = 203,              // none
  SETTINGS_FAILURE_TAG = 210,          // Error Code (see below)
  ACTIONS_FAILURE_TAG = 211,           // Error Code (see below)
  ACTIONS_SUCCESS_TAG = 212,           // Action File ID #
  EVENT_HTTP_FATAL_TAG = 213,          // 0,-> SCP fatal, other -> tze
  OTHER_HTTP_FATAL_TAG = 214,          // 0,-> UNKNOWN
                                       // 1 -> TZR failure
                                       // 2 -> TZA failure
                                       // 3 -> TZS failure
                                       // 4 -> LOG failure
                                       // 5 -> FRW failure
  DATA_REQUEST_TAG = 215,              // Number of seconds requested
  TZMR_SERVER_SETTINGS_CHANGED = 225,  // none
  TERMINATOR_TAG = 255                 // 0xFF
};

#define DATA_NONE       (0)
#define DATA_INT        (1 << 0)
#define DATA_LSB        (1 << 1)
#define DATA_MSB        (1 << 2)
#define DATA_CHAR       (1 << 3)
#define DATA_MASK_LOWER (1 << 4)
#define DATA_MASK_UPPER (1 << 5)
#define DATA_PERCENT    (1 << 6)

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

typedef string (*logCallback_t)(uint32_t data, uint32_t sequence, timestamp &parsedTime);

class _event {
 private:
  eventTags tag;
  string name;
  uint32_t dataType;
  logCallback_t callbackFunction;
  string label;

 public:
  _event(const char *pS, eventTags tg, uint32_t data);
  _event(const char *pS, eventTags tg, uint32_t data, logCallback_t callback);
  _event(const char *pS, eventTags tg, uint32_t data, const char *pL);

  eventTags getTag();

  string getName();

  string getEventText(uint32_t data, uint32_t sequence, timestamp &parsedTime);

  string getEventJson(uint32_t data, uint32_t sequence, timestamp &parsedTime);
};

_event *getEvent(string deviceID, eventTags tag);

string getEventText(
  string deviceID, eventTags tag, uint32_t data, uint32_t sequence, timestamp &parsedTime);

string getEventJson(
  string deviceID, eventTags tag, uint32_t data, uint32_t sequence, timestamp &parsedTime);

string listEvents(string deviceID);

#endif
