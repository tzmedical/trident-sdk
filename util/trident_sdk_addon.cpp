/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

// This is needed to make sure this package works in electron
#define NODE_API_NO_EXTERNAL_BUFFERS_ALLOWED

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------
#include <napi.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <map>

#include "trident_sdk_addon.h"
#include "settings_parser.h"
#include "settings_generator.h"
#include "settings_to_json.h"
#include "settings_from_json.h"
#include "setting_list.h"
#include "actions_parser.h"
#include "actions_generator.h"
#include "actions_to_json.h"
#include "actions_from_json.h"
#include "backup_generator.h"
#include "backup_parser.h"
#include "event_generator.h"
#include "event_parser.h"
#include "event_to_json.h"
#include "event_finder.h"
#include "interval_generator.h"
#include "interval_parser.h"
#include "interval_to_json.h"
#include "interval_finder.h"
#include "scp_parser.h"
#include "scp_generator.h"
#include "scp_to_json.h"
#include "drive_eject.h"
#include "accel_to_json.h"
#include "queue_to_json.h"
#include "accel_from_json.h"
#include "battery_info_parser.h"
#include "battery_info_generator.h"

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==========================================================================
//    StringToBuf()
//
//    This function allocates a javascript buffer and fills it with the data
//    from a string. A finalizer is attached to make it safe to return the
//    buffer to a javascript caller
//==========================================================================
Napi::Buffer<uint8_t> StringToBuf(Napi::Env env, std::string bufString)
{
  // Allocate and fill memory with a null terminator.
  uint8_t* output_chars = new uint8_t[bufString.size()];
  bufString.copy((char*)output_chars, bufString.size(), 0);

  // Return a new buffer with custom finalizer
  return Napi::Buffer<uint8_t>::NewOrCopy(
    env, output_chars, bufString.size(),
    [](Napi::Env env, uint8_t* finalizeData) { delete (finalizeData); });
}

//==========================================================================
//    ResultToObject()
//
//    This function bundles the arguments as a javascript object
//==========================================================================
Napi::Object ResultToObject(
  Napi::Env env, int status, std::string output, std::string error, std::string log)
{
  Napi::Object outputObject = Napi::Object::New(env);

  outputObject.Set(Napi::String::New(env, STATUS_NAME), Napi::Number::New(env, status));
  outputObject.Set(Napi::String::New(env, ERROR_NAME), Napi::String::New(env, error));
  outputObject.Set(Napi::String::New(env, LOG_NAME), Napi::String::New(env, log));
  outputObject.Set(Napi::String::New(env, OUTPUT_NAME), StringToBuf(env, output));
  return outputObject;
}

//==========================================================================
//    ErrorObject()
//
//    This function generates a javascript object containing error data
//==========================================================================
Napi::Object ErrorObject(Napi::Env env, std::string error)
{
  return ResultToObject(env, 1, "", error, "");
}

//==========================================================================
//    ProcessFile()
//
//    This function processes a file from a buffer in info using the
//    function in processor and returns a JSON object containing the
//    processed data and relevant error logs
//==========================================================================
Napi::Object ProcessFile(const Napi::CallbackInfo& info, parseFunction processor)
{
  // Convert the inputs to useable C++ Objects
  Napi::Env env = info.Env();
  uint8_t* inputData;
  int inputLength;

  std::string outString;
  std::string errString;
  std::string logString;

  // We can only accept buffers for now
  if (info.Length() < 1) {
    return ErrorObject(env, "Not Enough Arguments");
  }

  if (!info[0].IsBuffer()) {
    return ErrorObject(env, "Invalid Arguments\nExpected Buffer, got " + info[0].Type());
  }
  else {
    inputData = info[0].As<Napi::Buffer<uint8_t>>().Data();
    inputLength = info[0].As<Napi::Buffer<uint8_t>>().Length();
  }

  std::string inString((char*)inputData, inputLength);
  std::istringstream inStream(inString);

  Napi::Object propertyObj;
  Napi::Array propertyArr;
  std::map<std::string, std::string> extraArgs;

  // Copy all properties/values in the (optional) second argument to a map

  // The keys of this map can be accessed later with the loop:
  // for(std::map<string,string>::iterator itr = extraArgs.begin(); itr != extraArgs.end();
  // ++itr)
  // {
  //   std::string keyStr = itr->first;
  //   std::string valStr = itr->second;
  // }

  if (info.Length() >= 2 && info[1].IsObject()) {
    propertyObj = info[1].ToObject();
    propertyArr = propertyObj.GetPropertyNames();

    for (unsigned int i = 0; i < propertyArr.Length(); i++) {
      Napi::Value keyVal = propertyArr[i];
      std::string keyStr = keyVal.As<Napi::String>().Utf8Value();
      Napi::Value valVal = propertyObj[keyStr];
      std::string valStr = valVal.As<Napi::String>().Utf8Value();

      extraArgs[keyStr] = valStr;
    }
  }

  std::ostringstream outStream;
  std::ostringstream errStream;
  std::ostringstream logStream;

  int processStatus = processor(&inStream, extraArgs, &outStream, &errStream, &logStream);
  outString = outStream.str();
  errString = errStream.str();
  logString = logStream.str();

  return ResultToObject(env, processStatus, outString, errString, logString);
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
Napi::Object NapiActionsToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ActionsToText);
}

//==============================================================================
Napi::Object NapiActionsFromText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ActionsFromText);
}

//==============================================================================
Napi::Object NapiActionsToJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ActionsToJson);
}

//==============================================================================
Napi::Object NapiActionsFromJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ActionsFromJson);
}

//==============================================================================
Napi::Object NapiBackupToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, BackupToText);
}

//==============================================================================
Napi::Object NapiBackupFromText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, BackupFromText);
}

//==============================================================================
Napi::Object NapiBatteryInfoToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, BatteryInfoToText);
}

//==============================================================================
Napi::Object NapiBatteryInfoFromText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, BatteryInfoFromText);
}

//==============================================================================
Napi::Object NapiSettingsToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, SettingsToText);
}

//==============================================================================
Napi::Object NapiSettingsFromText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, SettingsFromText);
}

//==============================================================================
Napi::Object NapiSettingsToJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, SettingsToJson);
}

//==============================================================================
Napi::Object NapiSettingsFromJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, SettingsFromJson);
}

//==============================================================================
Napi::Object NapiSettingsMetaJson(const Napi::CallbackInfo& info)
{
  Napi::Env env = info.Env();

  // We can only accept strings for now
  if (info.Length() < 1) {
    return ErrorObject(env, "Not Enough Arguments");
  }
  else if (!info[0].IsString()) {
    return ErrorObject(env, "Invalid Arguments, Expected a String");
  }
  else {
    std::string deviceString = info[0].As<Napi::String>().Utf8Value();
    std::ostringstream outStream;

    SettingList settingList(deviceString);
    int processStatus = settingList.getSettingMetaJson(&outStream);

    std::string outString = outStream.str();

    if (processStatus != 0) {
      std::cout << "Encountered Error: " << processStatus << std::endl;
    }

    return ResultToObject(env, processStatus, outString, "", "");
  }
}

//==============================================================================
Napi::Object NapiEventToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, EventToText);
}

//==============================================================================
Napi::Object NapiEventFromJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, EventFromJson);
}

//==============================================================================
Napi::Object NapiEventToJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, EventToJson);
}

//==============================================================================
Napi::Object NapiEventFinder(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, FindEvent);
}

//==============================================================================
Napi::Object NapiAccelToJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, tzgToJson);
}

//==============================================================================
Napi::Object NapiAccelFromJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, tzgFromJson);
}

//==============================================================================
Napi::Object NapiIntervalToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, IntervalToText);
}

//==============================================================================
Napi::Object NapiIntervalFromJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, IntervalFromJson);
}

//==============================================================================
Napi::Object NapiIntervalToJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, IntervalToJson);
}

//==============================================================================
Napi::Object NapiIntervalFinder(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, FindInterval);
}

//==============================================================================
Napi::Object NapiScpToText(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ScpToText);
}

//==============================================================================
Napi::Object NapiScpFromJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ScpFromJson);
}

//==============================================================================
Napi::Object NapiScpToJson(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, ScpToJson);
}

//==============================================================================
Napi::Object NapiEjectDrive(const Napi::CallbackInfo& info)
{
  Napi::Env env = info.Env();

  // We can only accept strings for now
  if (info.Length() < 1) {
    return ErrorObject(env, "Not Enough Arguments");
  }
  else if (!info[0].IsString()) {
    return ErrorObject(env, "Invalid Arguments, Expected a String");
  }
  else {
    std::string inString = info[0].As<Napi::String>().Utf8Value();
    std::istringstream inStream(inString);
    std::ostringstream outStream;
    std::ostringstream errStream;
    std::ostringstream logStream;

    int result = EjectDrive(&inStream, &outStream, &errStream, &logStream);
    std::string outString = outStream.str();
    std::string errString = errStream.str();
    std::string logString = logStream.str();

    return ResultToObject(env, result, outString, errString, logString);
  }
}

//==============================================================================
Napi::Object NapiParseQueue(const Napi::CallbackInfo& info)
{
  return ProcessFile(info, QueueToJson);
}

//==============================================================================
Napi::Object Init(Napi::Env env, Napi::Object exports)
{
  exports.Set("tzaToText", Napi::Function::New(env, NapiActionsToText));
  exports.Set("tzaFromText", Napi::Function::New(env, NapiActionsFromText));
  exports.Set("tzaToJson", Napi::Function::New(env, NapiActionsToJson));
  exports.Set("tzaFromJson", Napi::Function::New(env, NapiActionsFromJson));
  exports.Set("backupToText", Napi::Function::New(env, NapiBackupToText));
  exports.Set("backupFromText", Napi::Function::New(env, NapiBackupFromText));
  exports.Set("batteryInfoToText", Napi::Function::New(env, NapiBatteryInfoToText));
  exports.Set("batteryInfoFromText", Napi::Function::New(env, NapiBatteryInfoFromText));
  exports.Set("tzsToText", Napi::Function::New(env, NapiSettingsToText));
  exports.Set("tzsFromText", Napi::Function::New(env, NapiSettingsFromText));
  exports.Set("tzsToJson", Napi::Function::New(env, NapiSettingsToJson));
  exports.Set("tzsFromJson", Napi::Function::New(env, NapiSettingsFromJson));
  exports.Set("tzsMetaJson", Napi::Function::New(env, NapiSettingsMetaJson));
  exports.Set("tzeToText", Napi::Function::New(env, NapiEventToText));
  exports.Set("tzeFromJson", Napi::Function::New(env, NapiEventFromJson));
  exports.Set("tzeToJson", Napi::Function::New(env, NapiEventToJson));
  exports.Set("tzeFinder", Napi::Function::New(env, NapiEventFinder));
  exports.Set("tzrToText", Napi::Function::New(env, NapiIntervalToText));
  exports.Set("tzrFromJson", Napi::Function::New(env, NapiIntervalFromJson));
  exports.Set("tzrToJson", Napi::Function::New(env, NapiIntervalToJson));
  exports.Set("tzrFinder", Napi::Function::New(env, NapiIntervalFinder));
  exports.Set("tzgToJson", Napi::Function::New(env, NapiAccelToJson));
  exports.Set("tzgFromJson", Napi::Function::New(env, NapiAccelFromJson));
  exports.Set("scpToText", Napi::Function::New(env, NapiScpToText));
  exports.Set("scpFromJson", Napi::Function::New(env, NapiScpFromJson));
  exports.Set("scpToJson", Napi::Function::New(env, NapiScpToJson));
  exports.Set("ejectDrive", Napi::Function::New(env, NapiEjectDrive));
  exports.Set("parseQueue", Napi::Function::New(env, NapiParseQueue));
  return exports;
}

NODE_API_MODULE(addon, Init)