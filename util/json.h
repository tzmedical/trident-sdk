#ifndef JSON_LABELS_H
#define JSON_LABELS_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <cstdint>

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

// ERROR
#define JSON_TIMESTAMP_LABEL        "datetime"
#define JSON_FIRMWARE_VERSION_LABEL "firmwareVersion"
#define JSON_MESSAGE_LABEL          "message"
#define JSON_ERROR_LABEL            "error"
#define JSON_ERROR_CODE_LABEL       "code"
#define JSON_ERROR_HTTP_LABEL       "httpCode"
#define JSON_ERROR_SOURCE_LABEL     "source"
#define JSON_ERROR_LINE_LABEL       "line"

// Global
#define JSON_FILE_FORMAT_LABEL      "format"
#define JSON_FILE_CRC_LABEL         "crc"
#define JSON_DEVICE_TYPE_LABEL      "deviceType"
#define JSON_DEVICE_SERIAL_LABEL    "deviceSerial"
#define JSON_FIRMWARE_VERSION_LABEL "firmwareVersion"
#define JSON_PATIENT_ID_LABEL       "patientId"
#define JSON_TIMESTAMP_LABEL        "datetime"
#define JSON_SEQUENCE_NUMBER_LABEL  "sequence"
#define JSON_SAMPLE_NUMBER_LABEL    "sample"
#define JSON_TAG_LABEL              "tag"
#define JSON_FILE_ID_LABEL          "fileId"

// TZE
#define JSON_EVENT_SCP_START_LABEL   "firstSequence"
#define JSON_EVENT_SCP_COUNT_LABEL   "ecgCount"
#define JSON_EVENT_DATA_LENGTH_LABEL "dataLength"
#define JSON_EVENT_DATA_LABEL        "data"
#define JSON_EVENT_INT_DATA_LABEL    "intData"

// TZR
#define JSON_EVENTS_LABEL             "events"
#define JSON_EVENT_TYPE_LABEL         "name"
#define JSON_EVENT_DATA_LABEL         "data"
#define JSON_EVENT_DATA_INT_LABEL     "intData"
#define JSON_EVENT_DATA_CHAR_LABEL    "charData"
#define JSON_EVENT_DATA_MASK_LABEL    "maskData"
#define JSON_EVENT_DATA_MSB_LABEL     "msbData"
#define JSON_EVENT_DATA_LSB_LABEL     "lsbData"
#define JSON_EVENT_DATA_PERCENT_LABEL "percentData"

// TZA
#define JSON_ACTIONS_LABEL      "actions"
#define JSON_ACTIONS_TYPE_LABEL "type"
#define JSON_ACTIONS_DATA_LABEL "data"

// TZS
#define JSON_CHECK_RANGES_LABEL  "checkSettingRanges"
#define JSON_SETTINGS_LABEL      "settings"
#define JSON_SETTINGS_DATA_LABEL "data"

// SCP
#define JSON_LEAD_COUNT_LABEL              "leadCount"
#define JSON_START_TIMESTAMP_LABEL         "startTime"
#define JSON_AVM_LABEL                     "avm"
#define JSON_SAMPLE_PERIOD_LABEL           "samplePeriod"
#define JSON_LEADS_LABEL                   "leads"
#define JSON_LEAD_NAME_LABEL               "leadName"
#define JSON_START_SAMPLE_LABEL            "startSample"
#define JSON_END_SAMPLE_LABEL              "endSample"
#define JSON_SAMPLE_COUNT_LABEL            "totalSamples"
#define JSON_WAVEFORM_LABEL                "data"
#define JSON_DEVICE_FIRMWARE_VERSION_LABEL "firmwareVersion"
#define JSON_HP_FILTER_LABEL               "hpFilter"
#define JSON_LP_FILTER_LABEL               "lpFilter"

// TZG
#define JSON_COUNTS_PER_G_LABEL "countsPerG"
#define JSON_AXES_LABEL         "axes"

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

int32_t output_error_json(const char *p_str, std::ostream *outStream);

#endif