/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "timestamp.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <regex>

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
// This converts our UTC time value to local time so we can append the zone info
//==============================================================================
void timestamp::adjust_utc_for_timezone(struct tm *p_t, const int time_zone)
{
  // Adjust `t` for timezone offset (right now it's in UTC)
#ifndef _WIN32
  time_t epoch = mktime(p_t);
  epoch += time_zone * 60;  // Add the timezone (in seconds) to epoch value
  localtime_r(&epoch, p_t);
#else
  time_t epoch = _mkgmtime(p_t);
  epoch += time_zone * 60;  // Add the timezone (in seconds) to epoch value
  gmtime_s(p_t, &epoch);
#endif
}

//==============================================================================
// This converts our local time value to UTC time so we can store in the file
//==============================================================================
void timestamp::convert_to_utc(struct tm *p_t, const int time_zone)
{
  // Adjust `t` for timezone offset (right now it's in UTC)
#ifndef _WIN32
  time_t epoch = mktime(p_t);
  epoch -= time_zone * 60;  // Subtract the timezone (in seconds) from epoch value
  localtime_r(&epoch, p_t);
#else
  time_t epoch = _mkgmtime(p_t);
  epoch -= time_zone * 60;  // Subtract the timezone (in seconds) from epoch value
  gmtime_s(p_t, &epoch);
#endif
}

//==============================================================================
static int16_t parseZoneString(string zoneString)
{
  int16_t zoneMinutes = 0;

  if (zoneString.find(':') != string::npos) {
    int hours = stoi(zoneString, nullptr, 10);
    string minuteString = zoneString.substr(zoneString.find(':') + 1);
    int mins = stoi(minuteString, nullptr, 10);
    zoneMinutes = 60 * hours + mins;
  }
  else {
    zoneMinutes = stoi(zoneString, nullptr, 10);
  }

  return zoneMinutes;
}

//==============================================================================
int timestamp::sanityCheck()
{
  if (t.tm_mon < 0 || t.tm_mon > 11) {
    cerr << "Invalid month: " << (int)(t.tm_mon + 1) << endl;
    return -2;
  }

  if (t.tm_mday < 1 || t.tm_mday > 31) {
    cerr << "Invalid day: " << (int)t.tm_mday << endl;
    return -3;
  }

  if (t.tm_hour < 0 || t.tm_hour > 23) {
    cerr << "Invalid hour: " << (int)t.tm_hour << endl;
    return -4;
  }

  if (t.tm_min < 0 || t.tm_min > 59) {
    cerr << "Invalid minute: " << (int)t.tm_min << endl;
    return -5;
  }

  if (t.tm_sec < 0 || t.tm_sec > 59) {
    cerr << "Invalid second: " << (int)t.tm_sec << endl;
    return -6;
  }

  if (milliseconds > 999) {
    cerr << "Invalid milliseconds: " << (int)milliseconds << endl;
    return -7;
  }

  return 0;
}

#ifndef _WIN32
//==============================================================================
time_t timestamp::calculateSecondsToUTC()
{
  time_t local_epoch = time(NULL);
  struct tm utc_t = {};
  gmtime_r(&local_epoch, &utc_t);
  time_t utc_epoch = mktime(&utc_t);
  return utc_epoch - local_epoch - 3600 * utc_t.tm_isdst;
}
#endif

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
timestamp::timestamp()
{
  t = {};
  milliseconds = 0;
  timeZone = 0;
#ifndef _WIN32
  mktimeCorrectionOffset = calculateSecondsToUTC();
#endif
}

//==============================================================================
timestamp::timestamp(const struct tm *p_t, uint32_t ms, int16_t zone)
{
  t = *p_t;
  milliseconds = ms;
  timeZone = zone;
#ifndef _WIN32
  mktimeCorrectionOffset = calculateSecondsToUTC();
#endif
}

//==============================================================================
int timestamp::parseBinary(uint8_t *pRead, int32_t zoneOffset, ostream *errStream)
{
  int i = 0;

  t = {};
  t.tm_year = pRead[i] + ((int16_t)pRead[i + 1] << 8) - 1900;  // Year
  i += 2;
  t.tm_mon = pRead[i] - 1;  // Month
  i += 1;
  t.tm_mday = pRead[i];  // Day
  i += 1;
  t.tm_hour = pRead[i];  // Hour
  i += 1;
  t.tm_min = pRead[i];  // Minute
  i += 1;
  t.tm_sec = pRead[i];  // Seconds
  i += 1;
  t.tm_isdst = -1;
  milliseconds = pRead[i] * 4;  // Milliseconds / 4
  i += 1;
  timeZone = pRead[i] + ((int16_t)pRead[i + 1] << 8);  // Time zone offset (minutes)
  i += 2;
  if (32767 == timeZone) {
    *errStream
      << "WARNING! Unspecified local time zone detected. Defaulting to local machine time ("
      << zoneOffset << ")" << endl;
    timeZone = zoneOffset;
  }
  else if ((timeZone > 780) || (timeZone < -780)) {
    *errStream << "WARNING! Invalid time zone detected (" << timeZone
               << "). Defaulting to local machine time (" << zoneOffset << ")" << endl;
    timeZone = zoneOffset;
  }

  return i;
}

//==============================================================================
int timestamp::parseText(string timeString)
{
  // Match the YYYY/MM/DD HH:MM:SS[.mmm] part
  static const regex reg(
    "(\\d{1,})[\\/-](\\d{2})[\\/-](\\d{2})[ T](\\d{2}):(\\d{2}):(\\d{2})(.\\d{3})?");
  smatch sm;
  regex_search(timeString, sm, reg);
  if (sm.size() < 7) {
    return -9;
  }
  t.tm_year = stoi(sm[1], nullptr, 10) - 1900;
  t.tm_mon = stoi(sm[2], nullptr, 10) - 1;
  t.tm_mday = stoi(sm[3], nullptr, 10);
  t.tm_hour = stoi(sm[4], nullptr, 10);
  t.tm_min = stoi(sm[5], nullptr, 10);
  t.tm_sec = stoi(sm[6], nullptr, 10);
  t.tm_isdst = -1;
  if (sm[7].length() > 1) {
    string ms = sm[7];
    ms.erase(0, 1);
    milliseconds = stoi(ms, nullptr, 10);
  }
  else {
    milliseconds = 0;
  }

  // Match the timezone bit
  int offset_sign;
  timeString.erase(0, sm[0].length());
  if (timeString.find('+') != string::npos) {
    string zoneString = timeString.substr(timeString.find('+') + 1);
    offset_sign = 1;

    timeZone = parseZoneString(zoneString);
  }
  else if (timeString.find('-') != string::npos) {
    string zoneString = timeString.substr(timeString.find('-') + 1);
    offset_sign = -1;

    timeZone = parseZoneString(zoneString);
  }
  else {
    cerr << "ERROR! No timezone in string: " << timeString << endl;
    return -8;
  }

  timeZone *= offset_sign;

  // Basic Sanity Checks
  int retVal = sanityCheck();
  if (retVal) return retVal;

  // "Correct" the time value for the timezone offset, since we store the
  // value in UTC in the file.
  convert_to_utc(&t, timeZone);

  return 0;
}

//==============================================================================
int timestamp::writeBinary(uint8_t *pBuffer)
{
  int i = 0;

  int year = t.tm_year + 1900;
  pBuffer[i++] = year & 0xff;
  year >>= 8;
  pBuffer[i++] = year & 0xff;

  pBuffer[i++] = (t.tm_mon + 1) & 0xff;
  pBuffer[i++] = t.tm_mday & 0xff;
  pBuffer[i++] = t.tm_hour & 0xff;
  pBuffer[i++] = t.tm_min & 0xff;
  pBuffer[i++] = t.tm_sec & 0xff;
  pBuffer[i++] = (milliseconds / 4) & 0xff;

  int zone = timeZone;
  pBuffer[i++] = zone & 0xff;
  zone >>= 8;
  pBuffer[i++] = zone & 0xff;

  return i;
}

//==============================================================================
void timestamp::writeBinary(vector<uint8_t> &buffer)
{
  int year = t.tm_year + 1900;
  buffer.push_back(int{year & 0xff});
  buffer.push_back(int{year >> 8});

  buffer.push_back(int{(t.tm_mon + 1) & 0xff});
  buffer.push_back(int{t.tm_mday & 0xff});
  buffer.push_back(int{t.tm_hour & 0xff});
  buffer.push_back(int{t.tm_min & 0xff});
  buffer.push_back(int{t.tm_sec & 0xff});
  buffer.push_back(uint32_t{(milliseconds / 4) & 0xff});

  buffer.push_back(int{timeZone & 0xff});
  buffer.push_back(int{timeZone >> 8});
}

//==============================================================================
string timestamp::getJson()
{
  struct tm tempTime = t;
  adjust_utc_for_timezone(&tempTime, timeZone);
  char buffer[80];
  strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", &tempTime);
  char zoneSign = '+';
  if (timeZone < 0) {
    zoneSign = '-';
    timeZone = 0 - timeZone;
  }
  int zoneHours = timeZone / 60;
  int zoneMinutes = timeZone % 60;

  stringstream ss;

  ss << "\"" << buffer << "." << setfill('0') << setw(3) << milliseconds << zoneSign << setw(2)
     << zoneHours << ":" << setw(2) << zoneMinutes << setfill(' ') << "\"";

  return ss.str();
}

//==============================================================================
string timestamp::getText()
{
  stringstream ss;

  ss << setw(4) << setfill('0') << (t.tm_year + 1900) << "/"  // Output the date stamp
     << setw(2) << (t.tm_mon + 1) << "/" << setw(2) << t.tm_mday << " ";
  ss << setw(2) << t.tm_hour << ":"  // Output the time stamp
     << setw(2) << t.tm_min << ":" << setw(2) << t.tm_sec << "." << setw(3) << milliseconds
     << setfill(' ') << " {" << timeZone << "}";

  return ss.str();
}

//==============================================================================
string timestamp::getTzrName()
{
  stringstream ss;

  ss << t.tm_year << setfill('0') << setw(2) << t.tm_mon << setw(2) << t.tm_mday << "_" << setw(2)
     << t.tm_hour << ".tzr";

  return ss.str();
}

//==============================================================================
uint64_t timestamp::getMsEpoch()
{
  time_t epoch;
#ifndef _WIN32
  epoch = mktime(&t) - mktimeCorrectionOffset;
#else
  epoch = _mkgmtime(&t);
#endif
  return (uint64_t)epoch * 1000 + milliseconds;
}

//==============================================================================
struct tm timestamp::getRawStruct()
{
  return t;
}

//==============================================================================
int16_t timestamp::getTimeZone()
{
  return timeZone;
}