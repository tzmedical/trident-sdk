
#ifndef ACTION_H
#define ACTION_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <cstdint>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <iostream>
#include "rapidjson/document.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define UNUSED_FIELD 0

/***   Possible TAG values for each entry   ***/
enum actionTags {
  ACTIONS_ENABLE_NEW_STUDY = 10,
  ACTIONS_REQUEST_SCP_BLOCK = 20,
  ACTIONS_RETRANSMIT_SCP_FILE = 21,
  ACTIONS_RECENT_SCP = 22,
  ACTIONS_REQUEST_TZR_FILE = 30,
  ACTIONS_UPDATE_SETTINGS = 40,
  ACTIONS_DISPLAY_MESSAGE = 50,
  ACTIONS_ERROR_LOG = 60,
  ACTIONS_FIRMWARE = 100,
  ACTIONS_GRAPHICS = 101,
  ACTIONS_END_STUDY = 200,
  ACTIONS_START_STUDY = 201,
  ACTIONS_SWAP_DEVICE = 202,
  ACTIONS_FORMAT_SD = 220,
  ACTIONS_SHUTDOWN = 221,

  BAD_TAG = 254,
  ACTIONS_TERMINATOR = 255
};

enum actionDataType {
  ACTION_TYPE_BOOLEAN = 0,
  ACTION_TYPE_NUMBER = 1,
  ACTION_TYPE_STRING = 3,
  ACTION_TYPE_VALIDATION_CONSTANT = 6,
};

enum actionAvailability { ALL_DEVICES = 0, MCT_ONLY = 1 };

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

class _actionData {
 private:
  string parseStr;
  string description;
  actionDataType type;
  uint8_t dataLength;
  uint8_t maxLength;

  uint8_t setFlag;
  uint64_t dataVal;
  string dataStr;

 public:
  _actionData(
    string parsedName, actionDataType dataType, uint8_t length, string descriptionText = "");
  _actionData(string parsedName, actionDataType dataType, uint8_t length, uint64_t constantValue);

  int set(string dS, string actionName, ostream *err_stream);
  int setBinary(
    const uint8_t *pData, const uint32_t readLength, string actionName, ostream *errStream = &cerr);
  int write(vector<uint8_t> &buffer);

  string getParseStr();
  string getJsonStr();
  uint8_t getDataLength();
  actionDataType getDataType();
  int isSet();
  string writeActionText();
  string writeActionJson();
};

class _action {
 private:
  string parseStr;
  uint8_t tag;
  string name;
  vector<_actionData> dataVector;
  uint8_t minFW;
  uint8_t maxFW;
  string tooltip;
  int maxPerFile;
  int actionIndex;
  actionAvailability availability;

 public:
  _action(
    string parsedName, uint8_t actionTag, string displayName,
    initializer_list<_actionData> actionData, uint8_t minFWVersion, uint8_t maxFWVersion,
    string tooltipText, size_t fileLimit, actionAvailability avail = ALL_DEVICES);

  int parseTextIndex(string dS, ostream *errStream = &cerr);
  int parseText(string dS, ostream *errStream = &cerr);
  int parseBinary(const uint8_t *pData, const uint32_t readLength, ostream *errStream = &cerr);
  int parseJson(rapidjson::Value &actionData, ostream *errStream = &cerr);
  int parseJson(string actionData, ostream *errStream = &cerr);

  int getTag();
  uint64_t getDataVal();
  string getParseStr();
  string getJsonStr();
  int getActionIndex();

  int writeActionBinary(
    vector<uint8_t> &buffer, bool badAlignment, uint8_t firmwareVersion, ostream *err_stream);
  int writeActionText(ostream &outStream);
  int writeActionJson(ostream &outStream);
  int writeMetaJson(ostream &outStream);

  int isSet();
  int checkFirmwareVersion(uint8_t firmware_version, uint8_t print_errs, ostream *errStream);
};

#endif
