/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "events.h"

#include <iostream>
#include <iomanip>

#include "util/json.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

static string patientEventLog(uint32_t data, uint32_t sequence, timestamp &parsedTime);
static string httpEventLog(uint32_t data, uint32_t sequence, timestamp &parsedTime);
static string httpOtherLog(uint32_t data, uint32_t sequence, timestamp &parsedTime);

static _event *getEventsList(string deviceID);
static int getEventsListLength(string deviceID);

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

_event eventsList[] = {
  _event("ECG Recording Started", START_TAG, DATA_INT),
  _event("ECG Recording Stopped", STOP_TAG, DATA_INT),
  _event("ECG Recording Interrupted", BREAK_TAG, DATA_INT),
  _event("ECG Recording Resumed", RESUME_TAG, DATA_INT),
  _event("ECG Recording Full", FULL_TAG, DATA_INT),
  _event("Pacemaker Activity", PACEMAKER_DETECTION_TAG, DATA_INT, " Pulses"),
  _event("QRS Location", QRS_DETECTION_TAG, DATA_INT | DATA_CHAR | DATA_MASK_UPPER),
  _event("Rhythm Change", RHYTHM_LABEL_TAG, DATA_INT | DATA_CHAR),
  _event("QRS Count", QRS_COUNT_TAG, DATA_INT),
  _event("PVC Count", PVC_COUNT_TAG, DATA_INT),
  _event("SCP End of File", SCP_EOF_TAG, DATA_INT),
  _event("Tachycardia Rate Change", TACHY_RATE_CHANGE_TAG, DATA_INT, " BPM"),
  _event("Bradycardia Rate Change", BRADY_RATE_CHANGE_TAG, DATA_INT, " BPM"),
  _event("AF Rate Change", AF_RATE_CHANGE_TAG, DATA_INT, " BPM"),
  _event("Electrodes Disconnected", LEAD_DISCONNECTED, DATA_INT | DATA_MASK_LOWER),
  _event("Manual Patient Event", PATIENT_TAG, DATA_INT | DATA_MSB | DATA_LSB, patientEventLog),
  _event("Pedometer Steps", PEDOMETER_TAG, DATA_INT),
  _event("Battery Percent Charge", BATTERY_SOC_TAG, DATA_INT | DATA_PERCENT, "%"),
  _event("Battery Voltage", BATTERY_VALUE_TAG, DATA_INT, " mV"),
  _event("Low Battery Voltage", BATTERY_LOW_TAG, DATA_INT, " mV"),
  _event("Charging Started", CHARGING_STARTED_TAG, DATA_NONE),
  _event("Charging Stopped", CHARGING_STOPPED_TAG, DATA_INT),
  _event("Battery Temperature", BATTERY_TEMP_TAG, DATA_INT, " C"),
  _event("Server ECG Request", SCP_REQUEST_TAG, DATA_INT),
  _event("Server Interval Request", TZR_REQUEST_TAG, DATA_INT | DATA_MSB | DATA_LSB),
  _event("Bulk ECG Upload", BULK_UPLOAD_TAG, DATA_INT),
  _event("Server ECG Retransmission Request", SCP_RETRANSMIT_TAG, DATA_INT),
  _event("Settings Download Successful", SETTINGS_SUCCESS_TAG, DATA_INT),
  _event("Settings Download Error", SETTINGS_FAILURE_TAG, DATA_INT),
  _event("Actions Download Error", ACTIONS_FAILURE_TAG, DATA_INT),
  _event("Actions Download Successful", ACTIONS_SUCCESS_TAG, DATA_INT),
  _event("Message Acknowledged", MSG_RECEIVED_TAG, DATA_INT),
  _event("HTTP Failure (SCP/TZE)", EVENT_HTTP_FATAL_TAG, DATA_INT, httpEventLog),
  _event("HTTP Failure (Other)", OTHER_HTTP_FATAL_TAG, DATA_INT, httpOtherLog),
  _event("Recent ECG Request", DATA_REQUEST_TAG, DATA_INT),
  _event("Final Entry", TERMINATOR_TAG, DATA_NONE),
};

_event tzmrEventsList[] = {
  _event("ECG Recording Started", START_TAG, DATA_INT),
  _event("ECG Recording Stopped", STOP_TAG, DATA_INT),
  _event("ECG Recording Interrupted", BREAK_TAG, DATA_INT),
  _event("ECG Recording Resumed", RESUME_TAG, DATA_INT),
  _event("ECG Recording Full", FULL_TAG, DATA_INT),
  _event("Pacemaker Activity", PACEMAKER_DETECTION_TAG, DATA_INT, " pulses"),
  _event("QRS Location", QRS_DETECTION_TAG, DATA_INT | DATA_CHAR | DATA_MASK_UPPER),
  _event("Samples used for Heart Rate", TZMR_BPM_COUNT_TAG, DATA_INT, " samples"),
  _event("Heart Rate STDEV", TZMR_BPM_STDEV_TAG, DATA_INT, " BPM"),
  _event("Average Heart Rate", TZMR_BPM_AVERAGE_TAG, DATA_INT, " BPM"),
  _event("Minimum Heart Rate", TZMR_BPM_MIN_TAG, DATA_INT, " BPM"),
  _event("Maximum Heart Rate", TZMR_BPM_MAX_TAG, DATA_INT, " BPM"),
  _event("Tachy Started", TZMR_TACHY_ENTRY_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Tachy Ended", TZMR_TACHY_EXIT_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Brady Started", TZMR_BRADY_ENTRY_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Brady Ended", TZMR_BRADY_EXIT_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Pause Started", TZMR_PAUSE_ENTRY_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("AF Started", TZMR_AFIB_ENTRY_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("AF Ended", TZMR_AFIB_EXIT_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Pause Ended", TZMR_PAUSE_EXIT_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Tachycardia Rate Change", TACHY_RATE_CHANGE_TAG, DATA_INT, " BPM"),
  _event("Bradycardia Rate Change", BRADY_RATE_CHANGE_TAG, DATA_INT, " BPM"),
  _event("PVC Detected", TZMR_PVC_ENTRY_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Arrhythmia Analysis Resumed", TZMR_ANALYSIS_RESUMED_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Arrhythmia Analysis Paused", TZMR_ANALYSIS_PAUSED_TAG, DATA_INT | DATA_MASK_LOWER),
  _event("Electrodes Disconnected", LEAD_DISCONNECTED, DATA_INT | DATA_MASK_LOWER),
  _event("Missing SCP File", TZMR_MISSING_SCP_TAG, DATA_NONE),
  _event("Missing TZE File", TZMR_MISSING_EVENT_TAG, DATA_INT, ".tze"),
  _event("Manual Patient Event", PATIENT_TAG, DATA_INT | DATA_MSB | DATA_LSB, patientEventLog),
  _event("Battery Voltage", BATTERY_VALUE_TAG, DATA_INT, " mV"),
  _event("Low Battery Voltage", BATTERY_LOW_TAG, DATA_INT, " mV"),
  _event("Charging Started", CHARGING_STARTED_TAG, DATA_NONE),
  _event("Charging Stopped", CHARGING_STOPPED_TAG, DATA_NONE),
  _event("Time Zone Changed", TZMR_TIME_ZONE_CHANGE_TAG, DATA_INT),
  _event("Server Interval Request", TZR_REQUEST_TAG, DATA_INT | DATA_MSB | DATA_LSB),
  _event("Bulk ECG Upload", BULK_UPLOAD_TAG, DATA_INT, " files"),
  _event("Server ECG Retransmission Request", SCP_RETRANSMIT_TAG, DATA_INT),
  _event("Server ECG Request", SCP_REQUEST_TAG, DATA_INT),
  _event("Settings Download Successful", SETTINGS_SUCCESS_TAG, DATA_INT),
  _event("SMS Command Error", TZMR_SMS_COMMAND_ERROR_TAG, DATA_NONE),
  _event("Message Acknowledged", MSG_RECEIVED_TAG, DATA_INT),
  _event("Settings Download Error", SETTINGS_FAILURE_TAG, DATA_INT),
  _event("Actions Download Error", ACTIONS_FAILURE_TAG, DATA_INT),
  _event("Actions Download Successful", ACTIONS_SUCCESS_TAG, DATA_INT),
  _event("Server Settings Changed", TZMR_SERVER_SETTINGS_CHANGED, DATA_INT),
  _event("Final Entry", TERMINATOR_TAG, DATA_NONE),
};

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
string patientEventLog(uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;

  if (!data) {
    outputStream << "---No Diary Info." << endl;
  }
  else {
    if (data & 0xff) {
      outputStream << "---Patient Symptom: " << (data & 0xff) << endl;
    }
    if (data & 0xff00) {
      outputStream << "---Patient Activity Level: " << ((data >> 8) & 0xff) << endl;
    }
  }

  return outputStream.str();
}

//==============================================================================
string httpEventLog(uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;

  if (0 == data) {
    outputStream << "SCP file " << setw(8) << setfill('0') << sequence << ".scp" << setfill(' ')
                 << " failed to upload (HTTP 400-series error)." << endl;
  }
  else {
    outputStream << "TZE file " << setw(8) << setfill('0') << sequence << "_" << data << ".tze"
                 << setfill(' ') << " failed to upload (HTTP 400-series error)." << endl;
  }

  return outputStream.str();
}

//==============================================================================
string httpOtherLog(uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;

  if (1 == data) {
    outputStream << "TZR file " << parsedTime.getTzrName()
                 << " failed to upload (HTTP 400-series error)." << endl;
  }
  else if (2 == data) {
    outputStream << "TZA file" << " failed to upload (HTTP 400-series error)." << endl;
  }
  else if (3 == data) {
    outputStream << "TZS file" << " failed to upload (HTTP 400-series error)." << endl;
  }
  else if (4 == data) {
    outputStream << "Error log file" << " failed to upload (HTTP 400-series error)." << endl;
  }
  else if (5 == data) {
    outputStream << "Firmware update file" << " failed to upload (HTTP 400-series error)." << endl;
  }
  else {
    outputStream << "Unknown file" << " failed to upload (HTTP 400-series error)." << endl;
  }

  return outputStream.str();
}

//==============================================================================
_event *getEventsList(string deviceID)
{
  if (string::npos != deviceID.find("H3R  ")) {
    return eventsList;
  }
  else if (string::npos != deviceID.find("HPR  ")) {
    return eventsList;
  }
  else if (string::npos != deviceID.find("TZMR")) {
    return tzmrEventsList;
  }
  else {
    return NULL;
  }
}

//==============================================================================
int getEventsListLength(string deviceID)
{
  if (string::npos != deviceID.find("H3R  ")) {
    return sizeof(eventsList) / sizeof(_event);
  }
  else if (string::npos != deviceID.find("HPR  ")) {
    return sizeof(eventsList) / sizeof(_event);
  }
  else if (string::npos != deviceID.find("TZMR")) {
    return sizeof(tzmrEventsList) / sizeof(_event);
  }
  else {
    return 0;
  }
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
_event::_event(const char *pS, eventTags tg, uint32_t data)
{
  tag = tg;
  name.assign(pS);
  dataType = data;
  callbackFunction = NULL;
}

//==============================================================================
_event::_event(const char *pS, eventTags tg, uint32_t data, logCallback_t callback)
{
  tag = tg;
  name.assign(pS);
  dataType = data;
  callbackFunction = callback;
}

//==============================================================================
_event::_event(const char *pS, eventTags tg, uint32_t data, const char *pL)
{
  tag = tg;
  name.assign(pS);
  dataType = data;
  callbackFunction = NULL;
  label.assign(pL);
}

//==============================================================================
eventTags _event::getTag()
{
  return tag;
}

//==============================================================================
string _event::getName()
{
  return name;
}

//==============================================================================
string _event::getEventText(uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;

  outputStream << name;

  if (dataType != DATA_NONE) {
    outputStream << ": ";
  }

  if (dataType & DATA_CHAR) {
    const char byte = data & 0xff;
    if ((byte >= ' ') && (byte <= '~')) {
      outputStream << byte;
    }
    else {
      outputStream << "0x" << hex << (int)byte << dec;
    }
    if (dataType & DATA_MASK_UPPER) {
      outputStream << " (Mask=0x" << hex << (int)(data >> 8) << ")" << dec;
    }
  }
  else if (dataType & DATA_MASK_LOWER) {
    outputStream << "Mask=0x" << hex << data << dec;
  }
  else if (dataType & DATA_PERCENT) {
    // We're adding 0.00001 to deal with a floating-point precision error in SOME compilers
    outputStream << setprecision(4) << fixed << ((double)data / 256.L) + 0.000001;
  }
  else if (dataType & DATA_LSB) {
    outputStream << "LSB=" << (int)(data & 0xFF);
    if (dataType & DATA_MSB) {
      outputStream << ", MSB=" << (int)((data >> 8) & 0xFF);
    }
  }
  else if (dataType) {
    outputStream << data;
  }

  if (label.length()) outputStream << label;

  outputStream << endl;

  if (NULL != callbackFunction) {
    outputStream << callbackFunction(data, sequence, parsedTime);
  }

  return outputStream.str();
}

//==============================================================================
string _event::getEventJson(uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;
  bool needComma = 0;

  outputStream << "\"" << JSON_EVENT_TYPE_LABEL << "\":\"" << name << "\",";
  outputStream << "\"" << JSON_EVENT_DATA_LABEL << "\":{";
  if (dataType & DATA_CHAR) {
    const char byte = data & 0xff;
    if ((byte >= ' ') && (byte <= '~')) {
      if (needComma) outputStream << ",";
      outputStream << "\"" << JSON_EVENT_DATA_CHAR_LABEL << "\":\"" << (char)(data & 0xFF) << "\"";
      needComma = 1;
    }
  }
  if (dataType & DATA_MASK_LOWER) {
    if (needComma) outputStream << ",";
    outputStream << "\"" << JSON_EVENT_DATA_MASK_LABEL << "\":" << (int)(data & 0xFF);
    needComma = 1;
  }
  if (dataType & DATA_MASK_UPPER) {
    if (needComma) outputStream << ",";
    outputStream << "\"" << JSON_EVENT_DATA_MASK_LABEL << "\":" << (int)((data >> 8) & 0xFF);
    needComma = 1;
  }
  if (dataType & DATA_LSB) {
    if (needComma) outputStream << ",";
    outputStream << "\"" << JSON_EVENT_DATA_LSB_LABEL << "\":" << (int)(data & 0xFF);
    needComma = 1;
  }
  if (dataType & DATA_MSB) {
    if (needComma) outputStream << ",";
    outputStream << "\"" << JSON_EVENT_DATA_MSB_LABEL << "\":" << (int)((data >> 8) & 0xFF);
    needComma = 1;
  }
  if (dataType & DATA_PERCENT) {
    if (needComma) outputStream << ",";
    // We're adding 0.000001 to deal with a floating-point precision error in SOME compilers
    outputStream << "\"" << JSON_EVENT_DATA_PERCENT_LABEL << "\":" << setprecision(4) << fixed
                 << ((double)data / 256.L) + 0.000001;
    needComma = 1;
  }
  if (dataType & DATA_INT) {
    if (needComma) outputStream << ",";
    outputStream << "\"" << JSON_EVENT_DATA_INT_LABEL << "\":" << (int)data;
    needComma = 1;
  }

  outputStream << "}";

  return outputStream.str();
}

//==============================================================================
_event *getEvent(string deviceID, eventTags tag)
{
  int listLength = getEventsListLength(deviceID);
  _event *pList = getEventsList(deviceID);
  bool matchFound = 0;
  int i;

  for (i = 0; i < listLength; i++) {
    if (tag == pList[i].getTag()) {
      matchFound = 1;
      break;
    }
  }

  if (matchFound)
    return &pList[i];
  else
    return NULL;
}

//==============================================================================
string getEventText(
  string deviceID, eventTags tag, uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;
  _event *pEvent = getEvent(deviceID, tag);

  if (NULL != pEvent) {
    outputStream << pEvent->getEventText(data, sequence, parsedTime);
  }
  else {
    outputStream << "ERROR: unknown tag(" << tag << ") data(0x" << hex << data << ")" << dec
                 << endl;
  }

  return outputStream.str();
}

//==============================================================================
string getEventJson(
  string deviceID, eventTags tag, uint32_t data, uint32_t sequence, timestamp &parsedTime)
{
  ostringstream outputStream;
  _event *pEvent = getEvent(deviceID, tag);

  if (NULL != pEvent) {
    outputStream << pEvent->getEventJson(data, sequence, parsedTime);
  }
  else {
    outputStream << "\"error\":\"Unknown tag\",\"data\":{\"intData\":" << data << "}";
  }

  return outputStream.str();
}

//==============================================================================
string listEvents(string deviceID)
{
  ostringstream outputStream;
  int listLength = getEventsListLength(deviceID);
  _event *pList = getEventsList(deviceID);
  int i;

  for (i = 0; i < listLength; i++) {
    outputStream << pList[i].getName() << ": " << pList[i].getTag() << endl;
  }

  return outputStream.str();
}