#ifndef DRIVE_EJECT_H
#define DRIVE_EJECT_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <string>
#include <sstream>
#include <map>

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

//! Eject a drive given a drive identifier.
//! Drive identifier should be the drive letter in windows,
//! TODO: Drive ejection on mac and linux
//! \return 0 if successful, -1 otherwise
int EjectDrive(
  std::istringstream* inStream, std::ostringstream* outStream, std::ostringstream* errStream,
  std::ostringstream* logStream);

#endif