/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "setting_list.h"
#include <initializer_list>

#include "util/json.h"

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

// clang-format off
//=============================================================================
vector<_setting> newH3RSettings()
{
  return {
    {// parseStr, tag, name
     "brady_bpm", BRADY_BPM, "Bradycardia Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 30, 100, 1, 11, 255,
     // tooltip
     "When the patient’s average heart rate drops BELOW the value set by this field, the "
     "device will report that the patient has entered Bradycardia.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "tachy_bpm", TACHY_BPM, "Tachycardia Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 60, 300, 2, 11, 255,
     // tooltip
     "When the patient’s average heart rate rises ABOVE the value set by this field, the "
     "device will report that the patient has entered Tachycardia.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "pause_duration", PAUSE_DURATION, "Pause Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1500, 15000, 2, 11, 255,
     // tooltip
     "When the patient’s R-R interval rises ABOVE the value set by this field, the device "
     "will report a Pause for each beat preceding such an interval.",
     "Milliseconds", MCT_ONLY},
    {// parseStr, tag, name
     "brady_rate_change", BRADY_RATE_CHANGE, "Bradycardia Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 11, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate drops by at least <value> BPM below the Bradycardia Heart Rate. If "
     "it drops again by at least <value>, another event will be reported. If it drops by "
     "twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "tachy_rate_change", TACHY_RATE_CHANGE, "Tachycardia Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 11, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate rises by at least <value> BPM above the Tachycardia Heart Rate. If "
     "it rises again by at least <value>, another event will be reported. If it rises by "
     "twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "arrhythmia_min_duration", ARRHYTHMIA_MIN_DURATION, "Arrhythmia Onset Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 11, 255,
     // tooltip
     "The value of this field sets the minimum arrhythmia duration necessary to report onset "
     "from NSR. In cases where several arrhythmias occur together in short succession, the "
     "algorithm will attempt to “combine” the results into a smaller number of sufficiently "
     "long arrhythmias for reporting.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "af_rate_change", AF_RATE_CHANGE, "AF Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 24, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate rises by at least <value> BPM above the Tachycardia Heart Rate threshold "
     "while the patient is in AF. If it rises again by at least <value>, another event will be "
     "reported. If it rises by twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "arrhythmia_offset_duration", ARRHYTHMIA_OFFSET_DURATION, "Arrhythmia Offset Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 25, 255,
     // tooltip
     "The value of this field sets the minimum arrhythmia duration necessary to report onset "
     "from another arrhythmia. In cases where several arrhythmias occur together in short "
     "succession, the algorithm will attempt to “combine” the results into a smaller number "
     "of sufficiently long arrhythmias for reporting.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "detect_pacemaker", DETECT_PACEMAKER, "Detect Pacemaker Spikes",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this value is set, the device will detect and record the width of pacemaker "
     "pulses.",
     "Boolean"},
    {// parseStr, tag, name
     "summarize_qrs_detection", SUMMARIZE_QRS_DETECTION, "Summarize QRS Detections",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 11, 255,
     // tooltip
     "When the value of this field is set greater than or equal to 10, the device will "
     "summarize the QRS detections as a number of normal QRS beats and a number of PVC beats "
     "detected within the last <value> seconds. This significantly reduces the size of "
     "interval "
     "files.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "pedometer_period", PEDOMETER_PERIOD, "Pedometer Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 11, 255,
     // tooltip
     "When this is set to a non-zero value, the device will record the number of detected "
     "steps every <value> seconds in the interval files.",
     "Seconds"},
    {// parseStr, tag, name
     "report_battery_temperature", REPORT_BATTERY_TEMPERATURE, "Report Battery Temperature",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 24, 255,
     // tooltip
     "When this value is set to 1, the battery temperature is reported to the TZR file.",
     ""},
    {// parseStr, tag, name
     "length_is_calendar_days", LENGTH_IS_CALENDAR_DAYS, "Study Length is Calendar Days",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 19, 255,
     // tooltip
     "When this value is set, the device will include recording interruptions in the "
     "recording length calculation, resulting in the recording ending exactly Study Hours "
     "after it is started, regardless of the amount of data stored.",
     "Boolean"},
    {// parseStr, tag, name
     "one_ecg_file", ONE_ECG_FILE, "One ECG File",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 16, 255,
     // tooltip
     "When this value is set, the device will store all ECG data in a single binary file "
     "consisting of concatenated SCP files, instead of using the directory structure for "
     "individual SCP files.",
     "Boolean"},
    {// parseStr, tag, name
     "study_hours", STUDY_HOURS, "Study Hours",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 744, 4, 10, 24,
     // tooltip
     "This setting defines the number of hours of data that the device will record before "
     "stopping the recording. This value is also used to update the “time remaining” and "
     "progress bar on the device status screen.",
     "Hours"},
     {// parseStr, tag, name
     "study_hours", STUDY_HOURS, "Study Hours",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 1464, 4, 25, 255,
     // tooltip
     "This setting defines the number of hours of data that the device will record before "
     "stopping the recording. This value is also used to update the “time remaining” and "
     "progress bar on the device status screen.",
     "Hours"},
    {// parseStr, tag, name
     "sample_rate", SAMPLE_RATE, "Sample Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 250, 4000, 2, 10, 255,
     // tooltip
     "The value in this field sets the sampling rate for each ECG channel in samples per "
     "second. Input values will be rounded up based on the capabilities of the device. Check "
     "the SCP files for actual recording rate. Generally, devices will round up to a multiple "
     "of the default value.",
     // units, availability, options
     "Hz", ALL_DEVICES, {250, 500, 1000, 2000, 4000}},
    {// parseStr, tag, name
     "report_pre_time", REPORT_PRE_TIME, "Report Pre-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 11, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported prior to a rhythm "
     "change event. Patient events are also controlled by this setting prior to v2.0. Setting "
     "this and Report Post-Time to 0 will disable upload of rhythm change events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "report_post_time", REPORT_POST_TIME, "Report Post-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 11, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported following a rhythm "
     "change event. Patient events are also controlled by this setting prior to v2.0. Setting "
     "this and Report Pre-Time to 0 will disable upload of rhythm change events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "digital_hp_filter", DIGITAL_HP_FILTER, "Digital High Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will apply a 2nd order high pass "
     "digital filter to the ECG data before being stored to the SD Card. The cutoff frequency "
     "in Hz will be the value of this setting divided by 100, with an effective setting range "
     "of 0.01 Hz to 1.00 Hz.",
     "Hz/100"},
    {// parseStr, tag, name
     "start_pin_code", START_OK_CODE, "Recording Confirmation Pin Code",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 9999, 2, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will wait to start a recording until "
     "this value is entered by the patient.",
     ""},
    {// parseStr, tag, name
     "digital_lp_filter", DIGITAL_LP_FILTER, "Digital Low Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will apply a 6th order low pass "
     "digital filter to the ECG data before being passed to the analysis algorithm or "
     "being stored to the SD Card.",
     "Hz"},
    {// parseStr, tag, name
     "digital_notch_filter", DIGITAL_NOTCH_FILTER, "Digital Notch Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will apply a 2nd order notch digital "
     "filter to the ECG data before being passed to the analysis algorithm or being stored to "
     "the SD Card.",
     "Hz"},
    {// parseStr, tag, name
     "min_time_between_events", MIN_TIME_BETWEEN_EVENTS, "Minimum Time Between Events",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 16, 255,
     // tooltip
     "When this is set to a non-zero value, Rhythm events occurring within this number of "
     "seconds of a previous event will only be logged in the interval file and not uploaded "
     "as a separate event.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "suppress_nsr_events", SUPP_RHYTHM_EVENTS, "Suppress NSR Events",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 17, 18,
     // tooltip
     "When this value is set, the device will not upload any Rhythm Change events for Normal "
     "Sinus Rhythm.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "suppress_rhythm_events", SUPP_RHYTHM_EVENTS, "Suppress Rhythm Events",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 15, 19, 255,
     // tooltip
     "When this value is set, the device will not upload any Rhythm Change events that match "
     "the characters in this setting.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "patient_event_pre_time", PATIENT_PRE_TIME, "Patient Event Pre-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 20, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported prior to a manual "
     "patient event. Setting this and Patient Event Post-Time to 0 will disable upload of "
     "patient events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "patient_event_post_time", PATIENT_POST_TIME, "Patient Event Post-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 20, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported following a manual "
     "patient event. Patient events are also controlled by this setting. Setting this and "
     "Patient Event Pre-Time to 0 will disable upload of patient events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "ignore_haptics_errors", IGNORE_HAPTICS_ERRORS, "Ignore Haptics Errors",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 23, 255,
     // tooltip
     "When this value is set, the device will not report 7002 errors from the haptics driver",
     "Boolean"},
    {// parseStr, tag, name
     "nag_on_low_battery", NAG_ON_LOW_BATTERY, "Nag On Low Battery",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 24, 255,
     // tooltip
     "When this is set to a non-zero value, the device will notify every <value> seconds when "
     "the battery drops below the recharge threshold. If set to a non-zero value less than "
     "10, the value will default to 10.",
     // units
     "Seconds"},
    {// parseStr, tag, name
     "allow_low_battery_start", ALLOW_LOW_BATTERY_START, "Allow Low Battery Monitoring Start",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the device will allow the user to start a study even if the "
     "battery is insufficiently charged for the duration of the recording.",
     "Boolean"},
    {// parseStr, tag, name
     "study_complete_sleep", STUDY_COMPLETE_SLEEP, "Study Complete Sleep Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 10, 255,
     // tooltip
     "When this is set to a non-zero value the device will shut off the screen once the "
     "recording is complete. If set to a value greater than 1, the device will wake up every "
     "N seconds and notify the patient that the recording is complete.",
     "Seconds"},
    {// parseStr, tag, name
     "nag_on_lead_off", NAG_ON_LEAD_OFF, "Lead Off Alert Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will vibrate every <value> seconds "
     "when a lead is disconnected to alert the patient to reconnect the lead.",
     "Seconds"},
    {// parseStr, tag, name
     "bulk_upload", BULK_UPLOAD, "Bulk Upload",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 11, 255,
     // tooltip
     "When this value is set, the device will upload all of the SCP files it has recorded "
     "each hour, along with the interval file.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "transmit_interval_reports", TRANSMIT_INTERVAL_REPORTS, "Transmit Interval Reports",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 11, 255,
     // tooltip
     "When this value is set, the device will transmit interval reports every hour.", "Boolean",
     MCT_ONLY},
    {// parseStr, tag, name
     "screen_sleep_time", SCREEN_SLEEP_TIME, "Screen Sleep Delay",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 5, 180, 1, 10, 255,
     // tooltip
     "The value in this field sets the number of seconds that the device status screen will "
     "display before the device goes back to sleep. This also controls how long each page of "
     "the Digital Patient Diary is displayed if the user does not interact with it.",
     "Seconds"},
    {// parseStr, tag, name
     "demo_mode", DEMO_MODE, "Demonstration Mode",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 2, 1, 10, 255,
     // tooltip
     "When set to 0, the device operates normally. When set to 1, the device will "
     "automatically restart a recording on power-up. When set to 2, it is impossible to "
     "start a recording.",
     ""},
    {// parseStr, tag, name
     "symptom_diary_entries", SYMPTOM_DIARY_COUNT, "Symptom Diary Entires",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 10, 1, 10, 255,
     // tooltip
     "When this value is set to a value greater than 0, the symptom diary will be enabled "
     "using the first <value> symptom entries settings.",
     "Entries"},
    {// parseStr, tag, name
     "activity_diary_entries", ACTIVITY_DIARY_COUNT, "Activity Level Diary Entries",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 10, 1, 10, 255,
     // tooltip
     "When this value is set to a value greater than 0, the activity level diary will be "
     "enabled using the first <value> activity level entries settings.",
     "Entries"},
    {// parseStr, tag, name
     "symptom_entry_1", DIARY_SYMPTOM_1, "Symptom Entry 1",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 1.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_2", DIARY_SYMPTOM_2, "Symptom Entry 2",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 2.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_3", DIARY_SYMPTOM_3, "Symptom Entry 3",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 3.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_4", DIARY_SYMPTOM_4, "Symptom Entry 4",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 4.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_5", DIARY_SYMPTOM_5, "Symptom Entry 5",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 5.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_6", DIARY_SYMPTOM_6, "Symptom Entry 6",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 6.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_7", DIARY_SYMPTOM_7, "Symptom Entry 7",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 7.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_8", DIARY_SYMPTOM_8, "Symptom Entry 8",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 8.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_9", DIARY_SYMPTOM_9, "Symptom Entry 9",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 9.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_10", DIARY_SYMPTOM_10, "Symptom Entry 10",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 10.",
     ""},
    {// parseStr, tag, name
     "activity_entry_1", DIARY_ACTIVITY_1, "Activity Level Entry 1",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 1.",
     ""},
    {// parseStr, tag, name
     "activity_entry_2", DIARY_ACTIVITY_2, "Activity Level Entry 2",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 2.",
     ""},
    {// parseStr, tag, name
     "activity_entry_3", DIARY_ACTIVITY_3, "Activity Level Entry 3",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 3.",
     ""},
    {// parseStr, tag, name
     "activity_entry_4", DIARY_ACTIVITY_4, "Activity Level Entry 4",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 4.",
     ""},
    {// parseStr, tag, name
     "activity_entry_5", DIARY_ACTIVITY_5, "Activity Level Entry 5",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 5.",
     ""},
    {// parseStr, tag, name
     "activity_entry_6", DIARY_ACTIVITY_6, "Activity Level Entry 6",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 6.",
     ""},
    {// parseStr, tag, name
     "activity_entry_7", DIARY_ACTIVITY_7, "Activity Level Entry 7",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 7.",
     ""},
    {// parseStr, tag, name
     "activity_entry_8", DIARY_ACTIVITY_8, "Activity Level Entry 8",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 8",
     ""},
    {// parseStr, tag, name
     "activity_entry_9", DIARY_ACTIVITY_9, "Activity Level Entry 9",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 9.",
     ""},
    {// parseStr, tag, name
     "activity_entry_10", DIARY_ACTIVITY_10, "Activity Level Entry 10",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 22, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 10.",
     ""},
    {// parseStr, tag, name
     "patient_id", PATIENT_ID, "Patient ID",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 39, 10, 255,
     // tooltip
     "Up to 39 ASCII characters identifying the patient to the host software system. This ID "
     "will be embedded in each SCP-ECG data file.",
     ""},
    {// parseStr, tag, name
     "top_banner", TOP_BANNER, "Top Banner",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This text will be displayed on the top of the display at all times. If left empty, the "
     "device will display its own serial number here instead.",
     ""},
    {// parseStr, tag, name
     "bottom_banner", BOTTOM_BANNER, "Bottom Banner",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 32, 19, 255,
     // tooltip
     "This text will be displayed on the bottom of the display at all times. If left empty, "
     "the device will display the patient ID here instead.",
     ""},
    {// parseStr, tag, name
     "server_address", SERVER_ADDRESS, "Server Address",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 255, 11, 255,
     // tooltip
     "This string is the address of the web server used for mobile uploading data and "
     "downloading actions files. It should omit the preceding \'https://\' and may be an IP "
     "address or a domain name. A custom port may be used at the end of the string. Here are "
     "some examples: \'8.8.8.8\' \'8.8.8.8:12345\' \'www.google.com\' "
     "\'www.google.com:12345\' "
     "\'8.8.8.8/h3r-devices\' \'8.8.8.8:12345/h3r-devices\' \'google.com/h3r-devices\' "
     "\'google.com:12345/h3r-devices\'",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "tls_identity", TLS_IDENTITY, "TLS Identity",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 11, 255,
     // tooltip
     "When this string is not empty (i.e. not \'\'), the device will use TLS for all "
     "connections. If using PSK, this string will be transmitted in the clear to the server "
     "as psk_identity. This should match the server’s configuration of key-identity pairs.  "
     "If using ECDSA, this string should be set to something like “use_ecdsa” to force TLS "
     "mode.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "tls_psk", TLS_PSK, "TLS PSK",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 11, 255,
     // tooltip
     "This string contains the pre-shared key for TLS PSK ciphers. This string should "
     "represent the binary value as a long hexadecimal number [0-9, a-e].",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "aes_iv", TLS_IDENTITY, "AES Initialization Vector",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_HEX, UNUSED_FIELD, UNUSED_FIELD, 16, 10, 255,
     // tooltip
     "This string contains an AES-128 initialization vector to be used when operating in TZMR "
     "Compatibility mode.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "aes_key", TLS_PSK, "AES Key",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_HEX, UNUSED_FIELD, UNUSED_FIELD, 16, 10, 255,
     // tooltip
     "This string contains an AES-128 pre-shared key to be used when operating in TZMR "
     "Compatibility mode.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "apn_address", APN_ADDRESS, "APN Address",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 255, 11, 255,
     // tooltip
     "This string is the address of the APN server in the carrier network used for wireless "
     "connections.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "apn_username", APN_USERNAME, "APN Username",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 11, 255,
     // tooltip
     "This string is the username used for authentication with the carrier network. It can be "
     "left blank (i.e. \'\') if no APN authentication is required.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "apn_password", APN_PASSWORD, "APN Password",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 11, 255,
     // tooltip
     "This string is the password associated with the APN Username. It can be left blank "
     "(i.e. \'\') if no APN authentication is required.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "http_bearer_token", HTTP_BEARER_TOKEN, "HTTP Bearer Token",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 11, 255,
     // tooltip
     "If this string is set, the device will add an authorization line to the HTTP header for "
     "each server connection according to the format: \'Authorization: Bearer %s\'.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "connection_timeout", CONNECTION_TIMEOUT, "Connection Timeout",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 240, 1, 11, 255,
     // tooltip
     "The time to wait for a modem to establish a connection before closing the connection "
     "and trying again.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "bad_tag", BAD_TAG, "Bad Tag",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "This setting is used for testing error handling of invalid settings tags.", "Boolean"},
    {// parseStr, tag, name
     "error_retries", ERROR_RETRIES, "Error Retry Count",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 250, 1, 10, 255,
     // tooltip
     "This value defines how many times the device will attempt to automatically recover from "
     "an error, within a given time-period, before reporting the error to the user.",
     "Retries"},
    {// parseStr, tag, name
     "error_period", ERROR_PERIOD, "Error Retry Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 1440, 2, 10, 255,
     // tooltip
     "This value defines the period within which the Error Retry Count must not be exceeded.",
     "Minutes"},
    {// parseStr, tag, name
     "zymed_compat", ZYMED_COMPAT, "Zymed Compatibility",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 19, 22,
     // tooltip
     "When this value is set, the device will store ECG data both in SCP files and in a "
     "<serialNumber>.rf file for import into Zymed software.",
     "Boolean"},
    {// parseStr, tag, name
     "zymed_compat", ZYMED_COMPAT, "Zymed Compatibility",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 2, 1, 23, 255,
     // tooltip
     "When this value is set, the device will store ECG data both in SCP files and in a "
     "<serialNumber>.rf file for import into Zymed software. When set to 1, calibration "
     "signals will be inserted when leads are disconnected; when set to 2 flatline will be "
     "inserted.",
     ""},
    {// parseStr, tag, name
     "tzmr_compat", TZMR_COMPAT, "TZMR Compatibility",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 12, 255,
     // tooltip
     "When this value is set, the device will emulate the connections of an Aera CT device.",
     "Boolean", MCT_ONLY},
  };
}

//=============================================================================
vector<_setting> newHPRSettings()
{
  return {
    {// parseStr, tag, name
     "brady_bpm", BRADY_BPM, "Bradycardia Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 30, 100, 1, 17, 255,
     // tooltip
     "When the patient’s average heart rate drops BELOW the value set by this field, the "
     "device will report that the patient has entered Bradycardia.",
     "BPM", MCT_ONLY},
     {// parseStr, tag, name
     "tachy_bpm", TACHY_BPM, "Tachycardia Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 60, 300, 2, 17, 255,
     // tooltip
     "When the patient’s average heart rate rises ABOVE the value set by this field, the "
     "device will report that the patient has entered Tachycardia.",
     "BPM", MCT_ONLY},
     {// parseStr, tag, name
     "pause_duration", PAUSE_DURATION, "Pause Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1500, 15000, 2, 17, 255,
     // tooltip
     "When the patient’s R-R interval rises ABOVE the value set by this field, the device "
     "will report a Pause for each beat preceding such an interval.",
     "Milliseconds", MCT_ONLY},
      {// parseStr, tag, name
     "brady_rate_change", BRADY_RATE_CHANGE, "Bradycardia Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 17, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate drops by at least <value> BPM below the Bradycardia Heart Rate. If "
     "it drops again by at least <value>, another event will be reported. If it drops by "
     "twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
      {// parseStr, tag, name
     "tachy_rate_change", TACHY_RATE_CHANGE, "Tachycardia Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 17, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate rises by at least <value> BPM above the Tachycardia Heart Rate. If "
     "it rises again by at least <value>, another event will be reported. If it rises by "
     "twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "arrhythmia_min_duration", ARRHYTHMIA_MIN_DURATION, "Arrhythmia Onset Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 17, 255,
     // tooltip
     "The value of this field sets the minimum arrhythmia duration necessary to report onset "
     "from NSR. In cases where several arrhythmias occur together in short succession, the "
     "algorithm will attempt to “combine” the results into a smaller number of sufficiently "
     "long arrhythmias for reporting.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "af_rate_change", AF_RATE_CHANGE, "AF Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 17, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate rises by at least <value> BPM above the Tachycardia Heart Rate threshold "
     "while the patient is in AF. If it rises again by at least <value>, another event will be "
     "reported. If it rises by twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "arrhythmia_offset_duration", ARRHYTHMIA_OFFSET_DURATION, "Arrhythmia Offset Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 17, 255,
     // tooltip
     "The value of this field sets the minimum arrhythmia duration necessary to report onset "
     "from another arrhythmia. In cases where several arrhythmias occur together in short "
     "succession, the algorithm will attempt to “combine” the results into a smaller number "
     "of sufficiently long arrhythmias for reporting.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "detect_pacemaker", DETECT_PACEMAKER, "Detect Pacemaker Spikes",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this value is set, the device will detect and record the width of pacemaker "
     "pulses.",
     "Boolean"},
      {// parseStr, tag, name
     "summarize_qrs_detection", SUMMARIZE_QRS_DETECTION, "Summarize QRS Detections",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 17, 255,
     // tooltip
     "When the value of this field is set greater than or equal to 10, the device will "
     "summarize the QRS detections as a number of normal QRS beats and a number of PVC beats "
     "detected within the last <value> seconds. This significantly reduces the size of "
     "interval "
     "files.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "pedometer_period", PEDOMETER_PERIOD, "Pedometer Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will record the number of detected "
     "steps every <value> seconds in the interval files.",
     "Seconds"},
    {// parseStr, tag, name
     "store_raw_accel", STORE_RAW_ACCEL, "Store Raw Accel",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the device will record raw acceleration waveforms in *.tzg "
     "files.",
     "Boolean"},
     {// parseStr, tag, name
     "report_battery_temperature", REPORT_BATTERY_TEMPERATURE, "Report Battery Temperature",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 17, 255,
     // tooltip
     "When this value is set to 1, the battery temperature is reported to the TZR file.",
     ""},
    {// parseStr, tag, name
     "length_is_calendar_days", LENGTH_IS_CALENDAR_DAYS, "Study Length is Calendar Days",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 13, 255,
     // tooltip
     "When this value is set, the device will include recording interruptions in the "
     "recording length calculation, resulting in the recording ending exactly Study Hours "
     "after it is started, regardless of the amount of data stored.",
     "Boolean"},
    {// parseStr, tag, name
     "one_ecg_file", ONE_ECG_FILE, "One ECG File",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this value is set, the device will store all ECG data in a single binary file "
     "consisting of concatenated SCP files, instead of using the directory structure for "
     "individual SCP files.",
     "Boolean"},
    {// parseStr, tag, name
     "study_hours", STUDY_HOURS, "Study Hours",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 744, 4, 10, 16,
     // tooltip
     "This setting defines the number of hours of data that the device will record before "
     "stopping the recording.",
     "Hours"},
     {// parseStr, tag, name
     "study_hours", STUDY_HOURS, "Study Hours",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 1464, 4, 17, 255,
     // tooltip
     "This setting defines the number of hours of data that the device will record before "
     "stopping the recording.",
     "Hours"},
    {// parseStr, tag, name
     "sample_rate", SAMPLE_RATE, "Sample Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 200, 1600, 2, 10, 255,
     // tooltip
     "The value in this field sets the sampling rate for each ECG channel in samples per "
     "second. Input values will be rounded up based on the capabilities of the device. Check "
     "the SCP files for actual recording rate. Generally, devices will round up to a multiple "
     "of the default value.",
     // units, availability, options
     "Hz", ALL_DEVICES, {200, 400, 800, 1600}},
     {// parseStr, tag, name
     "report_pre_time", REPORT_PRE_TIME, "Report Pre-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 17, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported prior to a rhythm "
     "change event. Setting this and Report Post-Time to 0 will disable upload of rhythm "
     "change events.",
     "Seconds", MCT_ONLY},
      {// parseStr, tag, name
     "report_post_time", REPORT_POST_TIME, "Report Post-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 17, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported following a rhythm "
     "change event. Setting this and Report Pre-Time to 0 will disable upload of rhythm "
     "change events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "digital_hp_filter", DIGITAL_HP_FILTER, "Digital High Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will apply a 2nd order high pass "
     "digital filter to the ECG data before being stored to the SD Card. The cutoff frequency "
     "in Hz will be the value of this setting divided by 100, with an effective setting range "
     "of 0.01 Hz to 1.00 Hz.",
     "Hz/100"},
    {// parseStr, tag, name
     "digital_lp_filter", DIGITAL_LP_FILTER, "Digital Low Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will apply a 6th order low pass "
     "digital filter to the ECG data before being passed to the analysis algorithm or being "
     "stored to the SD Card.",
     "Hz"},
    {// parseStr, tag, name
     "digital_notch_filter", DIGITAL_NOTCH_FILTER, "Digital Notch Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the device will apply a 2nd order notch digital "
     "filter to the ECG data before being passed to the analysis algorithm or being stored to "
     "the SD Card.",
     "Hz"},
         {// parseStr, tag, name
     "min_time_between_events", MIN_TIME_BETWEEN_EVENTS, "Minimum Time Between Events",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 17, 255,
     // tooltip
     "When this is set to a non-zero value, Rhythm events occurring within this number of "
     "seconds of a previous event will only be logged in the interval file and not uploaded "
     "as a separate event.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "suppress_rhythm_events", SUPP_RHYTHM_EVENTS, "Suppress Rhythm Events",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 15, 17, 255,
     // tooltip
     "When this value is set, the device will not upload any Rhythm Change events that match "
     "the characters in this setting.",
     "Boolean", MCT_ONLY},
         {// parseStr, tag, name
     "patient_event_pre_time", PATIENT_PRE_TIME, "Patient Event Pre-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 17, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported prior to a manual "
     "patient event. Setting this and Patient Event Post-Time to 0 will disable upload of "
     "patient events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "patient_event_post_time", PATIENT_POST_TIME, "Patient Event Post-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300, 2, 17, 255,
     // tooltip
     "The value of this field sets the minimum length of ECG data reported following a manual "
     "patient event. Patient events are also controlled by this setting. Setting this and "
     "Patient Event Pre-Time to 0 will disable upload of patient events.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "nag_on_replace_electrode", NAG_ON_REPLACE_ELECTRODE, "Replace Electrode Alert Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 13, 255,
     // tooltip
     "When this is set to a non-zero value, the device will beep every <value> seconds when "
     "the patch electrode needs to be replaced. If set to a non-zero value less than 10, the "
     "value will default to 10.",
     "Seconds"},
    {// parseStr, tag, name
     "nag_on_low_battery", NAG_ON_LOW_BATTERY, "Low Battery Alert Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 13, 255,
     // tooltip
     "When this is set to a non-zero value, the device will notify every <value> seconds when "
     "the battery drops below the recharge threshold. If set to a non-zero value less than "
     "10, the value will default to 10.",
     "Seconds"},
    {// parseStr, tag, name
     "electrode_max_wear_hours", PATCH_MAX_WEAR_LENGTH, "Electrode Max Wear Hours",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 168, 1, 13, 255,
     // tooltip
     "When this is set to a non-zero value the device will alert the patient when the "
     "wear-time for the electrode has been exceeded.",
     "Hours"},
    {// parseStr, tag, name
     "allow_low_battery_start", ALLOW_LOW_BATTERY_START, "Allow Low Battery Monitoring Start",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the device will allow the user to start a study even if the "
     "battery is insufficiently charged for the duration of the recording.",
     "Boolean"},
    {// parseStr, tag, name
     "study_complete_sleep", STUDY_COMPLETE_SLEEP, "Study Complete Sleep Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 13, 255,
     // tooltip
     "When this is set to a non-zero value the device will wake up every <value> seconds and "
     "notify the patient that the recording is complete. If set to a non-zero value less than "
     "10, the value will default to 10.",
     "Seconds"},
    {// parseStr, tag, name
     "nag_on_lead_off", NAG_ON_LEAD_OFF, "Lead Off Alert Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 13, 255,
     // tooltip
     "When this is set to a non-zero value, the device will beep every <value> seconds "
     "when a lead is disconnected to alert the patient to reconnect the lead. If set to a "
     "non-zero value less than 10, the value will default to 10.",
     "Seconds"},
         {// parseStr, tag, name
     "bulk_upload", BULK_UPLOAD, "Bulk Upload",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 17, 255,
     // tooltip
     "When this value is set, the device will upload all of the SCP files it has recorded "
     "each hour, along with the interval file.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "transmit_interval_reports", TRANSMIT_INTERVAL_REPORTS, "Transmit Interval Reports",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 17, 255,
     // tooltip
     "When this value is set, the device will transmit interval reports every hour.", "Boolean",
     MCT_ONLY},
    {// parseStr, tag, name
     "demo_mode", DEMO_MODE, "Demonstration Mode",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 10, 1, 10, 255,
     // tooltip
     "When set to 0, the device operates normally. When set to 1, the device will "
     "automatically restart a recording on power-up. When set to 2+, it is impossible to "
     "start a recording and the device will blink the Green LED with a period of 2.4 seconds "
     "+ this setting <value>.",
     ""},
    {// parseStr, tag, name
     "patient_id", PATIENT_ID, "Patient ID",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 39, 10, 255,
     // tooltip
     "Up to 39 ASCII characters identifying the patient to the host software system. This ID "
     "will be embedded in each SCP-ECG data file.",
     ""},
     {// parseStr, tag, name
     "server_address", SERVER_ADDRESS, "Server Address",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 255, 17, 255,
     // tooltip
     "This string is the address of the web server used for mobile uploading data and "
     "downloading actions files. It should omit the preceding \'https://\' and may be an IP "
     "address or a domain name. A custom port may be used at the end of the string. Here are "
     "some examples: \'8.8.8.8\' \'8.8.8.8:12345\' \'www.google.com\' "
     "\'www.google.com:12345\' "
     "\'8.8.8.8/hpr-devices\' \'8.8.8.8:12345/hpr-devices\' \'google.com/hpr-devices\' "
     "\'google.com:12345/hpr-devices\'",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "tls_identity", TLS_IDENTITY, "TLS Identity",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 17, 255,
     // tooltip
     "When this string is not empty (i.e. not \'\'), the device will use TLS for all "
     "connections. If using PSK, this string will be transmitted in the clear to the server "
     "as psk_identity. This should match the server’s configuration of key-identity pairs.  "
     "If using ECDSA, this string should be set to something like “use_ecdsa” to force TLS "
     "mode.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "tls_psk", TLS_PSK, "TLS PSK",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 17, 255,
     // tooltip
     "This string contains the pre-shared key for TLS PSK ciphers. This string should "
     "represent the binary value as a long hexadecimal number [0-9, a-e].",
     "", MCT_ONLY},
     {// parseStr, tag, name
     "wifi_ssid", WIFI_SSID, "WiFi SSID",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 33, 17, 255,
     // tooltip
     "This string is the name of the default WiFi network.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "wifi_password", WIFI_PASSWORD, "WiFi Password",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 65, 17, 255,
     // tooltip
     "This string is the password associated with the WiFi SSID.",
     "", MCT_ONLY},
     {// parseStr, tag, name
     "http_bearer_token", HTTP_BEARER_TOKEN, "HTTP Bearer Token",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 127, 17, 255,
     // tooltip
     "If this string is set, the device will add an authorization line to the HTTP header for "
     "each server connection according to the format: \'Authorization: Bearer %s\'.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "connection_timeout", CONNECTION_TIMEOUT, "Connection Timeout",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 240, 1, 17, 255,
     // tooltip
     "The time to wait for a modem to establish a connection before closing the connection "
     "and trying again.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "bad_tag", BAD_TAG, "Bad Tag",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "This setting is used for testing error handling of invalid settings tags.", "Boolean"},
     {// parseStr, tag, name
     "error_retries", ERROR_RETRIES, "Error Retry Count",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 250, 1, 10, 255,
     // tooltip
     "This value defines how many times the device will attempt to automatically recover from "
     "an error, within a given time-period, before reporting the error to the user.",
     "Retries"},
    {// parseStr, tag, name
     "error_period", ERROR_PERIOD, "Error Retry Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 1440, 2, 10, 255,
     // tooltip
     "This value defines the period within which the Error Retry Count must not be exceeded.",
     "Minutes"},
    {// parseStr, tag, name
     "zymed_compat", ZYMED_COMPAT, "Zymed Compatibility",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 13, 13,
     // tooltip
     "When this value is set, the device will store ECG data both in SCP files and in a "
     "<serialNumber>.rf file for import into Zymed software.",
     "Boolean"},
    {// parseStr, tag, name
     "zymed_compat", ZYMED_COMPAT, "Zymed Compatibility",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 2, 1, 14, 255,
     // tooltip
     "When this value is set, the device will store ECG data both in SCP files and in a "
     "<serialNumber>.rf file for import into Zymed software. When set to 1, calibration "
     "signals will be inserted when leads are disconnected; when set to 2 flatline will be "
     "inserted.",
     ""},
  };
}

//=============================================================================
vector<_setting> newTZMRSettings()
{
  return {
    {// parseStr, tag, name
     "brady_onset_bpm", BRADY_BPM, "Bradycardia Onset Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 20, 100, 1, 10, 255,
     // tooltip
     "When the patient’s average heart rate (calculated over 16 beats) drops BELOW the value "
     "set by this field, the Aera CT will report that the patient has entered Bradycardia. "
     "NOTE: Due to technological limitations, the device does not report heart rate values "
     "below 30 BPM, rendering 31 as the lowest effective value of this setting.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "brady_offset_bpm", TZMR_BRADY_OFFSET_BPM, "Bradycardia Offset Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 20, 100, 1, 10, 255,
     // tooltip
     "When the patient is in Bradycardia and the patient’s average heart rate (calculated "
     "over 16 beats)  rises ABOVE the value set by this field, the Aera CT will report that "
     "the patient has exited Bradycardia. NOTE: the value in this field MUST be GREATER THAN "
     "the value in the Bradycardia Onset field.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "brady_duration", TZMR_BRADY_DURATION, "Bradycardia Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 600, 2, 10, 255,
     // tooltip
     "The patient must remain in Bradycardia for longer than the duration set by the value in "
     "this field before the Aera CT will report the event. Setting this value to less than 15 "
     "will disable the onset duration feature and Bradycardia events will be immediately "
     "reported. NOTE: if the value of this setting is less than that of setting 6, the device "
     "will use that value instead.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "tachy_onset_bpm", TACHY_BPM, "Tachycardia Onset Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 100, 250, 1, 10, 255,
     // tooltip
     "When the patient’s average heart rate (calculated over 16 beats) rises ABOVE the value "
     "set by this field, the Aera CT will report that the patient has entered Tachycardia. "
     "NOTE: Due to technological limitations, the device does not report heart rate values "
     "above 250 BPM, rendering 249 as the highest effective value of this setting.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "tachy_offset_bpm", TZMR_TACHY_OFFSET_BPM, "Tachycardia Offset Heart Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 100, 250, 1, 10, 255,
     // tooltip
     "When the patient is in Tachycardia and the patient’s average heart rate (calculated "
     "over 16 beats) drops BELOW the value set by this field, the Aera CT will report that "
     "the patient has exited Tachycardia. NOTE: the value in this field MUST be LESS THAN the "
     "value in the Tachycardia Onset field.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "tachy_duration", TZMR_TACHY_DURATION, "Tachycardia Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 600, 2, 10, 255,
     // tooltip
     "The patient must remain in Tachycardia for longer than the duration set by the value in "
     "this field before the Aera CT will report the event. Setting this value to less than 15 "
     "will disable the onset duration feature and Tachycardia events will be immediately "
     "reported. NOTE: if the value of this setting is less than that of setting 3, the device "
     "will use that value instead.",
     "Seconds"},
    {// parseStr, tag, name
     "af_duration", TZMR_AF_DURATION, "AF Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 60, 1, 10, 255,
     // tooltip
     "The patient must remain in AF for longer than the duration set by the value in this "
     "field before the Aera CT will report the event. Further, the patient must remain out of "
     "AF for longer than this duration for an offset event to be reported. Setting this value "
     "to 0 will disable the arrhythmia duration feature and Atrial Fibrillation events will "
     "be reported as soon as they are detected.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "pause_duration", PAUSE_DURATION, "Pause Duration Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1500, 30000, 2, 10, 255,
     // tooltip
     "The patient must experience a cardiac pause with a duration GREATER THAN the duration "
     "set by the value in this field before the Aera CT will report the event. NOTE: Pauses "
     "greater than 15 seconds will be disregarded by the device, regardless of the value of "
     "this setting.",
     "Milliseconds", MCT_ONLY},
    {// parseStr, tag, name
     "brady_rate_change", BRADY_RATE_CHANGE, "Bradycardia Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 21, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate drops by at least <value> BPM below the Bradycardia Onset Heart "
     "Rate. If it drops again by at least <value>, another event will be reported. If it "
     "drops by twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "tachy_rate_change", TACHY_RATE_CHANGE, "Tachycardia Rate Change",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 250, 2, 21, 255,
     // tooltip
     "When this is set to a non-zero value, the device will create and upload an event each "
     "time the heart rate rises by at least <value> BPM above the Tachycardia Onset Heart "
     "Rate. If it rises again by at least <value>, another event will be reported. If it "
     "rises by twice <value>, only 1 event will be reported.",
     "BPM", MCT_ONLY},
    {// parseStr, tag, name
     "af_rr_count", TZMR_AF_RR_COUNT, "AF Detection R-R Count",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 32, 64, 1, 18, 255,
     // tooltip
     "This setting controls the number of R-R intervals used by the analysis algorithm for "
     "determining Atrial Fibrillation.",
     "R-R Intervals", MCT_ONLY},
    {// parseStr, tag, name
     "derive_for_analysis", TZMR_DERIVE_FOR_ANALYSIS, "Derive for Analysis",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 7, 1, 18, 255,
     // tooltip
     "When analysis on a channel is disabled (due to setting #30 or a lead-off condition) and "
     "the channel is set to 1 in this setting bitmask, the device will attempt to derive an "
     "ECG channel for analysis from the other 2 leads.",
     "Bitmask", MCT_ONLY},
    {// parseStr, tag, name
     "rpeak_threshold", TZMR_RPEAK_THRESHOLD, "Minimum QRS Amplitude",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 1023, 2, 10, 255,
     // tooltip
     "When set to a non-zero value, the algorithm will ignore QRS complexes with amplitudes "
     "less than this value.",
     "Scaled ADC Counts", MCT_ONLY},
    {// parseStr, tag, name
     "analysis_mask", TZMR_ANALYSIS_CHANNEL_MASK, "Analysis Enable Mask",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 7, 1, 10, 255,
     // tooltip
     "The Aera CT will only perform analysis on the channels specified by the value of this "
     "setting.",
     "Bitmask", MCT_ONLY},
    {// parseStr, tag, name
     "detect_pacemaker", DETECT_PACEMAKER, "Detect Pacemaker Spikes",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this value is set, the Aera CT will detect and record the width of pacemaker "
     "pulses.",
     "Boolean"},
    {// parseStr, tag, name
     "cross_channel_analysis", TZMR_CROSS_CHANNEL_ANALYSIS, "Cross Channel Analysis",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this value is set, the Aera CT will perform cross-channel arrhythmia analysis to "
     "reduce False Positives.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "channel_enable_mask", TZMR_CHANNEL_ENABLE_MASK, "Channel Enable Mask",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 7, 1, 17, 255,
     // tooltip
     "The Aera CT will only store ECG data for the channels specified by the value in this "
     "setting. If a channel is disabled by this setting, it will still have a “section” in "
     "the SCP file, its length will simply be reported as 0 and it will list samples 1 to 0 "
     "for its sample length.",
     "Bitmask"},
    {// parseStr, tag, name
     "use_arrhythmia_state_machine", TZMR_USE_ARRHYTHMIA_STATE_MACHINE,
     "Use Arrhythmia State Machine",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "Then this value is set, the Aera CT report a Tachy/Brady exit if an AF onset is "
     "detected while in Tachy or Brady. Likewise, the device will report a Tachy/Brady onset "
     "when an AF exit is detected if the heart rate is still beyond the threshold.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "pim_enable", TZMR_PIM_ENABLE, "Electrode Detection Enable",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 31, 1, 17, 255,
     // tooltip
     "The Aera CT will only check for electrode connections for the electrodes enabled by "
     "this setting. All other electrodes will be assumed to be connected as long as a patient "
     "cable is connected to the device. Bitmask for the values Is 0b000EASIG.",
     "Bitmask"},
    {// parseStr, tag, name
     "pim_threshold", TZMR_PIM_THRESHOLD, "Electrode Detection Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 17, 255,
     // tooltip
     "Increasing the value of this setting will increase the likelihood of the Aera CT "
     "detecting poor electrode connections and reporting this to the patient.",
     "Sensitivity"},
    {// parseStr, tag, name
     "sample_calibration", TZMR_SAMPLE_CALIBRATION, "Sample Rate Calibration",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 200, 1, 19, 255,
     // tooltip
     "Increasing the value of this setting will increase the sample rate and decreasing the "
     "value of this setting will decrease the sample rate. Overall effect on sample rate is "
     "approximately +/- 650 ppm. We recommend leaving this setting at 0 unless sample rate "
     "errors are detected.",
     ""},
    {// parseStr, tag, name
     "log_qrs_complexes", TZMR_LOG_QRS_COMPLEXES, "Log QRS Detections",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 21, 255,
     // tooltip
     "When this value is set, the Aera CT will record all QRS detections in the interval "
     "file.",
     "Boolean"},
    {// parseStr, tag, name
     "pim_hysteresis", TZMR_PIM_HYSTERESIS, "Electrode Detection Hysteresis",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 50, 1, 28, 255,
     // tooltip
     "Increasing this setting reduces the probability of rapid disconnect/reconnect event "
     "cycles. Decreasing this setting reduces the probability of difficulty connecting "
     "electrodes.",
     "Sensitivity"},
    {// parseStr, tag, name
     "report_start", TZMR_REPORT_START, "Report Start",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when a new recording is started.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_stop", TZMR_REPORT_STOP, "Report Stop",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when the recording process is stopped by SMS command or by the early out procedure.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_break", TZMR_REPORT_BREAK, "Report Suspend",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when the recording has been interrupted by a lead off condition or by power loss.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_resume", TZMR_REPORT_RESUME, "Report Resume",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when resuming recording following a \'Suspend\' condition.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_full", TZMR_REPORT_FULL, "Report Full",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when recording has been stopped due to reaching a predefined stopping point or due to "
     "filling the SD Card.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_pace_detected", TZMR_REPORT_PACE_DETECT, "Report Pacemaker Detections",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "for each SCP-ECG file that contains pacemaker detections, listing the number of "
     "pacemaker detections within the file.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_analysis_resumed", TZMR_REPORT_ANALYSIS_RESUMED, "Report Analysis Resumed",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 19, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the specified number of channels recover from an analysis "
     "paused condition.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_analysis_paused", TZMR_REPORT_ANALYSIS_PAUSED, "Report Analysis Paused",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the specified number of channels have un-analyzable ECG "
     "signal due to clipping or lead-off issues.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_lead_disconnected", TZMR_REPORT_LEAD_DISCONNECTED, "Report Lead Disconnected",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when a lead off condition is detected by the patient impedance monitor system.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_tachy_onset", TZMR_REPORT_TACHY_ONSET, "Report Tachycardia Onset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient enters Tachycardia on the set number of channels. "
     "If Cross Channel Analysis is enabled, values between 1 and 3 perform identically. NOTE: "
     "If wireless is disabled but TTM is enabled, this event will still be created and the "
     "device will add it to the event count displayed in the left corner of the screen.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_tachy_offset", TZMR_REPORT_TACHY_OFFSET, "Report Tachycardia Offset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient exits Tachycardia on the set number of channels. "
     "If Cross Channel Analysis is enabled, values between 1 and 3 perform identically.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_brady_onset", TZMR_REPORT_BRADY_ONSET, "Report Bradycardia Onset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient enters Bradycardia on the set number of channels. "
     "If Cross Channel Analysis is enabled, values between 1 and 3 perform identically. NOTE: "
     "If wireless is disabled but TTM is enabled, this event will still be created and the "
     "device will add it to the event count displayed in the left corner of the screen.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_brady_offset", TZMR_REPORT_BRADY_OFFSET, "Report Bradycardia Offset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient exits Bradycardia on the set number of channels. "
     "If Cross Channel Analysis is enabled, values between 1 and 3 perform identically.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_af_onset", TZMR_REPORT_AF_ONSET, "Report AF Onset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient enters Atrial Fibrillation on the set number of "
     "channels. If Cross Channel Analysis is enabled, values between 1 and 3 perform "
     "identically. NOTE: If wireless is disabled but TTM is enabled, this event will still be "
     "created and the device will add it to the event count displayed in the left corner of "
     "the screen.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_af_offset", TZMR_REPORT_AF_OFFSET, "Report AF Offset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient exits Atrial Fibrillation on the set number of "
     "channels. If Cross Channel Analysis is enabled, values between 1 and 3 perform "
     "identically.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_pause", TZMR_REPORT_PAUSE_ONSET, "Report Pause Onset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient experiences a cardiac pause on the set number of "
     "channels. This event will mark the beginning of the pause. If Cross Channel Analysis is "
     "enabled, values between 1 and 3 perform identically. NOTE: If wireless is disabled but "
     "TTM is enabled, this event will still be created and the device will add it to the "
     "event count displayed in the left corner of the screen.",
     "Channels", MCT_ONLY},
    {// parseStr, tag, name
     "report_patient_event", TZMR_REPORT_PATIENT_EVENT, "Report Patient Event",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "each time the patient holds the event button for more than 3 seconds. NOTE: If wireless "
     "is disabled but TTM is enabled, this event will still be created and the device will "
     "add it to the event count displayed in the left corner of the screen.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_battery_low", TZMR_REPORT_BATTERY_LOW, "Report Battery Low",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when the battery voltage drops below the Modem Shutoff Voltage. Since the wireless does "
     "not function at these voltages, this event will be uploaded to the serve once the "
     "battery has been charged.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_charging_started", TZMR_REPORT_CHARGING_STARTED, "Report Charging Started",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload and event report to the server "
     "when the wall charger is connected to the device.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_charging_stopped", TZMR_REPORT_CHARGING_STOPPED, "Report Charging Stopped",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when the wall charger is disconnected from the device.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_server_request", TZMR_REPORT_SERVER_REQUEST, "Report Server Request",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "in response to the actions file request for ECG data. NOTE: Disabling this will disable "
     "the ECG Data Request entirely. Also, if wireless is disabled, this event will still be "
     "created and the device will add it to the event count displayed in the left corner of "
     "the screen.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_pause_offset", TZMR_REPORT_PAUSE_OFFSET, "Report Pause Offset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3, 1, 19, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will create and upload an event "
     "report to the server when the patient experiences a cardiac pause on the set number of "
     "channels. This event will mark the end of the pause. If Cross Channel Analysis is "
     "enabled, values between 1 and 3 perform identically.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_scp_retransmit", TZMR_REPORT_SCP_RETRANSMIT, "Report SCP Retransmit",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 19, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when an SCP retransmission request is received in an actions file. The SCP file will "
     "still be retransmitted either way.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_tzr_request", TZMR_REPORT_TZR_REQUEST, "Report Interval Request",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 19, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when an interval report request is received in an actions file. The interval file will "
     "still be transmitted, either way.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "report_pvc_onset", TZMR_REPORT_PVC_ONSET, "Report PVC Onset",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 21, 255,
     // tooltip
     "When this is enabled, the Aera CT will create and upload an event report to the server "
     "when the patient experiences a PVC event. This event will mark the PVC location in the "
     "record. NOTE: If wireless is disabled but TTM is enabled, this event will still be "
     "created and the device will add it to the event count displayed in the left corner of "
     "the screen.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "invert_scp_ecg", TZMR_INVERT_SCP_ECG, "Invert SCP ECG",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 7, 1, 10, 255,
     // tooltip
     "When bits 0, 1, and/or 2 are set, the associated channel data will be inverted when "
     "written to an SCP file.",
     "Bitmask"},
    {// parseStr, tag, name
     "invert_onscreen_ecg", TZMR_INVERT_ONSCREEN_ECG, "Invert Onscreen ECG",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 7, 1, 10, 255,
     // tooltip
     "When bits 0, 1, and/or 2 are set, the associated channels will be inverted when "
     "displayed on the “Signal Quality” screen.",
     "Bitmask"},
    {// parseStr, tag, name
     "difference_encoding", TZMR_SCP_DIFFERENCE_ENCODING, "Difference Encoding Order",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 2, 1, 10, 255,
     // tooltip
     "When set to a value of 1 or 2, this setting enables first-order or second-order "
     "difference encoding of ECG data in SCP-ECG files, per the SCP format specifications.",
     "Order"},
    {// parseStr, tag, name
     "sample_rate", SAMPLE_RATE, "Sample Rate",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 200, 512, 2, 10, 255,
     // tooltip
     "The value in this field sets the sampling rate for each ECG channel in samples per "
     "second.",
     "Hz"},
    {// parseStr, tag, name
     "lp_filter", TZMR_LP_FILTER, "Low Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 30, 100, 1, 10, 255,
     // tooltip
     "The value in this field controls the corner frequency of the 5th Order Low-Pass filter "
     "for the ECG frontend. It is recommended that this be set at 40 Hz for adults and 60 Hz "
     "for infants.",
     "Hz"},
    {// parseStr, tag, name
     "scp_length", TZMR_SCP_LENGTH, "SCP-ECG Data File Length",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 10, 60, 1, 10, 255,
     // tooltip
     "The value in this field controls the length of ECG data stored in each SCP-ECG file, "
     "from 2 to 60 seconds. It is recommended that this be set to 30 seconds.",
     "Seconds"},
    {// parseStr, tag, name
     "lead_config", TZMR_LEAD_CONFIG, "Lead Configuration",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 6, 1, 10, 255,
     // tooltip
     "The value in this field specifies the locations of the electrodes for embedding in each "
     "SCP-ECG file. Based on this lead configuration, the device will emit an error if the "
     "incorrect patient cable is attached.",
     ""},
    {// parseStr, tag, name
     "report_onset_pre_time", REPORT_PRE_TIME, "Onset Pre Event Reporting Duration",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 30, 1, 10, 255,
     // tooltip
     "The value in this field controls how many FILES of ECG data prior to the onset event "
     "are transmitted along with the event report for events that have associated ECG data. "
     "NOTE: setting this value to 0 will result in the first file of data transmitted being "
     "the file that contains the event.",
     "Files", MCT_ONLY},
    {// parseStr, tag, name
     "report_onset_post_time", REPORT_POST_TIME, "Onset Post Event Reporting Duration",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 30, 1, 10, 255,
     // tooltip
     "The value in this field controls how many FILES of ECG data following the onset event "
     "are transmitted along with the event report for the events that have associated ECG "
     "data. NOTE: settings this value to 0 will result in the last file transmitted being the "
     "file that contains the event.",
     "Files", MCT_ONLY},
    {// parseStr, tag, name
     "study_length", TZMR_STUDY_LENGTH, "Study Length",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 300000, 4, 10, 255,
     // tooltip
     "The value in this field controls how many SCP-ECG data files will be recorded before "
     "the device will stop recording and shut down. If the SCP-ECG Data File Length is set to "
     "60, this corresponds to the number of minutes of ECG data recorded. NOTE: when this "
     "value is set to the maximum, the recording will be stopped by the SD card being full "
     "rather than by this setting.",
     "Files"},
    {// parseStr, tag, name
     "bit_resolution", TZMR_BIT_RESOLUTION, "SCP File Bit Resolution",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 8, 16, 1, 10, 255,
     // tooltip
     "The value in this field controls how many bits the Aera CT uses to store each "
     "measurement to the SCP-ECG Record. Setting this to a value of less than 12 will result "
     "in reduced ADC resolution (counts/mV). Setting this to a value of less than 10 will "
     "reduce the Aera CT’s dynamic range from +/-6 mV AC to +/-3 mV AC.",
     "Bits per Sample"},
    {// parseStr, tag, name
     "huffman_encoding", TZMR_HUFFMAN_ENCODING, "Enable Huffman Encoding",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the TZMR will encode the ECG data in the SCP-ECG file using "
     "Huffman tables. This setting is always recommended to be used.",
     "Boolean"},
    {// parseStr, tag, name
     "digital_hp_filter", DIGITAL_HP_FILTER, "Digital High Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will apply a 2nd order high pass "
     "digital filter to the ECG data before being passed to the analysis algorithm or being "
     "stored to the SD Card. The cutoff frequency in Hz will be the value of this setting "
     "divided by 100, with an effective setting range of 0.01 Hz to 1.00 Hz. The resolution "
     "of the cutoff frequency in this implementation is 1/10000 of the sample rate.",
     "Hz/100"},
    {// parseStr, tag, name
     "start_pin_code", START_OK_CODE, "Recording Confirmation Pin Code",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 9999, 2, 10, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will wait to start a recording until "
     "this value is entered by the patient or until a Start Recording action is downloaded "
     "from the server.",
     ""},
    {// parseStr, tag, name
     "digital_lp_filter", DIGITAL_LP_FILTER, "Digital Low Pass Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 19, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will apply a 2nd order low pass "
     "digital filter to the ECG data before being passed to the analysis algorithm or being "
     "stored to the SD Card. The resolution of the cutoff frequency in this implementation is "
     "1/100 of the sample rate.",
     "Hz"},
    {// parseStr, tag, name
     "digital_notch_filter", DIGITAL_NOTCH_FILTER, "Digital Notch Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 19, 255,
     // tooltip
     "When this is set to a non-zero value, the Aera CT will apply a 2nd order notch digital "
     "filter to the ECG data before being passed to the analysis algorithm or being stored to "
     "the SD Card. The resolution of the cutoff frequency in this implementation is 1/100 of "
     "the sample rate. Therefore, for proper 60 Hz rejection, a sample rate of 240 Hz or 250 "
     "Hz is recommended.",
     "Hz"},
    {// parseStr, tag, name
     "fill_square_waves", TZMR_FILL_SQUARE_WAVES, "Fill with Square Waves",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 7, 1, 19, 255,
     // tooltip
     "When bits 0, 1, and/or 2 are set, the associated channel data will be replaced with a 2 "
     "Hz (+/- 1 mV) calibration signal when the lead is disabled due to poor patient "
     "impedance.",
     "Bitmask"},
    {// parseStr, tag, name
     "record_continuous", TZMR_RECORD_CONTINUOUS, "Record Continuously",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 19, 255,
     // tooltip
     "When this is enabled, the Aera CT will continue to store data even when all 3 leads are "
     "in the disabled state due to poor patient impedance. The device will still display "
     "“Recording Suspended” to the user to prompt them to reconnect the electrodes. The "
     "device may still experience recording interruptions due to error recovery operations.",
     "Boolean"},
    {// parseStr, tag, name
     "report_offset_pre_time", TZMR_REPORT_OFFSET_PRE_TIME, "Offset Pre-Event Reporting Duration",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 30, 1, 20, 255,
     // tooltip
     "The value in this field controls how many FILES of ECG data prior to the offset event "
     "are transmitted along with the event report for events that have associated ECG data. "
     "NOTE: setting this value to 0 will result in the first file of data transmitted being "
     "the file that contains the event.",
     "Files", MCT_ONLY},
    {// parseStr, tag, name
     "report_offset_post_time", TZMR_REPORT_OFFSET_POST_TIME,
     "Offset Post-Event Reporting Duration",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 30, 1, 20, 255,
     // tooltip
     "The value in this field controls how many FILES of ECG data following the offset event "
     "are transmitted along with the event report for the events that have associated ECG "
     "data. NOTE: settings this value to 0 will result in the last file transmitted being the "
     "file that contains the event.",
     "Files", MCT_ONLY},
    {// parseStr, tag, name
     "report_bpm_period", TZMR_REPORT_BPM_PERIOD, "Report BPM Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3600, 2, 21, 255,
     // tooltip
     "The value in this field controls how often the BPM entries are recorded in the interval "
     "files. Setting this to 0 results in the old behavior where BPM values are recorded once "
     "per file. In all cases, the location of the BPM markers is the END of the period over "
     "which the BPM values were calculated.",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "ttm_mode", TZMR_TTM_MODE, "TTM Mode",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 15, 1, 21, 255,
     // tooltip
     "The value of this setting controls the features used in TTM transmissions, including "
     "FSK, event marks, 3X mode, and the flag byte of the header.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "ttm_top_byte", TZMR_TTM_TOP_BYTE, "TTM Top Byte",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 255, 1, 21, 255,
     // tooltip
     "The value in this setting is used for the MSB of the Serial Number packet in the FSK "
     "header for TTM mode. The 0x79 default is the value used by the King of Hearts "
     "transmitters.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "ttm_failover_period", 122, "TTM Failover Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 1440, 2, 26, 255,
     // tooltip
     "The value in this setting controls how many minutes after a valid TTM event is queued "
     "before the device switches to TTM mode. Any successful event transmission will reset "
     "the timer.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "ttm_enable", TZMR_TTM_FAILOVER_PERIOD, "TTM Enable",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 7, 1, 21, 255,
     // tooltip
     "When set to a non-zero value, the device will use TTM to send events if either wireless "
     "is disabled or the device has been unable to upload a file to the server for 4 hours or "
     "more. The device will only transmit the TTM channels enabled by this setting – meaning "
     "that if set to 1 it will only TTM channel 1, and if set to 4 it will only TTM channel "
     "3. TTM Failover only affects arrhythmia onset events and patient activated events. The "
     "device will store up to 6 TTM-eligible events (with the last reserved for a patient "
     "event), and will leave the remainder of the events in the wireless queue.",
     "Bitmask", MCT_ONLY},
    {// parseStr, tag, name
     "ttm_queue_length", TZMR_TTM_QUEUE_LENGTH, "TTM Queue Length",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 12, 1, 27, 255,
     // tooltip
     "The value in this setting controls the maximum number of TTM events that can be queued. "
     "The last slot is reserved for a patient event, so setting this to 1 will result in only "
     "1 patient event ever being queued. Setting it to 2 will result in either 1 auto trigger "
     "event and 1 patient event or 2 patient events being queued.",
     "Events", MCT_ONLY},
    {// parseStr, tag, name
     "display_signal_bars", TZMR_DISPLAY_SIGNAL_BARS, "Display Signal Bars",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 30, 255,
     // tooltip
     "When this is enabled, the device displays cellular signal strength on the screen.", "Boolean",
     MCT_ONLY},
    {// parseStr, tag, name
     "study_complete_sleep", STUDY_COMPLETE_SLEEP, "Study Complete Alert Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 900, 2, 10, 255,
     // tooltip
     "When this is set to a non-zero value the device will shut off the screen once the "
     "recording is complete. If set to a value greater than 1, the device will wake up every "
     "N seconds and notify the patient that the recording is complete.",
     "Seconds"},
    {// parseStr, tag, name
     "nag_on_lead_off", NAG_ON_LEAD_OFF, "Lead Off Alert Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 3600, 2, 21, 255,
     // tooltip
     "When this is set to a non-zero value, the device will vibrate every <value> seconds "
     "when a lead is disconnected to alert the patient to reconnect the lead.",
     "Seconds"},
    {// parseStr, tag, name
     "max_streaming_delay", TZMR_MAX_STREAMING_DELAY, "Max Streaming Delay",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 240, 1, 21, 255,
     // tooltip
     "When this is set to a non-zero value, the device will “skip ahead” in Real Time "
     "Monitoring mode whenever it falls behind the recording in file uploads by at least "
     "<value> files. If set to zero, the device will start at file 0.scp and upload each "
     "sequential file, no matter how long it takes.",
     "Files", MCT_ONLY},
    {// parseStr, tag, name
     "trim_events", TZMR_TRIM_EVENTS, "Trim Events",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will shorten event reports occurring immediately "
     "before or after an interruption of the recording to prevent transmission of "
     "discontinuous segments. We do NOT recommend disabling this setting.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "wireless_enable", TZMR_WIRELESS_ENABLE, "Wireless Enable",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will use its cellular modem to upload data to a "
     "server. When this is disabled, the Aera CT will function as a combination Holter "
     "recorder and TTM event monitor. Arrhythmia analysis and event generation will still "
     "occur if enabled.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "auto_answer", TZMR_AUTO_ANSWER, "Call Auto Answer",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will answer incoming phone calls in speaker mode "
     "following several rings. When this is disabled, the Aera CT will ignore ALL incoming "
     "phone calls. NOTE: Starting in Version 2.1 of the firmware, voice calls will no longer "
     "be supported. This setting will still be accepted, but will have no effect on operation "
     "of the device.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "sync_time", TZMR_SYNC_TIME, "Synchronize to Network Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will regularly update its internal clock to match the "
     "current network time. When disabled, the Aera CT can still be manually synced with the "
     "network from the setup menus. If this setting is disabled and the device is powered "
     "down for an extended period of time, the clock will revert to 2001/01/01 and remain "
     "there.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "streaming_mode", BULK_UPLOAD, "Real Time Monitoring",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 500, 2, 10, 255,
     // tooltip
     "When this is non-zero, the Aera CT will queue each SCP file for upload to the server. "
     "If it is set to 1, this will be done as soon as it is complete, starting with the first "
     "file in the record. For greater values, the files will be uploaded in blocks with a "
     "streaming event file. Depending on the value of the Max Streaming Delay setting, the "
     "device may or may not “skip ahead” to catch up on file uploads. If this is enabled "
     "while Demonstration Mode is enabled, the device will boot into Patient Demonstration "
     "Mode. In this mode, the device will always start on the PIN code screen (code = 1234) "
     "after which a recording will start and patient events may be created.",
     "Files", MCT_ONLY},
    {// parseStr, tag, name
     "transmit_interval_reports", TRANSMIT_INTERVAL_REPORTS, "Transmit Hourly Interval Reports",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will upload an interval report file each hour "
     "containing device status information as well as all of the outputs of the automated ECG "
     "analysis, regardless of the event reporting settings.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "silent_mode", TZMR_SILENT_MODE, "Silent Mode",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, audible feedback from the Aera CT will be disabled. This does not "
     "affect TTM operation.",
     "Boolean"},
    {// parseStr, tag, name
     "vib_clicks", TZMR_VIB_CLICKS, "VIB Clicks",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will vibrate in response to menu interactions.", "Boolean"},
    {// parseStr, tag, name
     "screen_sleep_time", SCREEN_SLEEP_TIME, "Screen Sleep Delay",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 5, 180, 1, 10, 255,
     // tooltip
     "The value in this field sets the number of seconds that the device status screen will "
     "display before the device goes back to sleep. This also controls how long each page of "
     "the Digital Patient Diary is displayed if the user does not interact with it.",
     "Seconds"},
    {// parseStr, tag, name
     "demo_mode", DEMO_MODE, "Demonstration Mode",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will simulate server downloads from the GUI "
     "(preserving the contents of the current config file), to facilitate use demonstrations. "
     "If this is enabled while Real Time Monitoring is enabled, the device will boot into "
     "Patient Demonstration Mode. In this mode, the device will always start on the PIN code "
     "screen (code = 1234) after which a recording will start and patient events may be "
     "created. The bottom of the screen will display either \'Demonstration Mode\' or "
     "\'Patient Demonstration\' when this setting is enabled.",
     "Boolean"},
    {// parseStr, tag, name
     "sms_enable", TZMR_SMS_ENABLE, "Enable SMS Actions Download",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT attempt to download an actions file whenever it "
     "receives an SMS message containing the serial number of the device in the body of the "
     "message (this could be the body or subject line or sender for an email-to-SMS "
     "service).",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "voice_enable", TZMR_VOICE_ENABLE, "Enable Voice Calls",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will receive voice calls using the method specified "
     "by the “Call Auto Answer” setting. NOTE: Starting in Version 2.1 of the firmware, voice "
     "calls will no longer be supported. This setting will still be accepted, but will have "
     "no effect on operation of the device.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "check_actions_interval", TZMR_CHECK_ACTIONS_INTERVAL, "Check Actions Interval",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 240, 1, 10, 255,
     // tooltip
     "The value in this field specifies how many minutes the Aera CT waits between attempting "
     "to download the actions file from the server. Setting this value to 0 will result "
     "automatic downloading being disabled.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "study_end_date_time", TZMR_STUDY_END_DATE_TIME, "Study Ending Date-Time",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_DATETIME, UNUSED_FIELD, UNUSED_FIELD, 5, 10, 255,
     // tooltip
     "The value in this field tells the Aera CT to stop recording ECG data on a given date at "
     "a given hour. Setting this value to 0 or omitting this tag disables this feature. "
     "Format is as follows: *Byte 0-1*: Year (LSB first) *Byte 2*: Month *Byte 3*: Day "
     "*Byte 4*: Hour",
     ""},
    {// parseStr, tag, name
     "symptom_diary_entries", SYMPTOM_DIARY_COUNT, "Symptom Diary Entries",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 10, 1, 10, 255,
     // tooltip
     "When this value is set to a value greater than 0, the symptom diary will be enabled "
     "using the first <value> symptom entries settings.",
     "Entries"},
    {// parseStr, tag, name
     "activity_diary_entries", ACTIVITY_DIARY_COUNT, "Activity Level Diary Entries",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 10, 1, 10, 255,
     // tooltip
     "When this value is set to a value greater than 0, the activity level diary will be "
     "enabled using the first <value> activity level entries settings.",
     "Entries"},
    {// parseStr, tag, name
     "stagger_interval_uploads", TZMR_STAGGER_INTERVAL_UPLOADS, "Stagger Interval Uploads",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 60, 1, 10, 255,
     // tooltip
     "When this value is set to a value greater than 0, the Aera CT will add a pseudo-random "
     "delay from 0 to value minutes before uploading the interval file to prevent excessive "
     "loading of the server.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "long_event_threshold", TZMR_LONG_EVENT_THRESHOLD, "Long Event Threshold",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 255, 1, 19, 255,
     // tooltip
     "The value in this field sets the minimum length in minutes for an event to be queued in "
     "the lower-priority event buffer.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "long_event_voltage", TZMR_LONG_EVENT_VOLTAGE, "Long Event Voltage",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 4200, 2, 19, 255,
     // tooltip
     "The value in this field sets the minimum battery voltage at which the device will "
     "transmit events queued in the lower-priority buffer.",
     "Millivolts", MCT_ONLY},
    {// parseStr, tag, name
     "modem_voltage", TZMR_MODEM_VOLTAGE, "Modem Shutoff Voltage",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 3350, 3800, 2, 19, 255,
     // tooltip
     "The value in this field sets the minimum battery voltage at which the device will open "
     "wireless connections.",
     "Millivolts", MCT_ONLY},
    {// parseStr, tag, name
     "modem_temperature", TZMR_MODEM_TEMPERATURE, "Modem Shutoff Temperature",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 100, 1, 19, 255,
     // tooltip
     "When this value is set to a value greater than 0, the Aera CT will shut off the modem "
     "for 10 minutes at a time if the modem’s internal temperature exceeds the value of this "
     "setting in Celsius.",
     "Celsius", MCT_ONLY},
    {// parseStr, tag, name
     "remember_last_diary", TZMR_REMEMBER_LAST_DIARY, "Remember Last Diary",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 24, 255,
     // tooltip
     "When this is enabled, the Aera CT will pre-select the most recently entered diary "
     "entries when recording new patient events.",
     "Boolean"},
    {// parseStr, tag, name
     "symptom_entry_1", DIARY_SYMPTOM_1, "Symptom Entry 1",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 1.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_2", DIARY_SYMPTOM_2, "Symptom Entry 2",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 2.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_3", DIARY_SYMPTOM_3, "Symptom Entry 3",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 3.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_4", DIARY_SYMPTOM_4, "Symptom Entry 4",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 4.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_5", DIARY_SYMPTOM_5, "Symptom Entry 5",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 5.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_6", DIARY_SYMPTOM_6, "Symptom Entry 6",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 6.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_7", DIARY_SYMPTOM_7, "Symptom Entry 7",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 7.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_8", DIARY_SYMPTOM_8, "Symptom Entry 8",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 8.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_9", DIARY_SYMPTOM_9, "Symptom Entry 9",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 9.",
     ""},
    {// parseStr, tag, name
     "symptom_entry_10", DIARY_SYMPTOM_10, "Symptom Entry 10",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient symptom diary when the Symptom Diary "
     "Entries value is set to at least 10.",
     ""},
    {// parseStr, tag, name
     "activity_entry_1", DIARY_ACTIVITY_1, "Activity Level Entry 1",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 1.",
     ""},
    {// parseStr, tag, name
     "activity_entry_2", DIARY_ACTIVITY_2, "Activity Level Entry 2",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 2.",
     ""},
    {// parseStr, tag, name
     "activity_entry_3", DIARY_ACTIVITY_3, "Activity Level Entry 3",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 3.",
     ""},
    {// parseStr, tag, name
     "activity_entry_4", DIARY_ACTIVITY_4, "Activity Level Entry 4",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 4.",
     ""},
    {// parseStr, tag, name
     "activity_entry_5", DIARY_ACTIVITY_5, "Activity Level Entry 5",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 5.",
     ""},
    {// parseStr, tag, name
     "activity_entry_6", DIARY_ACTIVITY_6, "Activity Level Entry 6",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 6.",
     ""},
    {// parseStr, tag, name
     "activity_entry_7", DIARY_ACTIVITY_7, "Activity Level Entry 7",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 7.",
     ""},
    {// parseStr, tag, name
     "activity_entry_8", DIARY_ACTIVITY_8, "Activity Level Entry 8",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 8.",
     ""},
    {// parseStr, tag, name
     "activity_entry_9", DIARY_ACTIVITY_9, "Activity Level Entry 9",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 9.",
     ""},
    {// parseStr, tag, name
     "activity_entry_10", DIARY_ACTIVITY_10, "Activity Level Entry 10",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "This string is used in the patient activity level diary when the Activity Level Diary "
     "Entries value is set to at least 10.",
     ""},
    {// parseStr, tag, name
     "patient_id", PATIENT_ID, "Patient ID",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 39, 10, 255,
     // tooltip
     "Up to 39 ASCII characters identifying the patient to the host software system."
     "This ID will be embedded in each SCP-ECG data file. When this value is changed"
     "from a non-NULL value to any other value, the device will abort the current recording"
     "in the same manner as if the Early-Out procedure were performed.",
     ""},
    {// parseStr, tag, name
     "event_static_payload", TZMR_EVENT_STATIC_PAYLOAD, "Event Static Payload",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 40, 19, 255,
     // tooltip
     "Up to 40 bytes to be appended to the end of every event file send to the server. The "
     "content and meaning of this data is up to the server implementation.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "incoming_number", TZMR_INCOMING_NUMBER, "Incoming Call Filter",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 16, 10, 255,
     // tooltip
     "When this is set to anything other than a NULL string, the Aera CT will ONLY accept "
     "incoming calls with numbers that match this string. NOTE: Starting in Version 2.1 of "
     "the firmware, voice calls will no longer be supported. This setting will still be "
     "accepted, but will have no effect on operation of the device.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "center_name", TOP_BANNER, "Center Name",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "The name of the Institution that is receiving and processing the data from the Aera CT. "
     "This name will be displayed at the bottom of the screen of the device during the "
     "recording. If the Center Name and Center Phone Number settings are both set to NULL, "
     "the device will display the serial number and firmware version on the bottom of the "
     "screen.",
     ""},
    {// parseStr, tag, name
     "center_number", BOTTOM_BANNER, "Center Number",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 28, 10, 255,
     // tooltip
     "The phone number of the Institution that is receiving and processing the data from the "
     "Aera CT. This number will be displayed on the screen of the device. If the Center Name "
     "and Center Phone Number settings are both set to NULL, the device will display the "
     "serial number and firmware version on the bottom of the screen.",
     ""},
    {// parseStr, tag, name
     "server_address", SERVER_ADDRESS, "Server Address",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 255, 10, 255,
     // tooltip
     "The URL of the server that the Aera CT will connect to, omitting the preceding "
     "“http://”. (Such as aera.my_domain_name.com)",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "server_user_name", TLS_IDENTITY, "Server Username",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 255, 10, 255,
     // tooltip
     "The username that the Aera CT will use when attempting to connect to the server. NOTE: "
     "leaving this blank will tell the Aera CT to use its own serial number as the user name. "
     "The device will always attempt to connect using HTTP simple authentication.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "server_password", TLS_PSK, "Server Password",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_STRING, UNUSED_FIELD, UNUSED_FIELD, 255, 10, 255,
     // tooltip
     "The password that the Aera CT will use when attempting to connect to the server. NOTE: "
     "leaving this blank will tell the Aera CT to use its own serial number as the password. "
     "The device will always attempt to connect using HTTP simple authentication.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "use_http", TZMR_USE_HTTP, "Use HTTP",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will use HTTP to communicate with the central server, "
     "rather than FTP. NOTE: Starting in Version 2.1 of the firmware, FTP connections will no "
     "longer be supported. This setting will still be accepted, but will have no effect on "
     "operation of the device.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "check_for_clean_server", TZMR_CHECK_FOR_CLEAN_SERVER, "Check for Clean Server",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will download an actions file and check for a “New "
     "Study” action before downloading a settings file using the patient setup GUI.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "verify_server_changes", TZMR_VERIFY_SERVER_CHANGES, "Server Setting Verification",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will attempt to verify the validity of any changes in "
     "the server settings (by uploading the server settings changed event) prior to saving "
     "them to internal memory.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "wait_for_http_response", TZMR_WAIT_FOR_HTTP_RESPONSE, "Wait for HTTP Response",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "When this is enabled, the Aera CT will wait for a “200 OK” response from the server "
     "before considering a file upload as successful. We do NOT recommend ever disabling this "
     "setting.",
     "Boolean", MCT_ONLY},
    {// parseStr, tag, name
     "redownload_delay", TZMR_REDOWNLOAD_DELAY, "Re-download Delay",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 240, 1, 10, 255,
     // tooltip
     "When this value is set to a value greater than 0, the Aera CT will automatically "
     "attempt to download a second settings file when it detects that the server settings "
     "have changed. When this value is set to 0, the Aera CT will return to the download page "
     "after the server settings have changed (as it will have deleted the current settings in "
     "response to the change).",
     "Seconds", MCT_ONLY},
    {// parseStr, tag, name
     "connection_timeout", CONNECTION_TIMEOUT, "Connection Timeout",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 240, 1, 20, 255,
     // tooltip
     "When this value is set to a value greater than 0, the Aera CT will abort connections "
     "that last longer than this value and attempt to restart them.",
     "Minutes", MCT_ONLY},
    {// parseStr, tag, name
     "aes_key", AES_KEY, "AES Shared Key",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_HEX, 0, 255, 255, 10, 255,
     // tooltip
     "The security key that the Aera CT will use to encrypt or decrypt all future files "
     "transferred between the Aera CT and the central server. Must be 16 Bytes long to enable "
     "AES encryption and decryption.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "aes_iv", AES_IV, "AES Initialization Vector",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_HEX, 0, 255, 255, 10, 255,
     // tooltip
     "The initialization vector that the Aera CT will use to encrypt or decrypt all future "
     "files transferred between the Aera CT and the central server. Must be 16 Bytes long. "
     "Must be 16 Bytes long to enable AES encryption and decryption.",
     "", MCT_ONLY},
    {// parseStr, tag, name
     "error_retries", ERROR_RETRIES, "Error Retry Count",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 250, 1, 17, 255,
     // tooltip
     "This value defines how many times the Aera CT will attempt to automatically recover "
     "from an error, within a given time-period, before reporting the error to the user.",
     "Retries"},
    {// parseStr, tag, name
     "error_period", ERROR_PERIOD, "Error Retry Period",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 1, 1440, 2, 17, 255,
     // tooltip
     "This value defines the period within which the Error Retry Count must not be exceeded.",
     "Minutes"},
    {// parseStr, tag, name
     "menu_language", BAD_TAG, "Menu Language",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_NUMBER, 0, 0, 1, 10, 255,
     // tooltip
     "This setting is used to change the assets used on the GUI to support other languages.", ""},
    {// parseStr, tag, name
     "bad_tag", ZYMED_COMPAT, "Bad Tag",
     // type, minValue, maxValue, maxLength, minFW, maxFW
     SETTING_TYPE_BOOLEAN, 0, 1, 1, 10, 255,
     // tooltip
     "This setting is used for testing error handling of invalid settings tags.", "Boolean"},
  };
}
// clang-format on

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__);
//
//-----------------------------------------------------------------------------

//=============================================================================
SettingList::SettingList(string deviceId)
{
  if (string::npos != deviceId.find("H3R")) {
    settingVector = newH3RSettings();
  }
  else if (string::npos != deviceId.find("HPR")) {
    settingVector = newHPRSettings();
  }
  else if (string::npos != deviceId.find("TZMR")) {
    settingVector = newTZMRSettings();
  }
  else {
    settingVector = vector<_setting>{};
  }
}

//=============================================================================
bool SettingList::isValid()
{
  bool retVal = 0;
  if (0 < settingVector.size()) {
    retVal = 1;
  }
  return retVal;
}

//=============================================================================
_setting* SettingList::locateSetting(string argument, uint8_t firmwareVersion, ostream* errStream)
{
  size_t i;
  bool matchFound = 0;
  int invalidFWMatch = -1;
  for (i = 0; i < settingVector.size(); i++) {
    if (argument.find(settingVector[i].getParseStr()) != string::npos) {
      // Only break if it's a valid setting
      if (!(settingVector[i].checkFirmwareVersion(firmwareVersion, 0, errStream))) {
        matchFound = 1;
        break;
      }
      else {
        invalidFWMatch = i;
      }
    }
  }
  if (matchFound) {
    return &settingVector[i];
  }
  else {
    if (0 <= invalidFWMatch) {
      // We already know it's invalid, but now we want to print why
      settingVector[invalidFWMatch].checkFirmwareVersion(firmwareVersion, 1, errStream);
    }
    else {
      *errStream << "ERROR! Unidentified setting: " << argument << endl;
    }
  }
  return NULL;
}

//=============================================================================
_setting* SettingList::locateSetting(
  settingTags tag, uint8_t firmwareVersion, bool isTzmr, bool isTzmrCompat, ostream* errStream)
{
  size_t i;
  bool matchFound = 0;
  int invalidFWMatch = -1;
  for (i = 0; i < settingVector.size(); i++) {
    if (tag == settingVector[i].getTag()) {
      // Only break if it's a valid setting
      if (!(settingVector[i].checkFirmwareVersion(firmwareVersion, 0, errStream))) {
        if (isTzmrCompat) {
          if (TLS_PSK == tag) {
            if (string::npos != settingVector[i].getParseStr().find("tls_psk")) {
              continue;
            }
          }
          else if (TLS_IDENTITY == tag) {
            if (string::npos != settingVector[i].getParseStr().find("tls_identity")) {
              continue;
            }
          }
        }
        else if (!isTzmr) {
          if (TLS_PSK == tag) {
            if (string::npos != settingVector[i].getParseStr().find("server_password")) {
              continue;
            }
          }
          else if (TLS_IDENTITY == tag) {
            if (string::npos != settingVector[i].getParseStr().find("server_user_name")) {
              continue;
            }
          }
        }
        matchFound = 1;
        break;
      }
      else {
        invalidFWMatch = i;
      }
    }
  }

  if (matchFound) {
    return &settingVector[i];
  }
  else if (tag != TERM_TAG) {
    if (0 <= invalidFWMatch) {
      // We already know it's invalid, but now we want to print why
      settingVector[invalidFWMatch].checkFirmwareVersion(firmwareVersion, 1, errStream);
    }
    else {
      *errStream << "ERROR! Unidentified tag: " << tag << endl;
    }
  }
  return NULL;
}

//=============================================================================
int SettingList::processJsonObject(
  rapidjson::Value& settings, uint8_t firmwareVersion, bool checkInputs, ostream* errStream)
{
  int retVal = 0;
  size_t i;
  for (i = 0; i < settingVector.size(); i++) {
    string jsonStr = settingVector[i].getJsonStr();

    if (settings.HasMember(jsonStr.c_str()) && settings[jsonStr.c_str()].IsObject()) {
      // Only break if it's a valid setting
      if (!(settingVector[i].checkFirmwareVersion(firmwareVersion, 0, errStream))) {
        rapidjson::Value settingObject = settings[jsonStr.c_str()].GetObject();

        if (settingObject.HasMember(JSON_SETTINGS_DATA_LABEL)) {
          retVal = settingVector[i].setJson(settingObject, checkInputs, errStream);
          if (retVal) return retVal;
        }
      }
    }
  }
  return retVal;
}

//=============================================================================
int SettingList::writeBinary(
  vector<uint8_t>& buffer, uint8_t firmwareVersion, bool badAlignment, int* retVal,
  ostream* errStream)
{
  int i = 0;
  size_t j;

  for (j = 0; j < settingVector.size(); j++) {
    if (settingVector[j].isSet()) {
      if (settingVector[j].checkFirmwareVersion(firmwareVersion, 1, errStream)) {
        *retVal = -1;
      }
      else {
        i += settingVector[j].write(buffer, badAlignment, firmwareVersion);
      }
    }
  }

  return i;
}

//=============================================================================
int SettingList::getSettingMetaJson(ostream* outStream)
{
  size_t i;
  bool commaNeeded = 0;
  *outStream << "[";
  for (i = 0; i < settingVector.size(); i++) {
    if (commaNeeded) {
      *outStream << ",";
    }
    *outStream << settingVector[i].getMetaJson();
    commaNeeded = 1;
  }
  *outStream << "]";

  return 0;
}
