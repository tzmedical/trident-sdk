/******************************************************************************
 *       Copyright (c) 2021, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "binary.h"

using namespace std;

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
void binary_append(vector<uint8_t> &buffer, uint64_t var, uint32_t length)
{
  uint32_t j;
  for (j = 0; j < length; j++) {
    buffer.push_back(uint8_t{(uint8_t)(var & 0xff)});
    var = var >> 8;
  }
}

//==============================================================================
void binary_append(vector<uint8_t> &buffer, string &var, uint32_t length)
{
  uint32_t j;
  for (j = 0; j < length; j++) {
    if (j < var.length()) {
      buffer.push_back(var[j]);
    }
    else {
      buffer.push_back('\0');
    }
  }
}

//==============================================================================
void binary_overwrite(vector<uint8_t> &buffer, size_t pos, uint64_t var, uint32_t length)
{
  uint32_t j;
  for (j = 0; j < length; j++) {
    buffer[pos + j] = (uint8_t)(var & 0xff);
    var = var >> 8;
  }
}

//==============================================================================
uint8_t binary_read8(uint8_t *pData, uint32_t *offset)
{
  uint8_t *pC = &pData[*offset];
  *offset += 1;
  return *pC;
}

//==============================================================================
uint16_t binary_read16(uint8_t *pData, uint32_t *offset)
{
  uint8_t *pC = &pData[*offset];
  *offset += 2;
  return (uint16_t)pC[0] + (uint16_t)pC[1] * 256;
}

//==============================================================================
uint32_t binary_read32(uint8_t *pData, uint32_t *offset)
{
  uint8_t *pC = &pData[*offset];
  *offset += 4;
  return (((uint32_t)pC[3] * 256 + (uint32_t)pC[2]) * 256 + (uint32_t)pC[1]) * 256
       + (uint32_t)pC[0];
}
