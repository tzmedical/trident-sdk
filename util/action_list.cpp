/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "action_list.h"
#include <initializer_list>

#include "util/json.h"

using namespace std;
using namespace rapidjson;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//=============================================================================
vector<_action> newH3RActions()
{
  return {
    {
      "server_ready",                 // parseStr
      ACTIONS_ENABLE_NEW_STUDY,       // tag
      "Allow new study (TZMR-only)",  // name
      {{
        "enable",             // data field name
        ACTION_TYPE_BOOLEAN,  // data field type
        0,                    // data field length
      }},
      11,   // minFW
      255,  // maxFW
      "",   // tooltip
      1,    // max per file
      MCT_ONLY,
    },
    {
      "scp_request",              // parseStr
      ACTIONS_REQUEST_SCP_BLOCK,  // tag
      "SCP Data Request",         // name
      {{
         "sequence_number",                                         // data field name
         ACTION_TYPE_NUMBER,                                        // data field type
         4,                                                         // data field length
         "The sequence number of the first SCP file to transmit.",  // data field description
       },
       {
         "file_count",        // data field name
         ACTION_TYPE_NUMBER,  // data field type
         1,                   // data field length
         "The max number of SCP files to transmit – will be truncated to the maximum possible "
         "if fewer SCP files exist than requested.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to transmit a block of SCP files. The first SCP file "
      "to transmit is provided as a parameter, allowing this action to request a random block "
      "of SCP files from anywhere in the record.",  // tooltip
      255,                                          // max per file
      MCT_ONLY,
    },
    {
      "retransmit_scp",             // parseStr
      ACTIONS_RETRANSMIT_SCP_FILE,  // tag
      "Retransmit SCP File",        // name
      {{
         "sequence_number",                                     // data field name
         ACTION_TYPE_NUMBER,                                    // data field type
         4,                                                     // data field length
         "The sequence number of the SCP file to retransmit.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,                                                                         // minFW
      255,                                                                        // maxFW
      "This action causes the MCT device to retransmit the indicated SCP file.",  // tooltip
      9,                                                                          // max per file
      MCT_ONLY,
    },
    {
      "recent_scp",               // parseStr
      ACTIONS_RECENT_SCP,         // tag
      "Recent SCP Data Request",  // name
      {{
         "seconds",                                   // data field name
         ACTION_TYPE_NUMBER,                          // data field type
         2,                                           // data field length
         "The amount of time requested in seconds.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action cause the MCT to transmit a block of the most recent SCP files. The first SCP "
      "file to transmit is based on the difference between the most recently completed SCP file "
      "and the amount of time requested .This allows this action to send a number of files with "
      "the last transmission being the most recent SCP file.",  // tooltip
      1,                                                        // max per file
      MCT_ONLY,
    },
    {
      "tzr_request",             // parseStr
      ACTIONS_REQUEST_TZR_FILE,  // tag
      "Interval Data Request",   // name
      {{
         "year",                                 // data field name
         ACTION_TYPE_NUMBER,                     // data field type
         2,                                      // data field length
         "The year of the TZR file requested.",  // data field description
       },
       {
         "month",                                 // data field name
         ACTION_TYPE_NUMBER,                      // data field type
         1,                                       // data field length
         "The month of the TZR file requested.",  // data field description
       },
       {
         "day",                                 // data field name
         ACTION_TYPE_NUMBER,                    // data field type
         1,                                     // data field length
         "The day of the TZR file requested.",  // data field description
       },
       {
         "hour",                                            // data field name
         ACTION_TYPE_NUMBER,                                // data field type
         1,                                                 // data field length
         "The time (in hours) of the TZR file requested.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to transmit the TZR file specified by the parameters if "
      "it exists.",  // tooltip
      255,           // max per file
      MCT_ONLY,
    },
    {
      "update_settings",        // parseStr
      ACTIONS_UPDATE_SETTINGS,  // tag
      "Update Settings",        // name
      {{
        "enable",             // data field name
        ACTION_TYPE_BOOLEAN,  // data field type
        0,                    // data field length
      }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to download the current settings from the server.",  // tooltip
      1,  // max per file
      MCT_ONLY,
    },
    {
      "send_message",           // parseStr
      ACTIONS_DISPLAY_MESSAGE,  // tag
      "Display Message",        // name
      {{
        "",                                                   // data field name
        ACTION_TYPE_STRING,                                   // data field type
        255,                                                  // data field length
        "The message to display as a string of characters.",  // data field description
      }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to display a given message on the screen of the device "
      "in an SMS like format.",  // tooltip
      1,                         // max per file
    },
    {
      "error_log",          // parseStr
      ACTIONS_ERROR_LOG,    // tag
      "Request Error Log",  // name
      {{
        "enable",             // data field name
        ACTION_TYPE_BOOLEAN,  // data field type
        0,                    // data field length
      }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to upload the onboard error log to the server for "
      "analysis.",  // tooltip
      1,            // max per file
      MCT_ONLY,
    },
    {
      // parseStr, tag, name
      "firmware_update",  // parseStr
      ACTIONS_FIRMWARE,   // tag
      "Update Firmware",  // name
      {{
        "",                                             // data field name
        ACTION_TYPE_STRING,                             // data field type
        255,                                            // data field length
        "The URI of the firmware update to download.",  // data field description
      }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to download a new firmware version from the provided "
      "URI.",  // tooltip
      1,       // max per file
      MCT_ONLY,
    },
    {
      "graphics_update",  // parseStr
      ACTIONS_GRAPHICS,   // tag
      "Update Graphics",  // name
      {{
        "",                                             // data field name
        ACTION_TYPE_STRING,                             // data field type
        255,                                            // data field length
        "The URI of the graphics update to download.",  // data field description
      }},
      14,   // minFW
      255,  // maxFW
      "This action causes the MCT device to download a new graphics version from the provided "
      "URI.",  // tooltip
      1,       // max per file
      MCT_ONLY,
    },
    {
      "end_study",        // parseStr
      ACTIONS_END_STUDY,  // tag
      "End Study",        // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0xa5,                             // data field value
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to stop any recordings and end an ongoing study.",  // tooltip
      1,  // max per file
      MCT_ONLY,
    },
    {
      "start_study",        // parseStr
      ACTIONS_START_STUDY,  // tag
      "Start Study",        // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x5a,                             // data field value
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to start a new study and begin recording with the current "
      " settings.",  // tooltip
      1,             // max per file
      MCT_ONLY,
    },
    {
      "swap_device",           // parseStr
      ACTIONS_SWAP_DEVICE,     // tag
      "Swap Device in Study",  // name
      {{
         "hours_offset",                                            // data field name
         ACTION_TYPE_NUMBER,                                        // data field type
         2,                                                         // data field length
         "The hours of recording already completed in the study.",  // data field description
       },
       {
         "sequence_number",                                   // data field name
         ACTION_TYPE_NUMBER,                                  // data field type
         4,                                                   // data field length
         "The sequence number to begin with for SCP files.",  // data field description
       },
       {
         "patient_id",                                                    // data field name
         ACTION_TYPE_STRING,                                              // data field type
         255,                                                             // data field length
         "The patient ID to be used for the continuation of the study.",  // data field description
       }},
      23,   // minFW
      255,  // maxFW
      "This action instructs the device to start recording at a specific SCP sequence number and "
      "with a specific patient ID value. This facilitates replacement of a device mid-study, "
      "allowing the data to be more easily stitched together.",  // tooltip
      1,                                                         // max per file
    },
    {
      "format_sd",        // parseStr
      ACTIONS_FORMAT_SD,  // tag
      "Format SD Card",   // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x42,                             // data field value
       },
       {
         "patient_id",        // data field name
         ACTION_TYPE_STRING,  // data field type
         40,                  // data field length
         "Optional format handshake containing the expected Patient ID. Leave these blank to "
         "execute the format action regardless of current device Patient ID.",  // data field
                                                                                // description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action causes the device to self-format the internal microSD card, wiping any data "
      "from the card in the process.",  // tooltip
      1,                                // max per file
    },
    {
      "shutdown",         // parseStr
      ACTIONS_SHUTDOWN,   // tag
      "Shutdown Device",  // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x24,                             // data field value
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      13,                                                 // minFW
      255,                                                // maxFW
      "This action causes the MCT device to shut down.",  // tooltip
      1,                                                  // max per file
      MCT_ONLY,
    },
  };
}

//=============================================================================
vector<_action> newHPRActions()
{
  return {
    {
      "format_sd",        // parseStr
      ACTIONS_FORMAT_SD,  // tag
      "Format SD Card",   // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x42,                             // data field value
       },
       {
         "patient_id",        // data field name
         ACTION_TYPE_STRING,  // data field type
         40,                  // data field length
         "Optional format handshake containing the expected Patient ID. Leave these blank to "
         "execute the format action regardless of current device Patient ID.",  // data field
                                                                                // description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      10,   // minFW
      255,  // maxFW
      "This action causes the device to self-format the internal microSD card, wiping any data "
      "from the card in the process.",  // tooltip
      1,                                // max per file
    },
  };
}

//=============================================================================
// NOTE: This is a copy of newH3RActions with the firmware version checking
// disabled to support "hacked"-together compat-mode servers.
//=============================================================================
vector<_action> newTZMRActions()
{
  return {
    {
      "server_ready",                 // parseStr
      ACTIONS_ENABLE_NEW_STUDY,       // tag
      "Allow new study (TZMR-only)",  // name
      {{
        "enable",             // data field name
        ACTION_TYPE_BOOLEAN,  // data field type
        0,                    // data field length
      }},
      0,    // minFW
      255,  // maxFW
      "",   // tooltip
      1,    // max per file
      MCT_ONLY,
    },
    {
      "scp_request",              // parseStr
      ACTIONS_REQUEST_SCP_BLOCK,  // tag
      "SCP Data Request",         // name
      {{
         "sequence_number",                                         // data field name
         ACTION_TYPE_NUMBER,                                        // data field type
         4,                                                         // data field length
         "The sequence number of the first SCP file to transmit.",  // data field description
       },
       {
         "file_count",        // data field name
         ACTION_TYPE_NUMBER,  // data field type
         1,                   // data field length
         "The max number of SCP files to transmit – will be truncated to the maximum possible "
         "if fewer SCP files exist than requested.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to transmit a block of SCP files. The first SCP file "
      "to transmit is provided as a parameter, allowing this action to request a random block "
      "of SCP files from anywhere in the record.",  // tooltip
      255,                                          // max per file
      MCT_ONLY,
    },
    {
      "retransmit_scp",             // parseStr
      ACTIONS_RETRANSMIT_SCP_FILE,  // tag
      "Retransmit SCP File",        // name
      {{
         "sequence_number",                                     // data field name
         ACTION_TYPE_NUMBER,                                    // data field type
         4,                                                     // data field length
         "The sequence number of the SCP file to retransmit.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,                                                                          // minFW
      255,                                                                        // maxFW
      "This action causes the MCT device to retransmit the indicated SCP file.",  // tooltip
      9,                                                                          // max per file
      MCT_ONLY,
    },
    {
      "recent_scp",               // parseStr
      ACTIONS_RECENT_SCP,         // tag
      "Recent SCP Data Request",  // name
      {{
         "seconds",                                   // data field name
         ACTION_TYPE_NUMBER,                          // data field type
         2,                                           // data field length
         "The amount of time requested in seconds.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,    // minFW
      255,  // maxFW
      "This action cause the MCT to transmit a block of the most recent SCP files. The first SCP "
      "file to transmit is based on the difference between the most recently completed SCP file "
      "and the amount of time requested .This allows this action to send a number of files with "
      "the last transmission being the most recent SCP file.",  // tooltip
      1,                                                        // max per file
      MCT_ONLY,
    },
    {
      "tzr_request",             // parseStr
      ACTIONS_REQUEST_TZR_FILE,  // tag
      "Interval Data Request",   // name
      {{
         "year",                                 // data field name
         ACTION_TYPE_NUMBER,                     // data field type
         2,                                      // data field length
         "The year of the TZR file requested.",  // data field description
       },
       {
         "month",                                 // data field name
         ACTION_TYPE_NUMBER,                      // data field type
         1,                                       // data field length
         "The month of the TZR file requested.",  // data field description
       },
       {
         "day",                                 // data field name
         ACTION_TYPE_NUMBER,                    // data field type
         1,                                     // data field length
         "The day of the TZR file requested.",  // data field description
       },
       {
         "hour",                                            // data field name
         ACTION_TYPE_NUMBER,                                // data field type
         1,                                                 // data field length
         "The time (in hours) of the TZR file requested.",  // data field description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      11,   // minFW
      255,  // maxFW
      "This action causes the MCT device to transmit the TZR file specified by the parameters if "
      "it exists.",  // tooltip
      255,           // max per file
      MCT_ONLY,
    },
    {
      "update_settings",        // parseStr
      ACTIONS_UPDATE_SETTINGS,  // tag
      "Update Settings",        // name
      {{
        "enable",             // data field name
        ACTION_TYPE_BOOLEAN,  // data field type
        0,                    // data field length
      }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to download the current settings from the server.",  // tooltip
      1,  // max per file
      MCT_ONLY,
    },
    {
      "send_message",           // parseStr
      ACTIONS_DISPLAY_MESSAGE,  // tag
      "Display Message",        // name
      {{
        "",                                                   // data field name
        ACTION_TYPE_STRING,                                   // data field type
        255,                                                  // data field length
        "The message to display as a string of characters.",  // data field description
      }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to display a given message on the screen of the device "
      "in an SMS like format.",  // tooltip
      1,                         // max per file
    },
    {
      "error_log",          // parseStr
      ACTIONS_ERROR_LOG,    // tag
      "Request Error Log",  // name
      {{
        "enable",             // data field name
        ACTION_TYPE_BOOLEAN,  // data field type
        0,                    // data field length
      }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to upload the onboard error log to the server for "
      "analysis.",  // tooltip
      1,            // max per file
      MCT_ONLY,
    },
    {
      // parseStr, tag, name
      "firmware_update",  // parseStr
      ACTIONS_FIRMWARE,   // tag
      "Update Firmware",  // name
      {{
        "",                                             // data field name
        ACTION_TYPE_STRING,                             // data field type
        255,                                            // data field length
        "The URI of the firmware update to download.",  // data field description
      }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to download a new firmware version from the provided "
      "URI.",  // tooltip
      1,       // max per file
      MCT_ONLY,
    },
    {
      "graphics_update",  // parseStr
      ACTIONS_GRAPHICS,   // tag
      "Update Graphics",  // name
      {{
        "",                                             // data field name
        ACTION_TYPE_STRING,                             // data field type
        255,                                            // data field length
        "The URI of the graphics update to download.",  // data field description
      }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to download a new graphics version from the provided "
      "URI.",  // tooltip
      1,       // max per file
      MCT_ONLY,
    },
    {
      "end_study",        // parseStr
      ACTIONS_END_STUDY,  // tag
      "End Study",        // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0xa5,                             // data field value
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to stop any recordings and end an ongoing study.",  // tooltip
      1,  // max per file
      MCT_ONLY,
    },
    {
      "start_study",        // parseStr
      ACTIONS_START_STUDY,  // tag
      "Start Study",        // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x5a,                             // data field value
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,    // minFW
      255,  // maxFW
      "This action causes the MCT device to start a new study and begin recording with the current "
      " settings.",  // tooltip
      1,             // max per file
      MCT_ONLY,
    },
    {
      "swap_device",           // parseStr
      ACTIONS_SWAP_DEVICE,     // tag
      "Swap Device in Study",  // name
      {{
         "hours_offset",                                            // data field name
         ACTION_TYPE_NUMBER,                                        // data field type
         2,                                                         // data field length
         "The hours of recording already completed in the study.",  // data field description
       },
       {
         "sequence_number",                                   // data field name
         ACTION_TYPE_NUMBER,                                  // data field type
         4,                                                   // data field length
         "The sequence number to begin with for SCP files.",  // data field description
       },
       {
         "patient_id",                                                    // data field name
         ACTION_TYPE_STRING,                                              // data field type
         255,                                                             // data field length
         "The patient ID to be used for the continuation of the study.",  // data field description
       }},
      0,    // minFW
      255,  // maxFW
      "This action instructs the device to start recording at a specific SCP sequence number and "
      "with a specific patient ID value. This facilitates replacement of a device mid-study, "
      "allowing the data to be more easily stitched together.",  // tooltip
      1,                                                         // max per file
    },
    {
      "format_sd",        // parseStr
      ACTIONS_FORMAT_SD,  // tag
      "Format SD Card",   // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x42,                             // data field value
       },
       {
         "patient_id",        // data field name
         ACTION_TYPE_STRING,  // data field type
         40,                  // data field length
         "Optional format handshake containing the expected Patient ID. Leave these blank to "
         "execute the format action regardless of current device Patient ID.",  // data field
                                                                                // description
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,    // minFW
      255,  // maxFW
      "This action causes the device to self-format the internal microSD card, wiping any data "
      "from the card in the process.",  // tooltip
      1,                                // max per file
    },
    {
      "shutdown",         // parseStr
      ACTIONS_SHUTDOWN,   // tag
      "Shutdown Device",  // name
      {{
         "validation",                     // data field name
         ACTION_TYPE_VALIDATION_CONSTANT,  // data field type
         1,                                // data field length
         0x24,                             // data field value
       },
       {
         "enable",             // data field name
         ACTION_TYPE_BOOLEAN,  // data field type
         0,                    // data field length
       }},
      0,                                                  // minFW
      255,                                                // maxFW
      "This action causes the MCT device to shut down.",  // tooltip
      1,                                                  // max per file
      MCT_ONLY,
    },
  };
}

//=============================================================================
_action* ActionList::locateAction(actionTags tag, uint8_t firmwareVersion, ostream* errStream)
{
  size_t i;
  bool matchFound = 0;
  int invalidFWMatch = -1;
  for (i = 0; i < actionVector.size(); i++) {
    if (tag == actionVector[i].getTag()) {
      // Only break if it's a valid action
      if (!(actionVector[i].checkFirmwareVersion(firmwareVersion, 0, errStream))) {
        matchFound = 1;
        break;
      }
      else {
        invalidFWMatch = i;
      }
    }
  }

  if (matchFound) {
    return &actionVector[i];
  }
  else if (tag != ACTIONS_TERMINATOR) {
    if (0 <= invalidFWMatch) {
      // We already know it's invalid, but now we want to print why
      actionVector[invalidFWMatch].checkFirmwareVersion(firmwareVersion, 1, errStream);
    }
    else {
      *errStream << "ERROR! Unidentified tag: " << tag << endl;
    }
  }
  return NULL;
}

//=============================================================================
_action* ActionList::locateNewAction(string argument, uint8_t firmwareVersion, ostream* errStream)
{
  size_t i;
  bool matchFound = 0;
  int invalidFWMatch = -1;
  for (i = 0; i < actionVector.size(); i++) {
    if (argument.find(actionVector[i].getParseStr()) != string::npos) {
      // Only break if it's a valid action
      if (!(actionVector[i].checkFirmwareVersion(firmwareVersion, 0, errStream))) {
        matchFound = 1;
        break;
      }
      else {
        invalidFWMatch = i;
      }
    }
  }
  if (matchFound) {
    return &actionVector[i];
  }
  else {
    if (0 <= invalidFWMatch) {
      // We already know it's invalid, but now we want to print why
      actionVector[invalidFWMatch].checkFirmwareVersion(firmwareVersion, 1, errStream);
    }
    else {
      *errStream << "ERROR! Unidentified action: " << argument << endl;
    }
  }
  return NULL;
}

//=============================================================================
_action* ActionList::locateJsonAction(string type, uint8_t firmwareVersion, ostream* errStream)
{
  size_t i;
  bool matchFound = 0;
  int invalidFWMatch = -1;
  for (i = 0; i < actionVector.size(); i++) {
    if (type.find(actionVector[i].getJsonStr()) != string::npos) {
      // Only break if it's a valid action
      if (!(actionVector[i].checkFirmwareVersion(firmwareVersion, 0, errStream))) {
        matchFound = 1;
        break;
      }
      else {
        invalidFWMatch = i;
      }
    }
  }
  if (matchFound) {
    return &actionVector[i];
  }
  else {
    if (0 <= invalidFWMatch) {
      // We already know it's invalid, but now we want to print why
      actionVector[invalidFWMatch].checkFirmwareVersion(firmwareVersion, 1, errStream);
    }
    else {
      *errStream << "ERROR! Unidentified action: " << type << endl;
    }
  }
  return NULL;
}

//=============================================================================
_action* ActionList::locateFileAction(string argument, uint8_t firmwareVersion, ostream* errStream)
{
  size_t i;
  bool matchFound = 0;
  for (i = 0; i < fileActionVector.size(); i++) {
    string actionParseStr = fileActionVector[i].getParseStr();
    size_t found = argument.find(actionParseStr);
    if (found != string::npos) {
      int index = fileActionVector[i].parseTextIndex(argument, errStream);
      if (index == fileActionVector[i].getActionIndex()) {
        // Only break if it's a valid action
        matchFound = 1;
        break;
      }
    }
  }
  if (matchFound) {
    return &fileActionVector[i];
  }
  return NULL;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__);
//
//-----------------------------------------------------------------------------

//=============================================================================
ActionList::ActionList(string deviceId)
{
  if (string::npos != deviceId.find("H3R")) {
    actionVector = newH3RActions();
  }
  else if (string::npos != deviceId.find("HPR")) {
    actionVector = newHPRActions();
  }
  else if (string::npos != deviceId.find("TZMR")) {
    actionVector = newTZMRActions();
  }
  else {
    actionVector = vector<_action>{};
  }
  fileActionVector = vector<_action>{};
}

//=============================================================================
int ActionList::parseBinary(
  uint8_t* pRead, uint32_t& j, uint8_t firmwareVersion, ostream* outStream, ostream* errStream)
{
  int retVal = 0;
  actionTags actionTag = (actionTags)pRead[j++];  // Action Tag
  uint32_t dataLength = pRead[j++];               // Data Length

  _action* pAction = locateAction(actionTag, firmwareVersion, errStream);

  if (pAction != NULL) {
    retVal = pAction->parseBinary(&pRead[j], dataLength, errStream);
    if (retVal) return retVal;
    fileActionVector.push_back(*pAction);
    if (!fileActionVector.back().isSet()) return -1;
  }
  else if (actionTag != ACTIONS_TERMINATOR) {
    return -1;
  }

  j += dataLength;

  if (pRead[j++] != '\0') {
    *errStream << "Missing NULL detected! (tag: " << actionTag << ")" << endl;
    return -1;
  }

  return retVal;
}

//=============================================================================
int ActionList::writeActionText(ostream* outStream, ostream* errStream)
{
  int retVal = 0;
  size_t i;
  for (i = 0; i < fileActionVector.size(); i++) {
    fileActionVector[i].writeActionText(*outStream);
    *outStream << endl;
  }

  return retVal;
}

//=============================================================================
int ActionList::writeActionJson(ostream* outStream, ostream* errStream)
{
  int retVal = 0;

  size_t i;
  for (i = 0; i < fileActionVector.size(); i++) {
    fileActionVector[i].writeActionJson(*outStream);
    if (i < (fileActionVector.size() - 1)) {
      *outStream << "," << endl;
    }
    else {
      *outStream << endl;
    }
  }

  return retVal;
}

//=============================================================================
int ActionList::parseText(string argument, uint8_t firmwareVersion, ostream* errStream)
{
  int retVal = 0;

  _action* pAction = locateFileAction(argument, firmwareVersion, errStream);

  if (NULL == pAction) {
    pAction = locateNewAction(argument, firmwareVersion, errStream);

    if (NULL == pAction) {
      return -1;
    }

    fileActionVector.push_back(*pAction);
    pAction = &fileActionVector.back();
  }

  if (pAction->parseText(argument, errStream)) {
    return -1;
  }

  return retVal;
}

//=============================================================================
bool ActionList::isValid()
{
  bool retVal = 0;
  if (0 < actionVector.size()) {
    retVal = 1;
  }
  return retVal;
}

//=============================================================================
int ActionList::processJsonArray(
  rapidjson::Value& actions, uint8_t firmwareVersion, ostream* errStream)
{
  int retVal = 0;
  for (SizeType arrayIndex = 0; arrayIndex < actions.Size(); arrayIndex++) {
    if (!actions[arrayIndex].HasMember(JSON_ACTIONS_TYPE_LABEL)) {
      *errStream << "ERROR! Missing `" << JSON_ACTIONS_TYPE_LABEL;
      *errStream << "` property for action" << endl;
      return -1;
    }
    string actionType = actions[arrayIndex][JSON_ACTIONS_TYPE_LABEL].GetString();

    _action* pAction = locateJsonAction(actionType, firmwareVersion, errStream);
    if (NULL == pAction) {
      return -1;
    }
    if (!actions[arrayIndex].HasMember(JSON_ACTIONS_DATA_LABEL)) {
      *errStream << "ERROR! Missing `" << JSON_ACTIONS_DATA_LABEL;
      *errStream << "` property for action: " << pAction->getJsonStr() << endl;
      return -1;
    }

    fileActionVector.push_back(*pAction);
    pAction = &fileActionVector.back();

    if (actions[arrayIndex][JSON_ACTIONS_DATA_LABEL].IsObject()) {
      Value actionData = actions[arrayIndex][JSON_ACTIONS_DATA_LABEL].GetObject();
      if (pAction->parseJson(actionData, errStream)) {
        return -1;
      }
    }
    else if (actions[arrayIndex][JSON_ACTIONS_DATA_LABEL].IsString()) {
      string actionData = actions[arrayIndex][JSON_ACTIONS_DATA_LABEL].GetString();
      if (pAction->parseJson(actionData, errStream)) {
        return -1;
      }
    }
  }

  return retVal;
}

//=============================================================================
int ActionList::writeActionBinary(
  vector<uint8_t>& buffer, uint8_t firmwareVersion, bool badAlignment, int* retVal,
  ostream* errStream)
{
  int i = 0;
  size_t j;

  for (j = 0; j < fileActionVector.size(); j++) {
    if (fileActionVector[j].isSet()) {
      if (fileActionVector[j].checkFirmwareVersion(firmwareVersion, 1, errStream)) {
        *retVal = -1;
      }
      else {
        i +=
          fileActionVector[j].writeActionBinary(buffer, badAlignment, firmwareVersion, errStream);
      }
    }
  }

  return i;
}

//=============================================================================
// int ActionList::getActionMetaJson(ostream* outStream)
// {
//   size_t i;
//   bool commaNeeded = 0;
//   *outStream << "[";
//   for (i = 0; i < actionVector.size(); i++) {
//     if (commaNeeded) {
//       *outStream << ",";
//     }
//     *outStream << actionVector[i].writeMetaJson();
//     commaNeeded = 1;
//   }
//   *outStream << "]";

//   return 0;
// }