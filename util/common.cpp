/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <sstream>
#include "common.h"
#include "json.h"

using namespace std;

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int common_get_json_file_type(const rapidjson::Value &v, string &file_type, ostream *errStream)
{
  if (v.HasMember(JSON_FILE_FORMAT_LABEL) && v[JSON_FILE_FORMAT_LABEL].IsString()) {
    file_type.assign(v[JSON_FILE_FORMAT_LABEL].GetString());
  }
  else {
    *errStream << "Missing File Type Identifier" << endl;
    return -1;
  }
  return 0;
}

//==============================================================================
int common_get_json_device_type(const rapidjson::Value &v, string &device_id, ostream *errStream)
{
  if (v.HasMember(JSON_DEVICE_TYPE_LABEL) && v[JSON_DEVICE_TYPE_LABEL].IsString()) {
    device_id.assign(v[JSON_DEVICE_TYPE_LABEL].GetString());
  }
  if (!device_id.length() || (device_id.length() > 5)) {
    *errStream << "Invalid Device ID: " << device_id << endl;
    return -1;
  }
  if (string::npos == device_id.find("TZMR")) {
    device_id.resize(5, ' ');
  }
  return 0;
}

//==============================================================================
int common_get_json_device_serial(
  const rapidjson::Value &v, string &device_serial, ostream *errStream)
{
  if (v.HasMember(JSON_DEVICE_SERIAL_LABEL) && v[JSON_DEVICE_SERIAL_LABEL].IsString()) {
    device_serial.assign(v[JSON_DEVICE_SERIAL_LABEL].GetString());
  }
  else {
    *errStream << "Missing Device Serial" << endl;
    return -1;
  }
  if (device_serial.length() > DEVICE_SERIAL_MAX_LENGTH - 1) {
    *errStream << "Invalid Device Serial (too long): " << device_serial << endl;
    return -1;
  }
  return 0;
}

//==============================================================================
int common_get_json_firmware_version(
  const rapidjson::Value &v, uint16_t &firmwareVersion, ostream *errStream)
{
  double firmwareDouble;
  if (v.HasMember(JSON_FIRMWARE_VERSION_LABEL) && v[JSON_FIRMWARE_VERSION_LABEL].IsNumber()) {
    firmwareDouble = v[JSON_FIRMWARE_VERSION_LABEL].GetDouble();
    firmwareVersion = 10 * firmwareDouble;
  }
  else {
    *errStream << "Missing Device Firmware Version" << endl;
    return -1;
  }
  return 0;
}

//==============================================================================
int common_get_json_patient_id(const rapidjson::Value &v, string &patient_id, ostream *errStream)
{
  if (v.HasMember(JSON_PATIENT_ID_LABEL) && v[JSON_PATIENT_ID_LABEL].IsString()) {
    patient_id.assign(v[JSON_PATIENT_ID_LABEL].GetString());
  }
  else {
    *errStream << "Missing Patient ID" << endl;
    return -1;
  }
  if (patient_id.length() > PATIENT_ID_MAX_LENGTH - 1) {
    *errStream << "Invalid Patient ID (too long): " << patient_id << endl;
    return -1;
  }
  return 0;
}

//==============================================================================
int common_get_json_file_id(const rapidjson::Value &v, uint32_t &fileId, ostream *errStream)
{
  if (v.HasMember(JSON_FILE_ID_LABEL) && v[JSON_FILE_ID_LABEL].IsInt()) {
    fileId = v[JSON_FILE_ID_LABEL].GetInt();
  }
  else {
    *errStream << "Missing File ID Field" << endl;
    return -1;
  }
  return 0;
}

//==============================================================================
int common_get_json_date_time(
  const rapidjson::Value &v, timestamp &parsedTime, const char *label, ostream *errStream)
{
  if (v.HasMember(label) && v[label].IsString()) {
    int retVal = parsedTime.parseText(v[label].GetString());
    if (retVal) {
      *errStream << "Invalid format for " << label << " field: " << v[label].GetString() << endl;
      return retVal;
    }
  }
  else {
    *errStream << "Missing " << label << " Field" << endl;
    return -1;
  }
  return 0;
}
