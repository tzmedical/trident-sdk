#ifndef UTIL_COMMON_H
#define UTIL_COMMON_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <string>
#include "rapidjson/document.h"

#include "util/timestamp.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

// File Identifiers
#define TZG_ID_STRING      "TZACC"
#define ACTIONS_ID_STRING  "TZACT"
#define EVENT_ID_STRING    "TZEVT"
#define TZR_ID_STRING      "TZINT"
#define SCP_ID_STRING      "SCPECG"
#define SETTINGS_ID_STRING "TZSET"

// Field Lengths
#define DEVICE_SERIAL_MAX_LENGTH   11
#define PATIENT_ID_MAX_LENGTH      40
#define SEQUENCE_STRING_MAX_LENGTH 9

// File size limits (Bytes)
#define MIN_TZG_FILE_SIZE (256)
#define MAX_TZG_FILE_SIZE (256 * 1024)
#define MAX_TZA_FILE_SIZE (4 * 1024)
#define MIN_TZR_FILE_SIZE (40)
#define MAX_TZR_FILE_SIZE (10 * 1024 * 1024)
#define MIN_SCP_FILE_SIZE (256)
#define MAX_SCP_FILE_SIZE (256 * 1024)

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

int common_get_json_file_type(const rapidjson::Value &v, string &file_type, ostream *errStream);

int common_get_json_device_type(const rapidjson::Value &v, string &device_id, ostream *errStream);

int common_get_json_device_serial(
  const rapidjson::Value &v, string &device_serial, ostream *errStream);

int common_get_json_firmware_version(
  const rapidjson::Value &v, uint16_t &firmwareVersion, ostream *errStream);

int common_get_json_patient_id(const rapidjson::Value &v, string &patient_id, ostream *errStream);

int common_get_json_file_id(const rapidjson::Value &v, uint32_t &fileId, ostream *errStream);

int common_get_json_date_time(
  const rapidjson::Value &v, timestamp &parsedTime, const char *label, ostream *errStream);

#endif