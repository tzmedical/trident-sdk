/* eslint-env node */

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

module.exports = {
  // ==============================================================================
  splitToFilteredArray: function splitToFilteredArray(inputText) {
    const inputArray = inputText.toString().replace(/\r\n/g, '\n').split('\n').sort()
    const trimmedArray = inputArray.map((s) => String.prototype.trim.apply(s))
    const filteredInput = trimmedArray.filter((value) => {
      if (value.startsWith('#')) return false
      if (value.length === 0) return false
      return true
    })
    return filteredInput
  },

  // ==============================================================================
  removeFilePaths: function removeFilePaths(inputArray) {
    const regex = /^.*[0-9_]+\.(tze|tzr) /
    return inputArray.map((value) => {
      return value.replace(regex, '')
    })
  },
}
