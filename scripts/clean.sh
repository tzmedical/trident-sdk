#!/bin/bash

cd actions/actions_generator/ && rm *.exe && cd ../..

cd actions/actions_parser/ && rm *.exe && cd ../..

cd backup/backup_generator/ && rm *.exe && cd ../..

cd backup/backup_parser/ && rm *.exe && cd ../..

cd battery/battery_info_generator/ && rm *.exe && cd ../..

cd battery/battery_info_parser/ && rm *.exe && cd ../..

cd error/error_to_json/ && rm *.exe && cd ../..

cd events/event_finder/ && rm *.exe && cd ../..

cd events/event_generator/ && rm *.exe && cd ../..

cd events/event_parser/ && rm *.exe && cd ../..

cd events/event_to_json/ && rm *.exe && cd ../..

cd interval/interval_finder/ && rm *.exe && cd ../..

cd interval/interval_generator/ && rm *.exe && cd ../..

cd interval/interval_parser/ && rm *.exe && cd ../..

cd interval/interval_to_json/ && rm *.exe && cd ../..

cd settings/settings_generator/ && rm *.exe && cd ../..

cd settings/settings_parser/ && rm *.exe && cd ../..

cd scp/one_ecg_splitter/ && rm *.exe && cd ../..

cd scp/scp_generator/ && rm *.exe && cd ../..

cd scp/scp_parser/ && rm *.exe && cd ../..

cd scp/scp_stripper/ && rm *.exe && cd ../..

cd scp/scp_to_json/ && rm *.exe && cd ../..
