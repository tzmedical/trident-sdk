#!/bin/bash

cd actions/actions_generator/ && make && cd ../..

cd actions/actions_parser/ && make && cd ../..

cd accel/accel_to_json/ && make && cd ../..

cd backup/backup_generator/ && make && cd ../..

cd backup/backup_parser/ && make && cd ../..

cd battery/battery_info_generator/ && make && cd ../..

cd battery/battery_info_parser/ && make && cd ../..

cd events/event_finder/ && make && cd ../..

cd events/event_generator/ && make && cd ../..

cd events/event_parser/ && make && cd ../..

cd events/event_to_json/ && make && cd ../..

cd interval/interval_finder/ && make && cd ../..

cd interval/interval_generator/ && make && cd ../..

cd interval/interval_parser/ && make && cd ../..

cd interval/interval_to_json/ && make && cd ../..

cd settings/settings_generator/ && make && cd ../..

cd settings/settings_parser/ && make && cd ../..

cd settings/settings_to_json/ && make && cd ../..

cd settings/settings_from_json/ && make && cd ../..

cd scp/one_ecg_splitter/ && make && cd ../..

cd scp/scp_generator/ && make && cd ../..

cd scp/scp_parser/ && make && cd ../..

cd scp/scp_stripper/ && make && cd ../..

cd scp/scp_to_json/ && make && cd ../..
