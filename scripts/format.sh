# /bin/bash

set -e

echo "Formatting ALL *.h and *.cpp files"
find . -iregex '^.*\.\(cpp\|h\)$' -not -path "./node_modules/*" | xargs clang-format -i

echo "Formatting ALL *.js files"
npx prettier --write "**/*.js"