#!/bin/bash

nyc --reporter lcov --reporter text mocha --exit './*.spec.js' --timeout 5000 -R mocha-multi-reporters --reporter-options configFile=scripts/test.json