#ifndef INTERVAL_PARSER_H
#define INTERVAL_PARSER_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <sstream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to convert an interval file into a text file
//! \return 0 if successful
int IntervalToText(
  std::istream* inStream, std::map<std::string, std::string>& extraArgs, std::ostream* outStream,
  std::ostream* errStream, std::ostream* logStream);

#endif