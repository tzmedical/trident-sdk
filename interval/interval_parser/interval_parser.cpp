/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       interval_parser.cpp
 *          - This program parses a *.tzr interval report file from <inFile> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./interval_parser -i sample.tzr > output.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include "util/common.h"
#include "util/crc.h"
#include "util/events.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define MAX_SUMMARIZE_QRS_PERIOD_SECONDS 900  // 900 is the max value for the setting

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

typedef struct {
  timestamp parsedTime;
  uint64_t ms_epoch;
  int qrs_count;
  int pvc_count;
  int milliseconds;
  int time_zone;
  int sequence;
  int sample;
} hr_state_t;

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static void reset_hr_state(hr_state_t *p_A, hr_state_t *p_B)
{
  *p_A = {};
  *p_B = {};
}

//==============================================================================
// This function calculates the average HR using two state values, printing the
// result to cout as a "virtual" event.
//==============================================================================
static void calculate_mean_hr(
  hr_state_t *p_A, hr_state_t *p_B, uint64_t *p_resume_ms_epoch, uint64_t *p_lead_off_ms_epoch,
  eventTags searchNumber, ostream *outStream, ostream *errStream)
{
  // Check for a "resume" interruption - we don't know delta_t for the first
  // event after a resume, so we need to skip the calculation for one step
  if (
    *p_resume_ms_epoch && (p_A->ms_epoch < *p_resume_ms_epoch)
    && (p_B->ms_epoch > *p_resume_ms_epoch)) {
    // Since B is after the resume, just shift and clear
    *p_A = *p_B;
    *p_B = {};
    *p_resume_ms_epoch = 0;
  }
  // Check for a "lead off" interruption - we don't know delta_t for the first
  // event after a lead off, so we need to skip the calculation for one step
  else if (
    *p_lead_off_ms_epoch && (p_A->ms_epoch < *p_lead_off_ms_epoch)
    && (p_B->ms_epoch > *p_lead_off_ms_epoch)) {
    // Since B is after the resume, just shift and clear
    *p_A = *p_B;
    *p_B = {};
    *p_lead_off_ms_epoch = 0;
  }
  else {
    // Clear out the interruption epochs if we have completely passed them.
    if (*p_resume_ms_epoch && (p_A->ms_epoch > *p_resume_ms_epoch)) *p_resume_ms_epoch = 0;
    if (*p_lead_off_ms_epoch && (p_A->ms_epoch > *p_lead_off_ms_epoch)) *p_lead_off_ms_epoch = 0;

    // Beats are binned by type, so we need to sum for accurate values.
    int total_beats = p_B->qrs_count + p_B->pvc_count;
    int64_t delta_t_ms = ((int64_t)p_B->ms_epoch) - ((int64_t)p_A->ms_epoch);
    float delta_t_seconds = (delta_t_ms) / 1000.0;

    // Make sure the delta value is valid
    if ((delta_t_seconds > 0) && (delta_t_seconds < MAX_SUMMARIZE_QRS_PERIOD_SECONDS)) {
      if (total_beats > 0) {
        // Calculate HR as a float for best accuracy
        float hr = (60.0 * total_beats) / delta_t_seconds;

        // Output the value as a "virtual" event
        if ((FIND_ALL == searchNumber) || (TZMR_BPM_AVERAGE_TAG == searchNumber)) {
          *outStream << "[" << setw(6) << setfill('0') << p_B->sequence << ", " << setw(6)
                     << p_B->sample << "] ";
          *outStream << p_B->parsedTime.getText() << " - ";
          *outStream << "Mean HR: " << hr << endl;
        }
      }

      // Shift B into A now that we're dont with A's time value, then clear B
      *p_A = *p_B;
      *p_B = {};
    }
    else {
      // Clear BOTH A and B so we don't output an unexpectedly low HR value
      *p_A = {};
      *p_B = {};
      *errStream << "WARNING: Skipping invalid HR calculation." << endl;
    }
  }
}

//==============================================================================
// This function compares the ms_epoch value for a new event against the current
// state to see if we need to perform any HR calculations. It returns a pointer
// to the appropriate state variable to use to store the current event's state
// information for future use.
//==============================================================================
static hr_state_t *step_hr_state(
  hr_state_t *p_A, hr_state_t *p_B, uint64_t count_epoch, uint64_t *p_resume_ms_epoch,
  uint64_t *p_lead_off_ms_epoch, eventTags searchNumber, ostream *outStream, ostream *errStream)
{
  if ((0 == p_A->ms_epoch) || (count_epoch == p_A->ms_epoch))
    return p_A;
  else {
    if ((0 != p_B->ms_epoch) && (count_epoch != p_B->ms_epoch)) {
      calculate_mean_hr(
        p_A, p_B, p_resume_ms_epoch, p_lead_off_ms_epoch, searchNumber, outStream, errStream);
    }
    if ((0 == p_A->ms_epoch) || (count_epoch == p_A->ms_epoch))
      return p_A;
    else
      return p_B;
  }
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int IntervalToText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  int length;                   // The length read from the file

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  int i;

  uint64_t inputEpoch = 0;
  eventTags searchNumber = FIND_ALL;
  string fileName("");
  bool printHeader = 1;
  int32_t zoneOffset = 0;
  bool skipCrc = 0;

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // Process the extra arguments
  map<string, string>::iterator itr;

  itr = extraArgs.find("epochMs");
  if (itr != extraArgs.end()) {
    char *endptr;
    inputEpoch = strtoull(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("tag");
  if (itr != extraArgs.end()) {
    char *endptr;
    searchNumber = (eventTags)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("printHeader");
  if (itr != extraArgs.end()) {
    char *endptr;
    printHeader = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("zoneOffset");
  if (itr != extraArgs.end()) {
    char *endptr;
    zoneOffset = (int32_t)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("fileName");
  if (itr != extraArgs.end()) {
    fileName = itr->second;
  }

  // Read in the first 16-byte block to see if the file has been encrypted
  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 16);  // Read the first block into RAM

  // Check for the format identifier string
  string str;
  str.assign((const char *)&firstBlock[6]);  // Copy what should be the "TZINT" string
  if (str.compare(0, 6, TZR_ID_STRING)) {
    *errStream << "ERROR: File corrupted! Aborting." << endl;
    return -1;
  }
  else {
    i = 0;
    shortCaster = (uint16_t *)&firstBlock[i];
    fileCrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *)&firstBlock[i];
    length = *intCaster;
    i += 4;

    if (!length && !fileCrcValue) {
      *errStream << "WARNING! Parsing an incomplete file. Skipping CRC check." << endl;
      *errStream << "   (" << extraArgs["fileName"] << ")" << endl;
      length = MAX_TZR_FILE_SIZE;
      skipCrc = 1;
      calculatedCrcValue = 0;
    }

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 16; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[16],
                   length - 16);  // Store the remainder of the file in memory

    length = inStream->gcount() + 16;

    if (length < 2) length = 2;
    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    i = 6;
    string format_id((const char *)&pRead[i]);
    i += 6;
    string deviceID((const char *)&pRead[i]);
    i += 6;
    // Parse out the firmware version
    uint8_t firmware_version = pRead[i];
    i += 1;
    // Parse out the Serial Number String
    string serial_number((const char *)&pRead[i]);
    if (string::npos != deviceID.find("TZMR")) {
      i += 8;
    }
    else if (string::npos != deviceID.find("H3R")) {
      i += 11;
    }
    else if (string::npos != deviceID.find("HPR")) {
      i += 11;
    }
    else {
      *errStream << ": ERROR - Unrecognized Device ID: " << deviceID << endl;
      return -1;
    }
    // Parse out the Patient ID String
    string patient_id((const char *)&pRead[i]);
    i += 40;

    if (printHeader) {
      if (skipCrc) {
        // Ignore the CRC values (debugging ONLY)
        *outStream << "Ignoring file CRC." << endl;
      }
      else {
        *outStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
      }
      *outStream << "Format Identifier String: " << format_id << endl;
      *outStream << "Device Identifier String: " << deviceID << endl;
      *outStream << "Firmware Version: " << firmware_version / 10 << "." << firmware_version % 10
                 << endl;
      *outStream << "Serial Number: " << serial_number << endl;
      *outStream << "Patient ID: " << patient_id << endl;
    }

    // Variables for processing summarize_qrs_detection events
    hr_state_t A, B;
    uint64_t resume_ms_epoch = 0;
    uint64_t lead_off_ms_epoch = 0;
    reset_hr_state(&A, &B);
    if (inputEpoch > 0) A.ms_epoch = inputEpoch;

    int skippedCount = 0;

    while (i < (length - 18)) {
      int j = 0;
      eventTags tag = (eventTags)pRead[i + j];
      j += 1;
      uint32_t sequenceNumber = pRead[i + j] + ((int)pRead[i + j + 1] << 8)
                              + ((int)pRead[i + j + 2] << 16) + ((int)pRead[i + j + 3] << 24);
      j += 4;
      uint32_t sampleCount = pRead[i + j] + ((short)pRead[i + j + 1] << 8);
      j += 2;

      timestamp parsedTime;
      j += parsedTime.parseBinary(&pRead[i + j], zoneOffset, errStream);

      uint32_t dataLength = pRead[i + j];
      j += 1;
      uint32_t data = 0, k, mult = 1;
      for (k = 0; k < dataLength && k < 4; k++) {
        data = data + pRead[i + j + k] * mult;
        mult *= 256;
      }
      j += dataLength;
      uint32_t nullByte = pRead[i + j];
      j += 1;

      if (
        nullByte != 0 || parsedTime.sanityCheck() || (dataLength > 4) || ((i + j) > length)
        || (0 == tag)) {
        // File is out of sync, a value was detected where a NULL should be
        i += 1;
        skippedCount++;
      }
      else {
        i += j;

        if (skippedCount > 0) {
          *errStream << "Warning! Data loss detected. Skipped " << skippedCount
                     << " bytes to recover." << endl;
          skippedCount = 0;
        }

        const uint64_t ms_epoch = parsedTime.getMsEpoch();

        // Output HR events as needed.
        hr_state_t *p_state;
        switch (tag) {
          case START_TAG:
            resume_ms_epoch = ms_epoch;
            break;
          case STOP_TAG:
            // This helps us output the last HR calculation before an
            // interruption in recording
            step_hr_state(
              &A, &B, ms_epoch, &resume_ms_epoch, &lead_off_ms_epoch, searchNumber, outStream,
              errStream);
            break;
          case BREAK_TAG:
            // This helps us output the last HR calculation before an
            // interruption in recording
            step_hr_state(
              &A, &B, ms_epoch, &resume_ms_epoch, &lead_off_ms_epoch, searchNumber, outStream,
              errStream);
            break;
          case RESUME_TAG:
            resume_ms_epoch = ms_epoch;
            break;
          case FULL_TAG:
            // This helps us output the last HR calculation before an
            // interruption in recording
            step_hr_state(
              &A, &B, ms_epoch, &resume_ms_epoch, &lead_off_ms_epoch, searchNumber, outStream,
              errStream);
            break;
          case QRS_COUNT_TAG:
            // Calculate the previous HR value, if needed
            p_state = step_hr_state(
              &A, &B, ms_epoch, &resume_ms_epoch, &lead_off_ms_epoch, searchNumber, outStream,
              errStream);
            // Then store the values for future calculations
            p_state->ms_epoch = ms_epoch;
            p_state->parsedTime = parsedTime;
            p_state->sequence = sequenceNumber;
            p_state->sample = sampleCount;
            p_state->qrs_count = data;
            break;
          case PVC_COUNT_TAG:
            // Calculate the previous HR value, if needed
            p_state = step_hr_state(
              &A, &B, ms_epoch, &resume_ms_epoch, &lead_off_ms_epoch, searchNumber, outStream,
              errStream);
            // Then store the values for future calculations
            p_state->ms_epoch = ms_epoch;
            p_state->parsedTime = parsedTime;
            p_state->sequence = sequenceNumber;
            p_state->sample = sampleCount;
            p_state->pvc_count = data;
            break;
          case LEAD_DISCONNECTED:
            // This helps us output the last HR calculation before a data
            // interruption in ECG data due to leads off
            if (7 == data) {
              lead_off_ms_epoch = ms_epoch;
              step_hr_state(
                &A, &B, ms_epoch, &resume_ms_epoch, &lead_off_ms_epoch, searchNumber, outStream,
                errStream);
            }
            break;
          case TERMINATOR_TAG:  // The final entry should always have a value of 0xff
            *logStream << "Final HR ms_epoch (ms): " << B.ms_epoch << endl;
            break;
          default:
            break;
        }

        if ((FIND_ALL == searchNumber) || (tag == searchNumber)) {
          if (fileName.length()) {
            *outStream << fileName << " ";
          }
          *outStream << "[" << setw(6) << setfill('0') << sequenceNumber << ", " << setw(6)
                     << sampleCount << "] ";
          *outStream << parsedTime.getText() << " - ";

          *outStream << getEventText(deviceID, tag, data, sequenceNumber, parsedTime);
        }
      }
    }
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return -1;
    }
  }

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads an interval file from cin and outputs text on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  string programName(argv[0]);
  string inputName;
  bool displayHelp = true;

  int i;

  if (2 == argc) {
    inputName.assign(argv[1]);
  }
  else {
    for (i = 0; i < argc; i++) {
      if (*argv[i] == '-') switch (*(argv[i] + 1)) {
          case 'i':
            if (++i >= argc) {
              cerr << programName << ": filename must follow -i." << endl;
              return -1;
            }
            inputName.assign(argv[i]);
            displayHelp = 0;
            break;
          case 'e':
            if (++i >= argc) {
              cerr << programName << ": ms_epoch (ms) value must follow -e." << endl;
              return -1;
            }
            extraArgs["epochMs"] = string(argv[i]);
            displayHelp = 0;
            break;
          default:
            cerr << programName << ": Unexpected option: " << *(argv[i] + 1) << endl;
            cerr << "Aborting (3)." << endl;
            return -1;
        }
    }

    // If there were no command line arguments, just display the usage info
    if (displayHelp) {
      cout << "Usage: " << programName << " [options] ..." << endl;
      cout << "Options:" << endl;
      cout << "  -i FILE\t\tRead FILE as input to parse." << endl;
      cout << "  -e EPOCH\t\tUse the ms_epoch (ms) value EPOCH for HR calculations at the "
              "beginning of the file"
           << endl;

      return 1;
    }
  }

  if (inputName.length() > 0) {
    inFile.open(inputName, ios::in | ios::binary);
    input = &inFile;
  }
  else {
    input = &cin;
  }

  return IntervalToText(input, extraArgs, &cout, &cerr, &clog);
}
#endif