/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       interval_finder.cpp
 *          - This program parses all *.tzr interval report file from <inDir> and
 *            outputs a text interpretation of the matching entries to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./interval_finder /sample_dir/ > output.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <stdlib.h>

#include "interval_parser.h"
#include "util/crc.h"
#include "util/events.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#include "util/win_dirent.h"
#else
#include <dirent.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   Feature Defines   ***/
#define DUMP_FILE_HEADERS (0)

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int process_interval_files(
  string *inDir, map<string, string> &extraArgs, std::ostream *outStream, std::ostream *errStream,
  std::ostream *logStream)
{
  DIR *pDir = NULL;
  struct dirent *direntry = NULL;
  struct stat entryStat;
  size_t found;
  int retVal = 0;

  if (inDir->find("/.") == string::npos) {
    pDir = opendir(inDir->c_str());

    if (pDir == NULL) {
      *errStream << "ERROR: Could not open Directory (" << *inDir << ")" << endl;
      return -1;
    }

    while (!retVal && (direntry = readdir(pDir))) {
      if (direntry == NULL) {
        *errStream << "ERROR: Could not read directory entry" << endl;
        continue;
      }

      string fileName;
      fileName.assign(*inDir);
      fileName.append((const char *)direntry->d_name);

      if (stat(fileName.c_str(), &entryStat)) {
        *errStream << "ERROR: Could not get entry stats for entry: " << direntry->d_name << endl;
        continue;
      }

      if (S_ISDIR(entryStat.st_mode)) {
        fileName.append("/");
        retVal = process_interval_files(&fileName, extraArgs, outStream, errStream, logStream);
        if (retVal) {
          closedir(pDir);
          return retVal;
        }
      }
      else {
        found = fileName.find(".tzr", 1);
        if (found != string::npos) {
          fstream inFile(fileName, ios::in | ios::binary);
          istream *input = &inFile;
          extraArgs["fileName"] = fileName;
          int ret = IntervalToText(input, extraArgs, outStream, errStream, logStream);
          if (ret) {
            *errStream << "   (" << fileName << ")" << endl;
          }
#if DUMP_FILE_HEADERS == 0
          extraArgs["printHeader"] = string("0");
#endif
        }
        else {
          found = fileName.find(".TZR", 1);
          if (found != string::npos) {
            fstream inFile(fileName, ios::in | ios::binary);
            istream *input = &inFile;
            extraArgs["fileName"] = fileName;
            int ret = IntervalToText(input, extraArgs, outStream, errStream, logStream);
            if (ret) {
              *errStream << "   (" << fileName << ")" << endl;
            }
#if DUMP_FILE_HEADERS == 0
            extraArgs["printHeader"] = string("0");
#endif
          }
        }
      }
    }

    closedir(pDir);
  }

  return retVal;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int FindInterval(
  std::istream *inStream, std::map<std::string, std::string> &extraArgs, std::ostream *outStream,
  std::ostream *errStream, std::ostream *logStream)
{
  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  string inDir(istreambuf_iterator<char>(*inStream), {});

  if (extraArgs.find("tag") == extraArgs.end()) {
    extraArgs["tag"] = string("0");
  }

  if (extraArgs.find("printHeader") == extraArgs.end()) {
    extraArgs["printHeader"] = string("1");
  }

  return process_interval_files(&inDir, extraArgs, outStream, errStream, logStream);
}

#ifdef USE_FINDER_MAIN
//==============================================================================
//    main()
//
//    This function processes each *.scp in the directory and outputs a
//    corresponding *.txt file with the ECG data for each valid file found.
//==============================================================================
int main(int argc, char *argv[])
{
  string searchNumber = "0";
  string inDir = "./";
  string deviceID("H3R  ");

  if (argc >= 2) {
    inDir.assign(argv[1]);
  }

  if (argc >= 3) {
    searchNumber = argv[2];
  }
  else {
    cerr << endl << "Available TAGs:" << endl << endl;

    cerr << "ALL EVENTS: " << 0 << endl;

    cerr << listEvents(deviceID);
    cerr << endl << endl;

    cerr << "Enter Event TAG to search for: ";
    cin >> searchNumber;
  }

  map<string, string> extraArgs;
  extraArgs["tag"] = string(searchNumber);
  extraArgs["printHeader"] = string("1");

  if (process_interval_files(&inDir, extraArgs, &cout, &cerr, &clog)) {
    cerr << "ERROR: Aborting!" << endl;
    return -1;
  }

  return 0;
}
#endif