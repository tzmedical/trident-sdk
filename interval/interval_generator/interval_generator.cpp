/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       intervalGenerator.cpp
 *          - This program generates an interval file for the H3R based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./interval_generator input.txt > output.tzr
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

#include "events.h"

using namespace std;
using namespace rapidjson;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int IntervalFromJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;
  uint32_t length;  // The length read from the file

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  // Parse a JSON string into DOM.
  IStreamWrapper is(*inStream);
  Document d;

  if (d.ParseStream(is).HasParseError()) {
    *errStream << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at "
               << d.GetErrorOffset() << endl;
    return -1;
  }
  string file_identifier("");
  retVal = common_get_json_file_type(d, file_identifier, errStream);
  if (retVal) {
    return retVal;
  }
  if (file_identifier.find(TZR_ID_STRING) == string::npos) {
    *errStream << "ERROR! Invalid Format ID: " << file_identifier << endl;
    return -1;
  }

  // Sets the serial number embedded in the header of the file
  string device_serial("");
  retVal = common_get_json_device_serial(d, device_serial, errStream);
  if (retVal) {
    return retVal;
  }
  *logStream << "Serial Number: " << device_serial << endl;

  // Sets the firmware version embedded in the header of the file
  uint16_t firmwareVersion = 0;
  retVal = common_get_json_firmware_version(d, firmwareVersion, errStream);
  if (retVal) {
    return retVal;
  }
  *logStream << "Firmware version: " << firmwareVersion << endl;

  // Sets the Patient ID embedded in the header of the file
  string patient_id("");
  retVal = common_get_json_patient_id(d, patient_id, errStream);
  if (retVal) {
    return retVal;
  }
  *logStream << "Patient ID: " << patient_id << endl;

  // Sets the DEVICE_ID field in the header
  string device_id("");
  retVal = common_get_json_device_type(d, device_id, errStream);
  if (retVal) {
    return retVal;
  }
  *logStream << "Device ID: " << device_id << endl;

  // The first 6 bytes are length and CRC - add these at the end
  vector<uint8_t> buffer(6, 0);

  // Output the header section
  string tzrID(TZR_ID_STRING);
  binary_append(buffer, tzrID, 6);            // The format identifier string
  binary_append(buffer, device_id, 6);        // The device ID string
  binary_append(buffer, firmwareVersion, 1);  // This byte is F. Vers. (*10)
  if (string::npos != device_id.find("TZMR")) {
    binary_append(buffer, device_serial, 8);  // The next 8 bytes are the serial number
  }
  else if (string::npos != device_id.find("H3R")) {
    binary_append(buffer, device_serial, 11);  // The next 11 bytes are the serial number
  }
  else if (string::npos != device_id.find("HPR")) {
    binary_append(buffer, device_serial, 11);  // The next 11 bytes are the serial number
  }
  binary_append(buffer, patient_id, PATIENT_ID_MAX_LENGTH);  // The patient ID string

  // Loop through the entries
  if (d.HasMember(JSON_EVENTS_LABEL) && d[JSON_EVENTS_LABEL].IsArray()) {
    const Value &events = d[JSON_EVENTS_LABEL];
    uint8_t tag = 0;
    uint32_t sequence = 0;
    uint32_t sample = 0;
    uint32_t data = 0;
    timestamp parsedTime;

    for (SizeType k = 0; k < events.Size(); k++) {
      // TAG
      if (events[k].HasMember(JSON_TAG_LABEL) && events[k][JSON_TAG_LABEL].IsNumber()) {
        tag = events[k][JSON_TAG_LABEL].GetInt();

        // If the tag is invalid, skip this whole event
        if (NULL == getEvent(device_id, (eventTags)tag)) {
          *errStream << "ERROR: Event has an invalid tag: (" << (int)tag << ")...Skipping event...";
          continue;
        }
      }
      *logStream << "tag: " << (int)tag << endl;

      // SEQUENCE
      if (
        events[k].HasMember(JSON_SEQUENCE_NUMBER_LABEL)
        && events[k][JSON_SEQUENCE_NUMBER_LABEL].IsNumber()) {
        sequence = events[k][JSON_SEQUENCE_NUMBER_LABEL].GetInt();
      }
      *logStream << "sequence: " << (int)sequence << endl;

      // SAMPLE_COUNT
      if (
        events[k].HasMember(JSON_SAMPLE_NUMBER_LABEL)
        && events[k][JSON_SAMPLE_NUMBER_LABEL].IsNumber()) {
        sample = events[k][JSON_SAMPLE_NUMBER_LABEL].GetInt();
      }
      *logStream << "sample: " << (int)sample << endl;

      // DATA
      if (events[k].HasMember(JSON_EVENT_DATA_LABEL)) {
        const Value &dataObject = events[k][JSON_EVENT_DATA_LABEL];

        if (
          dataObject.HasMember(JSON_EVENT_DATA_INT_LABEL)
          && dataObject[JSON_EVENT_DATA_INT_LABEL].IsNumber()) {
          data = dataObject[JSON_EVENT_DATA_INT_LABEL].GetInt();
        }
      }
      *logStream << "data: " << (int)data << endl;

      // TIMESTAMP
      retVal = common_get_json_date_time(events[k], parsedTime, JSON_TIMESTAMP_LABEL, errStream);
      if (retVal) {
        return retVal;
      }
      *logStream << "timestamp: " << parsedTime.getText() << endl;

      binary_append(buffer, tag, 1);
      binary_append(buffer, sequence, 4);
      binary_append(buffer, sample, 2);
      parsedTime.writeBinary(buffer);
      binary_append(buffer, sizeof(data), 1);
      binary_append(buffer, data, sizeof(data));
      binary_append(buffer, '\0', 1);
    }

    // Generally, there won't be a "Final Entry" tag in the JSON file, but if there was one
    // don't add a second here
    if (0xFF != tag) {
      tag = 0xFF;
      data = 0xFF;
      sequence = 0;
      sample = 0;
      // timestamp = last timestamp
      binary_append(buffer, tag, 1);
      binary_append(buffer, sequence, 4);
      binary_append(buffer, sample, 2);
      parsedTime.writeBinary(buffer);
      binary_append(buffer, 1, 1);
      binary_append(buffer, data, 1);
      binary_append(buffer, '\0', 1);
    }
  }

  // Finish the length/CRC in the header
  length = buffer.size();
  binary_overwrite(buffer, 2, length, 4);

  uint16_t crcValue = 0xffff;
  uint8_t *pBuf = buffer.data();
  crcBlock(&pBuf[2], length - 2, &crcValue);
  binary_overwrite(buffer, 0, crcValue, 2);

  // Write the binary file to cout (should be redirected to a file...)
  outStream->write((const char *)buffer.data(), length);

  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a json file from cin and outputs binary on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return IntervalFromJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif