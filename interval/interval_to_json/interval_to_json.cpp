/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       interval_to_json.cpp
 *          - This program parses a *.tzr interval report file from <cin> or
 *            <input_file> and outputs a JSON representation of the contents
 *            to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./interval_to_json <input.tzr 2>log.txt >output.txt
 *
 *                    -or-
 *
 *                cat input.tzr | ./interval_to_json 2>log.txt >output.txt
 *
 *                    -or-
 *
 *                ./interval_to_json input.tzr 2>log.txt >output.txt
 *
 *
 *
 *
 *****************************************************************************/

//-----------------------------------------------------------------------------
//                __             __   ___  __
//        | |\ | /  ` |    |  | |  \ |__  /__`
//        | | \| \__, |___ \__/ |__/ |___ .__/
//
//-----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <cstdint>

#include "util/common.h"
#include "util/crc.h"
#include "util/events.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int IntervalToJson(
  std::istream *inStream, map<string, string> &extraArgs, std::ostream *outStream,
  std::ostream *errStream, std::ostream *logStream)
{
  ostringstream output_stream;

  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  uint32_t i;

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // Process the extra arguments
  int32_t zoneOffset = 0;
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("zoneOffset");
  if (itr != extraArgs.end()) {
    char *endptr;
    zoneOffset = (int32_t)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  // Read in the first 16-byte block to see if the file has been encrypted
  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 16);  // Read the first block into RAM

  // Check for the format identifier string
  string str;
  str.assign((const char *)&firstBlock[6]);  // Copy what should be the "TZINT" string
  if (str.compare(0, 6, TZR_ID_STRING)) {
    *errStream << "ERROR: File corrupted! Aborting." << endl;
    return output_error_json("Corrupted format string", outStream);
  }
  else {
    i = 0;
    shortCaster = (uint16_t *)&firstBlock[i];
    fileCrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *)&firstBlock[i];
    length = *intCaster;
    i += 4;

    // Make sure the file is not too big for the format
    if (length > MAX_TZR_FILE_SIZE) {
      *errStream << "File size (" << length << ") is SIGNIFICANTLY larger than expected. Aborting."
                 << endl;
      return output_error_json("File too large", outStream);
    }

    // Make sure the file is not too small for the format
    if (length < MIN_TZR_FILE_SIZE) {
      *errStream << "File size is too small for an SCP file. Aborting." << endl;
      return output_error_json("File too small", outStream);
    }

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 16; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[16],
                   length - 16);  // Store the remainder of the file in memory

    if (length < 2) length = 2;
    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *logStream << "Ignoring file CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    i = 6;
    *logStream << "File Length: " << length << endl;  // Print out the file length

    string format_id((const char *)&pRead[i]);
    *logStream << "Format Identifier String: " << &pRead[i] << endl;
    i += 6;
    string deviceID((const char *)&pRead[i]);
    *logStream << "Device Identifier String: " << deviceID << endl;
    i += 6;
    // Parse out the firmware version
    uint8_t firmware_major = pRead[i] / 10;
    uint8_t firmware_minor = pRead[i] % 10;
    *logStream << "Firmware Version: " << firmware_major << "." << firmware_minor << endl;
    i += 1;
    *logStream << "Serial Number: " << &pRead[i] << endl;  // Parse out the Serial Number String

    // Start JSON object
    output_stream << "{\"" << JSON_FILE_FORMAT_LABEL << "\":\"" << format_id << "\"";
    output_stream << ",\"" << JSON_FILE_CRC_LABEL << "\":" << fileCrcValue << endl;
    output_stream << ",\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << deviceID << "\"";
    output_stream << ",\"" << JSON_DEVICE_SERIAL_LABEL << "\":\"" << &pRead[i] << "\"";
    output_stream << ",\"" << JSON_FIRMWARE_VERSION_LABEL << "\":" << (int)firmware_major << "."
                  << (int)firmware_minor;

    if (string::npos != deviceID.find("TZMR")) {
      i += 8;
    }
    else if (string::npos != deviceID.find("H3R")) {
      i += 11;
    }
    else if (string::npos != deviceID.find("HPR")) {
      i += 11;
    }
    *logStream << "Patient ID: " << &pRead[i] << endl;  // Parse out the Patient ID String
    output_stream << ",\"" << JSON_PATIENT_ID_LABEL << "\":\"" << &pRead[i] << "\"";
    i += 40;

    output_stream << ",\"" << JSON_EVENTS_LABEL << "\":[" << endl;

    int skippedCount = 0;

    while (i < (length - 18)) {
      int j = 0;
      eventTags tag = (eventTags)pRead[i + j];
      j += 1;
      uint32_t sequenceNumber = pRead[i + j] + ((int)pRead[i + j + 1] << 8)
                              + ((int)pRead[i + j + 2] << 16) + ((int)pRead[i + j + 3] << 24);
      j += 4;
      uint32_t sampleCount = pRead[i + j] + ((short)pRead[i + j + 1] << 8);
      j += 2;

      timestamp parsedTime;
      j += parsedTime.parseBinary(&pRead[i + j], zoneOffset, errStream);

      uint32_t dataLength = pRead[i + j];
      j += 1;
      uint32_t data = 0, k, mult = 1;
      for (k = 0; k < dataLength && k < 4; k++) {
        data = data + pRead[i + j + k] * mult;
        mult *= 256;
      }
      j += dataLength;
      uint32_t nullByte = pRead[i + j];
      j += 1;

      if (
        nullByte != 0 || parsedTime.sanityCheck() || (dataLength > 4) || ((i + j) > length)
        || (0 == tag)) {
        // File is out of sync, a value was detected where a NULL should be
        i += 1;
        skippedCount++;
      }
      else {
        i += j;

        if (skippedCount > 0) {
          *errStream << "Warning! Data loss detected. Skipped " << skippedCount
                     << " bytes to recover." << endl;
          skippedCount = 0;
        }

        *logStream << "[" << setw(8) << setfill('0') << sequenceNumber << ", " << setw(6)
                   << sampleCount << "] " << setfill(' ');
        *logStream << parsedTime.getText() << " - ";

        ostringstream event_stream;

        event_stream << "{\"" << JSON_TAG_LABEL << "\":" << (int)tag << ",";
        event_stream << "\"" << JSON_TIMESTAMP_LABEL << "\":" << parsedTime.getJson() << ",";
        event_stream << "\"" << JSON_SEQUENCE_NUMBER_LABEL << "\":" << sequenceNumber << ",";
        event_stream << "\"" << JSON_SAMPLE_NUMBER_LABEL << "\":" << sampleCount << ",";
        event_stream << getEventJson(deviceID, tag, data, sequenceNumber, parsedTime);

        eventTags next_tag = (eventTags)pRead[i];  // Identifier tag
        if (TERMINATOR_TAG != tag) {
          if (TERMINATOR_TAG != next_tag)
            output_stream << event_stream.str() << "}," << endl;
          else
            output_stream << event_stream.str() << "}" << endl;
        }
      }
    }

    output_stream << "]";
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return output_error_json("Invalid file CRC", outStream);
    }
  }

  *logStream << "Done." << endl;  // Signal completion of program

  // Close the JSON object
  *outStream << output_stream.str() << "}" << endl;

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads an interval file from cin and outputs json on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return IntervalToJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif