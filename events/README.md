# TRIDENT DEVICE EVENT FILE FORMAT #

## Overview ##

The Trident device saves individual event files (\*.tze) for each event recorded by the device. The format of the files is below, along with the possible event tags.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZEVT" to identify the file as an event file |
| Device String | 6 | The ASCII string "HPR  " or "H3R  " to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| Patient ID | 40 | ASCII String identifier unique to patient |
| Event Tag | 1 | This value identifies the event associated with the data to follow. Possible values are described below. |
| Sequence Number | 4 | This is the ECG sequence number of the file associated with this event, corresponding to tag 31 on page 34 of the SCP-ECG Standard version 2.2 |
| Sample Count | 2 | Number of samples since beginning of SCP file |
| Date | 4 | This is the date of the entry following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Time | 3 | This is the time of the entry (using UTC), following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Milliseconds | 1 | This is the number of 4 millisecond time units to pass since the last second change, to provide better distinction between events |
| Time Zone | 2 | This is the number of minutes that local time is offset from UTC. Valid range is -780 to 780 |
| Data Length | 1 | This is the number of bytes of data associated with this event |
| Data | <_length_> | The value of this section is dependent on the value of the tag. Refer to the events README |
| File Count | 1 | This is the number of SCP-ECG files associated with this event |
| Start File | 4 | This is the sequence number of the first SCP-ECG file associated with this event |
| Reserved | 1 | Null byte. This is a '\0' used to identify the end of each entry |

## Event Tag Tool Tips ##

The following table outlines the meaning of each event that can be reported by
Trident devices.

| Event Tag | Event Name | Event Upload | ECG Files | Tool Tip | Data Meaning |
| --- | --- | --- | --- | --- | -- |
| **1** | Recording Started | Always | User Set | This event indicates when a new recording is started using the device’s User Interface. | The file identifier from the most current setting file. |
| **2** | Recording Stopped | Always | 0 | This event indicates when the recording is stopped due to an actions file instruction. | **165** – stopped by “stop” action in actions file while recording. **166** – stopped by “stop” action in actions file while suspended. **167** – stopped by “stop” action in actions file while displaying ECG. |
| **3** | Recording Suspended | Always | 0 | This event indicates when the recording has been interrupted due to a low battery condition or the patient cable being disconnected. | **100** – Unknown source |
| **4** | Recording Resumed | Always | 0 | This event indicates when the recording is resumed following an interruption due to a low battery condition or a patient cable disconnect. | The file identifier from the most current setting file. |
| **5** | Recording Full | Always | 0 | This event indicates when the recording has been stopped due to reaching EITHER the predefined stopping point OR the maximum capacity of the SD Card, whichever comes first. | **11** – Preset length reached. **22** – SD Card is full. |
| **11** | Pacemaker Detection | Never | 0 | This event indicates that the associated SCP-ECG file contains at least one detected pacemaker pulse. | The width of the detected pulse in microseconds. |
| **12** | QRS Detection | Never | 0 | This event indicates the location of a QRS complex detected by the algorithm. | **Byte 0:** Letter describing the type of beat detected as follows: **_N_** – Normal Beat **_S_** – SVEB **_V_** – VEB **_F_** – Fusion Beat **_Q_** – Paced Beat or Unknown Beat Type *NOTE: Algorithm may not implement all of these types!* **Byte 1:** Channel mask for detection with LSB being channel 1. |
| **13** | Rhythm Change | User Set | User Set | This event indicates that the algorithm has detected a rhythm change. | Letter describing the newly detected Rhythm as follows: n – Normal Sinus Rhythm **_b_** – Bradycardia **_t_** – Tachycardia **_a_** – Atrial Fibrillation/Flutter **_p_** – Cardiac Pause **_U_** – Unreadable ECG Data *NOTE: Algorithm may also implement additional letters in the future to support new detection functionality!* |
| **14** | QRS Count | Never | 0 | This event indicates the number of non-PVC beats detected since the last QRS Count event, for the purpose of trend reporting. | The number of non-PVC beats detected in the last time period. |
| **15** | PVC Count | Never | 0 | This event indicates the number of PVC beats detected since the last PVC Count event, for the purpose of trend reporting. | The number of PVC beats detected in the last time period. |
| **16** | End of SCP File | Never | 0 | This event indicates the number of samples in a given SCP file to facilitate R-R Interval calculations. | The file identifier from the most current setting file. |
| **30** | Tachycardia Rate Change | User Set | User Set | This event indicates that the tachycardia heart rate has risen above the next “rate level” set by the user. Rate levels are a multiple of the rate step value set by the user. | The heart rate in BPM at the time the event was generated. |
| **31** | Bradycardia Rate Change | User Set | User Set | This event indicates that the bradycardia heart rate has dropped below above the next “rate level” set by the user. Rate levels are a multiple of the rate step. | The heart rate in BPM at the time the event was generated. |
| **51** | Lead Disconnected | Always | 0 | This event indicates that one or more snap electrodes have become disconnected. | A binary mask of the electrode connections. When set to 1, each bit indicates that the corresponding lead is off. |
| **101** | Patient Activated Event | User Set | User Set | This event indicates that the patient has felt something and held the patient event button on the device. | The symtom and activity level the patient reported in the format **0xTTSS**, where SS is the selected symptom number (0 == none) and TT is the selected activity level number (0 == none). |
| **102** | Step Count | Never | 0 | This event records the number of steps that the device detected since the last Step Count event. | The number of steps since the last Step Count event. |
| **150** | Battery State of Charge | Never | 0 | This event indicates the current percent charge of the battery.  | The percent charge times 256. (e.g. 100% -> 25600) |
| **151** | Battery Voltage | Never | 0 | This event indicates the current voltage of the battery. | The battery voltage, measured in mV. |
| **152** | Low Battery | Never | 0 | This event indicates that the battery is too low for wireless data transmission. | The battery voltage, measured in mV |
| **153** | Charging Started | Never | 0 | This event indicates that the battery charger has been connected to the device. | None |
| **154** | Charging Stopped | Never | 0 | This event indicates that the battery charger has been disconnected from the device. | None |
| **155** | Battery Temperature | Never | 0 | This event indicates the current temperature of the battery.  | The battery temperature, measured in Celsius |
| **190** | Interval File Request | Always | 0 | This event indicates that an interval file request was received in an actions file. | The truncated timestamp of the interval file in the format **0xDDHH**, where 0xDD is the day of the month and 0xHH is the hour of the day. |
| **198** | SCP Bulk Upload | Always | User Set | This event indicates that a block of ECG data has been automatically uploaded to the server. | The number of SCP files being transmitted for this event. |
| **199** | SCP Retransmission | Always | 1 | This event indicates that a single SCP file was requested to be retransmitted in an actions file. | A value of 0 means the request was successful, a non-zero value means the request failed. |
| **200** | Server ECG Data Request | Always | User Set | This event indicates that an actions file request for ECG data has been received and provided. | The number of files that were requested. |
| **201** | Settings File Download Success | Always | 0 | This event indicates that a new settings file has been successfully downloaded from the server. | The File Identifier from downloaded settings file. |
| **203** | Server Message Received | Always | 0 | This event indicates that the patient has acknowledged receipt of a message sent via SMS and displayed on the screen of the device. | The file identifier of the downloaded actions file. |
| **210** | Settings File Download Failure | Always | 0 | This event indicates that an unsuccessful attempt was made by the device to download a new settings file from the server. | The error result of the attempt to download a settings file, with the following possible values: **6001** – Invalid file format string. **6002** – Invalid device ID string. **6003** – Invalid file CRC. **6004** – Invalid firmware version. **6005** – Serial number does not match device. **6006** – Invalid file length. **6007** – Invalid setting data. **6008** – Invalid file alignment. **6009** – Invalid setting tag. |
| **211** | Action File Download Failure | Always | 0 | This event indicates that an unsuccessful attempt was made by the device to download a new actions file from the server. | The error result of the attempt to download a actions file, with the following possible values: **6011** – Invalid file format string. **6012** – Invalid device ID string. **6013** – Invalid file CRC. **6014** – Invalid firmware version. **6015** –Serial number does not match device. **6016** – Invalid file length. **6017** – Invalid action data. **6018** – Invalid file alignment. **6019** – Invalid action tag. |
| **212** | Action File Download Success | Always | 0 | This event indicates that a new action file has been successfully downloaded from the server. | The File Identifier from the downloaded file. |
| **213** | HTTP Fatal Data Error | Always | 0 | This event indicates that a fatal web error was encountered while handling a SCP or TZE file. | **0** – Fatal error handling a SCP file. **Other** – Fatal error handling a TZE file. Data indicates the current TZE count of the file. |
| **214** | HTTP Fatal Control Error | Always | 0 | This event indicates that a fatal web error was encountered while handling everything except a SCP or TZE files. | **0** – Unknown failure. **1** – Fatal error handling a TZR file. **2** – Fatal error handling a TZA file. **3** – Fatal error handling a TZS file. **4** – Fatal error handling a LOG file. **5** – Fatal error handling a FRW file. |
| **215** | Recent Data Request | Always | User Set | This event indicates that the most recently recorded ECG data was requested in an actions file. | The number of seconds of data requested. |
| **255** | Terminator | Always | 0 | This event indicates the end of a TZR file. | **255** – Equal to the Event Tag |
