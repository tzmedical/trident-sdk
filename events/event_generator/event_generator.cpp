/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       event_generator.cpp
 *          - This program generates a TZE file for the H3R based on the
 *                 values in the input JSON file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./event_generator.exe input.json > output.tze
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/events.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

using namespace std;
using namespace rapidjson;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//=============================================================================
int EventFromJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  // Parse a JSON string into DOM.
  BasicIStreamWrapper<istream> is(*inStream);
  Document d;
  if (d.ParseStream(is).HasParseError()) {
    *errStream << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at "
               << d.GetErrorOffset() << endl;
    return -1;
  }

  string file_identifier("");
  retVal = common_get_json_file_type(d, file_identifier, errStream);
  if (retVal) {
    return retVal;
  }
  if (file_identifier.find(EVENT_ID_STRING) == string::npos) {
    *errStream << "ERROR! Invalid Format ID: " << file_identifier << endl;
    return -1;
  }

  string device_id("");
  retVal = common_get_json_device_type(d, device_id, errStream);
  if (retVal) {
    return retVal;
  }

  string device_serial("");
  retVal = common_get_json_device_serial(d, device_serial, errStream);
  if (retVal) {
    return retVal;
  }

  uint16_t firmwareVersion = 0;
  retVal = common_get_json_firmware_version(d, firmwareVersion, errStream);
  if (retVal) {
    return retVal;
  }

  string patient_id("");
  retVal = common_get_json_patient_id(d, patient_id, errStream);
  if (retVal) {
    return retVal;
  }

  timestamp parsedTime;
  retVal = common_get_json_date_time(d, parsedTime, JSON_TIMESTAMP_LABEL, errStream);
  if (retVal) {
    return retVal;
  }

  uint32_t sequence_number = 0;
  if (d.HasMember(JSON_SEQUENCE_NUMBER_LABEL) && d[JSON_SEQUENCE_NUMBER_LABEL].IsInt()) {
    sequence_number = d[JSON_SEQUENCE_NUMBER_LABEL].GetInt();
  }
  else {
    *errStream << "Missing Linked SCP Sequence" << endl;
    return -1;
  }

  uint32_t event_sample = 0;
  if (d.HasMember(JSON_SAMPLE_NUMBER_LABEL) && d[JSON_SAMPLE_NUMBER_LABEL].IsInt()) {
    event_sample = d[JSON_SAMPLE_NUMBER_LABEL].GetInt();
  }
  else {
    *errStream << "Missing Linked SCP Sample Number" << endl;
    return -1;
  }

  uint32_t scp_event_start = 0;
  if (d.HasMember(JSON_EVENT_SCP_START_LABEL) && d[JSON_EVENT_SCP_START_LABEL].IsInt()) {
    scp_event_start = d[JSON_EVENT_SCP_START_LABEL].GetInt();
  }
  else {
    *errStream << "Missing Event Start SCP Sequence" << endl;
    return -1;
  }

  uint32_t num_scp_files = 0;
  if (d.HasMember(JSON_EVENT_SCP_COUNT_LABEL) && d[JSON_EVENT_SCP_COUNT_LABEL].IsInt()) {
    num_scp_files = d[JSON_EVENT_SCP_COUNT_LABEL].GetInt();
  }
  else {
    *errStream << "Missing Event Duration" << endl;
    return -1;
  }

  uint32_t event_tag = 0;
  if (d.HasMember(JSON_TAG_LABEL) && d[JSON_TAG_LABEL].IsInt()) {
    event_tag = d[JSON_TAG_LABEL].GetInt();
  }
  else {
    *errStream << "Missing Event Type Tag" << endl;
    return -1;
  }

  Value data_object;
  uint32_t data_payload = 0;
  if (d.HasMember(JSON_EVENT_DATA_LABEL) && d[JSON_EVENT_DATA_LABEL].IsObject()) {
    data_object = d[JSON_EVENT_DATA_LABEL].GetObject();

    if (
      data_object.HasMember(JSON_EVENT_INT_DATA_LABEL)
      && data_object[JSON_EVENT_INT_DATA_LABEL].IsInt()) {
      data_payload = data_object[JSON_EVENT_INT_DATA_LABEL].GetInt();
    }
  }
  else {
    *errStream << "Missing Event Data" << endl;
    return -1;
  }

  // The first 6 bytes are length and CRC - add these at the end
  vector<uint8_t> buffer(6, 0);

  binary_append(buffer, file_identifier, 6);  // The format identifier string
  binary_append(buffer, device_id, 6);        // The device ID string
  binary_append(buffer, firmwareVersion, 1);  // This byte is F. Vers. (*10)
  if (string::npos != device_id.find("TZMR")) {
    binary_append(buffer, device_serial, 8);  // The next 8 bytes are the serial number
  }
  else if (string::npos != device_id.find("H3R")) {
    binary_append(buffer, device_serial, 11);  // The next 11 bytes are the serial number
  }
  else if (string::npos != device_id.find("HPR")) {
    binary_append(buffer, device_serial, 11);  // The next 11 bytes are the serial number
  }
  binary_append(buffer, patient_id, 40);  // The patient ID string

  binary_append(buffer, event_tag, 1);
  binary_append(buffer, sequence_number, 4);
  binary_append(buffer, event_sample, 2);
  parsedTime.writeBinary(buffer);
  binary_append(buffer, sizeof(data_payload), 1);
  binary_append(buffer, data_payload, sizeof(data_payload));
  binary_append(buffer, num_scp_files, 1);
  binary_append(buffer, scp_event_start, 4);

  binary_append(buffer, '\0', 1);

  // Finish the length/CRC in the header
  uint32_t length = buffer.size();

  binary_overwrite(buffer, 2, length, 4);

  uint16_t crcValue = 0xffff;
  uint8_t *pBuf = buffer.data();
  crcBlock(&pBuf[2], length - 2, &crcValue);

  binary_overwrite(buffer, 0, crcValue, 2);

  outStream->write((const char *)buffer.data(), length);

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a backup file from cin and parses it
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return EventFromJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif