#ifndef FIND_EVENT_H
#define FIND_EVENT_H

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <sstream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to convert a find event files in a directory
//! \return 0 if successful
int FindEvent(
  std::istream* inStream, std::map<std::string, std::string>& extraArgs, std::ostream* outStream,
  std::ostream* errStream, std::ostream* logStream);

#endif