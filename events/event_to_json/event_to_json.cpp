/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       event_to_json.cpp
 *          - This program parses a *.tze event report file from <input.tze> and
 *            outputs a JSON interpretation on cout.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./event_to_json.exe input.tze > output.json
 *            -OR-
 *                ./event_to_json.exe < input.tze > output.json
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>

#include "util/common.h"
#include "util/crc.h"
#include "util/events.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//=============================================================================
int EventToJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  uint32_t i;

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // Process the extra arguments
  int32_t zoneOffset = 0;
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("zoneOffset");
  if (itr != extraArgs.end()) {
    char *endptr;
    zoneOffset = (int32_t)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  // Read in the first 16-byte block to see if the file has been encrypted
  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  // Read the first block into RAM
  inStream->read((char *)firstBlock, 16);

  // Check for the format identifier string
  string str;
  str.assign((const char *)&firstBlock[6]);  // Copy what should be the "TZEVT" string
  if (str.compare(0, 6, EVENT_ID_STRING)) {
    *errStream << "ERROR: File corrupted! Aborting." << endl;
    return -1;
  }
  else {
    i = 0;
    shortCaster = (uint16_t *)&firstBlock[i];
    fileCrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *)&firstBlock[i];
    length = *intCaster;
    i += 4;

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 16; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    // Store the remainder of the file in memory
    inStream->read((char *)&pRead[16], length - 16);

    // Calculate the CRC for the remainder of the file
    crcBlock(&pRead[2], length - 2, &calculatedCrcValue);
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *logStream << "Ignoring file CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    i = 6;
    *logStream << "File Length: " << length << endl;  // Print out the file length

    string format_id((const char *)&pRead[i]);
    *logStream << "Format Identifier String: " << format_id << endl;
    i += 6;
    string deviceID((const char *)&pRead[i]);
    *logStream << "Device Identifier String: " << deviceID << endl;
    i += 6;

    // Parse out the firmware version
    uint8_t firmware_major = pRead[i] / 10;
    uint8_t firmware_minor = pRead[i] % 10;
    *logStream << "Firmware Version: " << firmware_major << "." << firmware_minor << endl;
    i += 1;

    // Parse out the Serial Number String
    string dev_serial((const char *)&pRead[i]);
    *logStream << "Serial Number: " << &pRead[i] << endl;
    if (string::npos != deviceID.find("TZMR")) {
      i += 8;
    }
    else if (string::npos != deviceID.find("H3R")) {
      i += 11;
    }
    else if (string::npos != deviceID.find("HPR")) {
      i += 11;
    }

    string patient_id((const char *)&pRead[i]);
    *logStream << "Patient ID: " << &pRead[i] << endl;  // Parse out the Patient ID
    i += 40;

    eventTags tag = (eventTags)pRead[i];  // Identifier tag
    i += 1;
    uint32_t sequenceNumber = pRead[i] + ((int)pRead[i + 1] << 8) + ((int)pRead[i + 2] << 16)
                            + ((int)pRead[i + 3] << 24);  // Sequence number
    i += 4;
    uint32_t sampleCount = pRead[i] + ((short)pRead[i + 1] << 8);  // Sample count
    i += 2;

    timestamp parsedTime;
    i += parsedTime.parseBinary(&pRead[i], zoneOffset, errStream);

    uint32_t dataLength = pRead[i];  // Data Length
    i += 1;
    uint32_t data = 0, k, mult = 1;
    for (k = 0; k < dataLength; k++) {
      data = data + pRead[i + k] * mult;  // Data Value
      mult *= 256;
    }
    i += dataLength;
    uint32_t fCount = pRead[i];  // Number of files
    i += 1;
    uint32_t fStart = pRead[i] + ((int)pRead[i + 1] << 8)  // First file sent
                    + ((int)pRead[i + 2] << 16) + ((int)pRead[i + 3] << 24);
    i += 4;

    *logStream << "SCP-ECG Sequence Number: " << setw(8) << setfill('0') << sequenceNumber << endl;
    *logStream << "Event occurred at sample " << setw(6) << sampleCount << endl;
    *logStream << "Timestamp of Event: " << parsedTime.getText() << endl;
    *logStream << "Bytes of Data: " << dataLength << endl;
    *logStream << "Number of attached SCP-ECG files: " << fCount << endl;
    *logStream << "SCP-ECG Sequence Number of first file: " << fStart << endl;

    *logStream << "Event Reported: ";
    *logStream << getEventText(deviceID, tag, data, sequenceNumber, parsedTime);

    *outStream << "{\"" << JSON_FILE_FORMAT_LABEL << "\":\"" << format_id << "\"";
    *outStream << ",\"" << JSON_FILE_CRC_LABEL << "\":" << fileCrcValue << endl;
    *outStream << ",\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << deviceID << "\"";
    *outStream << ",\"" << JSON_DEVICE_SERIAL_LABEL << "\":\"" << dev_serial << "\"";
    *outStream << ",\"" << JSON_FIRMWARE_VERSION_LABEL << "\":" << (int)firmware_major << "."
               << (int)firmware_minor;
    *outStream << ",\"" << JSON_PATIENT_ID_LABEL << "\":\"" << patient_id << "\"";
    *outStream << ",\"" << JSON_TIMESTAMP_LABEL << "\":" << parsedTime.getJson();
    *outStream << ",\"" << JSON_SEQUENCE_NUMBER_LABEL << "\":" << sequenceNumber << "";
    *outStream << ",\"" << JSON_SAMPLE_NUMBER_LABEL << "\":" << sampleCount << "";
    *outStream << ",\"" << JSON_EVENT_SCP_START_LABEL << "\":" << fStart << "";
    *outStream << ",\"" << JSON_EVENT_SCP_COUNT_LABEL << "\":" << fCount << "";
    *outStream << ",\"" << JSON_TAG_LABEL << "\":" << tag << "";
    *outStream << "," << getEventJson(deviceID, tag, data, sequenceNumber, parsedTime);
    *outStream << "}";
    *outStream << endl;
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return output_error_json("Invalid file CRC", outStream);
    }
  }

  *logStream << "Done." << endl;  // Signal completion of program

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a backup file from cin and parses it
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdin), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return EventToJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif