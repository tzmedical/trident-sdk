/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       event_parser.cpp
 *          - This program parses a *.tze event report file from <input.tze> and
 *            outputs a text interpretation on <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./event_parser input.tze > output.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include "util/common.h"
#include "util/crc.h"
#include "util/events.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//=============================================================================
int EventToText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  uint32_t i;

  eventTags searchNumber = FIND_ALL;
  string fileName("");
  bool printHeader = 1;
  int32_t zoneOffset = 0;
  bool skipCrc = 0;

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // Process the extra arguments
  map<string, string>::iterator itr;
  itr = extraArgs.find("tag");
  if (itr != extraArgs.end()) {
    char *endptr;
    searchNumber = (eventTags)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("printHeader");
  if (itr != extraArgs.end()) {
    char *endptr;
    printHeader = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("zoneOffset");
  if (itr != extraArgs.end()) {
    char *endptr;
    zoneOffset = (int32_t)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("fileName");
  if (itr != extraArgs.end()) {
    fileName = itr->second;
  }

  // Read in the first 16-byte block to see if the file has been encrypted
  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 16);  // Read the first block into RAM

  // Check for the format identifier string
  string str;
  str.assign((const char *)&firstBlock[6]);  // Copy what should be the "TZEVT" string

  if (str.compare(0, 6, EVENT_ID_STRING)) {
    *errStream << "ERROR: File corrupted! Aborting." << endl;
    return -1;
  }
  else {
    i = 0;
    shortCaster = (uint16_t *)&firstBlock[i];
    fileCrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *)&firstBlock[i];
    length = *intCaster;
    i += 4;

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 16; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[16],
                   length - 16);  // Store the remainder of the file in memory

    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }
  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    i = 6;

    string formatId((const char *)&pRead[i]);
    i += 6;
    string deviceID((const char *)&pRead[i]);
    i += 6;

    uint32_t firmwareVersion = pRead[i];  // Firmware version (10 == 1.0)
    i += 1;

    // Parse out the Serial Number String
    string serialNumber((const char *)&pRead[i]);
    if (string::npos != deviceID.find("TZMR")) {
      i += 8;
    }
    else if (string::npos != deviceID.find("H3R")) {
      i += 11;
    }
    else if (string::npos != deviceID.find("HPR")) {
      i += 11;
    }

    // Parse out the Patient ID
    string patientId((const char *)&pRead[i]);
    i += 40;

    eventTags tag = (eventTags)pRead[i];  // Identifier tag
    i += 1;
    uint32_t sequenceNumber = pRead[i] + ((int)pRead[i + 1] << 8) + ((int)pRead[i + 2] << 16)
                            + ((int)pRead[i + 3] << 24);  // Sequence number
    i += 4;
    uint32_t sampleCount = pRead[i] + ((short)pRead[i + 1] << 8);  // Sample count
    i += 2;

    timestamp parsedTime;
    i += parsedTime.parseBinary(&pRead[i], zoneOffset, errStream);

    uint32_t dataLength = pRead[i];  // Data Length
    i += 1;
    uint32_t data = 0, k, mult = 1;
    for (k = 0; k < dataLength; k++) {
      data = data + pRead[i + k] * mult;  // Data Value
      mult *= 256;
    }
    i += dataLength;
    uint32_t fCount = pRead[i];  // Number of files
    i += 1;
    uint32_t fStart = pRead[i] + ((int)pRead[i + 1] << 8)  // First file sent
                    + ((int)pRead[i + 2] << 16) + ((int)pRead[i + 3] << 24);
    i += 4;

    if ((FIND_ALL == searchNumber) || (tag == searchNumber)) {
      if (printHeader) {
        if (skipCrc) {
          // Ignore the CRC values (debugging ONLY)
          *outStream << "Ignoring file CRC." << endl;
        }
        else {
          *outStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
        }
        *outStream << "File Length: " << length << endl;  // Print out the file length
        *outStream << "Format Identifier String: " << formatId << endl;
        *outStream << "Device Identifier String: " << deviceID << endl;
        *outStream << "Firmware Version: " << setprecision(1) << fixed
                   << (float)firmwareVersion / 10.0 << endl;
        *outStream << "Serial Number: " << serialNumber << endl;
        *outStream << "Patient ID: " << patientId << endl;
      }
      if (fileName.length()) {
        *outStream << fileName << " ";
      }
      *outStream << "[" << setw(6) << setfill('0') << sequenceNumber << ", " << setw(6)
                 << sampleCount << "] ";
      *outStream << "(" << setw(6) << fStart << " - " << setw(6);
      if (fCount) {
        *outStream << fStart + fCount - 1 << ") ";
      }
      else {
        *outStream << "000000) ";
      }
      *outStream << parsedTime.getText() << " - ";

      *outStream << getEventText(deviceID, tag, data, sequenceNumber, parsedTime);
    }
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return -1;
    }
  }

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a backup file from cin and parses it
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdin), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return EventToText(input, extraArgs, &cout, &cerr, &clog);
}
#endif