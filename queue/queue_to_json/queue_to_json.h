#ifndef QUEUE_JSON_H
#define QUEUE_JSON_H

#include <iostream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to convert a wireless queue file into a json file
//! \return 0 if successful
int QueueToJson(
  std::istream* inStream, std::map<std::string, std::string>& extraArgs, std::ostream* outStream,
  std::ostream* errStream, std::ostream* logStream);

#endif