/******************************************************************************
 *       Copyright (c) 2021, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       queue_to_json.cpp
 *          - This program parses the wireless.bak, wireless.ovf, and upload.log
 *            files from <sample_dir> and outputs a json interpretation of the
 *            wireless queue on <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./queue_to_json /sample_dir/ > outFile.json
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <sys/stat.h>
#include <set>
#include <vector>

#include "util/crc.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#include "util/win_dirent.h"
#else
#include <dirent.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define BYTES_PER_FILE_ENTRY (sizeof(file_queue_t))

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//! The possible filetypes that can be queued for upload or download
typedef enum {
  UNKNOWN = 0,
  TZE = 1,
  SCP = 2,
  TZR = 3,
  TZS = 4,
  TZA = 5,
  LOG = 6,
  FRW = 7,
  GFX = 8,
} file_type_t;

typedef struct {
  uint32_t sequence;
  uint32_t scp_start;
  uint16_t tze_count;
  uint8_t scp_count;
  uint8_t id;
} tze_info_t;

typedef struct timestamp_t {
  uint16_t date_year;
  uint8_t date_month;
  uint8_t date_day;
  uint8_t time_hour;
  uint8_t time_minute;
  uint8_t time_second;
  uint8_t time_ms;
  int16_t time_zone;
} timestamp_t;

//! This struct is used to queue ALL file uploads and downloads
typedef struct {
  file_type_t file_type;
  union {
    tze_info_t tze;             // upload_file->tze.*
    int32_t scp_sequence;       // upload_file->scp_sequence
    timestamp_t tzr_timestamp;  // upload_file->tzr_timestamp.*
    char filename[16];          // upload_file->filename
  };
} file_queue_t;

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int queue_parser(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream, const bool ovf_flag, vector<uint8_t> &upload_buffer,
  std::set<string> &scp_log)
{
  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file
  uint32_t num_files;           // The number of files from the queue

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  uint32_t i;
  file_type_t file_type;

  string tzmr_compat = "0";
  string serial_number = "0";
  bool skipCrc = 0;

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  // Process the extra arguments
  map<string, string>::iterator itr;

  itr = extraArgs.find("tzmrCompat");
  if (itr != extraArgs.end()) {
    tzmr_compat = itr->second;
  }
  itr = extraArgs.find("serialNumber");
  if (itr != extraArgs.end()) {
    serial_number = itr->second;
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  // Read in the first 16-byte block to see if the file has been encrypted
  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  // Read the first block into RAM
  inStream->read((char *)firstBlock, 16);

  i = 0;
  shortCaster = (uint16_t *)&firstBlock[i];
  fileCrcValue = *shortCaster;
  i += 2;

  intCaster = (uint32_t *)&firstBlock[i];
  length = *intCaster;
  i += 8;  // Skip an extra 4 bytes for the status which is unused

  if (!length) {  // If file is empty
    return 0;
  }

  // Calculate the number of files in the queue
  if (ovf_flag) {
    i = 16;
    num_files = (length - i) / BYTES_PER_FILE_ENTRY;
  }
  else {
    num_files = (length - i) / BYTES_PER_FILE_ENTRY;
  }

  pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
  for (i = 0; i < 16; i++) {
    pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
  }
  // Store the remainder of the file in memory
  inStream->read((char *)&pRead[16], length - 16);

  // Calculate the CRC for the remainder of the file
  if (ovf_flag) {
    crcBlock(&pRead[2], length - 2, &calculatedCrcValue);
  }
  else {
    crcBlock(&pRead[6], length - 6, &calculatedCrcValue);
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *logStream << "Ignoring file CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    file_queue_t *upload_file;
    uint32_t j;
    bool commaNeeded = false;
    if (!ovf_flag) {
      j = 10;
    }
    else {
      commaNeeded = true;
      j = 16;
    }

    // Cycle through all files
    for (i = 0; i < num_files; i++) {
      upload_file = (file_queue_t *)(&pRead[j]);
      j += BYTES_PER_FILE_ENTRY;

      file_type = upload_file->file_type;
      switch (file_type) {
        case UNKNOWN: {
          *errStream << "    Unknown File Type." << endl;
          break;
        }

        case TZE: {
          if ((int)upload_file->tze.scp_count) {
            uint8_t k;
            for (k = 0; k < (int)upload_file->tze.scp_count; k++) {
              // Convert the sequence number to a string
              uint32_t scp_sequence = upload_file->tze.scp_start + k;
              stringstream ssScp;
              ssScp << setfill('0') << setw(8) << scp_sequence;
              string sequence_string = ssScp.str();
              string filename = sequence_string;

              size_t numDirs = filename.length() / 2 - 1;
              size_t pos = 2;
              for (size_t l = 0; l < numDirs; l++) {
                filename.insert(pos, "/");
                pos += 3;
              }

              if (scp_log.insert(sequence_string).second) {
                bool print_scp = true;
                // Check upload.log
                if (!upload_buffer.empty()) {
                  uint32_t byte_number = std::stoi(sequence_string) / 8;
                  uint32_t bit_number = std::stoi(sequence_string) % 8;

                  if (byte_number < upload_buffer.size()) {
                    if (upload_buffer.at(byte_number) & (1 << bit_number)) {
                      print_scp = false;
                    }
                  }
                }

                if (print_scp) {
                  if (commaNeeded) {
                    *outStream << "," << endl;
                  }

                  *outStream << "  {" << endl;
                  // Print scp
                  if (!tzmr_compat.compare("0")) {
                    *outStream << "    \"method\": \"PUT\"," << endl;
                    *outStream << "    \"uriPath\": \"/ecgs/" << serial_number << "/"
                               << serial_number << "_" << sequence_string << ".scp\"," << endl;
                    *outStream << "    \"filePath\": \"./ecgs/" << filename << ".scp\"," << endl;
                    *outStream << "    \"sequence\": " << scp_sequence << endl;
                  }
                  else {
                    *outStream << "    \"method\": \"POST\"," << endl;
                    *outStream << "    \"uriPath\": \"/ecgs/upload/" << serial_number << "/"
                               << serial_number << "_" << sequence_string << ".scp\"," << endl;
                    *outStream << "    \"filePath\": \"./ecgs/" << filename << ".scp\"," << endl;
                    *outStream << "    \"sequence\": " << scp_sequence << endl;
                  }
                  *outStream << "  }";
                  commaNeeded = true;
                }
              }
            }
          }

          // Convert the sequence number to a string
          uint32_t scp_sequence = upload_file->tze.sequence;
          stringstream ssScp;
          ssScp << setfill('0') << setw(8) << scp_sequence;
          string sequence_string = ssScp.str();
          string filename = sequence_string;

          size_t numDirs = filename.length() / 2 - 1;
          size_t pos = 2;
          for (size_t l = 0; l < numDirs; l++) {
            filename.insert(pos, "/");
            pos += 3;
          }

          // Print tze
          uint16_t tze_count = upload_file->tze.tze_count;
          stringstream ssTze;
          ssTze << setfill('0') << setw(3) << tze_count;
          string tze_count_str = ssTze.str();

          if (commaNeeded) {
            *outStream << "," << endl;
          }
          *outStream << "  {" << endl;

          if (!tzmr_compat.compare("0")) {
            *outStream << "    \"method\": \"PUT\"," << endl;
            *outStream << "    \"uriPath\": \"/events/" << serial_number << "/" << serial_number
                       << "_" << sequence_string << "_" << tze_count_str << ".tze\"," << endl;
            *outStream << "    \"filePath\": \"./events/" << filename << "_" << tze_count_str
                       << ".tze\"" << endl;
          }
          else {
            *outStream << "    \"method\": \"POST\"," << endl;
            *outStream << "    \"uriPath\": \"/events/upload/" << serial_number << "/"
                       << serial_number << "_" << sequence_string << "_" << tze_count_str
                       << ".tze\"," << endl;
            *outStream << "    \"filePath\": \"./events/" << filename << "_" << tze_count_str
                       << ".tze\"" << endl;
          }
          *outStream << "  }";
          commaNeeded = true;
          break;
        }

        case SCP: {
          // TODO: Check if we need this?
          break;
        }

        case TZR: {
          if (commaNeeded) {
            *outStream << "," << endl;
          }
          *outStream << "  {" << endl;

          if (!tzmr_compat.compare("0")) {
            *outStream << "    \"method\": \"PUT\"," << endl;
            *outStream << "    \"uriPath\": \"/reports/" << serial_number << "/" << serial_number
                       << "_" << upload_file->tzr_timestamp.date_year << setw(2) << setfill('0')
                       << (int)upload_file->tzr_timestamp.date_month << setw(2) << setfill('0')
                       << (int)upload_file->tzr_timestamp.date_day << "_" << setw(2) << setfill('0')
                       << (int)upload_file->tzr_timestamp.time_hour << ".tzr\"," << endl;
          }
          else {
            *outStream << "    \"method\": \"POST\"," << endl;
            *outStream << "    \"uriPath\": \"/reports/upload/" << serial_number << "/"
                       << serial_number << "_" << upload_file->tzr_timestamp.date_year << setw(2)
                       << setfill('0') << (int)upload_file->tzr_timestamp.date_month << setw(2)
                       << setfill('0') << (int)upload_file->tzr_timestamp.date_day << "_" << setw(2)
                       << setfill('0') << (int)upload_file->tzr_timestamp.time_hour << ".tzr\","
                       << endl;
          }
          *outStream << "    \"filePath\": \"./tzr/" << upload_file->tzr_timestamp.date_year
                     << setw(2) << (int)upload_file->tzr_timestamp.date_month << setw(2)
                     << (int)upload_file->tzr_timestamp.date_day << "/" << setw(2)
                     << (int)upload_file->tzr_timestamp.time_hour << ".tzr\"" << endl;
          *outStream << "  }";
          commaNeeded = true;
          break;
        }

        case TZS: {
          if (commaNeeded) {
            *outStream << "," << endl;
          }
          *outStream << "  {" << endl;

          *outStream << "    \"method\": \"GET\"," << endl;
          if (!tzmr_compat.compare("0")) {
            *outStream << "    \"uriPath\": \"/settings/" << serial_number << "\"," << endl;
          }
          else {
            *outStream << "    \"uriPath\": \"/settings/download/" << serial_number << "\","
                       << endl;
          }
          *outStream << "    \"filePath\": \"./" << upload_file->filename << "\"" << endl;
          *outStream << "  }";
          commaNeeded = true;
          break;
        }

        case TZA: {
          if (commaNeeded) {
            *outStream << "," << endl;
          }
          *outStream << "  {" << endl;

          *outStream << "    \"method\": \"GET\"," << endl;
          if (!tzmr_compat.compare("0")) {
            *outStream << "    \"uriPath\": \"/actions/" << serial_number << "\"," << endl;
          }
          else {
            *outStream << "    \"uriPath\": \"/actions/download/" << serial_number << "\"," << endl;
          }
          *outStream << "    \"filePath\": \"./actions/" << serial_number << "\"" << endl;
          *outStream << "  }";
          commaNeeded = true;
          break;
        }

        case LOG: {
          if (commaNeeded) {
            *outStream << "," << endl;
          }
          *outStream << "  {" << endl;

          if (!tzmr_compat.compare("0")) {
            *outStream << "    \"method\": \"PUT\"," << endl;
          }
          else {
            *outStream << "    \"method\": \"POST\"," << endl;
          }
          *outStream << "    \"uriPath\": \"/logfile/" << serial_number << "/" << serial_number
                     << "_error.log\"," << endl;
          *outStream << "    \"filePath\": \"./error.log\"" << endl;
          *outStream << "  }";
          commaNeeded = true;
          break;
        }

        case FRW: {
          *errStream << "    File Type Not Supported." << endl;
          break;
        }

        case GFX: {
          *errStream << "    File Type Not Supported." << endl;
          break;
        }
      }
    }
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return output_error_json("Invalid file CRC", outStream);
    }
  }

  return 0;
}

//==============================================================================
static int process_queue_files(
  string *inDir, map<string, string> &extraArgs, std::ostream *outStream, std::ostream *errStream,
  std::ostream *logStream)
{
  DIR *pDir = NULL;
  struct dirent *direntry = NULL;
  struct stat entryStat;
  size_t found;
  int retVal = 0;
  vector<uint8_t> upload_buffer;  // Buffer that will hold the upload.log file
  std::set<string> scp_log;       // Holds scp sequences to prevent duplicates

  if (extraArgs["tzmrCompat"] == "1") {
    size_t length = extraArgs["serialNumber"].length();
    if (length > 7) {
      // Drop extra characters from the front until we get down to 7, then make the first
      // character a letter to avoid collisions with real TZMR device serial numbers
      string new_serial_num = extraArgs["serialNumber"].substr(length - 7);
      if ((new_serial_num[0] >= '0') && (new_serial_num[0] <= '9')) {
        // This makes 0 - 9 into H - P
        new_serial_num[0] += 'H' - '0';
      }
      extraArgs["serialNumber"] = new_serial_num;
    }
  }

  // Reject serial numbers that are too short or too long
  if (extraArgs["serialNumber"].length() < 7 || extraArgs["serialNumber"].length() > 10) {
    *errStream << "Invalid Serial Number: " << extraArgs["serialNumber"] << endl;
    return 1;
  }

  if (inDir->find("/.") == string::npos) {
    pDir = opendir(inDir->c_str());

    if (pDir == NULL) {
      *errStream << "ERROR: Could not open Directory (" << *inDir << ")" << endl;
      return -1;
    }

    // Find upload.log
    while (!retVal && (direntry = readdir(pDir))) {
      if (direntry == NULL) {
        *errStream << "ERROR: Could not read directory entry" << endl;
        continue;
      }

      string fileName;
      fileName.assign(*inDir);
      fileName.append("/");
      fileName.append((const char *)direntry->d_name);

      if (stat(fileName.c_str(), &entryStat)) {
        *errStream << "ERROR: Could not get entry stats for entry: " << direntry->d_name << endl;
        continue;
      }

      found = fileName.find("upload.log", 1);
      if (found != string::npos) {
        fstream inFile(fileName, ios::in | ios::binary);
        istream *input = &inFile;

        input->seekg(0, input->end);
        uint32_t upload_length = input->tellg();
        input->seekg(0, input->beg);

        upload_buffer = vector<uint8_t>(0);
        uint8_t temp;

        // Copy upload.log into buffer so it can be accessed in other functions
        for (uint32_t i = 0; i < upload_length; i++) {
          input->read((char *)&temp, 1);
          upload_buffer.push_back(temp);
        }
      }
    }
    rewinddir(pDir);

    // Open the JSON object
    *outStream << "[" << endl;

    // Find wireless.bak
    while (!retVal && (direntry = readdir(pDir))) {
      if (direntry == NULL) {
        *errStream << "ERROR: Could not read directory entry" << endl;
        continue;
      }

      string fileName;
      fileName.assign(*inDir);
      fileName.append("/");
      fileName.append((const char *)direntry->d_name);

      if (stat(fileName.c_str(), &entryStat)) {
        *errStream << "ERROR: Could not get entry stats for entry: " << direntry->d_name << endl;
        continue;
      }

      found = fileName.find("wireless.bak", 1);
      if (found != string::npos) {
        fstream inFile(fileName, ios::in | ios::binary);
        istream *input = &inFile;
        extraArgs["fileName"] = fileName;
        const bool ovf_flag = false;  // Process .bak file

        retVal = queue_parser(
          input, extraArgs, outStream, errStream, logStream, ovf_flag, upload_buffer, scp_log);
        if (retVal) {
          closedir(pDir);
          return retVal;
        }
      }
    }
    rewinddir(pDir);

    // Find wireless.ovf
    while (!retVal && (direntry = readdir(pDir))) {
      if (direntry == NULL) {
        *errStream << "ERROR: Could not read directory entry" << endl;
        continue;
      }

      string fileName;
      fileName.assign(*inDir);
      fileName.append("/");
      fileName.append((const char *)direntry->d_name);

      if (stat(fileName.c_str(), &entryStat)) {
        *errStream << "ERROR: Could not get entry stats for entry: " << direntry->d_name << endl;
        continue;
      }

      found = fileName.find("wireless.ovf", 1);
      if (found != string::npos) {
        fstream inFile(fileName, ios::in | ios::binary);
        istream *input = &inFile;
        extraArgs["fileName"] = fileName;
        const bool ovf_flag = true;  // Process .ovf file

        retVal = queue_parser(
          input, extraArgs, outStream, errStream, logStream, ovf_flag, upload_buffer, scp_log);
        if (retVal) {
          closedir(pDir);
          return retVal;
        }
      }
    }
    closedir(pDir);

    // Close the JSON Object
    *outStream << endl << "]" << endl;
  }

  return retVal;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int QueueToJson(
  std::istream *inStream, std::map<std::string, std::string> &extraArgs, std::ostream *outStream,
  std::ostream *errStream, std::ostream *logStream)
{
  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  string inDir(istreambuf_iterator<char>(*inStream), {});

  map<string, string>::iterator itr;

  itr = extraArgs.find("tzmrCompat");
  if (itr == extraArgs.end()) {
    extraArgs["tzmrCompat"] = "0";
  }
  itr = extraArgs.find("serialNumber");
  if (itr == extraArgs.end()) {
    *errStream << "Missing Serial Number" << endl;
    return 1;
  }

  return process_queue_files(&inDir, extraArgs, outStream, errStream, logStream);
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function processes the wireless.bak, wireless.ovf, and output.log in
//    the directory and outputs the wireless details in json.
//
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  string programName(argv[0]);
  bool displayHelp = true;
  bool serialInput = false;
  string inDir = "./";
  map<string, string> extraArgs;
  extraArgs["tzmrCompat"] = "0";
  extraArgs["serialNumber"] = "";
  int i;

  if (2 == argc) {
    inDir.assign(argv[1]);
  }
  else {
    for (i = 0; i < argc; i++) {
      if (*argv[i] == '-') {
        switch (*(argv[i] + 1)) {
          case 'i':
            if (++i >= argc) {
              cerr << programName << ": directory must follow -i." << endl;
              return -1;
            }
            inDir.assign(argv[i]);
            displayHelp = false;
            break;
          case 't':
            extraArgs["tzmrCompat"] = "1";
            displayHelp = false;
            break;
          case 's':
            if (++i >= argc) {
              cerr << programName << ": serial number must follow -s." << endl;
              return -1;
            }
            extraArgs["serialNumber"] = string(argv[i]);
            displayHelp = false;
            serialInput = true;
            break;
          default:
            cerr << programName << ": Unexpected option: " << *(argv[i] + 1) << endl;
            cerr << "Aborting (3)." << endl;
            return -1;
        }
      }
    }

    if (!serialInput) {
      cerr << programName << ": Please provide a serial number with -s." << endl;
      return -1;
    }

    // If there were no command line arguments, just display the usage info
    if (displayHelp) {
      cout << "Usage: " << programName << " [extraArgs] ..." << endl;
      cout << "extraArgs:" << endl;
      cout << "  -i DIR\t\tProcess DIR for inputs to parse." << endl;
      cout << "  -t\tProcess queue for a device with tzmr_compat=true." << endl;
      cout << "  -s SERIAL\t\tUse SERIAL when constructing upload paths for the device." << endl;

      return 1;
    }
  }

  if (process_queue_files(&inDir, extraArgs, &cout, &cerr, &clog)) {
    cerr << "ERROR: Aborting!" << endl;
    return -1;
  }

  return 0;
}
#endif