# TRIDENT DEVICE WIRELESS QUEUE FILE FORMAT

## Overview

The Trident device saves files in a wireless backup file as well as updating an upload log whenever a file from the backup is uploaded in order to avoid duplicates. The format of the files parsed is below.

## wireless.bak File Layout

| Item             | Bytes | Description                                                                           |
| ---------------- | ----- | ------------------------------------------------------------------------------------- |
| CRC              | 2     | CRC-CCITT of the entire file, excluding these two bytes and the following four bytes. |
| Length           | 4     | Length of the file in bytes, including the CRC.                                       |
| Status           | 4     | The queue status flag.                                                                |
| File Type        | 4     | A binary number, zero through eight, that corresponds with a file type.               |
| File Information | 16    | The information contained in the file, differs based on the file type.                |

## wireless.ovf File Layout

| Item             | Bytes | Description                                                                 |
| ---------------- | ----- | --------------------------------------------------------------------------- |
| CRC              | 2     | CRC-CCITT of the entire file, excluding these two bytes.                    |
| Length           | 4     | Length of the file in bytes, including the CRC.                             |
| Empty            | 10    | Blank bytes before the start of the data.                                   |
| File Type        | 4     | A binary number, zero through eight, that corresponds with a file type.     |
| File Information | 16    | The information contained in the file, this differs based on the file type. |

## upload.log File Layout

| Each bit of the upload.log file corresponds with an scp file and is set to one if the scp file has already been uploaded. The corresponding byte and bit can be found using the scp sequence. |

## File Types Found in Wireless Queue

| Number | Suffix            | Description           | Supported              |
| ------ | ----------------- | --------------------- | ---------------------- |
| 0      | Unknown File Type | Unknown File Type     | Not supported by parse |
| 1      | tze               | Event File            | Supported by parse     |
| 2      | scp               | SCP-ECG Data File     | Supported by parse     |
| 3      | tzr               | Interval Reports File | Supported by parse     |
| 4      | tzs               | Settings File         | Supported by parse     |
| 5      | tza               | Actions File          | Supported by parse     |
| 6      | log               | Error File            | Supported by parse     |
| 7      | frw               | Firmware File         | Not supported by parse |
| 8      | gfx               | Unsupported File Type | Not supported by parse |

## TZE File Layout

| Item      | Bytes | Description                                                               |
| --------- | ----- | ------------------------------------------------------------------------- |
| file type | 4     | The number one in binary.                                                 |
| sequence  | 4     | This is the ECG sequence number of the file associated with this event.   |
| scp_start | 4     | The SCP sequence number of the first SCP file associated with this event. |
| tze_count | 2     | The number of events that share the same SCP-ECG.                         |
| scp_count | 1     | The number of SCP-ECGs associated with the event.                         |
| id        | 3     | The event ID.                                                             |

## SCP File Layout

| Item         | Bytes | Description                  |
| ------------ | ----- | ---------------------------- |
| file type    | 4     | The number two in binary.    |
| scp_sequence | 4     | The SCP-ECG sequence number. |

## TZR File Layout

| Item        | Bytes | Description                                                  |
| ----------- | ----- | ------------------------------------------------------------ |
| file type   | 4     | The number three in binary.                                  |
| date_year   | 2     | The year of the report.                                      |
| date_month  | 1     | The month of the report.                                     |
| date_day    | 1     | The day of the report.                                       |
| time_hour   | 1     | The hour of the report.                                      |
| time_minute | 1     | The minute of the report.                                    |
| time_second | 1     | The second of the report.                                    |
| time_ms     | 1     | The milisecond of the report.                                |
| time_zone   | 2     | The time zone associated with the date and time information. |

## TZS File Layout

| Item      | Bytes | Description                |
| --------- | ----- | -------------------------- |
| file type | 4     | The number four in binary. |
| file name | 16    | The settings file name.    |

## TZA File Layout

| Item      | Bytes | Description                |
| --------- | ----- | -------------------------- |
| file type | 4     | The number five in binary. |
| file name | 16    | The actions file name.     |

## LOG File Layout

| Item      | Bytes | Description               |
| --------- | ----- | ------------------------- |
| file type | 4     | The number six in binary. |
| file name | 16    | The error file name.      |
