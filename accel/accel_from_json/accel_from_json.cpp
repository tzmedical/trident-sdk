/******************************************************************************
 *       Copyright (c) 2021, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       accel_from_json.cpp
 *          - This program generates a tzg file for the HPR based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./accel_from_json input.json > output.tzg
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#include "util/common.h"
#include "util/crc.h"
#include "util/json.h"
#include "util/timestamp.h"
#include "accel_from_json.h"
#include "util/binary.h"

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

using namespace std;
using namespace rapidjson;

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define NUM_AXES          (3)
#define MAX_ACCEL_SAMPLES (10500)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int tzgFromJson(
  std::istream *inStream, std::map<std::string, std::string> &extraArgs, std::ostream *outStream,
  std::ostream *errStream, std::ostream *logStream)
{
  int retVal = 0;

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  int numAxes = 0;
  uint8_t xDefined = 0;
  uint8_t yDefined = 0;
  uint8_t zDefined = 0;

  std::stringstream xString;
  std::stringstream yString;
  std::stringstream zString;

  IStreamWrapper is(*inStream);
  Document d;
  if (d.ParseStream(is).HasParseError()) {
    *errStream << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at "
               << d.GetErrorOffset() << endl;
    return -1;
  }

  string file_identifier("");
  retVal = common_get_json_file_type(d, file_identifier, errStream);
  if (retVal) {
    return retVal;
  }
  if (file_identifier.find(TZG_ID_STRING) == string::npos) {
    *errStream << "ERROR! Invalid Format ID: " << file_identifier << endl;
    return -1;
  }

  // Gets the serial number embedded in the header of the file
  string device_serial("");
  retVal = common_get_json_device_serial(d, device_serial, errStream);
  if (retVal) {
    return retVal;
  }

  // Gets the Patient ID embedded in the header of the file
  string patient_id("");
  retVal = common_get_json_patient_id(d, patient_id, errStream);
  if (retVal) {
    return retVal;
  }

  // Gets the Sequence Number embedded in the header of the file
  string sequence_number("");
  if (d.HasMember(JSON_SEQUENCE_NUMBER_LABEL) && d[JSON_SEQUENCE_NUMBER_LABEL].IsString()) {
    sequence_number.assign(d[JSON_SEQUENCE_NUMBER_LABEL].GetString());
    if (sequence_number.length() > SEQUENCE_STRING_MAX_LENGTH - 1) {
      *errStream << "ERROR! Invalid Sequence Number (too long!): " << sequence_number << endl;
      return -1;
    }
  }
  uint32_t sequenceNumber = atoi(sequence_number.c_str());

  // Gets the DEVICE_ID field in the header
  string device_id("");
  retVal = common_get_json_device_type(d, device_id, errStream);
  if (retVal) {
    return retVal;
  }

  // Gets the Firmware Version in the header
  uint16_t firmwareVersion = 0;
  retVal = common_get_json_firmware_version(d, firmwareVersion, errStream);
  if (retVal) {
    return retVal;
  }

  timestamp parsedTime;
  retVal = common_get_json_date_time(d, parsedTime, JSON_START_TIMESTAMP_LABEL, errStream);
  if (retVal) {
    return retVal;
  }

  vector<int8_t> xAxis;
  vector<int8_t> yAxis;
  vector<int8_t> zAxis;

  if (d.HasMember("axes") && d["axes"].IsObject()) {
    if (d["axes"].HasMember("xAxis") && d["axes"]["xAxis"].IsArray()) {
      const Value &xArray = d["axes"]["xAxis"];

      for (SizeType i = 0; i < xArray.Size(); i++) {
        if (xArray[i].IsInt()) {
          xAxis.push_back((int8_t)xArray[i].GetInt());
        }
      }
      numAxes++;
      xDefined = 1;
    }

    if (d["axes"].HasMember("yAxis") && d["axes"]["yAxis"].IsArray()) {
      const Value &yArray = d["axes"]["yAxis"];

      for (SizeType i = 0; i < yArray.Size(); i++) {
        if (yArray[i].IsInt()) {
          yAxis.push_back((int8_t)yArray[i].GetInt());
        }
      }
      numAxes++;
      yDefined = 1;
    }

    if (d["axes"].HasMember("zAxis") && d["axes"]["zAxis"].IsArray()) {
      const Value &zArray = d["axes"]["zAxis"];

      for (SizeType i = 0; i < zArray.Size(); i++) {
        if (zArray[i].IsInt()) {
          zAxis.push_back((int8_t)zArray[i].GetInt());
        }
      }
      numAxes++;
      zDefined = 1;
    }
  }

  // TODO: There is tentative support for <3 axes in this parser, but it has not been tested
  // yet. When HPR is updated to support a different number of axes, this can be tested and
  // verified.
  uint32_t dataSize = (xAxis.size() + yAxis.size() + zAxis.size()) / numAxes;
  uint32_t numSamples;

  if (
    (xAxis.size() == dataSize || xAxis.size() == 0)
    && (yAxis.size() == dataSize || yAxis.size() == 0)
    && (zAxis.size() == dataSize || zAxis.size() == 0)) {
    numSamples = dataSize;
  }
  else {
    *errStream << "ERROR! Different number of samples between axes!" << endl;
    return -1;
  }

  vector<uint8_t> buffer(6, 0);

  binary_append(buffer, file_identifier, 6);
  binary_append(buffer, device_id, 6);
  binary_append(buffer, firmwareVersion, 1);
  binary_append(buffer, device_serial, 11);
  binary_append(buffer, patient_id, 40);

  binary_append(buffer, sequenceNumber, 4);
  binary_append(buffer, numAxes, 1);

  parsedTime.writeBinary(buffer);

  binary_append(buffer, numSamples, 4);

  if (xDefined) {
    buffer.insert(buffer.end(), xAxis.begin(), xAxis.end());
  }
  if (yDefined) {
    buffer.insert(buffer.end(), yAxis.begin(), yAxis.end());
  }
  if (zDefined) {
    buffer.insert(buffer.end(), zAxis.begin(), zAxis.end());
  }

  // Finish the length/CRC in the header
  uint32_t length = buffer.size();

  binary_overwrite(buffer, 2, length, 4);

  uint16_t crcValue = 0xffff;
  uint8_t *pBuf = buffer.data();
  crcBlock(&pBuf[2], length - 2, &crcValue);

  binary_overwrite(buffer, 0, crcValue, 2);

  outStream->write((const char *)buffer.data(), length);
  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a json file from cin and outputs tzg on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif
  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return tzgFromJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif