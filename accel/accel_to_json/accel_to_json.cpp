/******************************************************************************
 *       Copyright (c) 2021, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       accel_to_json.cpp
 *          - This program parses an *.tzg accelerometer file from <input> and outputs a
 *            text interpretation of the information on <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./accel_to_json input.scp > output.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/json.h"
#include "util/timestamp.h"
#include "accel_to_json.h"

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define HEADER_SIZE (16)

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
ostringstream dataOut(vector<vector<char>> data, unsigned int numSamples)
{
  unsigned int i;
  ostringstream output_stream;

  output_stream << "\"xAxis\": [";
  for (i = 0; i < numSamples; i++) {
    output_stream << dec << (int)data[0][i];
    if (i < numSamples - 1) {
      output_stream << ",";
    }
  }
  output_stream << "]," << endl;
  output_stream << "\"yAxis\": [";
  for (i = 0; i < numSamples; i++) {
    output_stream << dec << (int)data[1][i];
    if (i < numSamples - 1) {
      output_stream << ",";
    }
  }
  output_stream << "]," << endl;
  output_stream << "\"zAxis\": [";
  for (i = 0; i < numSamples; i++) {
    output_stream << dec << (int)data[2][i];
    if (i < numSamples - 1) {
      output_stream << ",";
    }
  }
  output_stream << "]";
  return output_stream;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
//    main()
//
//    This function reads in a tzg file from inStream and parses out all of
//    the header information, ignoring the actual ECG data.
//==============================================================================
int tzgToJson(
  std::istream *inStream, std::map<std::string, std::string> &extraArgs, std::ostream *outStream,
  std::ostream *errStream, std::ostream *logStream)
{
  string fileName;

  ostringstream output_stream;

  unsigned short calculatedCrcValue;  // The CRC we calculate
  unsigned short fileCrcValue;        // The CRC from the file
  unsigned int fileLength;            // The length read from the file

  uint8_t *fBuff = NULL;  // Pointer used for the read command
  uint32_t bufferAddress = 0;

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  // Process the extra arguments
  int32_t zoneOffset = 0;
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("zoneOffset");
  if (itr != extraArgs.end()) {
    char *endptr;
    zoneOffset = (int32_t)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  // File Structure Elements
  string fileID;
  string deviceID;
  unsigned short firmwareVersion;
  string serialNumber;
  string patientID;

  unsigned int sequenceNumber;
  unsigned char numAxes;

  float countsPerG = 64.0;

  unsigned int numSamples;

  unsigned char header[HEADER_SIZE] = {};
  int i;
  int j;

  vector<vector<char>> accelData;

  // Read the header information-------------------------------------------------------------

  // Read in the first 16 byte block to see if the file format is correct
  inStream->read((char *)header, HEADER_SIZE);

  fileCrcValue = binary_read16(header, &bufferAddress);
  fileLength = binary_read32(header, &bufferAddress);
  fileID = string((const char *)&header[bufferAddress]);
  bufferAddress += 6;
  if (fileID.compare(TZG_ID_STRING)) {
    *errStream << "Unexpected File Identifier: \"" << fileID << "\". Aborting." << endl;
    return -1;
  }

  // Check the CRC----------------------------------------------------------------------------
  fBuff = new unsigned char[fileLength];

  for (i = 0; i < HEADER_SIZE; i++) {
    fBuff[i] = header[i];  // Copy the first block into the file buffer
  }

  // Make sure the file is not too big for the format
  if (fileLength > MAX_TZG_FILE_SIZE) {
    *errStream << "File size is SIGNIFICANTLY larger than expected. Aborting." << endl;
    return -1;
  }

  // Make sure the file is not too small for the format
  if (fileLength < MIN_TZG_FILE_SIZE) {
    *errStream << "File size is too small for an TZG file. Aborting." << endl;
    return -1;
  }

  // Store the remainder of the file in memory
  inStream->read((char *)&fBuff[HEADER_SIZE], fileLength - HEADER_SIZE);

  // Calculate the CRC for the remainder of the file
  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff
  crcBlock(&fBuff[2], fileLength - 2, &calculatedCrcValue);

  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *logStream << "Ignoring file CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    // Start JSON object
    output_stream << "{\"" << JSON_FILE_FORMAT_LABEL << "\": \"" << TZG_ID_STRING << "\"," << endl;

    deviceID = string((const char *)&fBuff[bufferAddress]);
    bufferAddress += 6;

    firmwareVersion = binary_read8(fBuff, &bufferAddress);

    serialNumber = string((const char *)&fBuff[bufferAddress]);
    bufferAddress += 11;

    patientID = string((const char *)&fBuff[bufferAddress]);
    bufferAddress += PATIENT_ID_MAX_LENGTH;

    sequenceNumber = binary_read32(fBuff, &bufferAddress);

    numAxes = binary_read8(fBuff, &bufferAddress);

    timestamp parsedTime;
    bufferAddress += parsedTime.parseBinary(&fBuff[bufferAddress], zoneOffset, errStream);

    numSamples = binary_read32(fBuff, &bufferAddress);

    accelData = vector<vector<char>>(numAxes);

    for (i = 0; i < numAxes; i++) {
      accelData[i] = vector<char>(numSamples);
      for (j = 0; (unsigned int)j < numSamples; j++) {
        accelData[i][j] = binary_read8(fBuff, &bufferAddress);
      }
    }

    // Print Header Information
    output_stream << "\"" << JSON_FILE_CRC_LABEL << "\": " << fileCrcValue << "," << endl;
    output_stream << "\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << deviceID << "\",";
    output_stream << "\"" << JSON_DEVICE_SERIAL_LABEL << "\": \"" << serialNumber << "\"," << endl;
    output_stream << "\"" << JSON_FIRMWARE_VERSION_LABEL << "\": " << setprecision(1) << fixed
                  << (float)firmwareVersion / 10.0 << "," << endl;
    output_stream << "\"" << JSON_PATIENT_ID_LABEL << "\": \"" << patientID << "\"," << endl;
    output_stream << "\"" << JSON_SEQUENCE_NUMBER_LABEL << "\": \"" << setfill('0') << setw(8)
                  << sequenceNumber << "\"," << endl;

    output_stream << "\"" << JSON_START_TIMESTAMP_LABEL << "\":" << parsedTime.getJson() << ","
                  << endl;

    output_stream << "\"" << JSON_COUNTS_PER_G_LABEL << "\": " << countsPerG << "," << endl;

    output_stream << "\"" << JSON_AXES_LABEL << "\": {" << endl;
    output_stream << dataOut(accelData, numSamples).str() << endl;
    output_stream << "}}" << endl;
  }

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << fileLength << endl;

    if (!skipCrc) {
      return output_error_json("Invalid file CRC", outStream);
    }
  }

  // Close the JSON object
  *outStream << output_stream.str() << endl;

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads an scp file from cin and outputs json on cout
//==============================================================================
int main(int argc, char *argv[])
{
  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return tzgToJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif