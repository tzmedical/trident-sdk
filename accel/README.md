# TZG File FORMAT

## Overview

The TZG file contains a header, with the serial number, firmware version, id number, sequence number, date and time, and note for unit conversion. After the header, the data is listed in three arrays for the three dimensions. The bytes representing each of these elements can be found in the layout below.

## Overall File Layout

| Item              | Bytes | Description                                                                     |
| ----------------- | ----- | ------------------------------------------------------------------------------- |
| CRC               | 2     | CRC-CCITT of the entire file, excluding these two bytes                         |
| Length            | 4     | Length of the file in bytes, including the CRC                                  |
| Device ID         | 6     | The ID of the device, 6 bytes long                                              |
| Firmware Version  | 1     | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc                        |
| Serial Number     | 11    | ASCII String of the serial number of the device, NULL Terminated, 11 bytes long |
| Patient ID        | 40    | The ID of the patient, 40 bytes long                                            |
| Sequence Number   | 4     | This sequence number mirrors the sequence number of the corresponding ECG data  |
| Number of Axes    | 1     | The number of axes to read data in                                              |
| Year              | 2     | The date year of the start of the recording                                     |
| Month             | 1     | The date month of the start of the recording                                    |
| Day               | 1     | The date day of the start of the recording                                      |
| Hour              | 1     | The hour at the start of the recording                                          |
| Minute            | 1     | The minute after the hour the recording started                                 |
| Second            | 1     | The second after the minute the recording started                               |
| Milliseconds      | 1     | The milliseconds after the minute the recording started                         |
| Time Zone         | 2     | Returns the difference from GMT                                                 |
| Number of Samples | 4     | The number of samples recorded in the file                                      |
| Each sample       | 1     | Each sample is stored as a 1-byte value                                         |
