# Communication Methods

Data may be transferred between the PC and Trident devices via direct access
through provided USB docks. The file system layout is outline below.
Alternately, MCT-capable devices may transmit data to a server using HTTPS
connections.

# USB Data Transfer (All devices)

The Trident device stores all the data it records on an SDHC card using a FAT
file system. These files can be accessed over USB using either the USB cable or
the multi-device USB dock.
The files are organized into directories and sub-directories to optimize device
access to files. The naming conventions are as follows:

| File Type        | Naming Convention          | Description                                                                                                                                                                                                                                                                                                                                                                                    |
| ---------------- | -------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SCP-ECG Data     | ecgs/SS/SS/SS/SS.scp       | Where SSSSSSSS is the 8-digit sequence number of each SCP-ECG file                                                                                                                                                                                                                                                                                                                             |
| Event Reports    | events/SS/SS/SS/SS_CCC.tze | Where SSSSSSSS is the 8-digit sequence number and CCC is the event number                                                                                                                                                                                                                                                                                                                      |
| Interval Reports | tzr/YYYYMMDD/HH.tzr        | Where YYYY is the year, MM is the month, DD is the day, and HH is the hour of the report, referenced to UTC                                                                                                                                                                                                                                                                                    |
| Settings File    | config.tzs                 | This file will always have the same name. If this file does not exist, the device will search for other files with the \*.tzs suffix and will rename the first one it finds to config.tzs (filling in un-set settings with what is stored internally on the device). If no such file is found, the device will generate a new config.tzs using current values stored internally to the device. |
| Actions File     | \*.tza                     | On boot-up, the device will search the root directory for a file with the \*.tza suffix, execute the contents, and then delete the file.                                                                                                                                                                                                                                                       |

# HTTPS Data Transfer (MCT-capable devices only)

Devices equipped with a cellular modem will use HTTP methods PUT and GET to
transfer data to and from a central server system. The device may be configured
to transfer files unencrypted (for debugging purposes ONLY!) or using TLS 1.2
for authentication and encryption. These H3R devices will use the URLs listed in
the following table to transfer data to and from the server system. The purpose
of each file type is described in the linked documents above. The server system must
be configured to handle POST or GET methods for each URL in the following table.
In each case, the binary file will be transmitted as the body of the HTTP
packet. Chunked transfer encoding is also supported.

| URL                                                      | Usage                                                 | File Suffix |
| -------------------------------------------------------- | ----------------------------------------------------- | ----------- |
| https://<server_address>/settings/<serial_no>            | Downloading settings files with HTTP GET              | \*.tzs      |
| https://<server_address>/actions/<serial_no>             | Downloading action files with HTTP GET                | \*.tza      |
| https://<server_address>/ecgs/<serial_no>/<file_name>    | Uploading SCP-ECG files with HTTP PUT                 | \*.scp      |
| https://<server_address>/reports/<serial_no>/<file_name> | Uploading interval report files with HTTP PUT         | \*.tzr      |
| https://<server_address>/events/<serial_no>/<file_name>  | Uploading event report files with HTTP PUT            | \*.tze      |
| https://<server_address>/logfile/<serial_no>/<file_name> | Uploading error log file with HTTP PUT                | \*.log      |
| https://<server_address>/<arbitrary_frw_uri>             | OPTIONAL: Downloading a firmware update with HTTP GET | \*.frw      |

Note: the `file_name` portion of the above URIs is based on the device's serial number and the name of the file in the file system. For example, given a device `H3R1234567` connected to `https://server.io`, the following files would be uploaded as shown:

| File Path                    | Upload URL                                                        |
| ---------------------------- | ----------------------------------------------------------------- |
| `ecgs/00/00/43/21.scp`       | `https://server.io/ecgs/H3R1234567/H3R1234567_00004321.scp`       |
| `tzr/20220704/21.tzr`        | `https://server.io/reports/H3R1234567/H3R1234567_20220704_21.tzr` |
| `events/00/56/43/21_002.tze` | `https://server.io/events/H3R1234567/H3R1234567_00564321_002.tze` |

## HTTP Headers

The following HTTP headers are examples of those used to communicate with the server:

### GET settings

```
GET /settings/H3R4002016 HTTP/1.1
Host: devices.tzmedical.com
User-Agent: H3R/v1.1
Connection: Keep-Alive
```

### GET actions

```
GET /actions/H3R4002016 HTTP/1.1
Host: devices.tzmedical.com
User-Agent: H3R/v1.1
Connection: Keep-Alive
```

### PUT ecgs

```
PUT /ecgs/H3R4002016/H3R4002016_00000000.scp HTTP/1.1
Host: devices.tzmedical.com
User-Agent: H3R/v1.1
Content-Type: application/octet-stream
Content-Length: 32768
Connection: Keep-Alive
```

### PUT interval

```
PUT /reports/H3R4002016/H3R4002016_20170418_20.tzr HTTP/1.1
Host: devices.tzmedical.com
User-Agent: H3R/v1.1
Content-Type: application/octet-stream
Content-Length: 33075
Connection: Keep-Alive
```

### PUT event

```
PUT /events/H3R4002016/H3R4002016_00000000_001.tze HTTP/1.1
Host: devices.tzmedical.com
User-Agent: H3R/v1.1
Content-Type: application/octet-stream
Content-Length: 97
Connection: Keep-Alive
```

## HTTP Security

All MCT-capable Trident devices are designed to use TLS1.2 with ephemeral key
exchanges for protection of patient data. This functionality can be disabled for
development or when operating with a secure VPN setup. Supported ciphers are
split into 2 categories: PSK ciphers and ECDSA ciphers.

### ECDSA Ciphers

The ECDSA ciphers depend on the server having an ECDSA certificate that has been
signed by a TZ Medical in-house CA. The device is also configured to provide a
client certificate signed by the same CA. This allows authentication of both the
server and the device if the server has been properly configured to require
client certificates. The following ciphers are enabled in this group in order of
priority:

- TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
- TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
- TLS_ECDHE_ECDSA_WITH_AES_128_CCM
- TLS_ECDHE_ECDSA_WITH_AES_256_CCM
- TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256
- TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384

### PSK Ciphers

The PSK cipher options offer low-cost authentication of both parties by means of
a pre-shared secret. However, they have the disadvantage of not being supported
by as many TLS implementations (e.g. openssl). The following ciphers are enabled
in this group in order of priority:

- TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA256
- TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA384
- TLS_DHE_PSK_WITH_AES_128_GCM_SHA256
- TLS_DHE_PSK_WITH_AES_256_GCM_SHA384
- TLS_DHE_PSK_WITH_AES_128_CCM
- TLS_DHE_PSK_WITH_AES_256_CCM
- TLS_DHE_PSK_WITH_AES_128_CBC_SHA256
- TLS_DHE_PSK_WITH_AES_256_CBC_SHA384
