/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       backup_parser.cpp
 *          - This program parses a bak.tmp backup file from <inFile> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./backup_parser inFile.tmp > outFile.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <cstdint>
#include <map>

#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define CONF_BOARD_BACKUP_URI_MAX_LENGTH (128)

#define BLOCK_SIZE        (512)
#define PATIENT_ID_LENGTH (40)
#define MAX_SAVED_EPOCHS  (3)
#define OLD_SAVED_EPOCHS  (4)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

// The SDK still requires different structs for each backup file version so that
// the *_from_buffer functions still work correctly
typedef struct {
  uint16_t crc_value;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
} v1_backup_t;

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint8_t firmware_version;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
  int32_t stopped_epoch;
  uint32_t last_scp_epoch;
  int32_t tzr_epochs[OLD_SAVED_EPOCHS];
  char frw_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  char gfx_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
} v2_backup_t;

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint8_t firmware_version;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
  time_t stopped_epoch;
  time_t last_scp_epoch;
  time_t tzr_epochs[MAX_SAVED_EPOCHS];
  time_t reserved_epoch;
  char frw_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  char gfx_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  uint16_t tza_file_crc;
  uint16_t tzs_file_id;
  uint16_t tzs_file_crc;
  int32_t start_sequence;
} v3_backup_t;

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint8_t firmware_version;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
  time_t stopped_epoch;
  time_t last_scp_epoch;
  time_t tzr_epochs[MAX_SAVED_EPOCHS];
  time_t reserved_epoch;
  char frw_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  char gfx_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  uint16_t tza_file_crc;
  uint16_t tzs_file_id;
  uint16_t tzs_file_crc;
  int32_t start_sequence;
  time_t first_scp_epoch;
  uint32_t last_file_samples;
  time_t full_tag_epoch;
} backup_t;

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int v1_backup_from_buffer(
  uint8_t* buffer, v1_backup_t* dest, uint16_t offset, uint16_t max_offset, ostream* errStream)
{
  uint16_t reported_crc = ((uint16_t*)buffer)[offset];
  uint16_t expected_length = sizeof(v1_backup_t);

  if (offset + expected_length > max_offset) {
    *errStream << "Expected length (" << expected_length << ") is larger than max offset ("
               << max_offset << "). Aborting." << endl;
    return -1;
  }

  uint16_t calculated_crc = 0xffff;
  crcBlock(&buffer[offset] + 2, expected_length - 2, &calculated_crc);

  if (reported_crc != calculated_crc) {
    *errStream << "Reported CRC (" << reported_crc << ") does not match calculated CRC ("
               << calculated_crc << "). Aborting." << endl;
    return -1;
  }

  memcpy(dest, &buffer[offset], expected_length);
  return 0;
}

//==============================================================================
static int v2_backup_from_buffer(
  uint8_t* buffer, v2_backup_t* dest, uint16_t offset, uint16_t max_offset, ostream* errStream)
{
  uint16_t reported_crc = ((uint16_t*)buffer)[offset];
  uint16_t reported_length = ((uint16_t*)buffer)[offset + 1];

  if (!reported_length) reported_length = sizeof(v2_backup_t);
  if (offset + reported_length > max_offset) {
    *errStream << "Reported length (" << reported_length << ") is larger than max offset ("
               << max_offset << "). Aborting." << endl;
    return -1;
  }

  uint16_t calculated_crc = 0xffff;
  crcBlock(&buffer[offset] + 2, reported_length - 2, &calculated_crc);

  if (reported_crc != calculated_crc) {
    *errStream << "Reported CRC (" << reported_crc << ") does not match calculated CRC ("
               << calculated_crc << "). Aborting." << endl;
    return -1;
  }

  memcpy(dest, &buffer[offset], sizeof(v2_backup_t));
  return 0;
}

//==============================================================================
static int v3_backup_from_buffer(
  uint8_t* buffer, v3_backup_t* dest, uint16_t offset, uint16_t max_offset, ostream* errStream)
{
  uint16_t reported_crc = ((uint16_t*)buffer)[offset];
  uint16_t reported_length = ((uint16_t*)buffer)[offset + 1];

  if (!reported_length) reported_length = sizeof(v3_backup_t);
  if (offset + reported_length > max_offset) {
    *errStream << "Reported length (" << reported_length << ") is larger than max offset ("
               << max_offset << "). Aborting." << endl;
    return -1;
  }

  uint16_t calculated_crc = 0xffff;
  crcBlock(&buffer[offset] + 2, reported_length - 2, &calculated_crc);

  if (reported_crc != calculated_crc) {
    *errStream << "Reported CRC (" << reported_crc << ") does not match calculated CRC ("
               << calculated_crc << "). Aborting." << endl;
    return -1;
  }

  memcpy(dest, &buffer[offset], sizeof(v3_backup_t));
  return 0;
}

//==============================================================================
static int backup_from_buffer(
  uint8_t* buffer, backup_t* dest, uint16_t offset, uint16_t max_offset, ostream* errStream)
{
  uint16_t reported_crc = ((uint16_t*)buffer)[offset];
  uint16_t reported_length = ((uint16_t*)buffer)[offset + 1];

  if (!reported_length) reported_length = sizeof(backup_t);
  if (offset + reported_length > max_offset) {
    *errStream << "Reported length (" << reported_length << ") is larger than max offset ("
               << max_offset << "). Aborting." << endl;
    return -1;
  }

  uint16_t calculated_crc = 0xffff;
  crcBlock(&buffer[offset] + 2, reported_length - 2, &calculated_crc);

  if (reported_crc != calculated_crc) {
    *errStream << "Reported CRC (" << reported_crc << ") does not match calculated CRC ("
               << calculated_crc << "). Aborting." << endl;
    return -1;
  }

  memcpy(dest, &buffer[offset], sizeof(backup_t));
  return 0;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int BackupToText(
  istream* inStream, map<string, string>& extraArgs, ostream* outStream, ostream* errStream,
  ostream* logStream)
{
  uint16_t i = 0;

  v1_backup_t v1_backup_struct;
  v2_backup_t v2_backup_struct;
  v3_backup_t v3_backup_struct;
  backup_t backup_struct;

  string deviceType;

  int backup_version_found = 4;

  const int min_input = sizeof(v1_backup_t);
  // this is v3 because v3 is the last released firmware that writes
  // the backup data twice
  const int max_input = BLOCK_SIZE + sizeof(v3_backup_t);

  uint8_t input_buffer[max_input] = {};

  // Process the extra arguments
  map<string, string>::iterator itr;
  itr = extraArgs.find("deviceType");
  if (itr != extraArgs.end()) {
    deviceType = itr->second;
  }

  int v4MinFirmwareVersion;
  int v3MinFirmwareVersion;

  if ("HPR" == deviceType) {
    v4MinFirmwareVersion = 17;
    v3MinFirmwareVersion = 16;
  }
  else if ("H3R" == deviceType) {
    v4MinFirmwareVersion = 24;
    v3MinFirmwareVersion = 22;
  }
  else {
    *errStream << "ERROR! Invalid device type: " << deviceType << "\n"
               << "Expected \"HPR\" or \"H3R\"" << endl;
    return -1;
  }

  // Read in the file
  while (!inStream->eof() && i < max_input) {
    input_buffer[i++] = inStream->get();
  }

  // Eat the rest of the input
  while (!inStream->eof()) {
    i++;
    inStream->get();
  }

  // The read that set input->eof() to true incremented i past the Calculated file size
  int file_size = i - 1;

  // Report any errors with input size here
  if (file_size > max_input) {
    *errStream << "File Size (" << file_size << ") is larger than expected (" << max_input
               << "). Aborting." << endl;
    return -1;
  }
  else if (file_size < min_input) {
    *errStream << "File Size (" << file_size << ") is smaller than expected (" << min_input
               << "). Aborting." << endl;
    return -1;
  }

  backup_version_found = 1;

  // Check the length of the backup file. If it is shorter than the v2 struct, parse as a
  // v1 backup, otherwise, check the firmware version and parse accordingly
  int v2_size = sizeof(v2_backup_t);
  if (BLOCK_SIZE < file_size) {
    v2_size = BLOCK_SIZE + sizeof(v2_backup_t);
  }
  if (v2_size > file_size) {
    if (v1_backup_from_buffer(input_buffer, &v1_backup_struct, 0, BLOCK_SIZE, errStream)) {
      backup_version_found = 0;
    }
  }
  else {
    uint16_t firmware_version = 0 + (input_buffer[4]);

    if (v4MinFirmwareVersion <= firmware_version) {
      backup_version_found = 4;
      if (backup_from_buffer(input_buffer, &backup_struct, 0, BLOCK_SIZE, errStream)) {
        backup_version_found = 0;
      }
    }
    else if (v3MinFirmwareVersion <= firmware_version) {
      backup_version_found = 3;
      if (v3_backup_from_buffer(input_buffer, &v3_backup_struct, 0, BLOCK_SIZE, errStream)) {
        backup_version_found = 0;
      }
    }
    else {
      backup_version_found = 2;
      if (v2_backup_from_buffer(input_buffer, &v2_backup_struct, 0, BLOCK_SIZE, errStream)) {
        backup_version_found = 0;
      }
    }
  }

  // Output the appropriate struct elements
  if (!backup_version_found) {
    return -1;
  }
  else {
    *outStream << "file.type=TZBAK" << endl;
    *outStream << "file.device_id=" << deviceType << endl;

    if (1 == backup_version_found) {
      // Output each part of the struct in plain text
      *outStream << "file.firmware_version=" << (("H3R" == deviceType) ? 14 : 13) << endl;
      *outStream << "bak.tza_file_id=" << v1_backup_struct.tza_file_id << endl;
      *outStream << "bak.total_samples=" << v1_backup_struct.total_samples << endl;
      *outStream << "bak.sequence=" << v1_backup_struct.sequence << endl;
      *outStream << "bak.last_samples=" << v1_backup_struct.last_samples << endl;
      *outStream << "bak.last_suspend=" << v1_backup_struct.last_suspend << endl;
      *outStream << "bak.last_bulk_sequence=" << v1_backup_struct.last_bulk_sequence << endl;
      *outStream << "bak.patient_id=" << v1_backup_struct.patient_id << endl;
    }
    else if (2 == backup_version_found) {
      // Output each part of the struct in plain text
      *outStream << "file.firmware_version=" << (uint16_t)v2_backup_struct.firmware_version << endl;
      *outStream << "bak.tza_file_id=" << v2_backup_struct.tza_file_id << endl;
      *outStream << "bak.total_samples=" << v2_backup_struct.total_samples << endl;
      *outStream << "bak.sequence=" << v2_backup_struct.sequence << endl;
      *outStream << "bak.last_samples=" << v2_backup_struct.last_samples << endl;
      *outStream << "bak.last_suspend=" << v2_backup_struct.last_suspend << endl;
      *outStream << "bak.last_bulk_sequence=" << v2_backup_struct.last_bulk_sequence << endl;
      *outStream << "bak.patient_id=" << v2_backup_struct.patient_id << endl;
      *outStream << "bak.stopped_epoch=" << v2_backup_struct.stopped_epoch << endl;
      *outStream << "bak.last_scp_epoch=" << v2_backup_struct.last_scp_epoch << endl;
    }
    else if (3 == backup_version_found) {
      // Output each part of the struct in plain text
      *outStream << "file.firmware_version=" << (uint16_t)v3_backup_struct.firmware_version << endl;
      *outStream << "bak.tza_file_id=" << v3_backup_struct.tza_file_id << endl;
      *outStream << "bak.total_samples=" << v3_backup_struct.total_samples << endl;
      *outStream << "bak.sequence=" << v3_backup_struct.sequence << endl;
      *outStream << "bak.last_samples=" << v3_backup_struct.last_samples << endl;
      *outStream << "bak.last_suspend=" << v3_backup_struct.last_suspend << endl;
      *outStream << "bak.last_bulk_sequence=" << v3_backup_struct.last_bulk_sequence << endl;
      *outStream << "bak.patient_id=" << v3_backup_struct.patient_id << endl;
      *outStream << "bak.stopped_epoch=" << v3_backup_struct.stopped_epoch << endl;
      *outStream << "bak.last_scp_epoch=" << v3_backup_struct.last_scp_epoch << endl;
      *outStream << "bak.start_sequence=" << v3_backup_struct.start_sequence << endl;
    }
    else if (4 == backup_version_found) {
      // Output each part of the struct in plain text
      *outStream << "file.firmware_version=" << (uint16_t)backup_struct.firmware_version << endl;
      *outStream << "bak.tza_file_id=" << backup_struct.tza_file_id << endl;
      *outStream << "bak.total_samples=" << backup_struct.total_samples << endl;
      *outStream << "bak.sequence=" << backup_struct.sequence << endl;
      *outStream << "bak.last_samples=" << backup_struct.last_samples << endl;
      *outStream << "bak.last_suspend=" << backup_struct.last_suspend << endl;
      *outStream << "bak.last_bulk_sequence=" << backup_struct.last_bulk_sequence << endl;
      *outStream << "bak.patient_id=" << backup_struct.patient_id << endl;
      *outStream << "bak.stopped_epoch=" << backup_struct.stopped_epoch << endl;
      *outStream << "bak.last_scp_epoch=" << backup_struct.last_scp_epoch << endl;
      *outStream << "bak.start_sequence=" << backup_struct.start_sequence << endl;
      *outStream << "bak.first_scp_epoch=" << backup_struct.first_scp_epoch << endl;
      *outStream << "bak.last_file_samples=" << backup_struct.last_file_samples << endl;
      *outStream << "bak.full_tag_epoch=" << backup_struct.full_tag_epoch << endl;
    }
  }

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a backup file from cin and parses it
//==============================================================================
int main(int argc, char* argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdin), O_BINARY);
#endif

  istream* input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return BackupToText(input, extraArgs, &cout, &cerr, &clog);
}
#endif
