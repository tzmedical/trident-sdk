/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       backup_generator.cpp
 *          - This program generates a backup file for the H3R based on the
 *                values in the input file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./backup_generator input.txt > bak.tmp
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cstdint>
#include <queue>

#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define CONF_BOARD_BACKUP_URI_MAX_LENGTH (128)

#define PATIENT_ID_LENGTH (40)
#define BLOCK_SIZE        (512)
#define MAX_SAVED_EPOCHS  (3)
#define OLD_SAVED_EPOCHS  (4)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

// The SDK still requires different structs for each backup file version so that
// the length that it generates is correct
typedef struct {
  uint16_t crc_value;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
} v1_backup_t;

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint8_t firmware_version;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
  int32_t stopped_epoch;
  uint32_t last_scp_epoch;
  int32_t tzr_epochs[OLD_SAVED_EPOCHS];
  char frw_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  char gfx_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  uint16_t tza_file_crc;
  uint16_t tzs_file_id;
  uint16_t tzs_file_crc;
} v2_backup_t;

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint8_t firmware_version;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
  time_t stopped_epoch;
  time_t last_scp_epoch;
  time_t tzr_epochs[MAX_SAVED_EPOCHS];
  time_t reserved_epoch;
  char frw_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  char gfx_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  uint16_t tza_file_crc;
  uint16_t tzs_file_id;
  uint16_t tzs_file_crc;
  int32_t start_sequence;
} v3_backup_t;

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint8_t firmware_version;
  uint16_t tza_file_id;
  uint64_t total_samples;
  int32_t sequence;
  uint64_t last_samples;
  int32_t last_suspend;
  int32_t last_bulk_sequence;
  char patient_id[PATIENT_ID_LENGTH];
  time_t stopped_epoch;
  time_t last_scp_epoch;
  time_t tzr_epochs[MAX_SAVED_EPOCHS];
  time_t reserved_epoch;
  char frw_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  char gfx_uri_str[CONF_BOARD_BACKUP_URI_MAX_LENGTH];
  uint16_t tza_file_crc;
  uint16_t tzs_file_id;
  uint16_t tzs_file_crc;
  int32_t start_sequence;
  time_t first_scp_epoch;
  uint32_t last_file_samples;
  time_t full_tag_epoch;
} backup_t;

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

int v1_generate_backup(v1_backup_t *backup, queue<string> arguments, ostream *errStream)
{
  while (!arguments.empty()) {
    string argument = arguments.front();
    arguments.pop();

    if (argument.find("bak.tza_file_id=") != string::npos) {
      backup->tza_file_id = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.total_samples=") != string::npos) {
      backup->total_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.sequence=") != string::npos) {
      backup->sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_samples=") != string::npos) {
      backup->last_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_suspend=") != string::npos) {
      backup->last_suspend = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_bulk_sequence=") != string::npos) {
      backup->last_bulk_sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.patient_id=") != string::npos) {
      string patient_id_str = argument.substr(argument.find_first_of('=') + 1);
      if (patient_id_str.length() > PATIENT_ID_LENGTH - 1) {
        *errStream << "ERROR! Invalid Device Serial (too long!): " << patient_id_str << endl;
        return -1;
      }
      patient_id_str.copy((char *)&(backup->patient_id), patient_id_str.length());
    }
  }

  uint16_t crcValue = 0xffff;
  crcBlock(((uint8_t *)backup) + 2, sizeof(v1_backup_t) - 2, &crcValue);
  backup->crc_value = crcValue;

  return 0;
}

int v2_generate_backup(v2_backup_t *backup, queue<string> arguments, ostream *errStream)
{
  while (!arguments.empty()) {
    string argument = arguments.front();
    arguments.pop();

    if (argument.find("bak.tza_file_id=") != string::npos) {
      backup->tza_file_id = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("file.firmware_version=") != string::npos) {
      backup->firmware_version = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.total_samples=") != string::npos) {
      backup->total_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.sequence=") != string::npos) {
      backup->sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_samples=") != string::npos) {
      backup->last_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_suspend=") != string::npos) {
      backup->last_suspend = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_bulk_sequence=") != string::npos) {
      backup->last_bulk_sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.stopped_epoch=") != string::npos) {
      backup->stopped_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_scp_epoch=") != string::npos) {
      backup->last_scp_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.patient_id=") != string::npos) {
      string patient_id_str = argument.substr(argument.find_first_of('=') + 1);
      if (patient_id_str.length() > PATIENT_ID_LENGTH - 1) {
        *errStream << "ERROR! Invalid Device Serial (too long!): " << patient_id_str << endl;
        return -1;
      }
      patient_id_str.copy((char *)&(backup->patient_id), patient_id_str.length());
    }
  }

  backup->length = sizeof(v2_backup_t);

  uint16_t crcValue = 0xffff;
  crcBlock(((uint8_t *)backup) + 2, backup->length - 2, &crcValue);
  backup->crc_value = crcValue;

  return 0;
}

int v3_generate_backup(v3_backup_t *backup, queue<string> arguments, ostream *errStream)
{
  while (!arguments.empty()) {
    string argument = arguments.front();
    arguments.pop();

    if (argument.find("bak.tza_file_id=") != string::npos) {
      backup->tza_file_id = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("file.firmware_version=") != string::npos) {
      backup->firmware_version = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.total_samples=") != string::npos) {
      backup->total_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.sequence=") != string::npos) {
      backup->sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_samples=") != string::npos) {
      backup->last_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_suspend=") != string::npos) {
      backup->last_suspend = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_bulk_sequence=") != string::npos) {
      backup->last_bulk_sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.stopped_epoch=") != string::npos) {
      backup->stopped_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_scp_epoch=") != string::npos) {
      backup->last_scp_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.start_sequence=") != string::npos) {
      backup->start_sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.patient_id=") != string::npos) {
      string patient_id_str = argument.substr(argument.find_first_of('=') + 1);
      if (patient_id_str.length() > PATIENT_ID_LENGTH - 1) {
        *errStream << "ERROR! Invalid Device Serial (too long!): " << patient_id_str << endl;
        return -1;
      }
      patient_id_str.copy((char *)&(backup->patient_id), patient_id_str.length());
    }
  }

  backup->length = sizeof(v3_backup_t);

  uint16_t crcValue = 0xffff;
  crcBlock(((uint8_t *)backup) + 2, backup->length - 2, &crcValue);
  backup->crc_value = crcValue;

  return 0;
}

int generate_backup(backup_t *backup, queue<string> arguments, ostream *errStream)
{
  while (!arguments.empty()) {
    string argument = arguments.front();
    arguments.pop();

    if (argument.find("bak.tza_file_id=") != string::npos) {
      backup->tza_file_id = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("file.firmware_version=") != string::npos) {
      backup->firmware_version = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.total_samples=") != string::npos) {
      backup->total_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.sequence=") != string::npos) {
      backup->sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_samples=") != string::npos) {
      backup->last_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_suspend=") != string::npos) {
      backup->last_suspend = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_bulk_sequence=") != string::npos) {
      backup->last_bulk_sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.stopped_epoch=") != string::npos) {
      backup->stopped_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_scp_epoch=") != string::npos) {
      backup->last_scp_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.start_sequence=") != string::npos) {
      backup->start_sequence = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.first_scp_epoch=") != string::npos) {
      backup->first_scp_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.last_file_samples=") != string::npos) {
      backup->last_file_samples = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.full_tag_epoch=") != string::npos) {
      backup->full_tag_epoch = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("bak.patient_id=") != string::npos) {
      string patient_id_str = argument.substr(argument.find_first_of('=') + 1);
      if (patient_id_str.length() > PATIENT_ID_LENGTH - 1) {
        *errStream << "ERROR! Invalid Device Serial (too long!): " << patient_id_str << endl;
        return -1;
      }
      patient_id_str.copy((char *)&(backup->patient_id), patient_id_str.length());
    }
  }

  backup->length = sizeof(backup_t);

  uint16_t crcValue = 0xffff;
  crcBlock(((uint8_t *)backup) + 2, backup->length - 2, &crcValue);
  backup->crc_value = crcValue;

  return 0;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

int BackupFromText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int firmwareVersion = -1;
  string deviceType;
  string fileId;

  int backupVersion = -1;

  v1_backup_t v1_backup = {0};
  v2_backup_t v2_backup = {0};
  v3_backup_t v3_backup = {0};
  backup_t backup = {0};

  int backupLength;
  int generatorStatus = -1;
  queue<string> arguments;

  // Loop through the entire input to get all the arguments
  while (!inStream->eof()) {
    // Get a single line from the input
    string argument;
    getline(*inStream, argument);

    string value;
    value = "";

    // Trim any leading whitespace
    const size_t begStr = argument.find_first_not_of(" \t");
    if (begStr != string::npos) argument.erase(0, begStr);

    // Trim any trailing whitespace
    const size_t endStr = argument.find_last_not_of(" \n\r\t");
    if (endStr != string::npos) argument.resize(endStr + 1);

    // Skip the line if it is a comment
    if (argument.find("#") == 0) {
      continue;
    }

    // Only process lines that have more than 8 characters in them
    if (argument.length() > 8) {
      // Trim whitespace before or after the "="
      bool done = 0;
      while (!done) {
        size_t found;
        if ((found = argument.find(" =")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("\t=")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("= ")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else if ((found = argument.find("=\t")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else {
          done = 1;
        }
      }

      // Parse file arguments here:
      if (argument.find("file.type=") != string::npos) {
        fileId = argument.substr(argument.find_first_of('=') + 1);
      }
      else if (argument.find("file.device_id=") != string::npos) {
        deviceType = argument.substr(argument.find_first_of('=') + 1);
      }
      else if (argument.find("file.firmware_version=") != string::npos) {
        firmwareVersion = stoi(argument.substr(argument.find_first_of('=') + 1));
      }

      arguments.push(argument);
    }
  }

  // Check header arguments
  if (0 >= firmwareVersion) {
    *errStream << "ERROR! Undefined firmware version. "
               << "Provide version in file header with \"file.firmware_version=\"" << endl;
    return -1;
  }

  if ("H3R" != deviceType && "HPR" != deviceType) {
    *errStream << "ERROR! Invalid device type: " << deviceType << "Expected \"HPR\" or \"H3R\""
               << endl;
    return -1;
  }

  if ("H3R" == deviceType) {
    if (firmwareVersion <= 13) backupVersion = 0;
    if (firmwareVersion >= 14 && firmwareVersion <= 18)
      backupVersion = 1;
    else if (firmwareVersion >= 19 && firmwareVersion <= 21)
      backupVersion = 2;
    else if (firmwareVersion >= 22 && firmwareVersion <= 23)
      backupVersion = 3;
    else if (firmwareVersion >= 24)
      backupVersion = 4;
  }
  else if ("HPR" == deviceType) {
    if (firmwareVersion <= 12) backupVersion = 0;
    if (firmwareVersion == 13)
      backupVersion = 1;
    else if (firmwareVersion >= 14 && firmwareVersion <= 15)
      backupVersion = 2;
    else if (firmwareVersion == 16)
      backupVersion = 3;
    else if (firmwareVersion >= 17)
      backupVersion = 4;
  }

  if (0 >= backupVersion) {
    *errStream << "ERROR! Unable to generate file for firmware version " << firmwareVersion
               << " on " << deviceType
               << "Minimum supported version: " << (("H3R" == deviceType ? 14 : 13)) << endl;
    return -1;
  }

  if (1 == backupVersion) {
    generatorStatus = v1_generate_backup(&v1_backup, arguments, errStream);
    backupLength = sizeof(v1_backup_t);

    if (!generatorStatus) {
      outStream->write((const char *)&v1_backup, backupLength);
    }
  }
  else if (2 == backupVersion) {
    generatorStatus = v2_generate_backup(&v2_backup, arguments, errStream);
    backupLength = sizeof(v2_backup_t);

    if (!generatorStatus) {
      outStream->write((const char *)&v2_backup, backupLength);
    }
  }
  else if (3 == backupVersion) {
    generatorStatus = v3_generate_backup(&v3_backup, arguments, errStream);
    backupLength = sizeof(v3_backup_t);

    if (!generatorStatus) {
      outStream->write((const char *)&v3_backup, backupLength);
    }
  }
  else if (4 == backupVersion) {
    generatorStatus = generate_backup(&backup, arguments, errStream);
    backupLength = sizeof(backup_t);

    if (!generatorStatus) {
      outStream->write((const char *)&backup, backupLength);
    }
  }

  if (generatorStatus) {
    cout << "Could not generate backup file: (" << generatorStatus << ")" << endl;
    return generatorStatus;
  }

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return BackupFromText(input, extraArgs, &cout, &cerr, &clog);
}
#endif
