## [6.12.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.12.0...v6.12.1) (2025-02-10)

# [6.12.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.11.1...v6.12.0) (2025-01-02)


### Bug Fixes

* updating packages due to audit failure ([273faa1](https://bitbucket.org/tzmedical/trident-sdk/commits/273faa19b31dd47adfcb1ebc7b698bce34d09db5))


### Features

* added new setting for asymmetrical control of arrhythmia onset and offset requirements ([eb46b4f](https://bitbucket.org/tzmedical/trident-sdk/commits/eb46b4f50aed0feb5ee65cac678ef5912a9e2800))

## [6.11.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.11.0...v6.11.1) (2024-11-13)


### Bug Fixes

* actually including the test files whoops ([dddeb23](https://bitbucket.org/tzmedical/trident-sdk/commits/dddeb23932706d9506cefe134e8be56c3255337d))
* fixed error log route parsing when using the parse command on a wireless queue ([c4f35bd](https://bitbucket.org/tzmedical/trident-sdk/commits/c4f35bd44eed571e1cd19c0af83d9a91335d8b03))

# [6.11.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.10.0...v6.11.0) (2024-11-06)


### Bug Fixes

* editing reference files ([dc8c005](https://bitbucket.org/tzmedical/trident-sdk/commits/dc8c0056e9852b54ea55edcd9ae0c1d090498bde))
* fixed the npm audit ([eaeb3eb](https://bitbucket.org/tzmedical/trident-sdk/commits/eaeb3eba3147f9cd64835750f320234af6a8645f))
* removed a temp file that I added ([3b60bfd](https://bitbucket.org/tzmedical/trident-sdk/commits/3b60bfd43127309f8a971b87b5bc770f569d2bfa))
* repaired the broken changes ([561c289](https://bitbucket.org/tzmedical/trident-sdk/commits/561c2895dbc75a972d7f69981e8590a3834f5644))
* updated descriptions and setting values based off of pr comments ([664c3d3](https://bitbucket.org/tzmedical/trident-sdk/commits/664c3d3fddd0409578e88ef604dad941b5604b0d))
* updated hpr_settings.json ([fadda78](https://bitbucket.org/tzmedical/trident-sdk/commits/fadda78edbd0b55cf19be90096b826c099986475))
* updating hprm settings ([54406bb](https://bitbucket.org/tzmedical/trident-sdk/commits/54406bbf61f17a60ee9156d7ab002b74c7bcb0fe))


### Features

* added the needed settings for the HPRM ([5c596b6](https://bitbucket.org/tzmedical/trident-sdk/commits/5c596b658051c40eb6a52c68280939502352f58a))
* adding reference files for HPRM settings ([f204865](https://bitbucket.org/tzmedical/trident-sdk/commits/f2048653e5d5b5cb100dd05ff7615d63e26c0c6e))

# [6.10.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.9.1...v6.10.0) (2024-07-30)


### Bug Fixes

* fixed failing tests ([e1a1a6d](https://bitbucket.org/tzmedical/trident-sdk/commits/e1a1a6d082ed30977aa02e4c3e472a322ef64600))
* fixed unit tests so now they all pass again ([4e2b285](https://bitbucket.org/tzmedical/trident-sdk/commits/4e2b285089c110c716032017a019406d42e018f0))
* no longer errors out when it encounters an invalid tag ([c9a07c8](https://bitbucket.org/tzmedical/trident-sdk/commits/c9a07c801cfdd7ad781b3efd3bd817a34585fbba))
* now correctly handles and tests for the interval_generator being used with unknown tags ([7063150](https://bitbucket.org/tzmedical/trident-sdk/commits/70631504e5587e25d321f78b498361ea68ad0fcb))
* now displays the toJson error correctly ([0a2031c](https://bitbucket.org/tzmedical/trident-sdk/commits/0a2031ce9a859ae32672863fe3369f035d725631))
* small adjustments and addressing comments on the PR ([722bd4b](https://bitbucket.org/tzmedical/trident-sdk/commits/722bd4bfd9c92a78fce5a90b28b2c26017477ca3))


### Features

* added tests for the interval stuff with an unknown tag ([0ba77a1](https://bitbucket.org/tzmedical/trident-sdk/commits/0ba77a1e1c98ae115996c6f6d35597b02d4aa9a4))
* added unit tests and fixed the toJson parsing ([4b160f6](https://bitbucket.org/tzmedical/trident-sdk/commits/4b160f67594c19ccc9123e2f053a46f6b27ef49b))

## [6.9.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.9.0...v6.9.1) (2024-07-30)


### Bug Fixes

* broken package ([fc92db0](https://bitbucket.org/tzmedical/trident-sdk/commits/fc92db0dc4a58405e26584d2c650c941291e7202))
* correcting inputs and outputs ([ea28e22](https://bitbucket.org/tzmedical/trident-sdk/commits/ea28e22107bf51434fb44a2a838a4bdca5393cb8))
* fixing minFW ([72f9bee](https://bitbucket.org/tzmedical/trident-sdk/commits/72f9bee85234374fb700b8f3806f30d4c923a367))
* updating inputs and outputs for the new settings ([522dfe6](https://bitbucket.org/tzmedical/trident-sdk/commits/522dfe6b4864607029b866e06970c9d52a170786))
* updating version ([db2926a](https://bitbucket.org/tzmedical/trident-sdk/commits/db2926ad145372d4ec7d85691c1252d6c9f8573d))

# [6.9.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.8.1...v6.9.0) (2024-07-08)


### Features

* added length check to the backup parser ([3248409](https://bitbucket.org/tzmedical/trident-sdk/commits/324840903f4d1097a4648703b1a4f181d717748d))

## [6.8.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.8.0...v6.8.1) (2024-06-18)


### Bug Fixes

* removing unwanted err printouts ([2fe775f](https://bitbucket.org/tzmedical/trident-sdk/commits/2fe775f1b7124ab78c4390bc53d23a12c37ec2b5))
* reporting start sequence properly now ([4aa6f40](https://bitbucket.org/tzmedical/trident-sdk/commits/4aa6f40d27a82a95dcd7449b4be85e0acd3377ff))

# [6.8.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.5...v6.8.0) (2024-06-04)


### Bug Fixes

* changed test to the correct device (HPR) and briefly updated readme file ([bcc9151](https://bitbucket.org/tzmedical/trident-sdk/commits/bcc91516c89339a65c360511c6d488d14e9e3578))
* configured formatter ([576f645](https://bitbucket.org/tzmedical/trident-sdk/commits/576f6450675d03c2ea92e8e2ad43179bf1e36509))
* fixed the failing test case due to battery temperature stuff ([8e279d4](https://bitbucket.org/tzmedical/trident-sdk/commits/8e279d4e9b183243ac7bfd21446b32ce63acf144))
* fixed the study hour versions for h3r and hpr in readme ([ccb8d4c](https://bitbucket.org/tzmedical/trident-sdk/commits/ccb8d4c151f83aac969d0778353fd31bd8758186))
* fixed the version number on the bad study hours test ([5c226a7](https://bitbucket.org/tzmedical/trident-sdk/commits/5c226a7b35d9bb2c4ab9259ae4a84f5b095a4e2c))
* remove not needed file ([f2bcaef](https://bitbucket.org/tzmedical/trident-sdk/commits/f2bcaeff5ed6aff939368c836d291fbdb836fb07))
* updated readme again ([7b5ff8f](https://bitbucket.org/tzmedical/trident-sdk/commits/7b5ff8f9e0c2053879609c990d7912607b5d44cf))
* updated to correct version numbers in tests and other misc fixes from pull request comments ([8614beb](https://bitbucket.org/tzmedical/trident-sdk/commits/8614beb816805b9b6cbf62bd9053444c454b740b))
* updated version in hpr settings file ([753b994](https://bitbucket.org/tzmedical/trident-sdk/commits/753b99419c62d319fb844bb0178d6af6588e7218))
* updating the study hours in the readme file and manually ran the linter ([868f751](https://bitbucket.org/tzmedical/trident-sdk/commits/868f751e52862d49912ac1b7663724d6e5220885))


### Features

* expanding the max study length for firmware v2.5+ ([465d866](https://bitbucket.org/tzmedical/trident-sdk/commits/465d8665c1a08dc7f9fac91e89d95626e41dd419))

## [6.7.5](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.4...v6.7.5) (2024-05-24)

## [6.7.5-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.4...v6.7.5-alpha.1) (2024-05-24)

## [6.7.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.3...v6.7.4) (2024-05-24)


### Bug Fixes

* fixing NAPI usage to work with latest electron versions ([12b376b](https://bitbucket.org/tzmedical/trident-sdk/commits/12b376b1b576f23391686eed716ecb822d0f2dbb))

## [6.7.4-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.3...v6.7.4-alpha.1) (2024-05-24)


### Bug Fixes

* fixing NAPI usage to work with latest electron versions ([12b376b](https://bitbucket.org/tzmedical/trident-sdk/commits/12b376b1b576f23391686eed716ecb822d0f2dbb))

## [6.7.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.2...v6.7.3) (2024-05-22)

## [6.7.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.1...v6.7.2) (2024-03-08)


### Bug Fixes

* **error.tojson:** fixing broken regexes causing invalid error objects for log entries ([ee3fa7b](https://bitbucket.org/tzmedical/trident-sdk/commits/ee3fa7b63d29c22760d3e9164e8a4fd703d4ca82))

## [6.7.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.0...v6.7.1) (2023-12-12)

## [6.7.1-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.7.0...v6.7.1-alpha.1) (2023-12-12)

# [6.7.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.6.1...v6.7.0) (2023-12-12)


### Features

* skipping some unsafe characters when parsing binary files to JSON ([92e002c](https://bitbucket.org/tzmedical/trident-sdk/commits/92e002c45ee91c8dde203ddf73898d0c62ce88d1))

## [6.6.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.6.0...v6.6.1) (2023-12-01)

# [6.6.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.5.0...v6.6.0) (2023-11-30)


### Features

* addinf setting and event support for AF rate change events ([0a26b32](https://bitbucket.org/tzmedical/trident-sdk/commits/0a26b3286eda338303cce13196a75939c7d24153))

# [6.5.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.4.3...v6.5.0) (2023-09-25)


### Bug Fixes

* modified to support full_tag_epoch ([0199992](https://bitbucket.org/tzmedical/trident-sdk/commits/0199992656dfdc756ce5fdf401125fbcddef1491))
* modified to support last_file_samples ([f5ced12](https://bitbucket.org/tzmedical/trident-sdk/commits/f5ced12e2738b50a17bc05198bc18ea331229605))
* updated backup parsing/generating to work with new changes ([d1a210e](https://bitbucket.org/tzmedical/trident-sdk/commits/d1a210edaca117d0327c15f33b175a228e461006))


### Features

* **new tag:** added support for handling new backup info for calendar days bug and updated tests ([6c82303](https://bitbucket.org/tzmedical/trident-sdk/commits/6c8230338a9eed950d0284e25f0292a57d0b7f79))

## [6.4.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.4.2...v6.4.3) (2023-09-23)

## [6.4.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.4.1...v6.4.2) (2023-09-13)


### Bug Fixes

* added support for new fields added to the batt info file ([d341d4a](https://bitbucket.org/tzmedical/trident-sdk/commits/d341d4a4b3677ee03e29e1bca51690307a785ea4))

## [6.4.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.4.0...v6.4.1) (2023-08-29)


### Bug Fixes

* fixed misplaced "|" in settings readme ([1269217](https://bitbucket.org/tzmedical/trident-sdk/commits/1269217223ac8450f8f3e561d51ac9f6e62bd321))

# [6.4.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.1...v6.4.0) (2023-08-29)


### Bug Fixes

* fixed test input/output files and added missing temperature report interval tests ([ec8becf](https://bitbucket.org/tzmedical/trident-sdk/commits/ec8becf603ba4df0c98f6d24bd784ce476b82c4f))


### Features

* **new tag:** added support for parsing battery temperature reports and TZR files for testing this ([1abf2eb](https://bitbucket.org/tzmedical/trident-sdk/commits/1abf2ebfa8bf5092167be763d7f3a6c5870ee74b))
* **new tag:** tag 43 added support for parsing/generating report_battery_temperature setting ([b2ac69f](https://bitbucket.org/tzmedical/trident-sdk/commits/b2ac69f0799fc2cd933155aa8249f07d60c03f48))

## [6.3.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.0...v6.3.1) (2023-08-24)

## [6.3.1-alpha.6](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.1-alpha.5...v6.3.1-alpha.6) (2023-08-23)

## [6.3.1-alpha.5](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.1-alpha.4...v6.3.1-alpha.5) (2023-08-23)

## [6.3.1-alpha.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.1-alpha.3...v6.3.1-alpha.4) (2023-08-23)

## [6.3.1-alpha.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.1-alpha.2...v6.3.1-alpha.3) (2023-08-23)

## [6.3.1-alpha.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.1-alpha.1...v6.3.1-alpha.2) (2023-08-23)

## [6.3.1-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.3.0...v6.3.1-alpha.1) (2023-08-23)

# [6.3.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.2.0...v6.3.0) (2023-08-22)


### Bug Fixes

* changed folder name and removed chip name from struct ([4cb6f07](https://bitbucket.org/tzmedical/trident-sdk/commits/4cb6f0700d04415dad1aedd8f83d1b3377cbfa6f))
* fixed error reporting ([f28a26b](https://bitbucket.org/tzmedical/trident-sdk/commits/f28a26b820a424fa4a384eae0ada60840bad4e21))


### Features

* adding support for parsing/generating new battery info file ([003cab4](https://bitbucket.org/tzmedical/trident-sdk/commits/003cab4371125d3ad5303dc1bd515beb7b170b74))

# [6.2.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.1.0...v6.2.0) (2023-06-15)


### Features

* added settings for battery alert timer ([7ac2a11](https://bitbucket.org/tzmedical/trident-sdk/commits/7ac2a116d91226d25d2c2fa56a7de4eb49c9827b))

# [6.1.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.0.2...v6.1.0) (2023-06-09)


### Features

* adding a warning output when invlaid timezone offsets are detected in files ([9345ad6](https://bitbucket.org/tzmedical/trident-sdk/commits/9345ad6cdd515005b5a0945a4905db4a358ce24a))
* adding support for numeric timezone offsets ([56275f5](https://bitbucket.org/tzmedical/trident-sdk/commits/56275f5d1e5322504638924fa6bbe69d9d91a450))

## [6.0.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.0.1...v6.0.2) (2023-05-17)

## [6.0.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.0.0...v6.0.1) (2023-05-10)

# [6.0.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v5.1.1...v6.0.0) (2023-05-04)


### Features

* **tzs:** throw an error when a sample rate is inputted that the device will round up ([355030d](https://bitbucket.org/tzmedical/trident-sdk/commits/355030d39ddc9850f999809cef19b7779b0dfec9))


### BREAKING CHANGES

* **tzs:** New errors are thrown based on input values; options are listed instead of min/max
for sample_rate setting in the `metaJson` output.

# [6.0.0-alpha.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.0.0-alpha.3...v6.0.0-alpha.4) (2023-04-27)

# [6.0.0-alpha.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.0.0-alpha.2...v6.0.0-alpha.3) (2023-04-27)

# [6.0.0-alpha.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v6.0.0-alpha.1...v6.0.0-alpha.2) (2023-04-19)

# [6.0.0-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v5.1.1...v6.0.0-alpha.1) (2023-04-19)


### Features

* **tzs:** throw an error when a sample rate is inputted that the device will round up ([355030d](https://bitbucket.org/tzmedical/trident-sdk/commits/355030d39ddc9850f999809cef19b7779b0dfec9))


### BREAKING CHANGES

* **tzs:** New errors are thrown based on input values; options are listed instead of min/max
for sample_rate setting in the `metaJson` output.

## [5.1.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v5.1.0...v5.1.1) (2022-11-04)


### Bug Fixes

* **tza.fromjson:** gracefully handling errors with malformed inputs ([9701257](https://bitbucket.org/tzmedical/trident-sdk/commits/97012575d34dc2a54345dd135e70adcd3def595f))

# [5.1.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v5.0.2...v5.1.0) (2022-10-21)


### Features

* adding support for ISO-format datetimes ([3a16d36](https://bitbucket.org/tzmedical/trident-sdk/commits/3a16d360bc69fc65e917ff980024eaaa3f99c715))

## [5.0.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v5.0.1...v5.0.2) (2022-09-09)

## [5.0.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v5.0.0...v5.0.1) (2022-08-17)


### Bug Fixes

* adding-requested-fixes ([ad52c2f](https://bitbucket.org/tzmedical/trident-sdk/commits/ad52c2f1360d70a741b1efeb3f7b238ab2c4d92e))
* deleted file ([7cf18c6](https://bitbucket.org/tzmedical/trident-sdk/commits/7cf18c63d1c9a73a897615fa662e6c8648741f97))
* reverting to previous version ([b2380e9](https://bitbucket.org/tzmedical/trident-sdk/commits/b2380e96ffc585b5b05f3ecefdcd8709debcfd05))
* typo ([a9a6b33](https://bitbucket.org/tzmedical/trident-sdk/commits/a9a6b33914d108ccb06454d32081a6913501f7f2))

# [5.0.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.1.0...v5.0.0) (2022-08-08)


### Bug Fixes

* testing breaking change detection ([feb052c](https://bitbucket.org/tzmedical/trident-sdk/commits/feb052c8781c6bf12c2f44b1c6742e3e6869c41a))


### BREAKING CHANGES

* This should trigger a major release.

# [5.0.0-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.1.0...v5.0.0-alpha.1) (2022-08-05)


### Bug Fixes

* testing breaking change detection ([feb052c](https://bitbucket.org/tzmedical/trident-sdk/commits/feb052c8781c6bf12c2f44b1c6742e3e6869c41a))


### BREAKING CHANGES

* This should trigger a major release.

# [4.1.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.0.3...v4.1.0) (2022-08-05)


### Features

* **json:** changing 3 JSON field names to more clearly match documentation ([9e71057](https://bitbucket.org/tzmedical/trident-sdk/commits/9e71057b27f4d82b425243fd95169bfd40708cd7))


### BREAKING CHANGES

* **json:** Parser outputs and generator inputs are incompatible with those from previous
release versions.

## [4.0.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.0.2...v4.0.3) (2022-07-28)


### Bug Fixes

* aaand forgot to commit the new test file ([367e313](https://bitbucket.org/tzmedical/trident-sdk/commits/367e31308a11fc2a0dc97b73d8d1aaaf26c8ef52))
* adding test files ([52e4f18](https://bitbucket.org/tzmedical/trident-sdk/commits/52e4f189e70114cbbf515c2bed771f588f672dcd))
* addressed some PR comments, need to make decisions on others ([257a242](https://bitbucket.org/tzmedical/trident-sdk/commits/257a242e8d5d584921884a33de1591947c555809))
* catch negative firmware versions ([8796e78](https://bitbucket.org/tzmedical/trident-sdk/commits/8796e78f7b19cc4f2b0acfc2e79cd5a60ac7d927))
* removed unneeded file ([bcddef1](https://bitbucket.org/tzmedical/trident-sdk/commits/bcddef122056e663f62bcf8bc45925b1d6508aef))
* woops! Previously committed an it.only test ([53a637a](https://bitbucket.org/tzmedical/trident-sdk/commits/53a637a3b43ac4fef41de8bc2f4ad1066d7c3164))

## [4.0.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.0.1...v4.0.2) (2022-07-11)

## [4.0.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.0.0...v4.0.1) (2022-06-23)

## [4.0.1-alpha.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v4.0.0...v4.0.1-alpha.1) (2022-06-21)

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [4.0.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.13.4...v4.0.0) (2022-06-21)


### ⚠ BREAKING CHANGES

* Changes to the outputted contents of the toText and find commands for all files
types.

### Features

* adding --skip-crc and --no-file-header flags for toText and toJson commands ([b0cf854](https://bitbucket.org/tzmedical/trident-sdk/commit/b0cf854b8d58dea5b901fb4ce6579a8ea0132fa3))

### [3.13.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.13.3...v3.13.4) (2022-06-14)


### Bug Fixes

* allowing CRC to be 0 if it's in fact actually 0 ([13c80e7](https://bitbucket.org/tzmedical/trident-sdk/commit/13c80e77f2d44938151d318f48c829d62736bb3a))

### [3.13.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.13.2...v3.13.3) (2022-06-13)

### [3.13.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.13.1...v3.13.2) (2022-06-08)


### Bug Fixes

* fixing the default file suffix for backup files to be correct ([e5f73fe](https://bitbucket.org/tzmedical/trident-sdk/commit/e5f73fe858cd3a2fb3c7cdb7191a60be7c2ea23f))

### [3.13.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.13.0...v3.13.1) (2022-06-08)


### Bug Fixes

* **fromjson:** fixing bug when processing all files in a directory ([8e85ba6](https://bitbucket.org/tzmedical/trident-sdk/commit/8e85ba666ef2adf806a20822f3c7deae14c4a7fd))

## [3.13.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.12.0...v3.13.0) (2022-06-06)


### Features

* ignoring bad tags ([21e1b2f](https://bitbucket.org/tzmedical/trident-sdk/commit/21e1b2f860be6de48e1f9bd652684fe8b357cef6))
* removed duplicate test ([42f34ce](https://bitbucket.org/tzmedical/trident-sdk/commit/42f34ce16bd1dcf3cf118167fa73fbf2a010a20a))
* updated tests even more ([e34c2d9](https://bitbucket.org/tzmedical/trident-sdk/commit/e34c2d934791c331d0f21db91cc9ec9c7925b8aa))

## [3.12.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.11.0...v3.12.0) (2022-06-01)


### Features

* **scp.retrievefile:** adding support for filenames in addition to directory names ([828f7ff](https://bitbucket.org/tzmedical/trident-sdk/commit/828f7ff6ad21596ccaec682335f9d764dd87a9f4))

## [3.11.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.10.0...v3.11.0) (2022-05-26)


### Features

* add support for device ejection on Linux ([38db630](https://bitbucket.org/tzmedical/trident-sdk/commit/38db630709376ce3fa36004cd6001dc670664104))
* improvement to Linux mounting procedure, style changes ([304ad98](https://bitbucket.org/tzmedical/trident-sdk/commit/304ad98fc71126e09385e425f291a6d9ed595613))


### Bug Fixes

* fix usage of async functions when unmounting ([69eb30e](https://bitbucket.org/tzmedical/trident-sdk/commit/69eb30e0d4b1af0e004db47a491664e8f4ab9b43))

## [3.10.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.9...v3.10.0) (2022-05-04)


### Features

* added setting for ignoring haptics errors ([606b6f1](https://bitbucket.org/tzmedical/trident-sdk/commit/606b6f1aab24e5b6e9cb44c4ca7bc2b126ab3848))

### [3.9.9](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.8...v3.9.9) (2022-05-03)


### Bug Fixes

* **tojson:** escaping '"' and '\' characters encountered when parsing settings and actions files ([d5b79c1](https://bitbucket.org/tzmedical/trident-sdk/commit/d5b79c1c234405b0097a84d5651d51f6c7464969))

### [3.9.8](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.7...v3.9.8) (2022-05-02)


### Bug Fixes

* **scp.tojson:** fixing local timezone parsing to match other file parsers ([d2f7499](https://bitbucket.org/tzmedical/trident-sdk/commit/d2f7499241993ee7c37df996d67ef303a10c2d8c))

### [3.9.7](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.6...v3.9.7) (2022-04-26)


### Bug Fixes

* **tojson:** fixing parsing of files with 32767 time zone offset values ([5616e52](https://bitbucket.org/tzmedical/trident-sdk/commit/5616e52f04621d746eafde401d4fe0e0e44110b1))

### [3.9.6](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.5...v3.9.6) (2022-04-04)


### Bug Fixes

* adding expanded zymed values ([f9dd5d7](https://bitbucket.org/tzmedical/trident-sdk/commit/f9dd5d785f15d4e3156c6a33ca8bbe3a51ee0195))
* updated expected results ([a0908e0](https://bitbucket.org/tzmedical/trident-sdk/commit/a0908e06ab34ce3df5089405a6be2e17eb970616))

### [3.9.5](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.4...v3.9.5) (2022-03-04)


### Bug Fixes

* updated fw version in action_list.cpp ([a78abf8](https://bitbucket.org/tzmedical/trident-sdk/commit/a78abf80b9468034064d3a8fe55f4513a94997cc))
* updating fw version in test file ([fc716b3](https://bitbucket.org/tzmedical/trident-sdk/commit/fc716b3ebf414ca1bfa5c3ee1089f2c842e19406))

### [3.9.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.3...v3.9.4) (2022-03-03)

### [3.9.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.2...v3.9.3) (2022-02-28)

### [3.9.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.1...v3.9.2) (2022-02-28)

### [3.9.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.9.0...v3.9.1) (2022-02-24)

## [3.9.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.8.1...v3.9.0) (2022-02-15)


### Features

* adding support for all H3R actions inside TZMR-formatted files ([28df6e1](https://bitbucket.org/tzmedical/trident-sdk/commit/28df6e16e49ef24f9b52f085d09d29f4513f707c))

### [3.8.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.8.0...v3.8.1) (2022-02-04)


### Bug Fixes

* making the crc field in JSON outputs a number instead of a string ([35bc64b](https://bitbucket.org/tzmedical/trident-sdk/commit/35bc64ba2916193e5179bee19fe6613495f40d95))

## [3.8.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.7.2...v3.8.0) (2022-02-03)


### Features

* adding file CRC value to parsed JSON output files ([b724e90](https://bitbucket.org/tzmedical/trident-sdk/commit/b724e9046ce83e143a3cbee63563e701cd33619e))

### [3.7.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.7.1...v3.7.2) (2022-01-11)

### [3.7.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.7.0...v3.7.1) (2022-01-08)

## [3.7.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.6.0...v3.7.0) (2022-01-06)


### Features

* adding util functions for detecting file type during json conversions ([e6c8950](https://bitbucket.org/tzmedical/trident-sdk/commit/e6c8950a1e34b5ebd47a0c77889bdbbab18a1778))

## [3.6.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.5.0...v3.6.0) (2021-12-15)


### Features

* **actions:** adding tza.fromJson support ([f67c72a](https://bitbucket.org/tzmedical/trident-sdk/commit/f67c72aa582cab97a31bcbf76d391f8be816bd55))

## [3.5.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.4.3...v3.5.0) (2021-12-10)


### Features

* **tza:** adding toJson parser for actions files ([1996ffa](https://bitbucket.org/tzmedical/trident-sdk/commit/1996ffa69633a3974a5ca204acaedff5b00bc5a7))

### [3.4.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.4.2...v3.4.3) (2021-12-10)

### [3.4.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.4.1...v3.4.2) (2021-12-09)

### [3.4.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.4.0...v3.4.1) (2021-12-06)

## [3.4.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.3.1...v3.4.0) (2021-11-04)


### Features

* queue parser now logs scp sequence numbers ([88eaf9b](https://bitbucket.org/tzmedical/trident-sdk/commit/88eaf9bf75ba23619c2370ea931b1dbbd672307d))

### [3.3.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.3.0...v3.3.1) (2021-11-02)

## [3.3.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.6...v3.3.0) (2021-10-08)


### Features

* added scp.readFile and cli readFile ([347e86d](https://bitbucket.org/tzmedical/trident-sdk/commit/347e86d8f35569119be70584c609cb0dbf54628e))

### [3.2.6](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.5...v3.2.6) (2021-09-17)

### [3.2.5](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.4...v3.2.5) (2021-09-10)

### [3.2.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.3...v3.2.4) (2021-08-25)


### Bug Fixes

* queue parsing now works when no/an empty ovf file is present. ([c3b1ef9](https://bitbucket.org/tzmedical/trident-sdk/commit/c3b1ef9982f12dc1b71d2eb3c8a1e2d8a609ad16))

### [3.2.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.2...v3.2.3) (2021-08-09)


### Bug Fixes

* off-by-one issues ([8712fd3](https://bitbucket.org/tzmedical/trident-sdk/commit/8712fd3bd56b50d843f6ddad67c75a9589bb5348))

### [3.2.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.1...v3.2.2) (2021-07-30)

### [3.2.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.2.0...v3.2.1) (2021-07-23)


### Bug Fixes

* **unix timestamp:** fixed epoch input argument on interval parser (linux machines not running utc) ([8d1705a](https://bitbucket.org/tzmedical/trident-sdk/commit/8d1705a2bfdc54d034fe45ba5a5ba11ee6a555ea))
* changed gmtime_r to localtime_r for linux epoch adjustment ([edacf9b](https://bitbucket.org/tzmedical/trident-sdk/commit/edacf9b0d805fb106bdebb45858a5f707541f730))

## [3.2.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.1.1...v3.2.0) (2021-07-01)


### Features

* **queue:** changing cli to use --tzmr-compat to avoid "alphabet soup" ([636cec4](https://bitbucket.org/tzmedical/trident-sdk/commit/636cec4a8c004b14d31594b885589f610715c4fa))
* **queue:** changing cli to use -t for tzmr_compat, since there are other possible compat settings ([ae97111](https://bitbucket.org/tzmedical/trident-sdk/commit/ae971116657d52cee72e888ec5e6bdf3b30c6eb2))
* allows parsing of wireless uploads to json (does not work yet) ([82bf44c](https://bitbucket.org/tzmedical/trident-sdk/commit/82bf44c744f4a5a485842bb88852fd451ce6ed3f))


### Bug Fixes

* **queue:** global variables removed ([bc328d7](https://bitbucket.org/tzmedical/trident-sdk/commit/bc328d722c291ac4079a6a3a797f4b310c27c615))
* **queue:** pointers to arguments fixed ([98305af](https://bitbucket.org/tzmedical/trident-sdk/commit/98305af48c5f9e6f53a376fe5c97aeac560b24bf))
* **queue:** fixing cli to match format of extraArgs expected by C++ code ([453fd15](https://bitbucket.org/tzmedical/trident-sdk/commit/453fd15b8703586648095a1e9208a2adfb989e44))
* **queue:** fixing non-JS cli to match options ([74a19e0](https://bitbucket.org/tzmedical/trident-sdk/commit/74a19e0c2d58eeffd3979add9e1f67b9e55f3642))
* **queue:** serial number checked in c++ code ([40f7fce](https://bitbucket.org/tzmedical/trident-sdk/commit/40f7fced02161969dd65dba3239ab47408070e6c))
* arrays changed to vectors ([3d3f42a](https://bitbucket.org/tzmedical/trident-sdk/commit/3d3f42af422d1a875225648a054307d5c50f89e5))
* converts to json but crc check is failing ([1aee54d](https://bitbucket.org/tzmedical/trident-sdk/commit/1aee54de07ff3c9c4f1bd7da71897814c50c6dea))
* could not open directory error no longer appears when it could in fact open the directory ([162ac16](https://bitbucket.org/tzmedical/trident-sdk/commit/162ac16e6b9dc2c925c95eee6f75447da1dfe133))
* crc now works and tabs changed to spaces ([0c9eec4](https://bitbucket.org/tzmedical/trident-sdk/commit/0c9eec4b218cb2301dfa75a8f3240adbaa92307e))
* file processor added to Trident SDK ([a78bf8b](https://bitbucket.org/tzmedical/trident-sdk/commit/a78bf8bc2eed2832c9dfb5013115df01e572da32))
* fixed some of Chris's comments ([8edbb76](https://bitbucket.org/tzmedical/trident-sdk/commit/8edbb76b7700ed8df23176314c233677d0c4b14a))
* function name changed to parse and tzmr compat allows 10-character serial number ([cb1e903](https://bitbucket.org/tzmedical/trident-sdk/commit/cb1e903ed5ebcd3a9c844ab23dccd74436c3119e))
* index tests passing ([168f55f](https://bitbucket.org/tzmedical/trident-sdk/commit/168f55fb405c7d9c1b123cfcac79b89a73f56809))
* new class "upload" added ([71b58bc](https://bitbucket.org/tzmedical/trident-sdk/commit/71b58bc6287a7ca4942b2458d55c9e63da9cdb38))
* parses .bak and .ovf ([4a83da5](https://bitbucket.org/tzmedical/trident-sdk/commit/4a83da556c6f13c67a759d70a416741a78c7e7a6))
* queue tests updated ([05714d1](https://bitbucket.org/tzmedical/trident-sdk/commit/05714d15cddd96708dd4327740a8b8f104e4f70e))
* scp duplicates eliminated ([117c9fb](https://bitbucket.org/tzmedical/trident-sdk/commit/117c9fb23014bfe36076b318c3ad8b95970c644f))
* serial number check fixed ([42030c5](https://bitbucket.org/tzmedical/trident-sdk/commit/42030c5045a4cca23009e9ec741dd8bdc84cf58c))
* upload.log implemented to avoid scp repeats ([019bc5b](https://bitbucket.org/tzmedical/trident-sdk/commit/019bc5bda7c764ec16e5196f8af5a7e9978dee08))

### [3.1.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.1.0...v3.1.1) (2021-06-29)


### Bug Fixes

* the toText functions now respect the `tag` argument, including HR calculation outputs ([adfcc3c](https://bitbucket.org/tzmedical/trident-sdk/commit/adfcc3c0f5e6a2f74c08eab9b371ea732083f192))

## [3.1.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.7...v3.1.0) (2021-06-25)


### Features

* added accel parser ([eff7a04](https://bitbucket.org/tzmedical/trident-sdk/commit/eff7a043ee2d68b7793b3de2d7a860b330ef4fed))


### Bug Fixes

* **cli:** fixing tzg references in cli options ([2731cde](https://bitbucket.org/tzmedical/trident-sdk/commit/2731cde0e95b5f84c3950b7376679709be4edb56))
* **tzg:** checking format identifier before blindly allocating memory ([90d6c48](https://bitbucket.org/tzmedical/trident-sdk/commit/90d6c484049cf4685cc7bc69deb026a000e6fffb))
* **tzg:** initializing timestamp object for safer cross-platform use ([4eedb4d](https://bitbucket.org/tzmedical/trident-sdk/commit/4eedb4d8f7f5bff3b142fc59119fb6f930bef6da))
* I don't know why your tzg file wasn't working. This one seems to ([0052744](https://bitbucket.org/tzmedical/trident-sdk/commit/005274481d8f9ddbe158c31689740f775a9b4ec6))

### [3.0.7](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.6...v3.0.7) (2021-06-23)


### Bug Fixes

* **tzs:** fixing generation of files with settings that have changed meaning between versions ([b7d7816](https://bitbucket.org/tzmedical/trident-sdk/commit/b7d78160ce385e1ab6b69a7899621bc77904d58d))

### [3.0.6](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.5...v3.0.6) (2021-06-07)


### Bug Fixes

* actual settings tool-tips updated ([726a5b8](https://bitbucket.org/tzmedical/trident-sdk/commit/726a5b8a78307c53b735bc44acbbbdd34ecba447))
* settings tool-tips updated with new min notification times ([3c3c9b9](https://bitbucket.org/tzmedical/trident-sdk/commit/3c3c9b98ed8e3af6223f6aa84521adcd399a1877))

### [3.0.5](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.4...v3.0.5) (2021-06-01)

### [3.0.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.3...v3.0.4) (2021-05-28)


### Bug Fixes

* **tzs.totext:** fixing issue with tzmr_compat setting ([ba52118](https://bitbucket.org/tzmedical/trident-sdk/commit/ba521185b740ab762c56f9d76f6280be641c96ad))

### [3.0.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.2...v3.0.3) (2021-05-27)


### Bug Fixes

* **tzs:** fixing generation and parsing when tzmr_compat setting is involved ([4a7ed5e](https://bitbucket.org/tzmedical/trident-sdk/commit/4a7ed5eb077bac83adfe54ebba095c639c67da71))

### [3.0.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.1...v3.0.2) (2021-05-27)


### Bug Fixes

* **tza.fromtext:** fixing alignment issue when generating a TZA with an invalid deviceId field ([25359a5](https://bitbucket.org/tzmedical/trident-sdk/commit/25359a525775b7c56a234e304c276048a0c025b9))

### [3.0.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v3.0.0...v3.0.1) (2021-05-27)

## [3.0.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v2.1.0...v3.0.0) (2021-05-25)


### ⚠ BREAKING CHANGES

* **tzs.metajson:** tzs.metaJson() now reutrns an array instead of a map.

TC-274

### Features

* adding tzs.fromJson() function ([df233b2](https://bitbucket.org/tzmedical/trident-sdk/commit/df233b278639f02cb55b2b515c9ca06715f19ff3))


### Bug Fixes

* **tzs.metajson:** handling multiple settings (for different versions) with the same id ([d7690ec](https://bitbucket.org/tzmedical/trident-sdk/commit/d7690ec3ee4fc465c6bc5f4187aaf8a047b938fa))

## [2.1.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v2.0.0...v2.1.0) (2021-05-21)


### Features

* **tzs:** adding mctOnly metadata to tzs.metaJson() ([5f995a4](https://bitbucket.org/tzmedical/trident-sdk/commit/5f995a421686097bcd0cff350157e018aec5587e))
* **tzs:** adding metaJson support for tzs files ([5926a44](https://bitbucket.org/tzmedical/trident-sdk/commit/5926a443f61f59be2c4d1960d993e27b64c2aecc))


### Bug Fixes

* **tzs:** fixing typos in metaJson() outputs ([50fda7f](https://bitbucket.org/tzmedical/trident-sdk/commit/50fda7f12cc4e8351189d27849db707d2282d74a))
* addressing PR comment and removing some unused functions ([d9afcab](https://bitbucket.org/tzmedical/trident-sdk/commit/d9afcaba49d0aad7d0d045a62dd0b5a80cd32e02))

## [2.0.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.11.3...v2.0.0) (2021-05-20)


### ⚠ BREAKING CHANGES

* **tzs:** tzs.toText will now output true/false for boolean settings, rather than 1/0

TC-274

### Features

* **tzs:** adding toJson support for tzs files ([d117e06](https://bitbucket.org/tzmedical/trident-sdk/commit/d117e060aa876f74950195931e2e971c6ac57c15))
* **tzs:** using true/false for boolean settings, rather than 1 or 0 ([65d6eab](https://bitbucket.org/tzmedical/trident-sdk/commit/65d6eab5007d56c36580ff0ea7d14dc0cb344571))

### [1.11.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.11.2...v1.11.3) (2021-05-20)

### [1.11.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.11.1...v1.11.2) (2021-05-17)

### [1.11.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.11.0...v1.11.1) (2021-05-11)

## [1.11.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.10.0...v1.11.0) (2021-05-10)


### Features

* added interval parser ([f86fa54](https://bitbucket.org/tzmedical/trident-sdk/commit/f86fa548e8f1e92306cd4691228b3d8b2ebd926e))
* added interval to json ([8a77261](https://bitbucket.org/tzmedical/trident-sdk/commit/8a77261da2866745e4f960cee7d7568f7d44265e))


### Bug Fixes

* removed typo'd file ([b1f13e3](https://bitbucket.org/tzmedical/trident-sdk/commit/b1f13e37b2518c538c2ba4d571e0bee79e32511e))

## [1.10.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.9.1...v1.10.0) (2021-05-05)


### Features

* added optional extraArgs, which can be passed to addons as an object ([7f6915a](https://bitbucket.org/tzmedical/trident-sdk/commit/7f6915ad77700438e4cc274de5b21101df2de4bd))

### [1.9.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.9.0...v1.9.1) (2021-05-03)

## [1.9.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.7.0...v1.9.0) (2021-05-03)


### Features

* added drive check to utils.ejectDevice ([358cfbd](https://bitbucket.org/tzmedical/trident-sdk/commit/358cfbddd68a2bff1864029bc2441dbf662ad80b))
* added drive ejection, to use call trident_sdk.ejectDrive("Drive Letter") ([7b86859](https://bitbucket.org/tzmedical/trident-sdk/commit/7b868590fe114a9a9ade318d7f391caa526a93e1))
* added simple sanity-check tests to drive ejection ([59e37c1](https://bitbucket.org/tzmedical/trident-sdk/commit/59e37c1e14ddda741f223df9306880d17dfc867c))
* refactored settings, so they're easier to read/edit ([8a701ab](https://bitbucket.org/tzmedical/trident-sdk/commit/8a701ab6f7361f3c346b773dd538afc235037a02))


### Bug Fixes

* **setting_list.cpp:** removed dependancy on the latest version oc c++ ([69cfafe](https://bitbucket.org/tzmedical/trident-sdk/commit/69cfafe97a668f7cfe8af4a24b4b647d47024658))
* added "default_settings.cpp" to makefile for settings generator and parser ([c0ebf1f](https://bitbucket.org/tzmedical/trident-sdk/commit/c0ebf1f6d0b4f1eb2c5086e4aebc3acb89c7c1ea))
* added all output streams to ejectDrive ([0b9f30b](https://bitbucket.org/tzmedical/trident-sdk/commit/0b9f30b370d4a92775b7b49cb0f35203951f13ca))
* added blank drive eject files for linux and mac ([fca2311](https://bitbucket.org/tzmedical/trident-sdk/commit/fca2311a8eddc7a6ef4e6b6e0e6b4b2bc053d9f6))
* added new files ([65dd0fd](https://bitbucket.org/tzmedical/trident-sdk/commit/65dd0fdb784d7e1b0dc1774df7849abd5924f52d))
* added path normalization to util functions ([79ababc](https://bitbucket.org/tzmedical/trident-sdk/commit/79ababc52b1f4a4606d23929d0db19334bac5072))
* added toString on path values ([4622d31](https://bitbucket.org/tzmedical/trident-sdk/commit/4622d31b9ca36a2d01461544634db3514ddd2387))
* changed "default_settings" to "setting_list" ([6a62371](https://bitbucket.org/tzmedical/trident-sdk/commit/6a62371ffa310f56df68f7154ca9b97caf773d56))
* comments for what arguments we could accept were swapped ([dd790f6](https://bitbucket.org/tzmedical/trident-sdk/commit/dd790f694ffddc30d1abfba1b77a03db9f0fd946))
* parse strings weren't formatted correctly ([07d43cf](https://bitbucket.org/tzmedical/trident-sdk/commit/07d43cfa67befee9468b789cb95489d8c9995015))
* removed 'only' from tests ([d97e11c](https://bitbucket.org/tzmedical/trident-sdk/commit/d97e11c7c273f1db75fa95bc10552efae9eff88d))
* updated drive for one of the tests ([cfbc1c0](https://bitbucket.org/tzmedical/trident-sdk/commit/cfbc1c0ee47db13a2852026d13614b0229b36bee))

## [1.7.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.6.1...v1.7.0) (2021-04-27)


### Features

* started adding interval processors, parser has segfault when accessing the instream ([f069d8e](https://bitbucket.org/tzmedical/trident-sdk/commit/f069d8ebf3456e0770ba7540ecbdcb3b2aa835d8))


### Bug Fixes

* re-adding the postinstall hook so we can merge this to master ([0db520d](https://bitbucket.org/tzmedical/trident-sdk/commit/0db520dc979211d1166cf832690b53a734442342))
* **husky:** fixing husky hook definitions to match the v6 changes ([0772bd3](https://bitbucket.org/tzmedical/trident-sdk/commit/0772bd3d3f6f2e58e41c5e93d55e424df176d8f7))
* **index.js:** changed how/when we print errors to the js console ([730f7e5](https://bitbucket.org/tzmedical/trident-sdk/commit/730f7e50823dad56a97d523f83ba822c8b410a23))
* updated index.js to remove compile.sh and ensure node-gyp configure is run ([db63caf](https://bitbucket.org/tzmedical/trident-sdk/commit/db63caf5f328e382ba8bbc5a55e4fba102b91a23))

### [1.6.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.6.0...v1.6.1) (2021-04-26)


### Bug Fixes

* adding cerr/clog outputs when using Napi implmentations ([d695a08](https://bitbucket.org/tzmedical/trident-sdk/commit/d695a08aa22483ff19f6d7363a2eb9630aa2e6ef))

## [1.6.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.5.1...v1.6.0) (2021-04-22)


### Features

* **interval_generator.cpp:** added interval_generator to addon ([72ba081](https://bitbucket.org/tzmedical/trident-sdk/commit/72ba081389a20be86650118b9ec416dbf3603341))
* added header files for the rest of the processors ([ab1fa87](https://bitbucket.org/tzmedical/trident-sdk/commit/ab1fa87258af2d2c747086427ef6a651951a6f9b))
* added more processors. event_generator is still broken ([88e2158](https://bitbucket.org/tzmedical/trident-sdk/commit/88e215803b73fd185e9cadf3d99f267a7274dd00))


### Bug Fixes

* found the real culprit, a static buffer that wasn't being reset to 0 ([8cfc3f3](https://bitbucket.org/tzmedical/trident-sdk/commit/8cfc3f3cb4e2ed5ebedc892837b3a7662bbced60))
* string::copy doesn't copy null terminators, which was leading to some problems ([ed61ecb](https://bitbucket.org/tzmedical/trident-sdk/commit/ed61ecb3a18f3e2b07e6df9f52f314657c87c3ca))
* **event_generator.cpp:** now using stream parser instead of string buffer ([becf576](https://bitbucket.org/tzmedical/trident-sdk/commit/becf5766d0d6033fc2fcf14cf65b3fbcb1a4b231))
* **index.js:** eslint cleanup ([c188497](https://bitbucket.org/tzmedical/trident-sdk/commit/c188497d8f558256bc51b3efa092e2c05d7bf67c))
* **index.js:** merge conflicts weren't resolved cleanly the first time ([9dc5518](https://bitbucket.org/tzmedical/trident-sdk/commit/9dc55180a2e7d7a33cb52e2c85bc1fff29601e2b))
* **interval_generator.cpp:** reverted interval generator ([923befc](https://bitbucket.org/tzmedical/trident-sdk/commit/923befc6d06227314bb126718236e99819b36a25))
* added stream libraries to node_addon.cpp ([34934aa](https://bitbucket.org/tzmedical/trident-sdk/commit/34934aac08b6e3ea40a5530184209d4b70330e72))
* cleaned up event generator ([baab672](https://bitbucket.org/tzmedical/trident-sdk/commit/baab67257a4bf32e21ff9febe0d1810fbcc0ea26))
* json parsing now works properly ([c039710](https://bitbucket.org/tzmedical/trident-sdk/commit/c039710a7424708622c707e38a58167c622aea1b))
* merged master and resolved conflicts ([ac8e67e](https://bitbucket.org/tzmedical/trident-sdk/commit/ac8e67e87ba506c5bc358b4d09dd6152d6217a0e))
* removed compile scripts ([b1fd89e](https://bitbucket.org/tzmedical/trident-sdk/commit/b1fd89e707e98cc9562c410cdae97255bed35ca7))
* removed duplicated code, refactored header files ([54e90ff](https://bitbucket.org/tzmedical/trident-sdk/commit/54e90ff79d1fc647ca807b313fe00503c2eaeb4d))
* removed header files ([2b4cf48](https://bitbucket.org/tzmedical/trident-sdk/commit/2b4cf48c748825896fa9dccfa32d10553ea5c169))
* **event_generator.cpp:** can now properly generate events from json files ([c83fe02](https://bitbucket.org/tzmedical/trident-sdk/commit/c83fe02643a838b381f762d0d6ff71dd247631a8))
* updated error parser with clog ([627d35d](https://bitbucket.org/tzmedical/trident-sdk/commit/627d35d368618715be3d70ca1c0e9261de8de90a))
* updated event processors with clog ([71ba91d](https://bitbucket.org/tzmedical/trident-sdk/commit/71ba91d214154f4f489a2bd77f79860012542d8a))

### [1.5.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.5.0...v1.5.1) (2021-04-19)

## [1.5.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.4.0...v1.5.0) (2021-04-13)


### Features

* added actions generator/parser ([f784135](https://bitbucket.org/tzmedical/trident-sdk/commit/f7841355dc88c1ab0c1805dbe5897a53ae5cdb1f))
* added backup generator and parser ([cbad810](https://bitbucket.org/tzmedical/trident-sdk/commit/cbad8106e01b82cbafbb457b284b0fb81a504c9c))
* added clog handling ([de993a7](https://bitbucket.org/tzmedical/trident-sdk/commit/de993a715a187a85482379fa8b85fa5612c5bf54))
* added define USE_MAIN to makefiles ([18d5eed](https://bitbucket.org/tzmedical/trident-sdk/commit/18d5eede7029dd1c2cef76d44507c4e982d7b536))
* added settings generator ([31d492b](https://bitbucket.org/tzmedical/trident-sdk/commit/31d492bf6dbc2caee933087a08d0a51e7e2898a6))
* adding addon functions to module exports ([aca60e5](https://bitbucket.org/tzmedical/trident-sdk/commit/aca60e5a75539da47dbb9346356dddb5c8e5bdc0))
* addon now returns a json object containing a success flag and both logs ([632edeb](https://bitbucket.org/tzmedical/trident-sdk/commit/632edeb26af099cbfdfd7d8344b8366ec2410ba4))
* initial node addon support, currently implements settings parser ([19a34c0](https://bitbucket.org/tzmedical/trident-sdk/commit/19a34c0e07240388f3a7bcd60402f3e050e93060))
* moved node addon functions to their own folder instead of the settings folder ([cb0bb32](https://bitbucket.org/tzmedical/trident-sdk/commit/cb0bb3205bb79b68955b32a10d70f89142c567ee))
* updated node-api functions to be compatible with current tests ([b9df9e4](https://bitbucket.org/tzmedical/trident-sdk/commit/b9df9e43e26c48f367027307c4f4fb423c39d763))


### Bug Fixes

* changed memcpy to string::copy, removed electron-rebuild ([5feba6b](https://bitbucket.org/tzmedical/trident-sdk/commit/5feba6b65a52d8d75908b3e9c034f9979c0b86fc))
* moved some windows os checks around ([eed6241](https://bitbucket.org/tzmedical/trident-sdk/commit/eed6241e7dc7ef9b05015334f5b99623aa7389d3))
* removed unnecessary `ios::binary` flags ([cd670bf](https://bitbucket.org/tzmedical/trident-sdk/commit/cd670bf8cbeef207e8f9e042aa8073817819e68e))
* **addon_utils.cpp:** replaced malloc/free with the more c++ standard new/delete ([39425f4](https://bitbucket.org/tzmedical/trident-sdk/commit/39425f4c11152fe593b9bd59314c326cbc47d47d))
* **package.json:** add node-gyp configure to pre-install ([362739e](https://bitbucket.org/tzmedical/trident-sdk/commit/362739e2a778c8dc1593ff44f435f25090fc3c6a))
* **setting.cpp:** re-routing error stream ([78eed8b](https://bitbucket.org/tzmedical/trident-sdk/commit/78eed8bdea0d0ef05806c14eff0ad1fa45cf8a62))
* general cleanup from comments ([33acc62](https://bitbucket.org/tzmedical/trident-sdk/commit/33acc62f7f8915664faa3017b3c9aee6a3c4b3e8))
* was printing stray newline to console after each file processed ([6813b7f](https://bitbucket.org/tzmedical/trident-sdk/commit/6813b7f623455b6ab91766445f252285462521bc))
* **index.js:** fixed how we were getting the reference to the addon ([11fded9](https://bitbucket.org/tzmedical/trident-sdk/commit/11fded924a84adc8e3a5223d47bc088d0ffbb280))
* **index.js:** only print the error log if the error log isn't empty ([24215dc](https://bitbucket.org/tzmedical/trident-sdk/commit/24215dcb5187dac0c883e0bd80689f4cd4526dc4))
* **package.json:** add node-gyp rebuild to install script ([de3538e](https://bitbucket.org/tzmedical/trident-sdk/commit/de3538e1b7c544bf15342ce4f00107ac131655cf))
* **package.json:** removing compile script ([c1308c7](https://bitbucket.org/tzmedical/trident-sdk/commit/c1308c7c5d0e5cd122b3202dc338618d6da42869))
* **settings_parser.cpp:** invalid crc no longer returns a success status ([35c5396](https://bitbucket.org/tzmedical/trident-sdk/commit/35c5396d500f5656b807924f6e3129ac66dcfc59))
* adding new files for gyp support ([3bc39cf](https://bitbucket.org/tzmedical/trident-sdk/commit/3bc39cf3b24396836b808a22cf8f5ebeb53c7521))
* function prototype needed to be in a namespaced block ([1e8328a](https://bitbucket.org/tzmedical/trident-sdk/commit/1e8328ae33c03046807f2d9b6261ed782c732f0f))
* general code cleanup ([d62d08d](https://bitbucket.org/tzmedical/trident-sdk/commit/d62d08d60e6bab8a979267437f44a9b9980a8b52))
* more explicit include paths to satisfy node-gyp ([9cdf671](https://bitbucket.org/tzmedical/trident-sdk/commit/9cdf6711bac922d75b6b5c7effecc8842fa94141))
* renamed file re-added ([8f372fd](https://bitbucket.org/tzmedical/trident-sdk/commit/8f372fdf48f752ca1f3fad46d9db0fc677c5292b))
* updated makefiles to define "USE_MAIN" for g++ compiling ([a31095b](https://bitbucket.org/tzmedical/trident-sdk/commit/a31095bc1d59da579314a947050267fc3ff7a80c))
* **settings_parser.cpp:** removed un-needed include of node.h ([27008b5](https://bitbucket.org/tzmedical/trident-sdk/commit/27008b5469853100a94f2eea96de751b6f668a5e))
* **trident_sdk_addon.h:** removed some un-needed function prototypes ([fcc0e7c](https://bitbucket.org/tzmedical/trident-sdk/commit/fcc0e7c1593e56fc1f8b04fd78d730562eb5b9a4))
* convert strings to buffers before passing them to the c++ addon ([8555210](https://bitbucket.org/tzmedical/trident-sdk/commit/85552109d4ac20ee04210e10daa073a8f089a391))
* merged master and fixed merge conflicts ([296a363](https://bitbucket.org/tzmedical/trident-sdk/commit/296a363bf8651808a1634e0cb66fb6baf4c62f67))

## [1.4.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.3.0...v1.4.0) (2021-03-08)


### Features

* **index.js:** add support for object, string, and buffer data to generate from json ([c3eb3f3](https://bitbucket.org/tzmedical/trident-sdk/commit/c3eb3f3d4cd1e2e7aed4f60b3d7278a2bb52b60b))
* **index.js:** add support to parser for accepting string data ([af21616](https://bitbucket.org/tzmedical/trident-sdk/commit/af21616deb0088bebb12c5c079e529086e135bfa))

## [1.3.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.2.4...v1.3.0) (2021-02-18)


### Features

* **error_to_json:** adding multiline error parsing for error.log files ([5313e49](https://bitbucket.org/tzmedical/trident-sdk/commit/5313e49ac61ad2cf207a7a08a3e0cbc2bb10c68e))

### [1.2.4](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.2.3...v1.2.4) (2021-02-09)

### [1.2.3](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.2.2...v1.2.3) (2021-01-27)


### Bug Fixes

* fixing JSON output when an invalid value for a charData event is parsed ([36ac1d7](https://bitbucket.org/tzmedical/trident-sdk/commit/36ac1d768006fa7cceaa422e942639ef7774917c))

### [1.2.2](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.2.1...v1.2.2) (2021-01-23)


### Bug Fixes

* setting fixed width to 4 instead of 3 ([c7fe399](https://bitbucket.org/tzmedical/trident-sdk/commit/c7fe399ae364e5613728cbe5053859ab57e6943e))
* tzmr ations parsing and fixed-length float values ([1014431](https://bitbucket.org/tzmedical/trident-sdk/commit/10144318fd8d836d210d5d5537668ba147312a22))

### [1.2.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.2.0...v1.2.1) (2021-01-05)


### Bug Fixes

* **hpr:** fixing length checking for the study_hours setting ([74ff9c6](https://bitbucket.org/tzmedical/trident-sdk/commit/74ff9c68457860b587b0ad15292b869468948a36))

## [1.2.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.1.1...v1.2.0) (2020-12-17)


### Features

* adding TZMR support for events and intervals ([130b80d](https://bitbucket.org/tzmedical/trident-sdk/commit/130b80d2236d527fcfc55c37f99d7d427f3cd669))


### Bug Fixes

* fixing data length usage when parsing binary settings ([1a605dd](https://bitbucket.org/tzmedical/trident-sdk/commit/1a605dd04b9369e4146a8c789af7489d49979269))
* fixing errors found while parsing settings files and searching events ([fce9522](https://bitbucket.org/tzmedical/trident-sdk/commit/fce9522130dc9673b5764508f1d6bdef3ddcc244))

### [1.1.1](https://bitbucket.org/tzmedical/trident-sdk/compare/v1.1.0...v1.1.1) (2020-12-16)

## [1.1.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v0.2.0...v1.1.0) (2020-12-11)


### Features

* adding generation and parsing of TZMR-formatted settings files ([9cb7c40](https://bitbucket.org/tzmedical/trident-sdk/commit/9cb7c404820ae358ccfef7d08ed7c203401bb28a))

## [1.0.0](https://bitbucket.org/tzmedical/trident-sdk/compare/v0.2.0...v1.0.0) (2020-12-09)
