/* eslint-env node */
/* eslint-disable max-classes-per-file */
/* eslint-disable no-console */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const binary = require('@mapbox/node-pre-gyp')
const path = require('path')

const bindingPath = binary.find(path.resolve(path.join(__dirname, './package.json')))
// eslint-disable-next-line import/no-dynamic-require
const tridentSdk = require(bindingPath)
const fs = require('fs')
const { DateTime } = require('luxon')
const util = require('util')
const { errorLogToJson } = require('./error/error_to_json/error_to_json')
const exec = util.promisify(require('child_process').exec)

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
function dataToBuffer(data) {
  if (Buffer.isBuffer(data)) return data
  if (typeof data === 'string') return Buffer.from(data, 'binary')
  return Buffer.from(JSON.stringify(data), 'binary')
}

// ==============================================================================
function timezoneToOffset(zoneName = DateTime.local().zoneName) {
  // Handle numeric offset inputs
  let inputZone = zoneName
  if (inputZone.toString().match(/^[-+]?[0-9]+$/)) {
    const zoneNumber = Number(inputZone)
    const hours = Math.floor(Math.abs(zoneNumber) / 60)
    const minutes = Math.abs(zoneNumber) % 60
    if (zoneNumber >= 0) {
      inputZone = `UTC+${hours}:${minutes.toString().padStart(2, '0')}`
    } else {
      inputZone = `UTC-${hours}:${minutes.toString().padStart(2, '0')}`
    }
  }

  // Throw an error on invalid zone inputs
  const datetime = DateTime.now().setZone(inputZone, { keepLocalTime: true })
  if (Number.isNaN(datetime.offset)) {
    throw new Error(`Invalid timezone value: ${zoneName}`)
  }
  return datetime.offset.toString()
}

// ==============================================================================
function napiParseToText(apiFunction, data, extraArgs = {}) {
  return new Promise((resolve, reject) => {
    const bufferedData = dataToBuffer(data)
    const outputData = apiFunction(bufferedData, {
      ...extraArgs,
      ...{ zoneOffset: timezoneToOffset(extraArgs.zone) },
    })

    if (outputData.log && outputData.log.length > 0) {
      console.log(outputData.log)
    }

    if (outputData.status === 0) {
      if (outputData.error && outputData.error.length > 0) {
        console.error(outputData.error)
      }

      resolve(outputData.output.toString())
    } else {
      const trimmedError = outputData.error.replace(/[\n\r]+$/, '')
      reject(new Error(`${trimmedError} (${outputData.status})`))
    }
  })
}

// ==============================================================================
function napiGenerateFile(apiFunction, data, extraArgs = {}) {
  return new Promise((resolve, reject) => {
    const bufferedData = dataToBuffer(data)
    const outputData = apiFunction(bufferedData, extraArgs)

    if (outputData.log && outputData.log.length > 0) {
      console.log(outputData.log)
    }

    if (outputData.status === 0) {
      if (outputData.error && outputData.error.length > 0) {
        console.error(outputData.error)
      }

      resolve(outputData.output)
    } else {
      const trimmedError = outputData.error.replace(/[\n\r]+$/, '')
      reject(new Error(`${trimmedError} (${outputData.status})`))
    }
  })
}

// ==============================================================================
async function napiParseToJson(apiFunction, data, extraArgs) {
  const text = await napiParseToText(apiFunction, data, extraArgs)
  return JSON.parse(text)
}

// ==============================================================================
async function napiMetaToJson(apiFunction, data) {
  return new Promise((resolve, reject) => {
    const outputData = apiFunction(data)

    if (outputData.log && outputData.log.length > 0) {
      console.log(outputData.log)
    }

    if (outputData.status === 0) {
      if (outputData.error && outputData.error.length > 0) {
        console.error(outputData.error)
      }

      resolve(JSON.parse(outputData.output.toString()))
    } else {
      const trimmedError = outputData.error.replace(/[\n\r]+$/, '')
      reject(new Error(`${trimmedError} (${outputData.status})`))
    }
  })
}

// ==============================================================================
function napiSearchDirectory(apiFunction, dir, extraArgs) {
  const fileBuffers = []
  const fullDir = dir.endsWith('/') ? dir : `${dir}/`

  return new Promise((resolve, reject) => {
    const bufferedPath = dataToBuffer(fullDir)
    const outputData = apiFunction(bufferedPath, {
      ...extraArgs,
      zoneOffset: timezoneToOffset(extraArgs.zone),
    })

    if (outputData.log && outputData.log.length > 0) {
      console.log(outputData.log)
    }

    if (outputData.status === 0) {
      if (outputData.error && outputData.error.length > 0) {
        console.error(outputData.error)
      }

      fileBuffers.push(outputData.output)
      resolve(Buffer.concat(fileBuffers).toString())
    } else {
      const trimmedError = outputData.error.replace(/[\n\r]+$/, '')
      reject(new Error(`${trimmedError} (${outputData.status})`))
    }
  })
}

// ==============================================================================
function napiParse(apiFunction, dir, extraArgs) {
  const fileBuffers = []
  const fullDir = dir.endsWith('/') ? dir : `${dir}/`

  return new Promise((resolve, reject) => {
    const bufferedPath = dataToBuffer(fullDir)
    const outputData = apiFunction(bufferedPath, extraArgs)

    if (outputData.log && outputData.log.length > 0) {
      console.log(outputData.log)
    }

    if (outputData.status === 0) {
      if (outputData.error && outputData.error.length > 0) {
        console.error(outputData.error)
      }

      fileBuffers.push(outputData.output)
      resolve(JSON.parse(Buffer.concat(fileBuffers).toString()))
    } else {
      const trimmedError = outputData.error.replace(/[\n\r]+$/, '')
      reject(new Error(`${trimmedError} (${outputData.status})`))
    }
  })
}

// ==============================================================================
function hasTZSFiles(devicePath) {
  let tzsCount = 0
  fs.readdirSync(devicePath).forEach((element) => {
    if (path.extname(element) === '.tzs') {
      tzsCount += 1
    }
  })

  return tzsCount === 1
}

// ==============================================================================
async function ejectDeviceWindows(normalPath) {
  if (normalPath !== path.parse(normalPath).root) {
    throw new Error('Invalid device path')
  }

  const outputData = tridentSdk.ejectDrive(normalPath)

  if (outputData.log && outputData.log.length > 0) {
    console.log(outputData.log)
  }

  if (outputData.status === 0) {
    if (outputData.error && outputData.error.length > 0) {
      console.error(outputData.error)
    }
  } else {
    throw new Error(outputData.error)
  }
}

// ==============================================================================
async function ejectDeviceLinux(drivePath) {
  // If there is a trailing slash on the path remove it
  const mtabPath = drivePath.endsWith('/') ? drivePath.slice(0, -1) : drivePath

  // Look for the device identifier in the mount tab
  // If device is not found in mtab, throw an error
  // This functions as a basic error handling or path validation
  const mtab = await exec(`cat /etc/mtab | grep ${mtabPath}`)
  if (mtab.stderr) {
    throw new Error(mtab.stderr)
  } else if (!mtab.stdout) {
    // If grep does not return anything, we probably are looking for
    // a path that is not mounted. Throw an error.
    throw new Error('Path not mounted')
  }

  // Parse out path ex '/dev/sda1' from mtab entry
  // mtab entries are space-delimited and formatted like:
  // <id> <mount point> <file system> <flags and uids/gids> ...
  const mountLine = mtab.stdout.trim().split(' ')[0]

  // Parse drive identifier from device id, ex 'sda1'
  // Get from /dev/sda1 entry, split on slashes
  const driveId = mountLine.split('/')[2]

  // Make a call to the bus to get the gdbus-friendly location
  // of the Trident device, using DBus' Properties.Get method
  // on UDisks2's block_devices object. This returns the name
  // that gdbus will be expecting us to use when we make the
  // second call to power off the device.
  const busLocation = await exec(
    `gdbus call --system --dest org.freedesktop.UDisks2 \
    --object-path /org/freedesktop/UDisks2/block_devices/${driveId} \
    --method org.freedesktop.DBus.Properties.Get \
    org.freedesktop.UDisks2.Block Drive`
  )

  if (busLocation.stderr) {
    throw new Error(busLocation.stderr)
  }

  // The output from the gdbus call will have the path of the Trident
  // device returned in an object representation. The part we want is
  // in between a pair of single quotes, grab that part with split
  const tridentPath = busLocation.stdout.split("'")[1]

  // Unmount the Trident using identifier ex /dev/sda1
  const unmount = await exec(`umount ${mountLine}`)
  if (unmount.stderr) {
    throw new Error(unmount.stderr)
  }

  // Send the PowerOff command over gdbus
  const PowerOff = await exec(
    `gdbus call --system --dest org.freedesktop.UDisks2 \
    --object-path ${tridentPath} \
    --method org.freedesktop.UDisks2.Drive.PowerOff {}`
  )
  if (PowerOff.stderr) {
    throw new Error(PowerOff.stderr)
  }
}

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

// ==============================================================================
class scp {
  static toJson(data, extraArgs = {}) {
    return napiParseToJson(tridentSdk.scpToJson, data, extraArgs)
  }

  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.scpToText, data, extraArgs)
  }

  static fromJson(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.scpFromJson, data, extraArgs)
  }

  static async retrieveFile(directoryOrFilename, sequenceNumber) {
    const bufSize = 32768
    const results = Buffer.alloc(bufSize)

    // If the input is a filename that ends in *.scp, open it
    if (directoryOrFilename.match(/.scp$/)) {
      return fs.readFileSync(directoryOrFilename)
    }

    // If the input is a filename that ends in scpecg.bin, we'll need to access
    // the correct block of it based on the sequence number
    let oneEcgDetected = false
    let fileName = directoryOrFilename
    let directoryName = directoryOrFilename
    if (directoryOrFilename.match(/scpecg.bin$/)) {
      fileName = directoryOrFilename
      oneEcgDetected = true
    } else {
      // If we're at the root of the drive, go into the "ecgs" directory
      let dirContents = fs.readdirSync(directoryName)
      if (dirContents.includes('ecgs')) {
        directoryName = path.join(directoryName, 'ecgs')
        dirContents = fs.readdirSync(directoryName)
      }

      // Now check the directory for the scpecg.bin file
      if (dirContents.includes('scpecg.bin')) {
        fileName = path.join(directoryName, 'scpecg.bin')
        oneEcgDetected = true
      }
    }

    if (oneEcgDetected) {
      if (!fs.existsSync(fileName)) {
        throw new Error(`File not found: ${fileName}`)
      }

      if (fs.statSync(fileName).size < bufSize * (parseInt(sequenceNumber, 10) + 1)) {
        throw new Error(`File does not contain sequence number ${sequenceNumber}`)
      }

      fs.readSync(fs.openSync(fileName), results, 0, bufSize, bufSize * sequenceNumber)
    } else {
      const sequenceValues = [0, 0, 0, 0]
      sequenceValues[0] = Math.floor(sequenceNumber / 1000000) % 1000000
      sequenceValues[1] = Math.floor(sequenceNumber / 10000) % 10000
      sequenceValues[2] = Math.floor(sequenceNumber / 100) % 100
      sequenceValues[3] = sequenceNumber % 100

      const sequenceStrings = ['00', '00', '00', '00']
      sequenceStrings[0] = `00${sequenceValues[0]}`.slice(-2)
      sequenceStrings[1] = `00${sequenceValues[1]}`.slice(-2)
      sequenceStrings[2] = `00${sequenceValues[2]}`.slice(-2)
      sequenceStrings[3] = `00${sequenceValues[3]}`.slice(-2)

      fileName = path.join(
        directoryName,
        sequenceStrings[0],
        sequenceStrings[1],
        sequenceStrings[2],
        `${sequenceStrings[3]}.scp`
      )

      if (!fs.existsSync(fileName)) {
        throw new Error(`File not found: ${fileName}`)
      }

      const scpFile = fs.openSync(fileName)
      fs.readSync(scpFile, results, 0, bufSize, 0)
    }
    return results
  }
}

// ==============================================================================
class tze {
  static toJson(data, extraArgs = {}) {
    return napiParseToJson(tridentSdk.tzeToJson, data, extraArgs)
  }

  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.tzeToText, data, extraArgs)
  }

  static fromJson(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzeFromJson, data, extraArgs)
  }

  static find(dir, extraArgs) {
    return napiSearchDirectory(tridentSdk.tzeFinder, dir, extraArgs)
  }
}

// ==============================================================================
class tzr {
  static toJson(data, extraArgs = {}) {
    return napiParseToJson(tridentSdk.tzrToJson, data, extraArgs)
  }

  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.tzrToText, data, extraArgs)
  }

  static fromJson(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzrFromJson, data, extraArgs)
  }

  static find(dir, extraArgs) {
    return napiSearchDirectory(tridentSdk.tzrFinder, dir, extraArgs)
  }
}

// ==============================================================================
class tzs {
  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.tzsToText, data, extraArgs)
  }

  static fromText(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzsFromText, data, extraArgs)
  }

  static toJson(data, extraArgs = {}) {
    return napiParseToJson(tridentSdk.tzsToJson, data, extraArgs)
  }

  static fromJson(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzsFromJson, data, extraArgs)
  }

  static metaJson(data, extraArgs = {}) {
    return napiMetaToJson(tridentSdk.tzsMetaJson, data, extraArgs)
  }
}

// ==============================================================================
class tza {
  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.tzaToText, data, extraArgs)
  }

  static fromText(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzaFromText, data, extraArgs)
  }

  static toJson(data, extraArgs = {}) {
    return napiParseToJson(tridentSdk.tzaToJson, data, extraArgs)
  }

  static fromJson(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzaFromJson, data, extraArgs)
  }
}

// ==============================================================================
class tzg {
  static toJson(data, extraArgs = {}) {
    return napiParseToJson(tridentSdk.tzgToJson, data, extraArgs)
  }

  static fromJson(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.tzgFromJson, data, extraArgs)
  }
}

// ==============================================================================
class bak {
  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.backupToText, data, extraArgs)
  }

  static fromText(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.backupFromText, data, extraArgs)
  }
}

// ==============================================================================
class batt {
  static toText(data, extraArgs = {}) {
    return napiParseToText(tridentSdk.batteryInfoToText, data, extraArgs)
  }

  static fromText(data, extraArgs = {}) {
    return napiGenerateFile(tridentSdk.batteryInfoFromText, data, extraArgs)
  }
}

// ==============================================================================
class queue {
  static parse(dir, extraArgs = {}) {
    return napiParse(tridentSdk.parseQueue, dir, extraArgs)
  }
}

// ==============================================================================
class error {
  static async toJson(data, extraArgs = {}) {
    const results = errorLogToJson(data.toString(), extraArgs.zone)
    if (results.length === 0) {
      throw new Error('No parsable logs found!')
    }
    return results
  }
}

// ==============================================================================
class utils {
  static toJson(fileName, data, extraArgs = {}) {
    if (!fileName) {
      throw new Error('A filename MUST be specified to use utils.toJson()!')
    }
    if (fileName.endsWith('.scp')) {
      return scp.toJson(data, extraArgs)
    }
    if (fileName.endsWith('.tzr')) {
      return tzr.toJson(data, extraArgs)
    }
    if (fileName.endsWith('.tze')) {
      return tze.toJson(data, extraArgs)
    }
    if (fileName.endsWith('.tzs')) {
      return tzs.toJson(data, extraArgs)
    }
    if (fileName.endsWith('.tza')) {
      return tza.toJson(data, extraArgs)
    }
    if (fileName.endsWith('.tzg')) {
      return tzg.toJson(data, extraArgs)
    }
    if (fileName.endsWith('.log')) {
      return error.toJson(data, extraArgs)
    }
    throw new Error('Unsupported file type!')
  }

  static fromJson(fileJson) {
    if (fileJson.format === 'SCPECG') {
      return scp.fromJson(fileJson)
    }
    if (fileJson.format === 'TZINT') {
      return tzr.fromJson(fileJson)
    }
    if (fileJson.format === 'TZEVT') {
      return tze.fromJson(fileJson)
    }
    if (fileJson.format === 'TZSET') {
      return tzs.fromJson(fileJson)
    }
    if (fileJson.format === 'TZACT') {
      return tza.fromJson(fileJson)
    }
    if (fileJson.format === 'TZACC') {
      return tzg.fromJson(fileJson)
    }
    throw new Error('Unsupported file type!')
  }

  static async ejectDevice(devicePath, checkValid = true) {
    const normalPath = path.normalize(devicePath.toString())

    if (checkValid && !hasTZSFiles(normalPath)) {
      throw new Error('No configuration found, not ejecting')
    }

    const { platform } = process

    switch (platform) {
      case 'linux':
        await ejectDeviceLinux(normalPath)
        break

      case 'win32':
        await ejectDeviceWindows(normalPath)
        break
      default:
        throw new Error(`${platform} does not support Trident device ejection`)
    }
  }
}

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

module.exports = {
  scp,
  tzr,
  tze,
  tzs,
  tza,
  tzg,
  bak,
  batt,
  queue,
  error,
  utils,
}
