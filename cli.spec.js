/* eslint-env node, mocha */
/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const helper = require(`./util/specHelper.js`)
const path = require('path')
const { expect } = require('chai')
const fs = require('fs')

const util = require('util')
const { exec } = require('child_process')

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const execPromise = util.promisify(exec)

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
function cli(args, cwd) {
  return execPromise(`node ${path.resolve('./cli')} ${args}`, { cwd }).then(
    (result) => result.stdout
  )
}

// ==============================================================================
function testCliToText(inputFileName, outputFileName, done, argString = '') {
  const outputText = fs.readFileSync(outputFileName)
  const filteredOutput = helper.splitToFilteredArray(outputText)

  cli(`toText ${inputFileName} ${argString}`, '.')
    .then((result) => {
      const filteredResult = helper.splitToFilteredArray(result)
      expect(filteredResult).deep.to.equal(filteredOutput)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliToJson(inputFileName, outputFileName, done) {
  const outputJson = require(outputFileName)

  cli(`toJson ${inputFileName}`, '.')
    .then((result) => {
      expect(JSON.parse(result)).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliMetaJson(inputDeviceType, inputFileType, outputFileName, done) {
  const outputJson = require(`${__dirname}/${outputFileName}`)

  cli(`metaJson ${inputDeviceType} -f ${inputFileType}`, '.')
    .then((result) => {
      expect(JSON.parse(result)).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliFind(inputFileName, outputFileName, done) {
  const outputText = fs.readFileSync(outputFileName)
  const filteredOutput = helper.splitToFilteredArray(outputText)
  const cleanedOutput = helper.removeFilePaths(filteredOutput).sort()

  cli(`find ${inputFileName}`, '.')
    .then((result) => {
      const filteredResult = helper.splitToFilteredArray(result)
      const cleanedResult = helper.removeFilePaths(filteredResult).sort()
      expect(cleanedResult).deep.to.equal(cleanedOutput)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliFromJsonSeparate(inputJsonFilePath, outputFileName, outputJsonFilePath, done) {
  const outputJson = require(outputJsonFilePath)
  cli(`fromJson ${inputJsonFilePath} ${outputFileName}`, '.')
    .then(() => {
      return cli(`toJson ${outputFileName}`, '.')
    })
    .then((result) => {
      delete outputJson.crc
      const parsedResult = JSON.parse(result)
      delete parsedResult.crc
      expect(parsedResult).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliFromJson(inputJsonFilePath, outputFileName, done) {
  testCliFromJsonSeparate(inputJsonFilePath, outputFileName, inputJsonFilePath, done)
}

// ==============================================================================
function testCliFromText(inputFileName, outputFileName, done, argString = '', parseString = '') {
  const outputText = fs.readFileSync(inputFileName)
  const filteredOutput = helper.splitToFilteredArray(outputText)

  cli(`fromText ${argString} ${inputFileName} ${outputFileName}`, '.')
    .then(() => {
      return cli(`toText ${parseString} ${outputFileName}`, '.')
    })
    .then((result) => {
      const filteredResult = helper.splitToFilteredArray(result)
      expect(filteredResult).deep.to.equal(filteredOutput)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliParse(inputFileName, outputFileName, serialNumber, tzmrCompat, done) {
  const outputJson = require(outputFileName)

  cli(`parse ${inputFileName} -s ${serialNumber} ${tzmrCompat ? ' --tzmr-compat' : ''}`, '.')
    .then((result) => {
      expect(JSON.parse(result)).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testCliRetrieve(fileType, inputDir, expectedFileName, sequenceNumber, done) {
  const tempFileName = `tempFile.${fileType}`
  cli(`retrieveFile -f ${fileType} -s ${sequenceNumber} ${inputDir} ${tempFileName}`, '.')
    .then(() => {
      const expectedRaw = fs.readFileSync(expectedFileName)
      const outputRaw = fs.readFileSync(tempFileName)
      expect(outputRaw).deep.to.equal(expectedRaw)
      done()
    })
    .catch(done)
}

//------------------------------------------------------------------------------
//     ___  __  __ ___  __
//      |  |_  (_   |  (_
//      |  |__ __)  |  __)
//
//------------------------------------------------------------------------------

// ==============================================================================
describe('trident-sdk toText', () => {
  it('should parse a TZA file to text', (done) => {
    testCliToText(
      './actions/actions_parser/h3r_input.tza',
      './actions/actions_parser/h3r_output.txt',
      done
    )
  })

  it('should parse a directory with TZA files to text', (done) => {
    testCliToText(
      './actions/actions_parser',
      './actions/actions_parser/directory_output.txt',
      done,
      '-f tza'
    )
  })

  it('should parse a TZA file to text when an output path is specified', (done) => {
    const inputFileName = './actions/actions_parser/h3r_input.tza'
    const outputFileName = './actions/actions_parser/h3r_output.txt'

    const outputText = fs.readFileSync(outputFileName)
    const filteredOutput = helper.splitToFilteredArray(outputText)

    const testOutput = './tempFile.tza.txt'

    cli(`toText ${inputFileName} ${testOutput}`, '.')
      .then(() => {
        const testOutputFile = fs.readFileSync(testOutput)
        const filteredResult = helper.splitToFilteredArray(testOutputFile)
        expect(filteredResult).deep.to.equal(filteredOutput)
        done()
      })
      .catch(done)
  })

  it('should parse a TZS file to text', (done) => {
    testCliToText(
      './settings/settings_parser/h3r_input.tzs',
      './settings/settings_parser/h3r_output.txt',
      done
    )
  })

  it('should parse a directory with TZS files to text', (done) => {
    testCliToText(
      './settings/settings_parser',
      './settings/settings_parser/directory_output.txt',
      done,
      '-f tzs'
    )
  })

  it('should parse a TZR file to text', (done) => {
    testCliToText(
      './interval/interval_parser/h3r_input.tzr',
      './interval/interval_parser/h3r_output.txt',
      done
    )
  })

  it('should parse a directory with TZR files to text', (done) => {
    testCliToText(
      './interval/interval_parser/',
      './interval/interval_parser/directory_output.txt',
      done,
      '-f tzr'
    )
  })

  it('should parse a TZR file with the epoch argument', (done) => {
    testCliToText(
      './interval/interval_parser/h3r_summarize.tzr',
      './interval/interval_parser/h3r_epoch_ms.txt',
      done,
      '-e 1568174400000'
    )
  })

  it('should parse a TZR file with the tag argument', (done) => {
    testCliToText(
      './interval/interval_parser/h3r_summarize.tzr',
      './interval/interval_parser/h3r_summarize_t19.txt',
      done,
      '-t 19'
    )
  })

  it('should parse a TZR file with an unknown tag', (done) => {
    testCliToText(
      './interval/interval_parser/bad_tag_input.tzr',
      './interval/interval_parser/bad_tag_output.txt',
      done
    )
  })

  it('should parse a TZE file to text', (done) => {
    testCliToText(
      './events/event_parser/h3r_input.tze',
      './events/event_parser/h3r_output.txt',
      done
    )
  })

  it('should parse a directory with TZE files to text', (done) => {
    testCliToText(
      './events/event_parser/',
      './events/event_parser/directory_output.txt',
      done,
      '-f tze'
    )
  })

  it('should parse an SCP file to text', (done) => {
    testCliToText('./scp/scp_parser/h3r_input.scp', './scp/scp_parser/h3r_output.txt', done)
  })

  it('should parse a directory with SCP files to text', (done) => {
    testCliToText('./scp/scp_parser/', './scp/scp_parser/directory_output.txt', done, '-f scp')
  })

  it('should parse a short v1 BAK file to text', (done) => {
    testCliToText(
      './backup/backup_parser/v1_short_input.tmp',
      './backup/backup_parser/v1_output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a long v1 BAK file to text', (done) => {
    testCliToText(
      './backup/backup_parser/v1_long_input.tmp',
      './backup/backup_parser/v1_output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a short v2 BAK file to text', (done) => {
    testCliToText(
      './backup/backup_parser/v2_short_input.tmp',
      './backup/backup_parser/v2_output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a long v2 BAK file to text', (done) => {
    testCliToText(
      './backup/backup_parser/v2_long_input.tmp',
      './backup/backup_parser/v2_output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a short v3 BAK file to text', (done) => {
    testCliToText(
      'backup/backup_parser/v3_short_input.tmp',
      'backup/backup_parser/v3_output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a long v3 BAK file to text', (done) => {
    testCliToText(
      'backup/backup_parser/v3_long_input.tmp',
      'backup/backup_parser/v3_output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a v4 BAK file to text', (done) => {
    testCliToText(
      'backup/backup_parser/input.tmp',
      'backup/backup_parser/output.txt',
      done,
      '--device-type H3R'
    )
  })

  it('should parse a BATT file to text', (done) => {
    testCliToText(
      './battery/battery_info_parser/input.batt',
      './battery/battery_info_parser/output.txt',
      done
    )
  })

  it('should parse a TZA file to text with the format flag', (done) => {
    testCliToText(
      './actions/actions_parser/h3r_input.tza',
      './actions/actions_parser/h3r_output.txt',
      done,
      '-f tza'
    )
  })

  it('should parse a TZS file to text with the format flag', (done) => {
    testCliToText(
      './settings/settings_parser/h3r_input.tzs',
      './settings/settings_parser/h3r_output.txt',
      done,
      '-f tzs'
    )
  })

  it('should parse a TZE file to text with the format flag', (done) => {
    testCliToText(
      './events/event_parser/h3r_input.tze',
      './events/event_parser/h3r_output.txt',
      done,
      '-f tze'
    )
  })

  it('should parse a TZR file to text with the format flag', (done) => {
    testCliToText(
      './interval/interval_parser/h3r_input.tzr',
      './interval/interval_parser/h3r_output.txt',
      done,
      '-f tzr'
    )
  })

  it('should parse an SCP file to text with the format flag', (done) => {
    testCliToText('scp/scp_parser/h3r_input.scp', 'scp/scp_parser/h3r_output.txt', done, '-f scp')
  })

  it('should parse a short v1 BAK file to text with the format flag', (done) => {
    testCliToText(
      './backup/backup_parser/v1_short_input.tmp',
      './backup/backup_parser/v1_output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should parse a long v1 BAK file to text with the format flag', (done) => {
    testCliToText(
      './backup/backup_parser/v1_long_input.tmp',
      './backup/backup_parser/v1_output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should parse a short v2 BAK file to text with the format flag', (done) => {
    testCliToText(
      './backup/backup_parser/v2_short_input.tmp',
      './backup/backup_parser/v2_output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should parse a long v2 BAK file to text with the format flag', (done) => {
    testCliToText(
      './backup/backup_parser/v2_long_input.tmp',
      './backup/backup_parser/v2_output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should parse a short v3 BAK file to text with the format flag', (done) => {
    testCliToText(
      'backup/backup_parser/v3_short_input.tmp',
      'backup/backup_parser/v3_output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should parse a long v3 BAK file to text with the format flag', (done) => {
    testCliToText(
      'backup/backup_parser/v3_long_input.tmp',
      'backup/backup_parser/v3_output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should parse a v4 BAK file to text with the format flag', (done) => {
    testCliToText(
      'backup/backup_parser/input.tmp',
      'backup/backup_parser/output.txt',
      done,
      '-f bak --device-type H3R'
    )
  })

  it('should throw an error when given an unsupported format type', (done) => {
    cli(`toText -f bad ./README.md ./tempFile.bad.txt`, '.')
      .then(() => {
        done(new Error('toText did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should throw an error when given an unsupported file type', (done) => {
    cli(`toText ./README.md ./tempFile.bad.txt`, '.')
      .then(() => {
        done(new Error('toText did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })
})

// ==============================================================================
describe('trident-sdk toJson', () => {
  it('should parse a TZR file to JSON', (done) => {
    testCliToJson(
      './interval/interval_to_json/h3r_input.tzr',
      './interval/interval_to_json/h3r_output.json',
      done
    )
  })

  it('should parse a TZE file to JSON', (done) => {
    testCliToJson(
      './events/event_to_json/h3r_input.tze',
      './events/event_to_json/h3r_output.json',
      done
    )
  })

  it('should parse a TZE file to JSON when an output file is specified', (done) => {
    const outputFileName = 'tempFile.tze.json'
    const outputJson = require(`./events/event_to_json/h3r_output.json`)

    cli(`toJson events/event_to_json/h3r_input.tze ${outputFileName}`, '.')
      .then(() => {
        const outputFile = require(`./${outputFileName}`)
        expect(outputFile).deep.to.equal(outputJson)
        done()
      })
      .catch(done)
  })

  it('should parse a TZA file to JSON', (done) => {
    testCliToJson(
      './actions/actions_to_json/h3r_input.tza',
      './actions/actions_to_json/h3r_output.json',
      done
    )
  })

  it('should parse a TZS file to JSON', (done) => {
    testCliToJson(
      './settings/settings_to_json/h3r_input.tzs',
      './settings/settings_to_json/h3r_output.json',
      done
    )
  })

  it('should parse a TZG file to JSON', (done) => {
    testCliToJson(
      './accel/accel_to_json/hpr_input.tzg',
      './accel/accel_to_json/hpr_output.json',
      done
    )
  })

  it('should parse an SCP file to JSON', (done) => {
    testCliToJson('./scp/scp_to_json/h3r_input.scp', './scp/scp_to_json/h3r_output.json', done)
  })

  it('should parse an error.log file to JSON', (done) => {
    testCliToJson(
      './error/error_to_json/h3r_error.log',
      './error/error_to_json/h3r_error.json',
      done
    )
  })

  it('should allow a timezone input', (done) => {
    testCliToJson(
      './events/event_to_json/h3r_input.tze  -z "America/New_York"',
      './events/event_to_json/h3r_output.json',
      done
    )
  })

  it('should throw an error when given an unsupported format type', (done) => {
    cli(`toJson -f bad ./README.md ./tempFile.bad.json`, '.')
      .then(() => {
        done(new Error('toJson did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should throw an error when given an unsupported file type', (done) => {
    cli(`toJson ./README.md ./tempFile.bad.json`, '.')
      .then(() => {
        done(new Error('toJson did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should parse a TZR file with an unknown tag', (done) => {
    testCliToJson(
      './interval/interval_to_json/bad_tag_input.tzr',
      './interval/interval_to_json/bad_tag_output.json',
      done
    )
  })
})

// ==============================================================================
describe('trident-sdk find', () => {
  it('should parse any found TZR files to text', (done) => {
    testCliFind(
      './interval/interval_finder/h3r_dir -f tzr',
      './interval/interval_finder/h3r_output.txt',
      done
    )
  })

  it('should parse any found TZE files to text', (done) => {
    testCliFind(
      './events/event_finder/h3r_dir -f tze',
      './events/event_finder/h3r_output.txt',
      done
    )
  })

  it('should parse any found TZE files with a specific tag to text', (done) => {
    testCliFind(
      './events/event_finder/h3r_dir -f tze -t 13',
      './events/event_finder/h3r_output_rhythms.txt',
      done
    )
  })

  it('should throw an error on unsupported file type', (done) => {
    cli(`find ./test_drives -f bad`, '.')
      .then(() => {
        done(new Error('find bad file type failed to throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })
})

// ==============================================================================
describe('trident-sdk fromJson', () => {
  it('should generate a TZR file from JSON', (done) => {
    testCliFromJson('./interval/interval_generator/h3r_input.json', 'tempFile.tzr', done)
  })

  it('should generate a TZE file from JSON', (done) => {
    testCliFromJson('./events/event_generator/h3r_input.json', 'tempFile.tze', done)
  })

  it('should generate a TZA file from JSON', (done) => {
    testCliFromJson('./actions/actions_from_json/h3r_input.json', 'tempFile.tza', done)
  })

  it('should generate a TZS file from JSON', (done) => {
    testCliFromJson('./settings/settings_from_json/h3r_input.json', 'tempFile.tzs', done)
  })

  it('should generate a TZG file from JSON', (done) => {
    testCliFromJson('./accel/accel_from_json/hpr_input.json', 'tempFile.tzg', done)
  })

  it('should generate an SCP file from JSON', (done) => {
    testCliFromJson('./scp/scp_generator/h3r_input.json', 'tempFile.scp', done)
  })

  it('should throw an error when given an unsupported format type', (done) => {
    cli(`fromJson -f bad ./README.md ./tempFile.bad`, '.')
      .then(() => {
        done(new Error('fromJson did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should throw an error when given an unsupported file type', (done) => {
    cli(`fromJson ./README.md ./tempFile.bad`, '.')
      .then(() => {
        done(new Error('fromJson did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should generate a TZR file with an unknown tag', (done) => {
    // Cannot use the "testCliFromJson()" as outputJson != inputJson
    testCliFromJsonSeparate(
      './interval/interval_generator/bad_tag_input.json',
      'bad_tag_output.tzr',
      './interval/interval_generator/bad_tag_output.json',
      done
    )
  })
})

// ==============================================================================
describe('trident-sdk metaJson', () => {
  it('should return settings meta-information for H3R devices', (done) => {
    testCliMetaJson('H3R', 'tzs', './settings/h3r_settings.json', done)
  })

  it('should return settings meta-information for HPR devices', (done) => {
    testCliMetaJson('HPR', 'tzs', './settings/hpr_settings.json', done)
  })

  it('should return settings meta-information for TZMR devices', (done) => {
    testCliMetaJson('TZMR', 'tzs', './settings/tzmr_settings.json', done)
  })
})

// ==============================================================================
describe('trident-sdk fromText', () => {
  it('should generate a TZA file from text', (done) => {
    testCliFromText('./actions/actions_generator/h3r_input.txt', 'tempFile.tza', done)
  })

  it('should generate a TZS file from text', (done) => {
    testCliFromText('./settings/settings_generator/h3r_input.txt', 'tempFile.tzs', done)
  })

  it('should generate a v1 BAK file from text', (done) => {
    testCliFromText(
      './backup/backup_generator/v1_input.txt',
      './tempFile.tmp',
      done,
      '',
      '--device-type H3R'
    )
  })

  it('should generate a v2 BAK file from text', (done) => {
    testCliFromText(
      './backup/backup_generator/v2_input.txt',
      './tempFile.tmp',
      done,
      '',
      '--device-type H3R'
    )
  })

  it('should generate a v3 BAK file from text', (done) => {
    testCliFromText(
      'backup/backup_generator/v3_input.txt',
      'tempFile.tmp',
      done,
      '',
      '--device-type H3R'
    )
  })

  it('should generate a v4 BAK file from text', (done) => {
    testCliFromText(
      'backup/backup_generator/input.txt',
      'tempFile.tmp',
      done,
      '',
      '--device-type H3R'
    )
  })

  it('should generate a BATT file from text', (done) => {
    testCliFromText('./battery/battery_info_generator/input.txt', './tempFile.batt', done)
  })

  it('should throw an error when given an unsupported format type', (done) => {
    cli(`fromText -f bad ./README.md ./tempFile.bad`, '.')
      .then(() => {
        done(new Error('fromText did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should throw an error when given an unsupported file type', (done) => {
    cli(`fromText ./README.md ./tempFile.bad`, '.')
      .then(() => {
        done(new Error('fromText did not throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })
})

// ==============================================================================
describe('trident-sdk parse', () => {
  it('should search a directory for wireless files and output in a json format accounting for the upload log', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_upload',
      './queue/queue_to_json/output_upload.json',
      'H3R3000048',
      false,
      done
    )
  })

  it('should search a directory for wireless files and output in a json format with no upload log', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_noUpload',
      './queue/queue_to_json/output_noUpload.json',
      'H3R3000048',
      false,
      done
    )
  })

  it('should search a directory for wireless files without the .ovf and output in a json format', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_noOVF',
      './queue/queue_to_json/output_noOVF.json',
      'H3R3000048',
      false,
      done
    )
  })

  it('should search a directory for wireless files without either .bak or .ovf and output in a json format', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_noBak',
      './queue/queue_to_json/output_noBak.json',
      'H3R3000048',
      false,
      done
    )
  })

  it('should search a directory for wireless files and output in a json format accounting for the upload log in tzmr compat mode', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_upload',
      './queue/queue_to_json/tzmrOutput_upload.json',
      'H3R3000048',
      true,
      done
    )
  })

  it('should search a directory for wireless files and output in a json format with no upload log in tzmr compat mode', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_noUpload',
      './queue/queue_to_json/tzmrOutput_noUpload.json',
      'H3R3000048',
      true,
      done
    )
  })

  it('should search a directory for wireless files without the .ovf and output in a json format in tzmr compat mode', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_noOVF',
      './queue/queue_to_json/tzmrOutput_noOVF.json',
      'H3R3000048',
      true,
      done
    )
  })

  it('should search a directory for wireless files without either .bak or .ovf and output in a json format in tzmr compat mode', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_noBak',
      './queue/queue_to_json/tzmrOutput_noBak.json',
      'H3R3000048',
      true,
      done
    )
  })

  it('should search a directory for wireless files and output in a json format including the error log routes', (done) => {
    testCliParse(
      './queue/queue_to_json/tmp_errorLog',
      './queue/queue_to_json/output_errorLog.json',
      'H3R1234567',
      false,
      done
    )
  })
})

// ==============================================================================
describe('trident-sdk retrieveFile', () => {
  it('should retrieve a scp buffer from a directory with one-ecg disabled', (done) => {
    const expected = fs.readFileSync(`./test_drives/multi_ecg/ecgs/00/00/00/42.scp`)
    fs.writeFileSync('tempFile.scp', expected)
    testCliRetrieve(`scp`, `./test_drives/multi_ecg`, 'tempFile.scp', 42, done)
  })

  it('should retrieve a scp buffer from a directory with one-ecg enabled', (done) => {
    const bufSize = 32768
    const expected = Buffer.alloc(bufSize)

    fs.readSync(
      fs.openSync(`./test_drives/one_ecg/ecgs/scpecg.bin`),
      expected,
      0,
      bufSize,
      bufSize * 42
    )

    fs.writeFileSync('tempFile.scp', expected)
    testCliRetrieve(`scp`, `./test_drives/one_ecg`, 'tempFile.scp', 42, done)
  })

  it('should throw an error if provided with a file type other than scp', (done) => {
    cli(`retrieveFile -f tzr -s 42 ./test_drives/one_ecg tempFile.tzr`, '.')
      .then(() => {
        done(new Error('retrieveFile file type failed to throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Unsupported file type!')
        done()
      })
  })

  it('should throw an error if provided with a sequence number larger than 99999999', (done) => {
    cli(`retrieveFile -f scp -s 100000000 ./test_drives/one_ecg tempFile.scp`, '.')
      .then(() => {
        done(new Error('retrieveFile sequence number failed to throw!'))
      })
      .catch((err) => {
        expect(err.stderr).to.contain('Invalid sequence number!')
        done()
      })
  })
})
