# Lines starting with "#" are comments
# This is an input file for actionsGenerator.exe
#
# Actions with indices can have up to 255 entries per input file.

file.firmware_version=23
file.type=TZACT
file.device_id=H3R
file.serial_number=
file.file_id=7331

action.scp_request.0.enable=true
action.scp_request.0.sequence_number=1234
action.scp_request.0.file_count=4

action.retransmit_scp.0.enable=true
action.retransmit_scp.0.sequence_number=4321
action.retransmit_scp.1.enable=true
action.retransmit_scp.1.sequence_number=1234

action.tzr_request.0.enable=true
action.tzr_request.0.year=2012
action.tzr_request.0.month=4
action.tzr_request.0.day=2
action.tzr_request.0.hour=12

action.tzr_request.1.enable=true
action.tzr_request.1.year=2013
action.tzr_request.1.month=7
action.tzr_request.1.day=31
action.tzr_request.1.hour=22

action.swap_device.hours_offset=1
action.swap_device.sequence_number=96
action.swap_device.patient_id=Swapped Patient

action.update_settings.enable=true

action.send_message=Hello world!

action.error_log.enable=true

action.firmware_update=firmware/h3r_update_010.frw

action.end_study.enable=true

action.start_study.enable=true

action.format_sd.enable=true

action.shutdown.enable=true
