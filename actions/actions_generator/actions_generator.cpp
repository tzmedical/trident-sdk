/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       actions_generator.cpp
 *          - This program generates an actions file for the H3R based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./actions_generator input.txt > output.tza
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/action.h"
#include "util/action_list.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define MAX_SCP_REQUESTS        255
#define MAX_TZR_REQUESTS        255
#define MAX_RETRANSMIT_REQUESTS 9

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

int ActionsFromText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  uint16_t firmwareVersion = 0;
  string serialNumber("");
  string deviceID;
  uint16_t fileID = 0;
  bool badCRC = 0;
  bool badAlignment = 0;
  bool badDeviceId = 0;
  bool badFileId = 0;

  ActionList actionList("");
  vector<_action> outputList;

  // Loop through the entire input
  while (!inStream->eof()) {
    // Get a single line from the input
    string argument;
    getline(*inStream, argument);

    // Trim any leading whitespace
    const size_t begStr = argument.find_first_not_of(" \t");
    if (begStr != string::npos) argument.erase(0, begStr);

    // Trim any trailing whitespace
    const size_t endStr = argument.find_last_not_of(" \n\r\t");
    if (endStr != string::npos) argument.resize(endStr + 1);

    // Skip the line if it is a comment
    if (argument.find("#") == 0) {
      continue;
    }

    // Only process lines that have more than 8 characters in them
    if (argument.length() > 8) {
      // Trim whitespace before or after the "="
      bool done = 0;
      while (!done) {
        size_t found;
        if ((found = argument.find(" =")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("\t=")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("= ")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else if ((found = argument.find("=\t")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else {
          done = 1;
        }
      }

      // Locate and parse any file-related parameters
      if (argument.find("file.") != string::npos) {
        // Make sure the file type is correct
        if (argument.find("type=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.find(ACTIONS_ID_STRING) == string::npos) {
            *errStream << "ERROR! Invalid File ID: " << tempStr << endl;
            return -1;
          }
        }
        // Sets the firmware version embedded in the header of the file
        else if (argument.find("firmware_version=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            stringstream argStream(tempStr);
            argStream >> firmwareVersion;
          }
        }
        // Sets the device is in the header of the file
        else if (argument.find("device_id=") != string::npos) {
          deviceID.assign(argument.substr(argument.find("=") + 1));
          if (deviceID.length() > 5) {
            *errStream << "ERROR! Invalid Device ID: " << argument << endl;
            return -1;
          }
          if (string::npos == deviceID.find("TZMR")) {
            deviceID.resize(5, ' ');
          }

          actionList = ActionList(deviceID);

          if (!actionList.isValid()) {
            *errStream << "ERROR! Unsupported device_id field (" << deviceID << ")." << endl;
            return -1;
          }
        }
        // Sets the serial number embedded in the header of the file
        else if (argument.find("serial_number=") != string::npos) {
          serialNumber.assign(argument.substr(argument.find("=") + 1));
          if (
            serialNumber.length()
            && ((serialNumber.length() < 7) || (serialNumber.length() > 10))) {
            *errStream << "ERROR! Invalid Serial Number: " << argument << endl;
            return -1;
          }
        }
        // Sets the file ID embedded in the header of the file
        else if (argument.find("file_id=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            stringstream argStream(tempStr);
            argStream >> fileID;
          }
        }
        else if (argument.find("use_bad_crc.enable=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badCRC = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badCRC = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: " << argument << endl;
              return -1;
            }
          }
        }
        else if (argument.find("use_bad_align.enable=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badAlignment = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badAlignment = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: " << argument << endl;
              return -1;
            }
          }
        }
        else if (argument.find("use_bad_device_id.enable=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badDeviceId = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badDeviceId = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: " << argument << endl;
              return -1;
            }
          }
        }
        else if (argument.find("use_bad_file_id.enable=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badFileId = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badFileId = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: " << argument << endl;
              return -1;
            }
          }
        }
        // Abort if an invalid file setting is detected
        else {
          *errStream << "ERROR! Unrecognized input: " << argument << endl;
          return -1;
        }
      }
      // Locate and process any actions from the file
      else if (argument.find("action.") != string::npos) {
        // Find any matches with the action array defined at the beginning of
        // this source file.
        if (!actionList.isValid()) {
          *errStream << "ERROR! Please provide device_id prior to actions." << endl;
          return -1;
        }

        retVal = actionList.parseText(argument, firmwareVersion, errStream);
      }
      // Error on unrecognized actions - don't just skip them!
      else {
        *errStream << "ERROR! Unrecognized input: " << argument << endl;
        return -1;
      }
    }
  }

  // The first 6 bytes are length and CRC - add these at the end
  vector<uint8_t> buffer(6, 0);

  if (!badFileId) {
    string actionsID(ACTIONS_ID_STRING);
    binary_append(buffer, actionsID, 6);  // The format identifier string
  }
  else {
    string actionsID("");
    binary_append(buffer, actionsID, 6);  // The format identifier string
  }

  if (!badDeviceId) {
    binary_append(buffer, deviceID, 6);  // The device ID string
  }
  else {
    string badDeviceId("");
    binary_append(buffer, badDeviceId, 6);  // The format identifier string
  }

  binary_append(buffer, firmwareVersion, 1);  // This byte is F. Vers. (*10)

  if (string::npos != deviceID.find("TZMR")) {
    binary_append(buffer, serialNumber, 8);  // The next 8 bytes are the serial number
  }
  else if (string::npos != deviceID.find("H3R")) {
    binary_append(buffer, serialNumber, 11);  // The next 11 bytes are the serial number
  }
  else if (string::npos != deviceID.find("HPR")) {
    binary_append(buffer, serialNumber, 11);  // The next 11 bytes are the serial number
  }
  else {
    *errStream << "Unrecognized Device ID" << endl;
    return -1;
  }
  binary_append(buffer, fileID, 2);  // uint16_t fileId

  actionList.writeActionBinary(buffer, firmwareVersion, badAlignment, &retVal, errStream);
  binary_append(buffer, ACTIONS_TERMINATOR, 1);
  binary_append(buffer, 1, 1);
  binary_append(buffer, 0xff, 1);
  binary_append(buffer, '\0', 1);

  // Finish the length/CRC in the header
  uint32_t length = buffer.size();
  binary_overwrite(buffer, 2, length, 4);

  uint16_t crcValue = 0xffff;
  uint8_t *pBuf = buffer.data();
  if (!badCRC) crcBlock(&pBuf[2], length - 2, &crcValue);
  binary_overwrite(buffer, 0, crcValue, 2);

  // Write the binary file to cout (should be redirected to a file...)
  if (!retVal) {
    outStream->write((const char *)buffer.data(), length);
  }

  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return ActionsFromText(input, extraArgs, &cout, &cerr, &clog);
}
#endif