/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       actions_from_json.cpp
 *          - This program generates a TZA file for the H3R based on the
 *                 values in the input JSON file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./actions_from_json.exe input.json > output.tza
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/json.h"
#include "util/action.h"
#include "util/action_list.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

using namespace std;
using namespace rapidjson;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//=============================================================================
int ActionsFromJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  ActionList actionList("");

  // Parse a JSON string into DOM.
  BasicIStreamWrapper<istream> is(*inStream);
  Document d;
  if (d.ParseStream(is).HasParseError()) {
    *errStream << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at "
               << d.GetErrorOffset() << endl;
    return -1;
  }

  string file_identifier("");
  retVal = common_get_json_file_type(d, file_identifier, errStream);
  if (retVal) {
    return retVal;
  }
  if (file_identifier.find(ACTIONS_ID_STRING) == string::npos) {
    *errStream << "ERROR! Invalid Format ID: " << file_identifier << endl;
    return -1;
  }

  string device_id("");
  retVal = common_get_json_device_type(d, device_id, errStream);
  if (retVal) {
    return retVal;
  }

  actionList = ActionList(device_id);
  if (!actionList.isValid()) {
    *errStream << "ERROR! Unsupported device_id field (" << device_id << ")." << endl;
    return -1;
  }

  string device_serial("");
  retVal = common_get_json_device_serial(d, device_serial, errStream);
  if (retVal) {
    return retVal;
  }

  uint16_t firmwareVersion = 0;
  retVal = common_get_json_firmware_version(d, firmwareVersion, errStream);
  if (retVal) {
    return retVal;
  }

  uint32_t fileIdNumber = 0;
  if (d.HasMember(JSON_FILE_ID_LABEL) && d[JSON_FILE_ID_LABEL].IsInt()) {
    fileIdNumber = d[JSON_FILE_ID_LABEL].GetInt();
  }
  else {
    *errStream << "Missing File ID Field" << endl;
    return -1;
  }

  Value data_array;
  if (d.HasMember(JSON_ACTIONS_LABEL) && d[JSON_ACTIONS_LABEL].IsArray()) {
    data_array = d[JSON_ACTIONS_LABEL].GetArray();

    if (actionList.processJsonArray(data_array, firmwareVersion, errStream)) {
      return -1;
    }
  }
  else {
    *errStream << "Missing Settings Data" << endl;
    return -1;
  }

  // The first 6 bytes are length and CRC - add these at the end
  vector<uint8_t> buffer(6, 0);

  binary_append(buffer, file_identifier, 6);  // The format identifier string
  binary_append(buffer, device_id, 6);        // The device ID string
  binary_append(buffer, firmwareVersion, 1);  // This byte is F. Vers. (*10)
  if (string::npos != device_id.find("TZMR")) {
    binary_append(buffer, device_serial, 8);  // The next 8 bytes are the serial number
  }
  else if (string::npos != device_id.find("H3R")) {
    binary_append(buffer, device_serial, 11);  // The next 11 bytes are the serial number
  }
  else if (string::npos != device_id.find("HPR")) {
    binary_append(buffer, device_serial, 11);  // The next 11 bytes are the serial number
  }
  else {
    *errStream << "Unrecognized Device ID" << endl;
    return -1;
  }
  binary_append(buffer, fileIdNumber, 2);  // uint16_t fileId

  actionList.writeActionBinary(buffer, firmwareVersion, 0, &retVal, errStream);
  binary_append(buffer, ACTIONS_TERMINATOR, 1);
  binary_append(buffer, 1, 1);
  binary_append(buffer, 255, 1);
  binary_append(buffer, '\0', 1);

  uint32_t length = buffer.size();
  binary_overwrite(buffer, 2, length, 4);

  uint16_t crcValue = 0xffff;
  uint8_t *pBuf = buffer.data();
  crcBlock(&pBuf[2], length - 2, &crcValue);
  binary_overwrite(buffer, 0, crcValue, 2);

  if (!retVal) {
    outStream->write((const char *)buffer.data(), length);
  }

  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a backup file from cin and parses it
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return ActionsFromJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif