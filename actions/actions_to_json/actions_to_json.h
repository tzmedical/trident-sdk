#ifndef ACTION_TO_JSON_H
#define ACTION_TO_JSON_H

#include <iostream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to convert an actions file into a JSON file
//! \return 0 if successful
int ActionsToJson(
  std::istream* inStream, std::map<std::string, std::string>& extraArgs, std::ostream* outStream,
  std::ostream* errStream, std::ostream* logStream);

#endif