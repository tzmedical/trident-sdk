/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       actions_to_json.cpp
 *          - This program parses a *.tza actions file from <cin> and
 *            outputs a json interpretation of each entry on <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./actions_to_json inFile.tza > outFile.json
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cstdint>

#include "actions_to_json.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/action.h"
#include "util/action_list.h"
#include "util/json.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

int ActionsToJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  uint32_t i;

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // Process the extra arguments
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 16);  // Read the first block into RAM

  string str;
  str.assign((const char *)&firstBlock[6]);
  if (str.compare(0, 6, ACTIONS_ID_STRING)) {
    *errStream << "ERROR: File corrupted! (" << str << ") Aborting." << endl;
    return -1;
  }
  else {
    i = 0;
    shortCaster = (uint16_t *)&firstBlock[i];
    fileCrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *)&firstBlock[i];
    length = *intCaster;
    i += 4;

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 16; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[16],
                   length - 16);  // Store the remainder of the file in memory

    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *logStream << "Ignoring file CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    *outStream << "{" << endl;

    uint32_t j = 6;

    *outStream << "\"" << JSON_FILE_FORMAT_LABEL << "\":\"" << &pRead[j] << "\",";
    *outStream << "\"" << JSON_FILE_CRC_LABEL << "\":" << fileCrcValue << "," << endl;
    j += 6;
    string device_id((const char *)&pRead[j]);
    *outStream << "\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << device_id << "\",";
    j += 6;

    uint8_t firmwareVersion = pRead[j];
    *outStream << "\"" << JSON_FIRMWARE_VERSION_LABEL << "\":" << fixed << setprecision(1)
               << (float)firmwareVersion / 10.0 << ",";
    j += 1;

    *outStream << "\"" << JSON_DEVICE_SERIAL_LABEL << "\":\"" << &pRead[j] << "\",";

    if (string::npos != device_id.find("TZMR")) {
      j += 8;
    }
    else if (string::npos != device_id.find("H3R  ")) {
      j += 11;
    }
    else if (string::npos != device_id.find("HPR  ")) {
      j += 11;
    }
    else {
      while ('\0' != pRead[j++]) {
        // Find the end of the string
      }
    }

    uint16_t fileID = pRead[j] + ((short)pRead[j + 1] << 8);
    j += 2;
    *outStream << "\"" << JSON_FILE_ID_LABEL << "\":" << fileID << ",";

    ActionList actionList(device_id);
    if (!actionList.isValid()) {
      *errStream << "ERROR! Unsupported device_id field (" << device_id << ")." << endl;
      return -1;
    }

    *outStream << "\"" << JSON_ACTIONS_LABEL << "\":[" << endl;

    while (j < (length - 2)) {  // Parse the events until the end of the file
      retVal = actionList.parseBinary(pRead, j, firmwareVersion, outStream, errStream);
      if (retVal) return retVal;
    }

    actionList.writeActionJson(outStream, errStream);

    *outStream << "]" << endl;
    *outStream << "}" << endl;
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return output_error_json("Invalid file CRC", outStream);
    }
  }

  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return ActionsToJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif