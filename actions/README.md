# TRIDENT DEVICE ACTIONS FILE FORMAT #

## Overview ##

The Trident device will execute actions according to the contents of actions files (\*.tza). On reset the device will search its root directory for any files with the \*.tza suffix and will execute the first such file that it finds. The format of these files is described below.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZACT" to identify the file as an actions file |
| Device String | 6 | The ASCII string "HPR  " or "H3R  " to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| File Identifier | 2 | File ID # used to distinguish file versions |
| Action 1 | *Variable* | First action entry. Format described below |
| ... | ... | ... |
| Action N | *Variable* | Last action entry. Format described below |
| Terminator | 4 | Terminator entry. Format described below |

## Action Entry Format ##

Each entry in the actions file will contain an instruction for one action to be
executed by the Trident device. These entries will consist of an identifier byte, a
data length byte, a variable number of data bytes, and a reserved byte for
internal use by the Trident device. The values of the identifier tags may be found below.
Each entry will adhere to the following format:

| Item | Bytes | Description |
| --- | --- | --- |
| Actions Tag | 1 | This value identifies the action associated with the data to follow. Possible values are described below. |
| Length | 1 | This is the number of bytes of data associated with this action |
| Data | <_length_> | This is the data required for this action |
| Reserved | 1 | This byte is reserved for use by the Trident device and should always be set to 0 |

## Action Tag Tool Tips ##

The following table outlines the meaning of each action that can be interpreted
by Trident devices.

| Action Tag | Action Name | Availability | Tool Tip | Data Meaning |
| --- | --- | --- | --- | --- |
| **20** | Request a random Block of SCP Files | H3R: v1.1+ | This action causes the MCT device to transmit a block of SCP files. The first SCP file to transmit is provided as a parameter, allowing this action to request a random block of SCP files from anywhere in the record. | **Bytes 1 – 4:** The sequence number of the first SCP file to transmit. **Byte 5:** The max number of SCP files to transmit – will be truncated to the maximum possible if fewer SCP files exist than requested. |
| **21** | Request a retransmit of a SCP file | H3R: v1.1+ | This action causes the MCT device to retransmit the indicated SCP file. | **Bytes 1 – 4:** The sequence number of the SCP file to retransmit. |
| **22** | Request a block of recent SCP files | H3R: v1.1+ | This action cause the MCT to transmit a block of the most recent SCP files. The first SCP file to transmit is based on the difference between the most recent complete SCP file the amount of time requested. This allows this action to send a number of files with the last transmission being the most recent SCP file. | **Bytes 1 – 2:** The amount of time requested in seconds. |
| **30** | Request a TZR file | H3R: v1.1+ | This action causes the MCT device to transmit the TZR file specified by the parameters if it exists. | **Bytes 1 – 2:** The year of the TZR file requested. **Byte 3:** The month of the TZR file requested. **Byte 4:** The day of the TZR file requested. **Byte 5:** The time (in hours) of the TZR file requested. |
| **40** | Update the settings of the device | H3R: v1.1+ | This action causes the MCT device to download the current settings from the server. | None. |
| **50** | Display a message on the device | H3R: v1.1+ | This action causes the MCT device to display a given message on the screen of the device in an SMS like format. | **Byte 1 – Message length:** The message to display as a string of characters. |
| **60** | Request the error log from the device | H3R: v1.1+ | This action causes the MCT device to upload the onboard error log to the server for analysis. | None. |
| **100** | Request the device updates the firmware | H3R: v1.1+ | This action causes the MCT device to download a new firmware version from the provided URI. | **Byte 1 – Up to byte 128:** The URI of the firmware to update to. |
| **101** | Request the device updates the graphics | H3R: v1.4+ | This action causes the MCT device to download a new graphics version from the provided URI. | **Byte 1 – Up to byte 128:** The URI of the graphics to update to. |
| **200** | Tell the device to end an ongoing study | H3R: v1.1+ | This action causes the MCT device to stop any recordings and end an ongoing study. | **Byte 1:** 0xa5 – Validity check. |
| **201** | Tell the device to start a study | H3R: v1.1+ | This action causes the MCT device to start a new study and begin recording with the current settings. | **Byte 1:** 0x5a – Validity check. |
| **202** | Tell the device to start a study as a swapped device | H3R: v2.3+ | This action causes the MCT device to continue a study that was started on a different device. | **Byte 1 - 2:** The number of hours already completed on the study. **Byte 3 - 6** The latest sequence number completed on the study. **Byte 7 - 46** The patient id of the original study. |
| **220** | Format microSD Card | H3R: v1.1+ HPR: v1.0+ | This action causes the device to self-format the internal microSD card, wiping any data from the card in the process. | **Byte 1:** 0x42 – Validity check **Byte 2 – Up to byte 42:** Optional format handshake containing the expected Patient ID. Leave these blank to execute the format action regardless of current device Patient ID. |
| **221** | Tell the device to shutdown | H3R: v1.3+ | This action causes the MCT device to shut down. | **Byte 1:** 0x24 – Validity check. |
| **255** | Terminator Tag | H3R: v1.1+ HPR: v1.0+ | This action indicates the end of a string of actions in a TZA file. | **Byte 1:** 0xFF – Validity check. |
