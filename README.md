# Trident Software Development Kit

[![npm version](https://img.shields.io/npm/v/@tzmedical/trident-sdk.svg?logo=npm)](https://www.npmjs.org/package/@tzmedical/trident-sdk)
[![npm downloads](https://img.shields.io/npm/dm/@tzmedical/trident-sdk?logo=npm)](https://www.npmjs.org/package/@tzmedical/trident-sdk)
[![CircleCI](https://img.shields.io/circleci/build/bitbucket/tzmedical/trident-sdk?logo=CircleCI)](https://app.circleci.com/pipelines/bitbucket/tzmedical/trident-sdk)

This repository contains a collection of programs written in C++ and Javascript to help
speed development of programs for interacting with the binary files generated and
consumed by TZ Medical Trident devices.

# Trident File Specifications

To expedite development of software this repository also includes specifications
for the File Formats used by the Trident devices, including the Actions, Events,
and Settings generated and consumed by TZ Medical Trident devices. These
specifications can be found at the following locations:

- [Communication Methods](https://bitbucket.org/tzmedical/trident-sdk/src/master/COMMUNICATION.md)
- [Acceleration Data (\*.tzg)](https://bitbucket.org/tzmedical/trident-sdk/src/master/accel/README.md)
- [Actions Files (\*.tza)](https://bitbucket.org/tzmedical/trident-sdk/src/master/actions/README.md)
- [Event Files (\*.tze)](https://bitbucket.org/tzmedical/trident-sdk/src/master/events/README.md)
- [Interval Files (\*.tzr)](https://bitbucket.org/tzmedical/trident-sdk/src/master/interval/README.md)
- [SCP-ECG Files (\*.scp)](https://bitbucket.org/tzmedical/trident-sdk/src/master/scp/README.md)
- [Settings Files (\*.tzs)](https://bitbucket.org/tzmedical/trident-sdk/src/master/settings/README.md)

# Use in Node.js

The programs in the repository are published as an NPM package to simplify
deployment.

## Javascript Usage

Each file type is encapsulated as a class, with functions for each class
available according to the following table:

| File Type | `toText()` | `fromText()` | `toJson()` | `fromJson()` | `find()` | `metaJson()` | `parse()` |
| --------- | ---------- | ------------ | ---------- | ------------ | -------- | ------------ | --------- |
| tze       | X          |              | X          | X            | X        |              |           |
| tzr       | X          |              | X          | X            | X        |              |           |
| tza       | X          | X            | X          | X            |          |              |           |
| tzs       | X          | X            | X          | X            |          | X            |           |
| tzg       |            |              | X          | X            |          |              |           |
| scp       | X          |              | X          | X            |          |              |           |
| bak       | X          | X            |            |              |          |              |           |
| error     |            |              | X          |              |          |              |           |
| queue     |            |              |            |              |          |              | X         |

Here is a simple usage example for parsing an \*.tze file into JSON. See the file
[`cli.js`](cli.js) for more detailed usage examples.

```
const {tze, tzr, tza, tzs, scp} = require('@tzmedical/trident-sdk');
const fs = require("fs");

function parseTze(inFile) {
  const buffer = fs.readFileSync(inFile);

  return tze.toJson(buffer)
    .then(result => {
      console.log(JSON.stringify(result));
    });
}
```

## Command-line Usage

When installed using `npm install -g @tzmedical/trident-sdk` or when git cloned
and linked using `npm install && npm link`, a cli tool is available with the
following commands.

```
Usage: trident-sdk [options] [command]

Options:
  -V, --version                                 output the version number
  -h, --help                                    display help for command

Commands:
  toText [options] <inPath> [outPath]           Parse a binary file into a text format, based on the suffix of the file.
  fromText [options] <inPath> <outPath>         Generate a binary file from a text format, based on the suffix of the output file.
  toJson [options] <inPath> [outPath]           Parse a binary file into a JSON format, based on the suffix of the file.
  fromJson [options] <inPath> <outPath>         Generate a binary file from a JSON format, based on the suffix of the file.
  metaJson [options] <deviceType>               Output metadata for the filetype in a JSON format.
  find [options] <directory> [outPath]          Search a directory.
  parse [options] <directory> [outPath]         Find and parse wireless files into a JSON format from the tmp folder.
  retrieveFile [options] <directory> <outPath>  Find an SCP file in a directory given a sequence number.
  help [command]                                display help for command
```

Each command will try to automatically detect the file format based on file
suffixes, but this detection can also be overridden with an options flag as
shown below:

```
Usage: trident-sdk toText [options] <inFile> [outFile]

Parse a binary file into a text format, based on the suffix of the file.

Options:
  -f, --format <type>        the format of file to parse: tze, tzr, tza, tzs, scp, or bak
  -e, --epoch-ms <offset>    the starting ms epoch for HR calculations in tzr files
  -t, --tag <tag>            event tag number to search for in tzr or tze files
  --skip-crc                 ignore the crc value in the file and print the parsing results
  --no-file-header           don't print out the file header, just the entries
  --device-type <device>     type of device that created this backup file
  -h, --help                 display help for command
```

# Contributing

To contribute to this NPM package, you will need Python 3.7 or later available
on your system before installing. For Windows PCs, we recommend [installing
Node.js LTS](https://nodejs.org/en/download/). When asked if you would like to
install build tools, select yes to ensure the required dependencies are also
installed.

When developing in VS Code, add the following line to your settings file to make include
warnings from Intellisense go away:

```
{
  "local.node-gyp.node-cache": "C:/Users/<UserName>/AppData/Local/node-gyp/Cache/<NodeVersion>"
}
```

# Standalone Executables

It is also possible to build standalone executables for each of the programs by
cloning the repository and then running the shell script `scripts/compile.sh`.
Running this script will require the use of `bash`, `make`, and `clang`. In
general, it is usually preferable to use the CLI executable provided by the
Node.js packaging system, rather than compiling standalone executables.
