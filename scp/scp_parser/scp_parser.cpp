/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_parser.cpp
 *          - This program parses an *.scp ECG file from <input.scp> and outputs a
 *            text interpretation of the header information on <cout>, ignoring
 *            the actual ECG data.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./scp_parser input.scp > output.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   Feature Defines   ***/
// #define PACEMAKER_EXTENDED_INFO     // Comment this line to shorten pacemaker outputs

#define DEFAULT_CODE_COUNT 19

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

static const int default_prefix[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26,
};
static const int default_total[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10,
};
static const int default_value[DEFAULT_CODE_COUNT] = {
  0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0,
};
static const int default_code[DEFAULT_CODE_COUNT] = {
  0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023,
};

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int sectionHeader(uint8_t *pData, uint32_t *length, ostream *outStream, bool skipCrc)
{
  uint32_t i = 0;
  uint16_t fileCrcValue = binary_read16(pData, &i);  // CRC for section
  uint32_t id = binary_read16(pData, &i);            // Section ID for Section
  *length = binary_read32(pData, &i);                // Length for Section
  uint32_t sVersion = binary_read8(pData, &i);       // Section Version (2.2)
  uint32_t pVersion = binary_read8(pData, &i);       // Protocol Version (2.2)

  *outStream << "\n****** Section " << id << " Header ******" << endl;

  uint16_t calculatedCrcValue = 0xffff;
  crcBlock(&pData[2], (*length) - 2, &calculatedCrcValue);

  if (calculatedCrcValue != fileCrcValue) {
    *outStream << "CRC error! file: 0x" << hex << fileCrcValue << " - calculated: 0x"
               << calculatedCrcValue << dec << endl;
  }
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      *outStream << "Ignoring section CRC." << endl;
    }
    else {
      *outStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }
    *outStream << "Section Length: " << *length << endl;
    *outStream << "Section Version: " << sVersion / 10 << "." << sVersion % 10 << endl;
    *outStream << "Protocol Version: " << pVersion / 10 << "." << pVersion % 10 << endl;
  }
  else {
    *outStream << "Aborting." << endl;
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

//==============================================================================
class subSection {
 private:
  uint16_t id;
  uint32_t length;
  uint32_t index;
  uint8_t *pData;

 public:
  //========================================================================
  subSection()
  {
    id = 0;
    length = 0;
    index = 0;
    pData = new uint8_t[2];
  }

  //========================================================================
  ~subSection()
  {
    delete[] pData;
  }

  //========================================================================
  void init(uint16_t i, uint32_t len, uint32_t ind, uint8_t *pD)
  {
    id = i;
    length = len;
    index = ind;
    delete[] pData;
    pData = new uint8_t[length];
    uint32_t j;
    for (j = 0; j < length; j++) {
      pData[j] = pD[j + index - 1];
    }
  }

  //========================================================================
  unsigned read_8(uint32_t *offset, ostream *errStream)
  {
    if (*offset > (length - 1)) {
      *errStream << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read8(pData, offset);
  }

  //========================================================================
  unsigned read_16(uint32_t *offset, ostream *errStream)
  {
    if (*offset > (length - 2)) {
      *errStream << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read16(pData, offset);
  }

  //========================================================================
  unsigned read_32(uint32_t *offset, ostream *errStream)
  {
    if (*offset > (length - 4)) {
      *errStream << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read32(pData, offset);
  }

  //========================================================================
  void readString(uint32_t *offset, string *str, uint32_t len, ostream *errStream)
  {
    if (*offset > (length - len)) {
      *errStream << "Section Read Overflow!" << endl;
      return;
    }
    str->assign((const char *)&(pData[*offset]));
    *offset += len;
  }

  //========================================================================
  int readHeader(uint32_t *length, ostream *outStream, bool skipCrc)
  {
    return sectionHeader(pData, length, outStream, skipCrc);
  }

  //========================================================================
  bool exists()
  {
    // *outStream << "exist check on #" << id << " - Length: " << length << " - Index: " <<
    // index << endl;
    return length ? 1 : 0;
  }
};

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int ScpToText(
  std::istream *inStream, std::map<std::string, std::string> &extraArgs, std::ostream *outStream,
  std::ostream *errStream, std::ostream *logStream)
{
  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead = NULL;  // Pointer used for the read command

  uint8_t firstBlock[32];

  uint32_t i;

  // Process the extra arguments
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  // Read in the first TWO 16 byte blocks to see if the file has been encrypted
  for (i = 0; i < 32; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 32);  // Read the first block into RAM

  string str;                                    // Check that we are reading an SCP file
  str.assign((const char *)&firstBlock[16], 6);  // Copy what should be the "SCPECG" string
  if (str.compare(0, 6, SCP_ID_STRING)) {
    *errStream << "ERROR: File corrupted! Aborting." << endl;
    return -1;
  }
  else {
    uint32_t j = 0;
    fileCrcValue = binary_read16(firstBlock, &j);
    length = binary_read32(firstBlock, &j);

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 32; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[32],
                   length - 32);  // Store the remainder of the file in memory

    calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff
    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }

  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *outStream << "Ignoring file CRC." << endl;
    }
    else {
      *outStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    *outStream << "File Length: " << length << endl;  // Print out the file length

    uint32_t sI = 6;
    uint32_t j;

    /**********************************************************************************************
     *          Section 0
     **********************************************************************************************/
    if (sectionHeader(&pRead[sI], &length, outStream, skipCrc)) {
      // Parse the header for section 0
      delete[] pRead;
      return -1;  // Return value -> CRC Error
    }

    subSection sections[12];  // Array of sections to store file data

    for (j = 16; j < length;) {
      uint16_t id = binary_read16(&pRead[sI], &j);   // Section Number: 0 - 11
      uint32_t len = binary_read32(&pRead[sI], &j);  // Section Length
      uint32_t ind = binary_read32(&pRead[sI], &j);  // Section Start Index
      *outStream << "Section: " << id << " - Length: " << len << " - Index: " << ind << endl;
      if (id < 12) {
        // Copy data into section classes for later access
        sections[id].init(id, len, ind, pRead);
      }
      else {  // We've hit some sort of error
        *errStream << "Unexpected section index(" << id << ")! Aborting." << endl;
        delete[] pRead;
        return -1;
      }
    }

    delete[] pRead;  // Free the buffer we used for file input

    /**********************************************************************************************
     *          Section 1
     **********************************************************************************************/

    if (sections[1].exists()) {
      if (sections[1].readHeader(&length, outStream, skipCrc)) return -1;
      for (j = 16; j < length;) {
        uint8_t tag = sections[1].read_8(&j, errStream);
        uint16_t tagLength = sections[1].read_16(&j, errStream);
        *outStream << "(tag=" << (int)tag << ") (length=" << (int)tagLength << ") ";
        switch (tag) {
          case 0: {
            string name;
            sections[1].readString(&j, &name, tagLength, errStream);
            *outStream << "[TAG] Last Name: " << name << endl;
          } break;

          case 1: {
            string name;
            sections[1].readString(&j, &name, tagLength, errStream);
            *outStream << "[TAG] First Name: " << name << endl;
          } break;

          case 2: {  // Patient ID
            string patientID;
            sections[1].readString(&j, &patientID, tagLength, errStream);
            *outStream << "[TAG] Patient ID: " << patientID << endl;
          } break;

          case 3: {
            string name;
            sections[1].readString(&j, &name, tagLength, errStream);
            *outStream << "[TAG] SecondLast Name: " << name << endl;
          } break;

          case 14: {
            // Device ID
            uint32_t institution =
              sections[1].read_16(&j, errStream);  // Not sure what this is, set to 0
            uint32_t department =
              sections[1].read_16(&j, errStream);  // Not sure what this is, set to 0
            uint32_t deviceID =
              sections[1].read_16(&j, errStream);                 // Not sure what this is, set to 0
            bool deviceType = sections[1].read_8(&j, errStream);  // 0 = cart, 1 = Host
            uint32_t mfgCode = sections[1].read_8(&j, errStream);  // 255 = check string at end

            string modelDesc;
            sections[1].readString(&j, &modelDesc, 6,
                                   errStream);  // ASCII description of device
            uint32_t protocolRev = sections[1].read_8(&j, errStream);      // SCP protocol version
            uint32_t protocolCompat = sections[1].read_8(&j, errStream);   // Category I or II
            uint32_t languageSupport = sections[1].read_8(&j, errStream);  // 0 = ASCII
            uint32_t deviceCapability =
              sections[1].read_8(&j, errStream);  // print, interpret, store, acquire
            uint32_t mainsFreq = sections[1].read_8(&j, errStream);  // 50 Hz, 60 Hz, Unspecified

            j += 16;
            uint32_t apRevLength = sections[1].read_8(&j, errStream);  // Length of Revision String

            string apRev;
            sections[1].readString(
              &j, &apRev, apRevLength,
              errStream);  // ASCII Revision of analysis prog.
            uint32_t devSerNoLength =
              sections[1].read_8(&j, errStream);  // Length of Serial Number String
            string devSerNo;
            sections[1].readString(
              &j, &devSerNo, devSerNoLength,
              errStream);  // ASCII Device Serial Number
            uint32_t devSoftNoLength =
              sections[1].read_8(&j, errStream);  // Length of software string

            string devSoftNo;
            sections[1].readString(
              &j, &devSoftNo, devSoftNoLength,
              errStream);  // ASCII Software Version
            uint32_t devSCPidLength =
              sections[1].read_8(&j, errStream);  // Length of SCP software ID

            string devSCPid;
            sections[1].readString(
              &j, &devSCPid, devSCPidLength,
              errStream);                                                  // ASCII SCP software ID
            uint32_t mfgStringLength = sections[1].read_8(&j, errStream);  // Length of mfg name

            string mfgString;
            sections[1].readString(
              &j, &mfgString, mfgStringLength,
              errStream);  // Manufacturer Name

            *outStream << "[TAG] Device ID:" << endl;
            *outStream << "---Institution Number: " << institution << endl;
            *outStream << "---Department Number: " << department << endl;
            *outStream << "---Device ID: " << deviceID << endl;
            *outStream << "---Device Type: " << deviceType << (deviceType ? " (Host)" : " (Cart)")
                       << endl;
            *outStream << "---Manufacturer Number: " << mfgCode
                       << ((mfgCode == 255) ? " (See string)" : "") << endl;
            *outStream << "---Model Description: " << modelDesc << endl;
            *outStream << "---SCP-ECG Version: " << (protocolRev / 10) << "." << (protocolRev % 10)
                       << endl;
            *outStream << "---SCP-ECG Compatibility: 0x" << hex << protocolCompat << dec;
            if ((protocolCompat & 0xf0) == 0xd0)
              *outStream << " (Category I)" << endl;
            else if ((protocolCompat & 0xf0) == 0xe0)
              *outStream << " (Category II)" << endl;
            else
              *outStream << " (unknown)" << endl;
            *outStream << "---Language Support: 0x" << hex << languageSupport << dec
                       << (languageSupport ? " (unknown)" : " (ASCII Only)") << endl;
            *outStream << "---Device Capabilities: 0x" << hex << deviceCapability << dec << endl;
            *outStream << "------Print: " << ((deviceCapability & 0x10) ? "yes" : "no") << endl;
            *outStream << "------Interpret: " << ((deviceCapability & 0x20) ? "yes" : "no") << endl;
            *outStream << "------Store: " << ((deviceCapability & 0x40) ? "yes" : "no") << endl;
            *outStream << "------Acquire: " << ((deviceCapability & 0x80) ? "yes" : "no") << endl;
            *outStream << "---Mains Frequency Environment: " << mainsFreq;
            if (mainsFreq == 0)
              *outStream << " (Unspecified)" << endl;
            else if (mainsFreq == 1)
              *outStream << " (50 Hz)" << endl;
            else if (mainsFreq == 2)
              *outStream << " (60 Hz)" << endl;
            else
              *outStream << " (ERROR)" << endl;
            *outStream << "---Analysis Program Revision:  (" << apRevLength << ")" << apRev << endl;
            *outStream << "---Device Serial Number: (" << devSerNoLength << ") " << devSerNo
                       << endl;
            *outStream << "---Device Software Number:  (" << devSoftNoLength << ")" << devSoftNo
                       << endl;
            *outStream << "---Device SCP Software:  (" << devSCPidLength << ")" << devSCPid << endl;
            *outStream << "---Device Manufacturer (" << mfgStringLength << "): " << mfgString
                       << endl;
          } break;

          case 25: {  // Date of Acquisition
            if (tagLength != 4) {
              *errStream << "Error Parsing Date! Length = " << (int)tagLength << endl;
              return -1;
            }
            else {
              uint16_t year = sections[1].read_16(&j, errStream);
              uint8_t month = sections[1].read_8(&j, errStream);
              uint8_t day = sections[1].read_8(&j, errStream);
              *outStream << "[TAG] Date: " << setw(4) << setfill('0') << (int)year << "/" << setw(2)
                         << (int)month << "/" << setw(2) << (int)day << setfill(' ') << endl;
            }
          } break;

          case 26: {  // Time of Acquisition
            if (tagLength != 3) {
              *errStream << "Error Parsing Time! Length = " << tagLength << endl;
              return -1;
            }
            else {
              uint8_t hour = sections[1].read_8(&j, errStream);
              uint8_t minutes = sections[1].read_8(&j, errStream);
              uint8_t seconds = sections[1].read_8(&j, errStream);
              *outStream << "[TAG] Time: " << setw(2) << setfill('0') << (int)hour << ":" << setw(2)
                         << (int)minutes << ":" << setw(2) << (int)seconds << setfill(' ') << endl;
            }
          } break;

          case 27: {  // High Pass Filter
            if (tagLength != 2) {
              *errStream << "Error Parsing HP Filter! Length = " << (int)tagLength << endl;
              return -1;
            }
            else {
              uint16_t hpFilter = sections[1].read_16(&j, errStream);
              *outStream << "[TAG] High Pass Filter: " << (int)hpFilter / 100 << "." << setw(2)
                         << setfill('0') << (int)hpFilter % 100 << " Hz" << setfill(' ') << endl;
            }
          } break;

          case 28: {  // Low Pass Filter
            if (tagLength != 2) {
              *errStream << "Error Parsing LP Filter! Length = " << (int)tagLength << endl;
              return -1;
            }
            else {
              uint16_t lpFilter = sections[1].read_16(&j, errStream);
              *outStream << "[TAG] Low Pass Filter: " << (int)lpFilter << " Hz" << endl;
            }
          } break;

          case 31: {
            string ecgSequence;
            sections[1].readString(&j, &ecgSequence, tagLength, errStream);
            *outStream << "[TAG] ECG Sequence Number: " << ecgSequence << endl;
          } break;

          case 34: {  // Time Zone
            signed short offset = sections[1].read_16(&j, errStream);
            uint16_t index = sections[1].read_16(&j, errStream);
            string desc;
            sections[1].readString(&j, &desc, tagLength - 4, errStream);
            *outStream << "[TAG] Date Time Zone:" << endl;
            *outStream << "---Minutes Offset from UTC: " << offset << endl;
            *outStream << "---Time Zone Index (unused): " << index << endl;
            *outStream << "---Time Zone Description (unused): \"" << desc << "\"" << endl;
          } break;

          case 255: {  // Terminator - we're done.
            if (tagLength != 0) {
              *errStream << "Error Parsing Terminator! Length = " << (int)tagLength << endl;
            }
            else {
              *outStream << "[TAG] Terminator." << endl;
              j = length;
            }
          } break;
          default: {
            *outStream << "[TAG] " << (int)tag << " (unsupported)" << endl;
            j += tagLength;
          } break;
        }
      }
    }

    /**********************************************************************************************
     *          Section 2
     **********************************************************************************************/

    uint32_t **prefixBits = NULL;
    uint32_t **totalBits = NULL;
    uint32_t **switchByte = NULL;
    uint32_t **baseValue = NULL;
    uint32_t **baseCode = NULL;
    uint32_t tableCount = 0;
    uint32_t *codeCount = NULL;
    if (sections[2].exists()) {
      if (sections[2].readHeader(&length, outStream, skipCrc)) return -1;
      j = 16;
      tableCount = sections[2].read_16(&j, errStream);
      uint32_t l;
      if (19999 != tableCount) {
        *outStream << "Number of Huffman Tables: " << tableCount << endl;
        prefixBits = new uint32_t *[tableCount];
        totalBits = new uint32_t *[tableCount];
        switchByte = new uint32_t *[tableCount];
        baseValue = new uint32_t *[tableCount];
        baseCode = new uint32_t *[tableCount];

        codeCount = new uint32_t[tableCount];

        for (l = 0; l < tableCount; l++) {
          codeCount[l] = sections[2].read_16(&j, errStream);
          *outStream << "Number of codes in Table #" << (l + 1) << ": " << codeCount[l] << endl;
          prefixBits[l] = new uint32_t[codeCount[l]];
          totalBits[l] = new uint32_t[codeCount[l]];
          switchByte[l] = new uint32_t[codeCount[l]];
          baseValue[l] = new uint32_t[codeCount[l]];
          baseCode[l] = new uint32_t[codeCount[l]];
          uint32_t m;
          for (m = 0; m < codeCount[l]; m++) {
            prefixBits[l][m] = sections[2].read_8(&j, errStream);
            totalBits[l][m] = sections[2].read_8(&j, errStream);
            switchByte[l][m] = sections[2].read_8(&j, errStream);
            baseValue[l][m] = sections[2].read_16(&j, errStream);
            baseCode[l][m] = sections[2].read_32(&j, errStream);

            *outStream << "---Code #" << (m + 1) << " details:" << endl;
            *outStream << "------Prefix Bits: " << prefixBits[l][m] << endl;
            *outStream << "------Total Bits: " << totalBits[l][m] << endl;
            *outStream << "------Switch Byte: " << switchByte[l][m] << endl;
            *outStream << "------Base Value: " << baseValue[l][m] << endl;
            *outStream << "------Base Code: 0b";  // << baseCode[m] << " (0b";

            uint32_t n;
            uint32_t bitmask = 1;
            for (n = 0; n < prefixBits[l][m]; n++) {
              *outStream << ((baseCode[l][m] & bitmask) ? 1 : 0);
              bitmask = bitmask << 1;
            }
            *outStream << " (" << baseCode[l][m] << ")" << endl;
          }
        }
      }
      else {
        *outStream << "Default Huffman Table used!" << endl;
        prefixBits = new uint32_t *[1];
        totalBits = new uint32_t *[1];
        switchByte = new uint32_t *[1];
        baseValue = new uint32_t *[1];
        baseCode = new uint32_t *[1];

        codeCount = new uint32_t[1];

        codeCount[0] = DEFAULT_CODE_COUNT;
        prefixBits[0] = new uint32_t[codeCount[0]];
        totalBits[0] = new uint32_t[codeCount[0]];
        switchByte[0] = new uint32_t[codeCount[0]];
        baseValue[0] = new uint32_t[codeCount[0]];
        baseCode[0] = new uint32_t[codeCount[0]];

        uint32_t m;
        for (m = 0; m < codeCount[0]; m++) {
          prefixBits[0][m] = default_prefix[m];
          totalBits[0][m] = default_total[m];
          switchByte[0][m] = 1;
          baseValue[0][m] = default_value[m];
          baseCode[0][m] = default_code[m];

          *outStream << "---Code #" << (m + 1) << " details:" << endl;
          *outStream << "------Prefix Bits: " << prefixBits[0][m] << endl;
          *outStream << "------Total Bits: " << totalBits[0][m] << endl;
          *outStream << "------Switch Byte: " << switchByte[0][m] << endl;
          *outStream << "------Base Value: " << baseValue[0][m] << endl;
          *outStream << "------Base Code: 0b";  // << baseCode[m] << " (0b";

          uint32_t n;
          uint32_t bitmask = 1;
          for (n = 0; n < prefixBits[0][m]; n++) {
            *outStream << ((baseCode[0][m] & bitmask) ? 1 : 0);
            bitmask = bitmask << 1;
          }
          *outStream << " (" << baseCode[0][m] << ")" << endl;
        }
      }
    }

    /**********************************************************************************************
     *          Section 3
     **********************************************************************************************/

    uint32_t leadCount = 0;
    uint32_t *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
    i = 0;
    if (sections[3].exists()) {
      if (sections[3].readHeader(&length, outStream, skipCrc)) return -1;
      j = 16;
      leadCount = sections[3].read_8(&j, errStream);
      uint32_t flags = sections[3].read_8(&j, errStream);
      sampleStart = new uint32_t[leadCount];
      sampleEnd = new uint32_t[leadCount];
      leadID = new uint32_t[leadCount];
      for (i = 0; i < leadCount; i++) {
        sampleStart[i] = sections[3].read_32(&j, errStream);
        sampleEnd[i] = sections[3].read_32(&j, errStream);
        leadID[i] = sections[3].read_8(&j, errStream);
      }
      *outStream << "Number of Leads: " << leadCount << endl;
      *outStream << "Flags: 0x" << hex << flags << dec << endl;
      *outStream
        << ((flags & 0x01) ? "---Reference beat subtraction used for compression"
                           : "---Reference beat subtraction not used for compression")
        << endl;
      *outStream
        << ((flags & 0x04) ? "---Leads recorded simultaneously"
                           : "---Leads not recorded simultaneously")
        << endl;
      *outStream << "---Number of simultaneously recorded leads: " << (flags / 8) << endl;
      for (i = 0; i < leadCount; i++) {
        *outStream << "Details for Lead #" << i << endl;
        *outStream << "---Starting Sample Number: " << sampleStart[i] << endl;
        *outStream << "---Ending Sample Number: " << sampleEnd[i] << endl;
        *outStream << "---Lead Identification Code: " << leadID[i];
        if (leadID[i] == 131)
          *outStream << " (ES)" << endl;
        else if (leadID[i] == 132)
          *outStream << " (AS)" << endl;
        else if (leadID[i] == 133)
          *outStream << " (AI)" << endl;
        else
          *outStream << endl;
      }
    }

    /**********************************************************************************************
     *          Section 6
     **********************************************************************************************/

    uint32_t *leadLength = NULL;
    uint32_t **dataArrays = NULL;
    if (sections[6].exists()) {
      if (sections[6].readHeader(&length, outStream, skipCrc)) return -1;
      j = 16;
      uint32_t ampMult = sections[6].read_16(&j, errStream);
      uint32_t samplePeriod = sections[6].read_16(&j, errStream);
      uint32_t encoding = sections[6].read_8(&j, errStream);
      uint32_t compression = sections[6].read_8(&j, errStream);
      leadLength = new uint32_t[leadCount];
      dataArrays = new uint32_t *[leadCount];
      for (i = 0; i < leadCount; i++) {
        leadLength[i] = sections[6].read_16(&j, errStream);
        if (sampleEnd[i] > sampleStart[i]) {
          dataArrays[i] = new uint32_t[sampleEnd[i] - sampleStart[i]];
        }
        else
          dataArrays[i] = NULL;
      }
      *outStream << "Amplitude multiplier: " << ampMult << " (nV/count)" << endl;
      if (ampMult) {
        *outStream << "---ADC Gain: " << (1000000. / (float)ampMult) << " (counts/mV)" << endl;
      }
      *outStream << "Sample Period: " << samplePeriod << " (us)" << endl;
      if (samplePeriod) {
        *outStream << "---Sample Rate: " << (1000000 / samplePeriod) << " (Hz)" << endl;
      }
      *outStream << "Data encoding: " << encoding << endl;
      if (encoding == 0)
        *outStream << "---Real data" << endl;
      else if (encoding == 1)
        *outStream << "---First difference data" << endl;
      else if (encoding == 2)
        *outStream << "---Second difference data" << endl;
      else
        *outStream << "---Unknown data" << endl;
      *outStream << "Compression: " << compression
                 << (compression ? " (Bimodal compression)" : " (No compression)") << endl;
      for (i = 0; i < leadCount; i++) {
        *outStream << "Bytes of data for lead #" << i << ": " << leadLength[i] << endl;
      }
    }

    /**********************************************************************************************
     *          Section 7
     **********************************************************************************************/

    uint32_t *paceTime = NULL, *paceAmplitude = NULL;
    uint32_t *paceType = NULL, *paceSource = NULL, *paceIndex = NULL, *paceWidth = NULL;
    if (sections[7].exists()) {
      if (sections[7].readHeader(&length, outStream, skipCrc)) return -1;
      j = 16;
      uint32_t referenceCount = sections[7].read_8(&j, errStream);
      uint32_t paceCount = sections[7].read_8(&j, errStream);
      uint32_t rrInterval = sections[7].read_16(&j, errStream);
      uint32_t ppInterval = sections[7].read_16(&j, errStream);
      paceTime = new uint32_t[paceCount];
      paceAmplitude = new uint32_t[paceCount];
      for (i = 0; i < paceCount; i++) {
        paceTime[i] = sections[7].read_16(&j, errStream);
        paceAmplitude[i] = sections[7].read_16(&j, errStream);
      }
      paceType = new uint32_t[paceCount];
      paceSource = new uint32_t[paceCount];
      paceIndex = new uint32_t[paceCount];
      paceWidth = new uint32_t[paceCount];
      for (i = 0; i < paceCount; i++) {
        paceType[i] = sections[7].read_8(&j, errStream);
        paceSource[i] = sections[7].read_8(&j, errStream);
        paceIndex[i] = sections[7].read_16(&j, errStream);
        paceWidth[i] = sections[7].read_16(&j, errStream);
      }
      *outStream << "Number of reference beat types: " << referenceCount << endl;
      ;
      *outStream << "Number of pacemaker spikes: " << paceCount << endl;
      *outStream << "Average RR interval: ";
      if (rrInterval == 29999)
        *outStream << "not calculated" << endl;
      else
        *outStream << rrInterval << " (ms)" << endl;
      *outStream << "Average PP interval: ";
      if (ppInterval == 29999)
        *outStream << "not calculated" << endl;
      else
        *outStream << ppInterval << " (ms)" << endl;
      for (i = 0; i < paceCount; i++) {
        *outStream << "Details for pacemaker spike #" << i + 1 << endl;
#ifdef PACEMAKER_EXTENDED_INFO
        *outStream << "(" << paceTime[i] << " " << paceAmplitude[i] << " " << paceType[i] << " "
                   << paceSource[i] << " " << paceIndex[i] << " " << paceWidth[i] << ")" << endl;
        *outStream << "---Time: " << paceTime[i] << " (ms)" << endl;
        *outStream << "---Amplitude: ";
        if (paceAmplitude[i] == 29999)
          *outStream << "not calculated" << endl;
        else
          *outStream << paceAmplitude[i] << " (uV)" << endl;
        *outStream << "---Type: ";
        if (paceType[i] == 255)
          *outStream << "not calculated" << endl;
        else if (paceType[i] == 0)
          *outStream << "unknown" << endl;
        else if (paceType[i] == 1)
          *outStream << "triggers neither P-wave nor QRS" << endl;
        else if (paceType[i] == 2)
          *outStream << "triggers a QRS" << endl;
        else if (paceType[i] == 3)
          *outStream << "triggers a P-wave" << endl;
        else
          *outStream << "ERROR" << endl;
        *outStream << "---Source: ";
        if (paceSource[i] == 0)
          *outStream << "unknown" << endl;
        else if (paceSource[i] == 1)
          *outStream << "internal" << endl;
        else if (paceSource[i] == 2)
          *outStream << "external" << endl;
        else
          *outStream << "ERROR" << endl;
        *outStream << "---Triggered QRS complex: ";
        if (paceIndex[i] == 0)
          *outStream << "none" << endl;
        else
          *outStream << paceIndex[i];
        *outStream << "---Pulse Width: ";
        if (paceWidth[i] == 0)
          *outStream << "unknown" << endl;
        else
          *outStream << paceWidth[i] << " (us)" << endl;
#else
        *outStream << "---Pulse Width: ";
        if (paceWidth[i] == 0)
          *outStream << "unknown";
        else
          *outStream << paceWidth[i] << " (us)";
        *outStream << " at time: " << paceTime[i] << " (ms)" << endl;
#endif
      }
    }

    if (sections[2].exists()) {
      uint32_t l;
      for (l = 0; l < tableCount; l++) {
        delete[] prefixBits[l];
        delete[] totalBits[l];
        delete[] switchByte[l];
        delete[] baseValue[l];
        delete[] baseCode[l];
      }
      delete[] codeCount;
      delete[] prefixBits;
      delete[] totalBits;
      delete[] switchByte;
      delete[] baseValue;
      delete[] baseCode;
    }

    if (sections[3].exists()) {
      delete[] sampleStart;
      delete[] sampleEnd;
      delete[] leadID;
    }

    if (sections[6].exists()) {
      delete[] leadLength;
      uint32_t l;
      for (l = 0; l < leadCount; l++) {
        delete[] dataArrays[l];
      }
      delete[] dataArrays;
    }

    if (sections[7].exists()) {
      delete[] paceTime;
      delete[] paceAmplitude;
      delete[] paceType;
      delete[] paceSource;
      delete[] paceIndex;
      delete[] paceWidth;
    }
  }

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return -1;
    }
  }

  *outStream << "Done.\n" << endl;  // Signal completion of program

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads an scp file from cin and outputs text on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return ScpToText(input, extraArgs, &cout, &cerr, &clog);
}
#endif
