CRC Valid: 0x639e
File Length: 32768

****** Section 0 Header ******
CRC Valid: 0xef56
Section Length: 136
Section Version: 2.2
Protocol Version: 2.2
Section: 0 - Length: 136 - Index: 7
Section: 1 - Length: 184 - Index: 143
Section: 2 - Length: 56 - Index: 327
Section: 3 - Length: 90 - Index: 383
Section: 4 - Length: 0 - Index: 473
Section: 5 - Length: 0 - Index: 473
Section: 6 - Length: 31274 - Index: 473
Section: 7 - Length: 1022 - Index: 31747
Section: 8 - Length: 0 - Index: 32769
Section: 9 - Length: 0 - Index: 32769
Section: 10 - Length: 0 - Index: 32769
Section: 11 - Length: 0 - Index: 32769

****** Section 1 Header ******
CRC Valid: 0x8083
Section Length: 184
Section Version: 2.2
Protocol Version: 2.2
(tag=2) (length=40) [TAG] Patient ID: caFRUr4+NCsRQ3NSUTNOU1VU
(tag=14) (length=75) [TAG] Device ID:
---Institution Number: 0
---Department Number: 0
---Device ID: 0
---Device Type: 0 (Cart)
---Manufacturer Number: 255 (See string)
---Model Description: H3R  
---SCP-ECG Version: 2.2
---SCP-ECG Compatibility: 0xd0 (Category I)
---Language Support: 0x0 (ASCII Only)
---Device Capabilities: 0xc0
------Print: no
------Interpret: no
------Store: yes
------Acquire: yes
---Mains Frequency Environment: 0 (Unspecified)
---Analysis Program Revision:  (1)
---Device Serial Number: (11) H3R4002016
---Device Software Number:  (5)1.1D
---Device SCP Software:  (1)
---Device Manufacturer (17): TZ Medical, Inc.
(tag=25) (length=4) [TAG] Date: 2017/03/28
(tag=26) (length=3) [TAG] Time: 21:19:07
(tag=34) (length=5) [TAG] Date Time Zone:
---Minutes Offset from UTC: -420
---Time Zone Index (unused): 0
---Time Zone Description (unused): ""
(tag=27) (length=2) [TAG] High Pass Filter: 0.05 Hz
(tag=28) (length=2) [TAG] Low Pass Filter: 50 Hz
(tag=31) (length=9) [TAG] ECG Sequence Number: 00000000
(tag=255) (length=0) [TAG] Terminator.

****** Section 2 Header ******
CRC Valid: 0x3949
Section Length: 56
Section Version: 2.2
Protocol Version: 2.2
Number of Huffman Tables: 1
Number of codes in Table #1: 4
---Code #1 details:
------Prefix Bits: 2
------Total Bits: 8
------Switch Byte: 1
------Base Value: 0
------Base Code: 0b00 (0)
---Code #2 details:
------Prefix Bits: 2
------Total Bits: 16
------Switch Byte: 1
------Base Value: 0
------Base Code: 0b01 (2)
---Code #3 details:
------Prefix Bits: 2
------Total Bits: 24
------Switch Byte: 1
------Base Value: 0
------Base Code: 0b10 (1)
---Code #4 details:
------Prefix Bits: 2
------Total Bits: 32
------Switch Byte: 1
------Base Value: 0
------Base Code: 0b11 (3)

****** Section 3 Header ******
CRC Valid: 0x4d39
Section Length: 90
Section Version: 2.2
Protocol Version: 2.2
Number of Leads: 8
Flags: 0x44
---Reference beat subtraction not used for compression
---Leads recorded simultaneously
---Number of simultaneously recorded leads: 8
Details for Lead #0
---Starting Sample Number: 1
---Ending Sample Number: 9800
---Lead Identification Code: 1
Details for Lead #1
---Starting Sample Number: 1
---Ending Sample Number: 9800
---Lead Identification Code: 2
Details for Lead #2
---Starting Sample Number: 1
---Ending Sample Number: 9800
---Lead Identification Code: 61
Details for Lead #3
---Starting Sample Number: 1
---Ending Sample Number: 1
---Lead Identification Code: 0
Details for Lead #4
---Starting Sample Number: 1
---Ending Sample Number: 1
---Lead Identification Code: 0
Details for Lead #5
---Starting Sample Number: 1
---Ending Sample Number: 1
---Lead Identification Code: 0
Details for Lead #6
---Starting Sample Number: 1
---Ending Sample Number: 1
---Lead Identification Code: 0
Details for Lead #7
---Starting Sample Number: 1
---Ending Sample Number: 1
---Lead Identification Code: 0

****** Section 6 Header ******
CRC Valid: 0xcbbb
Section Length: 31274
Section Version: 2.2
Protocol Version: 2.2
Amplitude multiplier: 2304 (nV/count)
---ADC Gain: 434.028 (counts/mV)
Sample Period: 4000 (us)
---Sample Rate: 250 (Hz)
Data encoding: 1
---First difference data
Compression: 0 (No compression)
Bytes of data for lead #0: 10412
Bytes of data for lead #1: 10412
Bytes of data for lead #2: 10412
Bytes of data for lead #3: 0
Bytes of data for lead #4: 0
Bytes of data for lead #5: 0
Bytes of data for lead #6: 0
Bytes of data for lead #7: 0

****** Section 7 Header ******
CRC Valid: 0xb8cf
Section Length: 1022
Section Version: 2.2
Protocol Version: 2.2
Number of reference beat types: 0
Number of pacemaker spikes: 0
Average RR interval: not calculated
Average PP interval: not calculated
Done.

