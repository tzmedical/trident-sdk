Welcome to the one_ecg_splitter.

This file format exists to save space and to transfer data at higher speeds over the default nested scp file structure.

To use the ecg splitter, you will need the ecg_splitter.exe program. This is generated by running the "Make" command in the one_ecg_splitter folder. This command will use the makefile, ecg_splitter.cpp and one_ecg_splitter.h to generate the .exe file.

To run this program, run ecg_splitter.exe with the second argument as a path to the scpecg.bin file. The easiest way to do this is to simply copy the scpecg.bin file into the one_ecg_splitter folder and then run "./ecg_splitter.exe ./scpecg.bin".

The output of this program will be a folder with the current system time. This folder will contain the normal file structure of the ecgs folder containing .scp files which can then be parsed with the trident-sdk.
