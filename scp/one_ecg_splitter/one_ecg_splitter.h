#ifndef ONE_ECG_SPLITTER_H
#define ONE_ECG_SPLITTER_H

#include <iostream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to convert a split a combined ecg file
//! \return 0 if successful
int SplitScp(
  std::istream* inStream, std::ostream* outStream, std::ostream* errStream,
  std::ostream* logStream);

#endif
