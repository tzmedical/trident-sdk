/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       ecg_splitter.cpp
 *          - This program parses an *.bin ECG file from <input.scp> and outputs
 *              all the individual files
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./ecg_splitter scpecg.bin
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <sys/stat.h>

#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   Feature Defines   ***/
// #define PACEMAKER_EXTENDED_INFO     // Comment this line to shorten pacemaker outputs

#define DEFAULT_CODE_COUNT (19)
#define SCP_MAX_SIZE       (294912)  // 9 x 32k, will allow up to 8 channel files in the future.

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
//    main()
//
//    This function reads in an SCP-ECG file from inFile and parses out all of
//    the header information, ignoring the actual ECG data.
//==============================================================================
int main(int argc, char *argv[])
{
  auto t = time(nullptr);
  struct tm tm;

#ifdef _WIN32
  _setmode(_fileno(stdin), O_BINARY);
  gmtime_s(&tm, &t);
#else
  localtime_r(&t, &tm);
#endif

  ostringstream base_dir;

  base_dir << "./" << put_time(&tm, "%Y%m%d%H%M%S") << "/";

  uint32_t i;

  if (argc < 2) {
    cerr << "No input file specified! Aborting." << endl;
    return -1;
  }

  // Open the file and check the file length
  ifstream inFile(argv[1], ios::in | ios::binary);
  inFile.seekg(0, ios::end);
  uint32_t fileLength = inFile.tellg();
  inFile.seekg(0, ios::beg);

#ifdef _WIN32
  string baseDirStr = base_dir.str();
  wstring wideBaseDir = wstring(baseDirStr.begin(), baseDirStr.end());
  _wmkdir(wideBaseDir.c_str());
#else
  mkdir(base_dir.str().c_str(), 0777);
#endif

  int sequence = 0;
  uint32_t lengthRead = 0;
  uint32_t scpLength;

  char scp_buf[SCP_MAX_SIZE];
  ostringstream fileNameBuilder;

  while (lengthRead < fileLength) {
    inFile.seekg(lengthRead + 2);
    inFile.read((char *)&scpLength, 4);
    inFile.seekg(lengthRead);

    uint32_t dir_names[4];

    dir_names[0] = (sequence - (sequence % 1000000)) / 1000000;
    dir_names[1] = ((sequence % 1000000) - (sequence % 10000)) / 10000;
    dir_names[2] = ((sequence % 10000) - (sequence % 100)) / 100;
    dir_names[3] = (sequence % 100);

    fileNameBuilder.str("");
    fileNameBuilder.clear();

    fileNameBuilder << base_dir.str();

    for (i = 0; i < 3; i++) {
      fileNameBuilder << setw(2) << setfill('0') << dir_names[i] << "/";
#ifdef _WIN32
      string fileNameStr = fileNameBuilder.str();
      wstring wideFileName = wstring(fileNameStr.begin(), fileNameStr.end());
      _wmkdir(wideFileName.c_str());
#else
      mkdir(fileNameBuilder.str().c_str(), 0777);
#endif
    }

    fileNameBuilder << setw(2) << setfill('0') << dir_names[3] << ".scp";

    ofstream outFile(fileNameBuilder.str(), ios_base::out | ios_base::binary);

    inFile.read(&scp_buf[0], scpLength);
    outFile.write(&scp_buf[0], scpLength);
    outFile.close();

    lengthRead += scpLength;
    sequence++;
  }

  cout << "Done.\n" << endl;  // Signal completion of program

  return 0;
}
