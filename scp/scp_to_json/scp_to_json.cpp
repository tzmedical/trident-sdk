/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_to_json.cpp
 *          - This program parses an *.scp ECG file from <cin> or <input_file>
 *            and outputs a JSON representation of the contents that should
 *            be suitable for rendering the waveforms.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./scp_to_json <input.scp 2>log.txt >output.txt
 *
 *
 *
 *****************************************************************************/

//-----------------------------------------------------------------------------
//                __             __   ___  __
//        | |\ | /  ` |    |  | |  \ |__  /__`
//        | | \| \__, |___ \__/ |__/ |___ .__/
//
//-----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <cstdint>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   File specs   ***/
#define DEFAULT_CODE_COUNT 19

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

/***   For default implementation of Huffman encoding   ***/
static const int32_t default_prefix[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26,
};
static const int32_t default_total[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10,
};
static const int32_t default_value[DEFAULT_CODE_COUNT] = {
  0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0,
};
static const int32_t default_code[DEFAULT_CODE_COUNT] = {
  0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023,
};

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

static int32_t sectionHeader(
  uint8_t *pData, uint32_t *length, ostream *outStream, ostream *errStream, ostream *logStream,
  bool skipCrc);
static uint8_t *get_bits(
  int32_t *out_data, uint32_t bits, uint8_t *in_byte, uint8_t *bit_num, uint32_t sign_extend,
  uint32_t debug);

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int32_t sectionHeader(
  uint8_t *pData, uint32_t *length, ostream *outStream, ostream *errStream, ostream *logStream,
  bool skipCrc)
{
  uint32_t i = 0;
  uint16_t fileCrcValue = binary_read16(pData, &i);  // CRC for section
  uint32_t id = binary_read16(pData, &i);            // Section ID for Section
  *length = binary_read32(pData, &i);                // Length for Section
  uint32_t sVersion = binary_read8(pData, &i);       // Section Version (2.2)
  uint32_t pVersion = binary_read8(pData, &i);       // Protocol Version (2.2)

  *logStream << "\n****** Section " << id << " Header ******" << endl;

  uint16_t calculatedCrcValue = 0xffff;
  crcBlock(&pData[2], (*length) - 2, &calculatedCrcValue);

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "CRC error! file: 0x" << hex << fileCrcValue << " - calculated: 0x"
               << calculatedCrcValue << dec << endl;
  }
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      *logStream << "Ignoring section CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }
    *logStream << "Section Length: " << *length << endl;
    *logStream << "Section Version: " << sVersion / 10 << "." << sVersion % 10 << endl;
    *logStream << "Protocol Version: " << pVersion / 10 << "." << pVersion % 10 << endl;
  }
  else {
    return output_error_json("Invalid CRC", outStream);
  }

  return 0;
}

//==============================================================================
static uint8_t *get_bits(
  int32_t *out_data, uint32_t bits, uint8_t *in_byte, uint8_t *bit_num, uint32_t sign_extend,
  uint32_t debug)
{
  uint32_t sign_bit = bits - 1;
  *out_data = 0;

  while (bits) {
    if (bits <= *bit_num) {
      *out_data |= ((*in_byte) >> (*bit_num - bits)) & (0xff >> (8 - bits));
      *bit_num -= bits;
      bits = 0;
    }
    else {
      *out_data |= ((int32_t)*in_byte & (0x00ff >> (8 - *bit_num))) << (bits - *bit_num);
      bits -= *bit_num;
      *bit_num = 0;
    }

    if (0 == *bit_num) {
      *bit_num += 8;
      in_byte++;
    }
  }

  // Sign extend the value we read
  if (sign_extend && (*out_data & (1 << sign_bit))) {
    *out_data |= (~0ULL) << sign_bit;
  }

  return in_byte;
}

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

//==============================================================================
class subSection {
 private:
  uint16_t id;
  uint32_t length;
  uint32_t index;
  uint8_t *pData;

 public:
  //========================================================================
  subSection()
  {
    id = 0;
    length = 0;
    index = 0;
    pData = new uint8_t[2];
  }

  //========================================================================
  ~subSection()
  {
    delete[] pData;
  }

  //========================================================================
  void init(uint16_t i, uint32_t len, uint32_t ind, uint8_t *pD)
  {
    id = i;
    length = len;
    index = ind;
    delete[] pData;
    pData = new uint8_t[length];
    uint32_t j;
    for (j = 0; j < length; j++) {
      pData[j] = pD[j + index - 1];
    }
  }

  //========================================================================
  int32_t read_8(uint32_t *offset, ostream *errStream)
  {
    if (*offset > (length - 1)) {
      *errStream << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read8(pData, offset);
  }

  //========================================================================
  int32_t read_16(uint32_t *offset, ostream *errStream)
  {
    if (*offset > (length - 2)) {
      *errStream << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read16(pData, offset);
  }

  //========================================================================
  int32_t read_32(uint32_t *offset, ostream *errStream)
  {
    if (*offset > (length - 4)) {
      *errStream << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read32(pData, offset);
  }

  //========================================================================
  void readString(uint32_t *offset, string *str, uint32_t len, ostream *errStream)
  {
    if (*offset > (length - len)) {
      *errStream << "Section Read Overflow!" << endl;
      return;
    }
    str->assign((const char *)&(pData[*offset]), (int32_t)len);
    *offset += len;
  }

  //========================================================================
  int32_t readHeader(
    uint32_t *length, ostream *outStream, ostream *errStream, ostream *logStream, bool skipCrc)
  {
    return sectionHeader(pData, length, outStream, errStream, logStream, skipCrc);
  }

  //========================================================================
  bool exists()
  {
    // *logStream << "exist check on #" << id << " - Length: " << length << " - Index: " <<
    // index << endl;
    return length ? 1 : 0;
  }
};

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int32_t ScpToJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  ostringstream output_stream;

  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead = NULL;  // Pointer used for the read command

  uint8_t firstBlock[32];

  uint32_t i;

  // Process the extra arguments
  int32_t zoneOffset = 0;
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("zoneOffset");
  if (itr != extraArgs.end()) {
    char *endptr;
    zoneOffset = (int32_t)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  // Read in the first TWO 16 byte blocks to see if the file has been encrypted
  for (i = 0; i < 32; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 32);  // Read the first block into RAM

  string str;                                    // Check that we are reading an SCP file
  str.assign((const char *)&firstBlock[16], 6);  // Copy what should be the "SCPECG" string
  if (str.compare(0, 6, SCP_ID_STRING)) {
    *errStream << "ERROR: File corrupted! Aborting." << endl;
    return output_error_json("Corrupted format string", outStream);
  }
  else {
    uint32_t j = 0;
    fileCrcValue = binary_read16(firstBlock, &j);
    length = binary_read32(firstBlock, &j);

    // Make sure the file is not too big for the format
    if (length > MAX_SCP_FILE_SIZE) {
      *errStream << "File size (" << length << ") is SIGNIFICANTLY larger than expected. Aborting."
                 << endl;
      return output_error_json("File too large", outStream);
    }

    // Make sure the file is not too small for the format
    if (length < MIN_SCP_FILE_SIZE) {
      *errStream << "File size is too small for an SCP file. Aborting." << endl;
      return output_error_json("File too small", outStream);
    }

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 32; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[32],
                   length - 32);  // Store the remainder of the file in memory

    calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff
    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    if (skipCrc) {
      // Ignore the CRC values (debugging ONLY)
      *logStream << "Ignoring file CRC." << endl;
    }
    else {
      *logStream << "CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
    }

    *logStream << "File Length: " << length << endl;  // Print out the file length

    // Start JSON object
    output_stream << "{\"" << JSON_FILE_FORMAT_LABEL << "\":\"" << SCP_ID_STRING << "\",";
    output_stream << "\"" << JSON_FILE_CRC_LABEL << "\":" << fileCrcValue << "," << endl;

    uint32_t sI = 6;
    uint32_t j;

    /**********************************************************************************************
     *          Section 0
     **********************************************************************************************/
    if (sectionHeader(&pRead[sI], &length, outStream, errStream, logStream, skipCrc)) {
      // Parse the header for section 0
      delete[] pRead;  // Return value -> CRC Error
      return -1;
    }

    subSection sections[12];  // Array of sections to store file data

    for (j = 16; j < length;) {
      uint16_t id = binary_read16(&pRead[sI], &j);   // Section Number: 0 - 11
      uint32_t len = binary_read32(&pRead[sI], &j);  // Section Length
      uint32_t ind = binary_read32(&pRead[sI], &j);  // Section Start Index
      *logStream << "Section: " << id << " - Length: " << len << " - Index: " << ind << endl;
      if (id < 12) {
        // Copy data into section classes for later access
        sections[id].init(id, len, ind, pRead);
      }
      else {  // We've hit some sort of error
        *errStream << "Unexpected section index(" << id << ")! Aborting." << endl;
        return output_error_json("Unsupported SCP section", outStream);
        delete[] pRead;
        return -1;
      }
    }

    delete[] pRead;  // Free the buffer we used for file input

    /**********************************************************************************************
     *          Section 1
     **********************************************************************************************/

    if (sections[1].exists()) {
      struct tm t = {};
      char time_valid = 0;
      char date_valid = 0;
      int time_zone = 0;
      if (sections[1].readHeader(&length, outStream, errStream, logStream, skipCrc)) return -1;
      for (j = 16; j < length;) {
        uint8_t tag = sections[1].read_8(&j, errStream);
        uint16_t tagLength = sections[1].read_16(&j, errStream);
        *logStream << "(tag=" << (int32_t)tag << ") (length=" << (int32_t)tagLength << ") ";
        switch (tag) {
          case 0: {
            string name;
            sections[1].readString(&j, &name, tagLength, errStream);
            *logStream << "[TAG] Last Name: " << name << endl;
          } break;
          case 1: {
            string name;
            sections[1].readString(&j, &name, tagLength, errStream);
            *logStream << "[TAG] First Name: " << name << endl;
          } break;
          case 2: {  // Patient ID
            string patientID;
            sections[1].readString(&j, &patientID, tagLength, errStream);
            *logStream << "[TAG] Patient ID: " << patientID << endl;
            output_stream << "\"" << JSON_PATIENT_ID_LABEL << "\":\"" << patientID.c_str() << "\",";
          } break;
          case 3: {
            string name;
            sections[1].readString(&j, &name, tagLength, errStream);
            output_stream << "[TAG] SecondLast Name: " << name << endl;
          } break;

          case 14: {  // Device ID
            uint32_t institution =
              sections[1].read_16(&j, errStream);  // Not sure what this is, set to 0
            uint32_t department =
              sections[1].read_16(&j, errStream);  // Not sure what this is, set to 0
            uint32_t deviceID =
              sections[1].read_16(&j, errStream);  // Not sure what this is, set to 0
            bool deviceType = (1 == sections[1].read_8(&j, errStream));  // 0 = cart, 1 = Host
            uint32_t mfgCode = sections[1].read_8(&j, errStream);  // 255 = check string at end
            string modelDesc;
            sections[1].readString(&j, &modelDesc, 6,
                                   errStream);  // ASCII description of device
            uint32_t protocolRev = sections[1].read_8(&j, errStream);      // SCP protocol version
            uint32_t protocolCompat = sections[1].read_8(&j, errStream);   // Category I or II
            uint32_t languageSupport = sections[1].read_8(&j, errStream);  // 0 = ASCII
            uint32_t deviceCapability =
              sections[1].read_8(&j, errStream);  // print, interpret, store, acquire
            uint32_t mainsFreq = sections[1].read_8(&j, errStream);  // 50 Hz, 60 Hz, Unspecified
            j += 16;
            uint32_t apRevLength = sections[1].read_8(&j, errStream);  // Length of Revision String
            string apRev;
            sections[1].readString(
              &j, &apRev, apRevLength,
              errStream);  // ASCII Revision of analysis prog.
            uint32_t devSerNoLength =
              sections[1].read_8(&j, errStream);  // Length of Serial Number String
            string devSerNo;
            sections[1].readString(
              &j, &devSerNo, devSerNoLength,
              errStream);  // ASCII Device Serial Number
            uint32_t devSoftNoLength =
              sections[1].read_8(&j, errStream);  // Length of software string
            string devSoftNo;
            sections[1].readString(
              &j, &devSoftNo, devSoftNoLength,
              errStream);  // ASCII Software Version
            uint32_t devSCPidLength =
              sections[1].read_8(&j, errStream);  // Length of SCP software ID
            string devSCPid;
            sections[1].readString(
              &j, &devSCPid, devSCPidLength,
              errStream);                                                  // ASCII SCP software ID
            uint32_t mfgStringLength = sections[1].read_8(&j, errStream);  // Length of mfg name
            string mfgString;
            sections[1].readString(
              &j, &mfgString, mfgStringLength,
              errStream);  // Manufacturer Name
            *logStream << "[TAG] Device ID:" << endl;
            *logStream << "---Institution Number: " << institution << endl;
            *logStream << "---Department Number: " << department << endl;
            *logStream << "---Device ID: " << deviceID << endl;
            *logStream << "---Device Type: " << deviceType << (deviceType ? " (Host)" : " (Cart)")
                       << endl;
            *logStream << "---Manufacturer Number: " << mfgCode
                       << ((mfgCode == 255) ? " (See string)" : "") << endl;
            *logStream << "---Model Description: " << modelDesc << endl;
            *logStream << "---SCP-ECG Version: " << (protocolRev / 10) << "." << (protocolRev % 10)
                       << endl;
            *logStream << "---SCP-ECG Compatibility: 0x" << hex << protocolCompat << dec;
            if ((protocolCompat & 0xf0) == 0xd0)
              *logStream << " (Category I)" << endl;
            else if ((protocolCompat & 0xf0) == 0xe0)
              *logStream << " (Category II)" << endl;
            else
              *logStream << " (unknown)" << endl;
            *logStream << "---Language Support: 0x" << hex << languageSupport << dec
                       << (languageSupport ? " (unknown)" : " (ASCII Only)") << endl;
            *logStream << "---Device Capabilities: 0x" << hex << deviceCapability << dec << endl;
            *logStream << "------Print: " << ((deviceCapability & 0x10) ? "yes" : "no") << endl;
            *logStream << "------Interpret: " << ((deviceCapability & 0x20) ? "yes" : "no") << endl;
            *logStream << "------Store: " << ((deviceCapability & 0x40) ? "yes" : "no") << endl;
            *logStream << "------Acquire: " << ((deviceCapability & 0x80) ? "yes" : "no") << endl;
            *logStream << "---Mains Frequency Environment: " << mainsFreq;
            if (mainsFreq == 0)
              *logStream << " (Unspecified)" << endl;
            else if (mainsFreq == 1)
              *logStream << " (50 Hz)" << endl;
            else if (mainsFreq == 2)
              *logStream << " (60 Hz)" << endl;
            else
              *errStream << " (ERROR)" << endl;
            *logStream << "---Analysis Program Revision:  (" << apRevLength << ")" << apRev << endl;
            *logStream << "---Device Serial Number: (" << devSerNoLength << ") " << devSerNo
                       << endl;
            *logStream << "---Device Software Number:  (" << devSoftNoLength << ")" << devSoftNo
                       << endl;
            *logStream << "---Device SCP Software:  (" << devSCPidLength << ")" << devSCPid << endl;
            *logStream << "---Device Manufacturer (" << mfgStringLength << "): " << mfgString
                       << endl;
            output_stream << "\"" << JSON_DEVICE_SERIAL_LABEL << "\":\"" << devSerNo.c_str()
                          << "\",";
            output_stream << "\"" << JSON_DEVICE_TYPE_LABEL << "\":\"" << modelDesc.c_str()
                          << "\",";
            output_stream << "\"" << JSON_DEVICE_FIRMWARE_VERSION_LABEL << "\":\""
                          << devSoftNo.c_str() << "\",";
          } break;
          case 25: {  // Date of Acquisition
            if (tagLength != 4) {
              *errStream << "Error Parsing Date! Length = " << (int32_t)tagLength << endl;
              return output_error_json("Date length not 4", outStream);
            }
            else {
              date_valid = 1;
              t.tm_year = sections[1].read_16(&j, errStream) - 1900;
              t.tm_mon = sections[1].read_8(&j, errStream) - 1;
              t.tm_mday = sections[1].read_8(&j, errStream);
              *logStream << "[TAG] Date: " << setw(4) << setfill('0') << (int32_t)(t.tm_year + 1900)
                         << "/" << setw(2) << (int32_t)t.tm_mon << "/" << setw(2)
                         << (int32_t)t.tm_mday << setfill(' ') << endl;
            }
          } break;
          case 26: {  // Time of Acquisition
            if (tagLength != 3) {
              *errStream << "Error Parsing Time! Length = " << tagLength << endl;
              return output_error_json("Time length not 3", outStream);
            }
            else {
              time_valid = 1;
              t.tm_hour = sections[1].read_8(&j, errStream);
              t.tm_min = sections[1].read_8(&j, errStream);
              t.tm_sec = sections[1].read_8(&j, errStream);
              t.tm_isdst = -1;
              *logStream << "[TAG] Time: " << setw(2) << setfill('0') << (int32_t)t.tm_hour << ":"
                         << setw(2) << (int32_t)t.tm_min << ":" << setw(2) << (int32_t)t.tm_sec
                         << setfill(' ') << endl;
            }
          } break;
          case 27: {  // High Pass Filter
            if (tagLength != 2) {
              *errStream << "Error Parsing HP Filter! Length = " << (int32_t)tagLength << endl;
              return output_error_json("HP filter length not 2", outStream);
            }
            else {
              uint16_t hpFilter = sections[1].read_16(&j, errStream);
              *logStream << "[TAG] High Pass Filter: " << (int32_t)hpFilter / 100 << "." << setw(2)
                         << setfill('0') << (int32_t)hpFilter % 100 << " Hz" << setfill(' ')
                         << endl;
              output_stream << "\"" << JSON_HP_FILTER_LABEL << "\":\"" << (int32_t)hpFilter
                            << "\",";
            }
          } break;
          case 28: {  // Low Pass Filter
            if (tagLength != 2) {
              *errStream << "Error Parsing LP Filter! Length = " << (int32_t)tagLength << endl;
              return output_error_json("LP filter length not 2", outStream);
            }
            else {
              uint16_t lpFilter = sections[1].read_16(&j, errStream);
              *logStream << "[TAG] Low Pass Filter: " << (int32_t)lpFilter << " Hz" << endl;
              output_stream << "\"" << JSON_LP_FILTER_LABEL << "\":\"" << (int32_t)lpFilter
                            << "\",";
            }
          } break;
          case 31: {
            string ecgSequence;
            sections[1].readString(&j, &ecgSequence, tagLength, errStream);
            *logStream << "[TAG] ECG Sequence Number: " << ecgSequence << endl;
            output_stream << "\"" << JSON_SEQUENCE_NUMBER_LABEL << "\":\"" << ecgSequence.c_str()
                          << "\",";
          } break;
          case 34: {  // Time Zone
            int16_t offset = sections[1].read_16(&j, errStream);
            uint16_t index = sections[1].read_16(&j, errStream);
            string desc;
            sections[1].readString(&j, &desc, tagLength - 4, errStream);
            time_zone = offset;
            if (32767 == time_zone) {
              time_zone = zoneOffset;
            }
            *logStream << "[TAG] Date Time Zone:" << endl;
            *logStream << "---Minutes Offset from UTC: " << offset << endl;
            *logStream << "---Time Zone Index (unused): " << index << endl;
            *logStream << "---Time Zone Description (unused): \"" << desc << "\"" << endl;
          } break;
          case 255: {  // Terminator - we're done.
            if (tagLength != 0) {
              *errStream << "Error Parsing Terminator! Length = " << (int32_t)tagLength << endl;
            }
            else {
              *logStream << "[TAG] Terminator." << endl;
              j = length;
            }
          } break;
          default: {
            *logStream << "[TAG] " << (int32_t)tag << " (unsupported)" << endl;
            j += tagLength;
          } break;
        }
      }

      if (date_valid && time_valid) {
        timestamp parsedTime(&t, 0, time_zone);
        output_stream << "\"" << JSON_START_TIMESTAMP_LABEL << "\":" << parsedTime.getJson() << ",";
      }
    }

    /**********************************************************************************************
     *          Section 2
     **********************************************************************************************/

    uint32_t **prefixBits = NULL;
    uint32_t **totalBits = NULL;
    uint32_t **switchByte = NULL;
    uint32_t **baseValue = NULL;
    uint32_t **baseCode = NULL;
    uint32_t tableCount = 0;
    uint32_t *codeCount = NULL;
    uint8_t prefix_min_bits = 0xff;
    uint8_t prefix_max_bits = 0;

    if (sections[2].exists()) {
      if (sections[2].readHeader(&length, outStream, errStream, logStream, skipCrc)) return -1;
      j = 16;
      tableCount = sections[2].read_16(&j, errStream);
      uint32_t l;
      if (19999 != tableCount) {
        *logStream << "Number of Huffman Tables: " << tableCount << endl;
        prefixBits = new uint32_t *[tableCount];
        totalBits = new uint32_t *[tableCount];
        switchByte = new uint32_t *[tableCount];
        baseValue = new uint32_t *[tableCount];
        baseCode = new uint32_t *[tableCount];

        codeCount = new uint32_t[tableCount];

        for (l = 0; l < tableCount; l++) {
          codeCount[l] = sections[2].read_16(&j, errStream);
          *logStream << "Number of codes in Table #" << (l + 1) << ": " << codeCount[l] << endl;
          prefixBits[l] = new uint32_t[codeCount[l]];
          totalBits[l] = new uint32_t[codeCount[l]];
          switchByte[l] = new uint32_t[codeCount[l]];
          baseValue[l] = new uint32_t[codeCount[l]];
          baseCode[l] = new uint32_t[codeCount[l]];
          uint32_t m;
          for (m = 0; m < codeCount[l]; m++) {
            prefixBits[l][m] = sections[2].read_8(&j, errStream);
            if (prefixBits[l][m] < prefix_min_bits) prefix_min_bits = prefixBits[l][m];
            if (prefixBits[l][m] > prefix_max_bits) prefix_max_bits = prefixBits[l][m];
            totalBits[l][m] = sections[2].read_8(&j, errStream);
            switchByte[l][m] = sections[2].read_8(&j, errStream);
            baseValue[l][m] = sections[2].read_16(&j, errStream);
            baseCode[l][m] = sections[2].read_32(&j, errStream);

            *logStream << "---Code #" << (m + 1) << " details:" << endl;
            *logStream << "------Prefix Bits: " << prefixBits[l][m] << endl;
            *logStream << "------Total Bits: " << totalBits[l][m] << endl;
            *logStream << "------Switch Byte: " << switchByte[l][m] << endl;
            *logStream << "------Base Value: " << baseValue[l][m] << endl;
            *logStream << "------Base Code: 0b";  // << baseCode[m] << " (0b";

            uint32_t n;
            uint32_t bitmask = 1;
            for (n = 0; n < prefixBits[l][m]; n++) {
              *logStream << ((baseCode[l][m] & bitmask) ? 1 : 0);
              bitmask = bitmask << 1;
            }
            *logStream << " (" << baseCode[l][m] << ")" << endl;

            // Reverse the bit-order for ease of use later
            uint32_t k;
            uint32_t temp = 0;
            for (k = 0; k < prefixBits[l][m]; k++) {
              temp = (temp << 1) | ((baseCode[l][m] & (1 << k)) ? 1 : 0);
            }
            baseCode[l][m] = temp;
          }
        }
      }
      else {
        *logStream << "Default Huffman Table used!" << endl;
        prefixBits = new uint32_t *[1];
        totalBits = new uint32_t *[1];
        switchByte = new uint32_t *[1];
        baseValue = new uint32_t *[1];
        baseCode = new uint32_t *[1];

        codeCount = new uint32_t[1];

        codeCount[0] = DEFAULT_CODE_COUNT;
        prefixBits[0] = new uint32_t[codeCount[0]];
        totalBits[0] = new uint32_t[codeCount[0]];
        switchByte[0] = new uint32_t[codeCount[0]];
        baseValue[0] = new uint32_t[codeCount[0]];
        baseCode[0] = new uint32_t[codeCount[0]];

        uint32_t m;
        for (m = 0; m < codeCount[0]; m++) {
          prefixBits[0][m] = default_prefix[m];
          totalBits[0][m] = default_total[m];
          switchByte[0][m] = 1;
          baseValue[0][m] = default_value[m];
          baseCode[0][m] = default_code[m];

          *logStream << "---Code #" << (m + 1) << " details:" << endl;
          *logStream << "------Prefix Bits: " << prefixBits[0][m] << endl;
          *logStream << "------Total Bits: " << totalBits[0][m] << endl;
          *logStream << "------Switch Byte: " << switchByte[0][m] << endl;
          *logStream << "------Base Value: " << baseValue[0][m] << endl;
          *logStream << "------Base Code: 0b";  // << baseCode[m] << " (0b";

          uint32_t n;
          uint32_t bitmask = 1;
          for (n = 0; n < prefixBits[0][m]; n++) {
            *logStream << ((baseCode[0][m] & bitmask) ? 1 : 0);
            bitmask = bitmask << 1;
          }
          *logStream << " (" << baseCode[0][m] << ")" << endl;
        }
      }
    }

    /**********************************************************************************************
     *          Section 3
     **********************************************************************************************/

    uint32_t leadCount = 0;
    uint32_t *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
    i = 0;
    if (sections[3].exists()) {
      if (sections[3].readHeader(&length, outStream, errStream, logStream, skipCrc)) return -1;
      j = 16;
      leadCount = sections[3].read_8(&j, errStream);
      uint32_t flags = sections[3].read_8(&j, errStream);
      sampleStart = new uint32_t[leadCount];
      sampleEnd = new uint32_t[leadCount];
      leadID = new uint32_t[leadCount];
      for (i = 0; i < leadCount; i++) {
        sampleStart[i] = sections[3].read_32(&j, errStream);
        sampleEnd[i] = sections[3].read_32(&j, errStream);
        leadID[i] = sections[3].read_8(&j, errStream);
      }
      *logStream << "Number of Leads: " << leadCount << endl;
      *logStream << "Flags: 0x" << hex << flags << dec << endl;
      *logStream
        << ((flags & 0x01) ? "---Reference beat subtraction used for compression"
                           : "---Reference beat subtraction not used for compression")
        << endl;
      *logStream
        << ((flags & 0x04) ? "---Leads recorded simultaneously"
                           : "---Leads not recorded simultaneously")
        << endl;
      *logStream << "---Number of simultaneously recorded leads: " << (flags / 8) << endl;

      for (i = 0; i < leadCount; i++) {
        *logStream << "Details for Lead #" << i << endl;
        *logStream << "---Starting Sample Number: " << sampleStart[i] << endl;
        *logStream << "---Ending Sample Number: " << sampleEnd[i] << endl;
        *logStream << "---Lead Identification Code: " << leadID[i];
        if (leadID[i] == 131)
          *logStream << " (ES)" << endl;
        else if (leadID[i] == 132)
          *logStream << " (AS)" << endl;
        else if (leadID[i] == 133)
          *logStream << " (AI)" << endl;
        else
          *logStream << endl;
      }
    }

    /**********************************************************************************************
     *          Section 6
     **********************************************************************************************/

    uint32_t *leadLength = NULL;
    int32_t **dataArrays = NULL;
    if (sections[6].exists()) {
      if (sections[6].readHeader(&length, outStream, errStream, logStream, skipCrc)) return -1;
      j = 16;
      uint32_t ampMult = sections[6].read_16(&j, errStream);
      uint32_t samplePeriod = sections[6].read_16(&j, errStream);
      uint32_t encoding = sections[6].read_8(&j, errStream);
      uint32_t compression = sections[6].read_8(&j, errStream);
      leadLength = new uint32_t[leadCount];
      dataArrays = new int32_t *[leadCount];
      int32_t actual_lead_count = leadCount;
      for (i = 0; i < leadCount; i++) {
        leadLength[i] = sections[6].read_16(&j, errStream);
        if (sampleEnd[i] > sampleStart[i]) {
          dataArrays[i] = new int32_t[sampleEnd[i] - sampleStart[i] + 1];
        }
        else {
          dataArrays[i] = NULL;
          actual_lead_count--;
        }
      }
      *logStream << "Amplitude multiplier: " << ampMult << " (nV/count)" << endl;
      if (ampMult) {
        *logStream << "---ADC Gain: " << (1000000. / (float)ampMult) << " (counts/mV)" << endl;
      }
      *logStream << "Sample Period: " << samplePeriod << " (us)" << endl;
      if (samplePeriod) {
        *logStream << "---Sample Rate: " << (1000000 / samplePeriod) << " (Hz)" << endl;
      }
      *logStream << "Data encoding: " << encoding << endl;
      if (encoding == 0)
        *logStream << "---Real data" << endl;
      else if (encoding == 1)
        *logStream << "---First difference data" << endl;
      else if (encoding == 2)
        *logStream << "---Second difference data" << endl;
      else
        *logStream << "---Unknown data" << endl;
      *logStream << "Compression: " << compression
                 << (compression ? " (Bimodal compression)" : " (No compression)") << endl;
      output_stream << "\"" << JSON_LEAD_COUNT_LABEL << "\":\"" << actual_lead_count << "\",";
      output_stream << "\"" << JSON_AVM_LABEL << "\":\"" << ampMult << "\",";
      output_stream << "\"" << JSON_SAMPLE_PERIOD_LABEL << "\":\"" << samplePeriod << "\",";

      map<uint32_t, const char *> lead_names;

      lead_names[0] = "Unspecified";
      lead_names[1] = "Lead I";
      lead_names[2] = "Lead II";
      lead_names[3] = "V1";
      lead_names[4] = "V2";
      lead_names[5] = "V3";
      lead_names[6] = "V4";
      lead_names[7] = "V5";
      lead_names[8] = "V6";
      lead_names[20] = "CM5";
      lead_names[61] = "Lead III";
      lead_names[64] = "aVF";
      lead_names[87] = "V";
      lead_names[131] = "ES";
      lead_names[132] = "AS";
      lead_names[133] = "AI";

      output_stream << "\"" << JSON_LEADS_LABEL << "\":[";
      for (i = 0; i < leadCount; i++) {
        *logStream << "Bytes of data for lead #" << i << ": " << leadLength[i] << endl;
        if (leadLength[i] > 0) {
          if (*lead_names[leadID[i]] != '\0') {
            output_stream << "{\"" << JSON_LEAD_NAME_LABEL << "\":\"" << lead_names[leadID[i]]
                          << "\",";
          }
          else {
            output_stream << "{\"" << JSON_LEAD_NAME_LABEL << "\":\"" << lead_names[0] << "\",";
          }
          output_stream << "\"" << JSON_START_SAMPLE_LABEL << "\":\"" << sampleStart[i] << "\",";
          output_stream << "\"" << JSON_END_SAMPLE_LABEL << "\":\"" << sampleEnd[i] << "\",";
          int32_t total_samples = sampleEnd[i] - sampleStart[i] + 1;
          output_stream << "\"" << JSON_SAMPLE_COUNT_LABEL << "\":\"" << total_samples << "\",";

          // Here's the waveform
          uint8_t *rawData = new uint8_t[leadLength[i]];
          uint32_t l;
          for (l = 0; l < leadLength[i]; l++) {
            rawData[l] = sections[6].read_8(&j, errStream);
          }

          if (!sections[2].exists()) {
            int32_t *caster = (int32_t *)rawData;
            for (l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++) {
              if (l < (leadLength[i] / 2)) {
                dataArrays[i][l] = caster[l];
                if (dataArrays[i][l] & (1 << (16 - 1))) {
                  dataArrays[i][l] |= (~0ULL) << (16);
                }
              }
              else
                dataArrays[i][l] = 0;
            }
          }
          else {
            uint8_t currentBit = 8;
            uint8_t *currentByte = rawData;
            uint32_t currentTable = 0;
            for (l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++) {
              uint32_t k = 0;
              int32_t prefixValue = 0;
              uint32_t bits = 0;
              uint8_t matchFound = 0;
              // Match the next code in the bit stream to our Huffman
              // table from Section 2
              while (!matchFound) {
                if (prefix_min_bits == prefix_max_bits) {
                  if (!bits) {
                    currentByte =
                      get_bits(&prefixValue, prefix_max_bits, currentByte, &currentBit, 0, 0);
                    bits += prefix_max_bits;
                  }
                  else
                    break;
                }
                else {
                  currentByte = get_bits(&prefixValue, 1, currentByte, &currentBit, 0, 0);
                  bits += 1;
                  if (bits > prefix_max_bits) break;
                }

                if ((bits >= prefix_min_bits) && (bits <= prefix_max_bits)) {
                  for (k = 0; k < codeCount[currentTable]; k++) {
                    if (!prefixBits[currentTable][k]) {
                      matchFound = 1;
                      break;
                    }
                    else if (prefixBits[currentTable][k] == bits) {
                      if (baseCode[currentTable][k] == (uint32_t)prefixValue) {
                        matchFound = 1;
                        break;
                      }
                    }
                  }
                }

                if ((currentByte - rawData) > leadLength[i]) {
                  sampleEnd[i] = l + sampleStart[i];
                  break;
                }
              }

              // SwitchByte == 1 means we decode a value
              if (switchByte) {
                currentByte = get_bits(
                  &dataArrays[i][l], totalBits[currentTable][k] - prefixBits[currentTable][k],
                  currentByte, &currentBit, 1, l < 3);
              }
              // SwitchByte == 0 means we switch to a different
              // table. (We don't use this on TZ Medical Monitors).
              else {
                // This is where we would switch tables
              }
            }
          }

          delete[] rawData;

          // Now we have the samples, but they might be difference encoded
          int32_t *D1 = new int32_t[leadCount];
          int32_t *D2 = new int32_t[leadCount];
          output_stream << "\"" << JSON_WAVEFORM_LABEL << "\":[";

          for (l = sampleStart[i]; l <= sampleEnd[i]; l++) {
            int32_t tempData = dataArrays[i][l - 1];
            int32_t outputData = 0;
            // *logStream << "delta[" << l << "] = " << tempData << endl;

            if (encoding == 1) {
              // First differences
              if (l == sampleStart[i]) {
                outputData = tempData;
              }
              else {
                outputData = D1[i] + tempData;
              }
              D1[i] = outputData;
            }
            else if (encoding == 2) {
              // Second Differences
              if (l <= (sampleStart[i] + 1)) {
                outputData = tempData;
              }
              else {
                outputData = tempData + (2 * D1[i]) - D2[i];
              }
              D2[i] = D1[i];
              D1[i] = outputData;
            }
            else {
              outputData = tempData;
            }

            if (l > (sampleEnd[i] - 1))
              output_stream << outputData;
            else
              output_stream << outputData << ",";
          }
          output_stream << "]";

          delete[] D1;
          delete[] D2;

          if (--actual_lead_count)
            output_stream << "},";
          else
            output_stream << "}";
        }
      }
      output_stream << "]";
    }

    /**********************************************************************************************
     *          Section 7
     **********************************************************************************************/

    uint32_t *paceTime = NULL, *paceAmplitude = NULL;
    uint32_t *paceType = NULL, *paceSource = NULL, *paceIndex = NULL, *paceWidth = NULL;
    if (sections[7].exists()) {
      if (sections[7].readHeader(&length, outStream, errStream, logStream, skipCrc)) return -1;
      j = 16;
      uint32_t referenceCount = sections[7].read_8(&j, errStream);
      uint32_t paceCount = sections[7].read_8(&j, errStream);
      uint32_t rrInterval = sections[7].read_16(&j, errStream);
      uint32_t ppInterval = sections[7].read_16(&j, errStream);
      paceTime = new uint32_t[paceCount];
      paceAmplitude = new uint32_t[paceCount];
      for (i = 0; i < paceCount; i++) {
        paceTime[i] = sections[7].read_16(&j, errStream);
        paceAmplitude[i] = sections[7].read_16(&j, errStream);
      }
      paceType = new uint32_t[paceCount];
      paceSource = new uint32_t[paceCount];
      paceIndex = new uint32_t[paceCount];
      paceWidth = new uint32_t[paceCount];
      for (i = 0; i < paceCount; i++) {
        paceType[i] = sections[7].read_8(&j, errStream);
        paceSource[i] = sections[7].read_8(&j, errStream);
        paceIndex[i] = sections[7].read_16(&j, errStream);
        paceWidth[i] = sections[7].read_16(&j, errStream);
      }
      *logStream << "Number of reference beat types: " << referenceCount << endl;
      ;
      *logStream << "Number of pacemaker spikes: " << paceCount << endl;
      *logStream << "Average RR interval: ";
      if (rrInterval == 29999)
        *logStream << "not calculated" << endl;
      else
        *logStream << rrInterval << " (ms)" << endl;
      *logStream << "Average PP interval: ";
      if (ppInterval == 29999)
        *logStream << "not calculated" << endl;
      else
        *logStream << ppInterval << " (ms)" << endl;
      for (i = 0; i < paceCount; i++) {
        *logStream << "Details for pacemaker spike #" << i + 1 << endl;
        *logStream << "---Pulse Width: ";
        if (paceWidth[i] == 0)
          *logStream << "unknown";
        else
          *logStream << paceWidth[i] << " (us)";
        *logStream << " at time: " << paceTime[i] << " (ms)" << endl;
      }
    }

    if (sections[2].exists()) {
      uint32_t l;
      for (l = 0; l < tableCount; l++) {
        delete[] prefixBits[l];
        delete[] totalBits[l];
        delete[] switchByte[l];
        delete[] baseValue[l];
        delete[] baseCode[l];
      }
      delete[] codeCount;
      delete[] prefixBits;
      delete[] totalBits;
      delete[] switchByte;
      delete[] baseValue;
      delete[] baseCode;
    }

    if (sections[3].exists()) {
      delete[] sampleStart;
      delete[] sampleEnd;
      delete[] leadID;
    }

    if (sections[6].exists()) {
      delete[] leadLength;
      uint32_t l;
      for (l = 0; l < leadCount; l++) {
        delete[] dataArrays[l];
      }
      delete[] dataArrays;
    }

    if (sections[7].exists()) {
      delete[] paceTime;
      delete[] paceAmplitude;
      delete[] paceType;
      delete[] paceSource;
      delete[] paceIndex;
      delete[] paceWidth;
    }
  }

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return output_error_json("Invalid file CRC", outStream);
    }
  }

  *logStream << "Done." << endl;  // Signal completion of program

  // Close the JSON object
  *outStream << output_stream.str() << "}" << endl;

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads an scp file from cin and outputs json on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return ScpToJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif