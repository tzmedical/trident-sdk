/******************************************************************************
 *       Copyright (c) 2019, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_generator.cpp
 *          - This program generates an SCP file for the H3R based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./scp_generator input.json > output.scp
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <vector>

#include "util/common.h"
#include "util/crc.h"
#include "util/json.h"
#include "util/timestamp.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"

#include "scp_section.h"
#include "static_scp.h"
#include "mrepeat.h"

using namespace std;
using namespace rapidjson;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int lead_name_to_id(string lead_name)
{
  if (0 == lead_name.compare("Lead I")) return (1);
  if (0 == lead_name.compare("Lead II")) return (2);
  if (0 == lead_name.compare("V1")) return (3);
  if (0 == lead_name.compare("V2")) return (4);
  if (0 == lead_name.compare("V3")) return (5);
  if (0 == lead_name.compare("V4")) return (6);
  if (0 == lead_name.compare("V5")) return (7);
  if (0 == lead_name.compare("V6")) return (8);
  if (0 == lead_name.compare("CM5")) return (20);
  if (0 == lead_name.compare("Lead III")) return (61);
  if (0 == lead_name.compare("aVF")) return (64);
  if (0 == lead_name.compare("V")) return (87);
  if (0 == lead_name.compare("ES")) return (131);
  if (0 == lead_name.compare("AS")) return (132);
  if (0 == lead_name.compare("AI")) return (133);
  return (0);
}

//==============================================================================
//! Write ECG data to file buffer
//==============================================================================
static uint16_t scp_append_data(
  scp_file_t *p_scp, vector<vector<int>> samples, const uint8_t num_channels, uint16_t num_samples,
  ostream *errStream)
{
  // Assume success
  uint16_t ret_val = 0;
  bool file_overflow = 0;
  uint32_t chan;
  uint32_t sample;
  uint32_t total_samples[SCP_CHANNELS] = {};
  uint32_t total_bytes[SCP_CHANNELS] = {};

  uint8_t *lead_data[SCP_CHANNELS];
  uint32_t lead_index = 0;
  uint32_t max_bytes;

  // Setup
  const uint32_t huffman_mask[4] = {
    0xFFFFFFE0UL,
    0xFFFFE000UL,
    0xFFE00000UL,
    0xE0000000UL,
  };
  const uint32_t huffman_code[4] = {
    0x00000000UL,
    0x00004000UL,
    0x00800000UL,
    0xC0000000UL,
  };

  for (chan = 0; chan < num_channels; chan++) {
    uint32_t channel_mask = 1 << chan;
    {
      // Store the channel data only if the channel is enabled and the cable
      // has that many channels
      if (channel_mask) {
        // Determine how many channels we're using
        // This is switched by cable type and channel_enable setting
        switch (num_channels) {
          case 1:
            lead_data[lead_index] = p_scp->section_6.lead_data_1[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 1;
            break;
          case 2:
            lead_data[lead_index] = p_scp->section_6.lead_data_2[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 2;
            break;
          case 3:
            lead_data[lead_index] = p_scp->section_6.lead_data_3[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 3;
            break;
          case 4:
            lead_data[lead_index] = p_scp->section_6.lead_data_4[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 4;
            break;
          case 5:
            lead_data[lead_index] = p_scp->section_6.lead_data_5[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 5;
            break;
          case 6:
            lead_data[lead_index] = p_scp->section_6.lead_data_6[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 6;
            break;
          case 7:
            lead_data[lead_index] = p_scp->section_6.lead_data_7[lead_index];
            max_bytes = SCP_TOTAL_DATA_SIZE / 7;
            break;
          default:
            lead_data[lead_index] = p_scp->section_6.lead_data_max[chan];
            max_bytes = SCP_TOTAL_DATA_SIZE / SCP_CHANNELS;
            break;
        }

        // Get the current number of samples in the record
        total_samples[chan] = p_scp->section_3.group_1_details[chan].sample_end
                            - p_scp->section_3.group_1_details[chan].sample_start + 1;

        // Get the current number of bytes in the record
        total_bytes[chan] = p_scp->scp_internal.lead_bytes[chan];

        int32_t last_sample = 0;

        for (sample = 0; sample < num_samples; sample++) {
          int32_t sample_value = samples[chan][sample];

          // Difference Encoding

          int32_t temp_value;

          temp_value = sample_value - last_sample;

          last_sample = sample_value;
          sample_value = temp_value;

          // Increment the total samples here so difference encoding works right
          // (We'll store the final value after the loop)
          total_samples[chan]++;

          // Huffman Encoding
          // If negative, invert and mask
          uint32_t temp_sample;
          if (sample_value & 0x80000000) {
            temp_sample = (~sample_value) & ~0x80000000;
          }
          else
            temp_sample = sample_value;

          // Figure out how many bytes we're using
          uint8_t num_bytes;
          for (num_bytes = 0; num_bytes < 3; num_bytes++) {
            if (!(temp_sample & huffman_mask[num_bytes])) break;
          }

          // Mask the bits and add the code
          sample_value = (sample_value & ~(huffman_mask[num_bytes] << 1)) | huffman_code[num_bytes];

          // Index of 0 means store 1 byte, 1 means store 2 bytes, etc.
          num_bytes++;

          // Make sure there is enough space in the buffer
          if ((total_bytes[chan] + num_bytes) > max_bytes) {
            file_overflow = 1;  // This means we didn't write _anything_
            break;              // Stop iterating through samples - we're done!
          }
          else {
            // Write bits to buffer
            uint8_t *caster = (uint8_t *)&sample_value;
            while (num_bytes--) {
              lead_data[lead_index][total_bytes[chan]++] = caster[num_bytes];
            }
          }
        }  // for each sample

        // Stop processing _any_ channel if there's an overflow - we'll store
        // all the data in the _next_ SCP file.
        if (file_overflow) break;

        // Increment the lead_index so we're ready for the next channel in the
        // iteration
        lead_index++;

      }  // if channel is enabled
    }  // if 0 != enable_mask

    p_scp->section_6.lead_lengths[chan] = max_bytes;  // total_bytes[chan];

  }  // for each channel

  if (!file_overflow) {
    for (chan = 0; chan < SCP_CHANNELS; chan++) {
      // Update the Number of Samples
      if (total_samples[chan]) {
        p_scp->section_3.group_1_details[chan].sample_end =
          total_samples[chan] + p_scp->section_3.group_1_details[chan].sample_start - 1;
      }
      else {
        p_scp->section_3.group_1_details[chan].sample_end = 1;
      }

      p_scp->scp_internal.lead_bytes[chan] = total_bytes[chan];
    }
  }
  else {
    // We didn't actually write anything
    ret_val = num_samples;

    *errStream << "Data Overflow!" << endl;
  }

  return ret_val;
}

//==============================================================================
static int clean_up_scp(scp_file_t *p_scp, uint32_t num_leads)
{
  int res = 0;
  uint16_t crc;

  // Calculate CRC for Section 0
  crc = 0xffff;
  res = crcBlock(
    (uint8_t *)&p_scp->section_0.section_header.section_id,
    p_scp->section_0.section_header.length - 2, &crc);
  if (res) return res;
  p_scp->section_0.section_header.crc = crc;

  // Calculate CRC for Section 1
  crc = 0xffff;
  res = crcBlock(
    (uint8_t *)&p_scp->section_1.section_header.section_id,
    p_scp->section_1.section_header.length - 2, &crc);
  if (res) return res;
  p_scp->section_1.section_header.crc = crc;

  // Calculate CRC for Section 2
  crc = 0xffff;
  res = crcBlock(
    (uint8_t *)&p_scp->section_2.section_header.section_id,
    p_scp->section_2.section_header.length - 2, &crc);
  if (res) return res;
  p_scp->section_2.section_header.crc = crc;

  // Calculate CRC for Section 3
  crc = 0xffff;
  res = crcBlock(
    (uint8_t *)&p_scp->section_3.section_header.section_id,
    p_scp->section_3.section_header.length - 2, &crc);
  if (res) return res;
  p_scp->section_3.section_header.crc = crc;

  // Calculate CRC for Section 6
  crc = 0xffff;
  res = crcBlock(
    (uint8_t *)&p_scp->section_6.section_header.section_id,
    p_scp->section_6.section_header.length - 2, &crc);
  if (res) return res;
  p_scp->section_6.section_header.crc = crc;

  // Pack the pulse detections properly
  uint8_t pace_count = p_scp->section_7.pacemaker_pulses;
  void *p_dst = &p_scp->section_7.pace_list[pace_count];
  void *p_src = &p_scp->section_7.pace_details[0];
  memmove(p_dst, p_src, pace_count * sizeof(section_7_pace_details_t));
  p_src = &p_scp->section_7.pace_details[pace_count];
  uint32_t len = (SCP_PACE_COUNT - pace_count) * sizeof(section_7_pace_details_t);
  memset(p_src, 0, len);

  // Calculate CRC for Section 7, IF we're not packing data
  crc = 0xffff;
  res = crcBlock(
    (uint8_t *)&p_scp->section_7.section_header.section_id,
    p_scp->section_7.section_header.length - 2, &crc);
  if (res) return res;
  p_scp->section_7.section_header.crc = crc;

  // Calculate CRC for full file
  crc = 0xffff;
  res = crcBlock((uint8_t *)&p_scp->file_length, p_scp->file_length - 2, &crc);
  if (res) return res;
  p_scp->file_crc = crc;

  return res;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int ScpFromJson(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  // DISABLE CLOG DEBUG OUTPUT
  logStream->rdbuf(NULL);

  uint32_t i;
  scp_file_t scp_out_file = *(get_base_scp());

  // Parse a JSON string into DOM.
  IStreamWrapper is(*inStream);
  Document d;
  if (d.ParseStream(is).HasParseError()) {
    *errStream << "Parse Error: " << GetParseError_En(d.GetParseError()) << " at "
               << d.GetErrorOffset() << endl;
    return -1;
  }

  string file_identifier("");
  retVal = common_get_json_file_type(d, file_identifier, errStream);
  if (retVal) {
    return retVal;
  }
  if (file_identifier.find(SCP_ID_STRING) == string::npos) {
    *errStream << "ERROR! Invalid Format ID: " << file_identifier << endl;
    return -1;
  }

  // Gets the serial number embedded in the header of the file
  string device_serial("");
  retVal = common_get_json_device_serial(d, device_serial, errStream);
  if (retVal) {
    return retVal;
  }

  // Gets the Patient ID embedded in the header of the file
  string patient_id("");
  retVal = common_get_json_patient_id(d, patient_id, errStream);
  if (retVal) {
    return retVal;
  }

  // Gets the Sequence Number embedded in the header of the file
  string sequence_number("");
  if (d.HasMember(JSON_SEQUENCE_NUMBER_LABEL) && d[JSON_SEQUENCE_NUMBER_LABEL].IsString()) {
    sequence_number.assign(d[JSON_SEQUENCE_NUMBER_LABEL].GetString());
    if (sequence_number.length() > SEQUENCE_LENGTH - 1) {
      *errStream << "ERROR! Invalid Sequence Number (too long!): " << sequence_number << endl;
      return -1;
    }
  }

  // Gets the DEVICE_ID field in the header
  string device_id("");
  retVal = common_get_json_device_type(d, device_id, errStream);
  if (retVal) {
    return retVal;
  }

  string firmware_version("");
  if (
    d.HasMember(JSON_DEVICE_FIRMWARE_VERSION_LABEL)
    && d[JSON_DEVICE_FIRMWARE_VERSION_LABEL].IsString()) {
    firmware_version.assign(d[JSON_DEVICE_FIRMWARE_VERSION_LABEL].GetString());

    if (!firmware_version.length() || (firmware_version.length() > 6)) {
      *errStream << "ERROR! Invalid Firmware Version: " << firmware_version << endl;
      return -1;
    }
  }

  timestamp parsedTime;
  retVal = common_get_json_date_time(d, parsedTime, JSON_START_TIMESTAMP_LABEL, errStream);
  if (retVal) {
    return retVal;
  }

  uint32_t hp_filter = 0;
  string hp_filter_s("");
  if (d.HasMember(JSON_HP_FILTER_LABEL) && d[JSON_HP_FILTER_LABEL].IsString()) {
    hp_filter_s.assign(d[JSON_HP_FILTER_LABEL].GetString());
    hp_filter = stoi(hp_filter_s);
  }

  uint32_t lp_filter = 0;
  string lp_filter_s("");
  if (d.HasMember(JSON_LP_FILTER_LABEL) && d[JSON_LP_FILTER_LABEL].IsString()) {
    lp_filter_s.assign(d[JSON_LP_FILTER_LABEL].GetString());
    lp_filter = stoi(lp_filter_s);
  }

  uint32_t avm = 0;
  string avm_s("");
  if (d.HasMember(JSON_AVM_LABEL) && d[JSON_AVM_LABEL].IsString()) {
    avm_s.assign(d[JSON_AVM_LABEL].GetString());
    avm = stoi(avm_s);
  }
  else {
    *errStream << "ERROR! Missing AVM!" << endl;
    return -1;
  }

  uint32_t sample_period = 0;
  string sample_period_s("");
  if (d.HasMember(JSON_SAMPLE_PERIOD_LABEL) && d[JSON_SAMPLE_PERIOD_LABEL].IsString()) {
    sample_period_s.assign(d[JSON_SAMPLE_PERIOD_LABEL].GetString());
    sample_period = stoi(sample_period_s);
  }
  else {
    *errStream << "ERROR! Missing Sample Period!" << endl;
    return -1;
  }

  uint32_t lead_count = 0;
  string lead_count_s("");
  if (d.HasMember(JSON_LEAD_COUNT_LABEL) && d[JSON_LEAD_COUNT_LABEL].IsString()) {
    lead_count_s.assign(d[JSON_LEAD_COUNT_LABEL].GetString());
    lead_count = stoi(lead_count_s);
  }
  else {
    *errStream << "ERROR! Missing Lead Count!" << endl;
    return -1;
  }

  const rapidjson::Value &leads = d[JSON_LEADS_LABEL];

  vector<vector<int>> samples;
  vector<int> num_samples;
  vector<string> lead_names;

  for (uint8_t i = 0; i < lead_count; i++) {
    if (samples.size() <= i) {
      samples.push_back({});
    }

    string temp_lead_name("");
    if (leads[i].HasMember(JSON_LEAD_NAME_LABEL) && leads[i][JSON_LEAD_NAME_LABEL].IsString()) {
      temp_lead_name.assign(leads[i][JSON_LEAD_NAME_LABEL].GetString());
      lead_names.push_back(temp_lead_name);
    }

    const rapidjson::Value &lead_data = leads[i][JSON_WAVEFORM_LABEL];

    int lead_samples = lead_data.Size();
    num_samples.push_back(lead_samples);

    for (int j = 0; j < lead_samples; j++) {
      samples[i].push_back(lead_data[j].GetInt());
    }
  }

  int num_samples_check = num_samples[0];

  for (uint8_t i = 0; i < lead_count; i++) {
    if (num_samples_check != num_samples[i]) {
      *errStream << "ERROR! Different number of samples between leads!" << endl;
      return -1;
    }
  }

  // Fill in the struct with the data we've read
  patient_id.copy(scp_out_file.section_1.patient_id, PATIENT_ID_LENGTH - 1, 0);
  device_serial.copy(
    scp_out_file.section_1.tag_14_data.serial_number_string, SERIAL_NUMBER_LENGTH, 0);
  device_id.copy(scp_out_file.section_1.tag_14_data.model_desc, 6 - 1, 0);
  firmware_version.copy(
    scp_out_file.section_1.tag_14_data.firmware_version_string, FVERSION_STRING_LENGTH - 1, 0);
  sequence_number.copy(scp_out_file.section_1.sequence_string, SEQUENCE_LENGTH - 1, 0);

  struct tm t = parsedTime.getRawStruct();
  int16_t timezone_offset = parsedTime.getTimeZone();
  scp_out_file.section_1.date_year = t.tm_year + 1900;
  scp_out_file.section_1.date_month = t.tm_mon + 1;
  scp_out_file.section_1.date_day = t.tm_mday;
  scp_out_file.section_1.time_hour = t.tm_hour;
  scp_out_file.section_1.time_minute = t.tm_min;
  scp_out_file.section_1.time_second = t.tm_sec;
  scp_out_file.section_1.time_zone_offset = timezone_offset;

  scp_out_file.section_1.hp_filter_value = hp_filter;
  scp_out_file.section_1.lp_filter_value = lp_filter;

  for (i = 0; i < lead_count; i++) {
    scp_out_file.section_3.group_1_details[i].lead_id = lead_name_to_id(lead_names[i]);
  }

  scp_out_file.section_6.avm = avm;
  scp_out_file.section_6.sample_period = sample_period;

  scp_append_data(&scp_out_file, samples, lead_count, num_samples_check, errStream);

  clean_up_scp(&scp_out_file, lead_count);

  const char *scp_buffer = (const char *)&scp_out_file;

  outStream->write(scp_buffer, scp_out_file.file_length);
  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a file from cin and outputs scp on cout
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return ScpFromJson(input, extraArgs, &cout, &cerr, &clog);
}
#endif
