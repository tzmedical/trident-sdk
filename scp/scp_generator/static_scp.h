#ifndef SCP_STATIC_H
#define SCP_STATIC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "scp_section.h"

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

const scp_file_t* get_base_scp();

#ifdef __cplusplus
}
#endif

#endif