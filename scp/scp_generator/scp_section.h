//! \file
//! \brief -- THIS FILE NEEDS A BRIEF DESCRIPTION --

#ifndef SCP_SECTION_H
#define SCP_SECTION_H

#include <inttypes.h>

// Push current packing values so we can restore them at the end of the file
#pragma pack(push, scp_section)

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define SERIAL_NUMBER_LENGTH   10
#define FVERSION_STRING_ARRAY  "v1.7"
#define FVERSION_STRING_LENGTH (sizeof(FVERSION_STRING_ARRAY))

//! Number of channels supported by the SCP implementation.
#define SCP_CHANNELS 8

//! Number of bytes reserved for each channel of data. This number is set to result in a file
//! size of < 32768
#define SCP_TOTAL_DATA_SIZE (31236)

//! Number of SCP sections for table of contents. Min value is 12.
#define SCP_SECTION_COUNT 12

//! Number of bytes for the analysis program revision length. 1 means unused
#define ANALYSIS_REV_LENGTH 1

//! Number of bytes for the scp program revision length. 1 means unused.
#define SCP_SOFTWARE_LENGTH 1

//! Number of bytes for the mfg id  string.
#define MFG_STRING_LENGTH 17

#define MFG_STRING "TZ Medical, Inc."  //!< Manufacturer ID Sting for tag 14

//! Number of bytes for the time zone description string. 1 means unused.
#define TIME_ZONE_DESC_LENGTH 1

#define SCP_PACE_COUNT \
  100  //!< Number of pacemaker spikes allowed
       //!< per file.

//! Number of bytes (plus 1 for '\0') for the patient id field
#define PATIENT_ID_LENGTH 40

//! Number of bytes (plus 1 for '\0') for the sequence number
#define SEQUENCE_LENGTH 9

//! Number of huffman codes stored in section 2
#define SECTION_2_NUM_CODES 4

// From scp_generator

#define MAX_FILE_LENGTH (10 * 1024 * 1024)

#define SCP_ID_STRING       "SCPECG"
#define SCP_VERSION         22
#define MAX_SEQUENCE_NUMBER 300000
#define MAX_SAMPLE_COUNT    30720

#define MACRO_FILL(unused1, val) val,

#define PROTOCOL_COMPAT  (0xD0)
#define ECG_CAPABILITIES (0xC0)

#define LEAD_ID_UNSPECIFIED (0)
#define LEAD_ID_LEAD_I      (1)
#define LEAD_ID_LEAD_II     (2)
#define LEAD_ID_V1          (3)
#define LEAD_ID_V2          (4)
#define LEAD_ID_V3          (5)
#define LEAD_ID_V4          (6)
#define LEAD_ID_V5          (7)
#define LEAD_ID_V6          (8)
#define LEAD_ID_CM5         (20)
#define LEAD_ID_LEAD_III    (61)
#define LEAD_ID_aVF         (64)
#define LEAD_ID_V           (87)
#define LEAD_ID_ES          (131)
#define LEAD_ID_AS          (132)
#define LEAD_ID_AI          (133)

#define SECTION_0_IDX   (7)
#define SECTION_0_SIZE  (sizeof(section_0_t))
#define SECTION_1_IDX   (SECTION_0_IDX + SECTION_0_SIZE)
#define SECTION_1_SIZE  (sizeof(section_1_t))
#define SECTION_2_IDX   (SECTION_1_IDX + SECTION_1_SIZE)
#define SECTION_2_SIZE  (sizeof(section_2_t))
#define SECTION_3_IDX   (SECTION_2_IDX + SECTION_2_SIZE)
#define SECTION_3_SIZE  (sizeof(section_3_t))
#define SECTION_4_IDX   (SECTION_3_IDX + SECTION_3_SIZE)
#define SECTION_4_SIZE  (0)
#define SECTION_5_IDX   (SECTION_4_IDX + SECTION_4_SIZE)
#define SECTION_5_SIZE  (0)
#define SECTION_6_IDX   (SECTION_5_IDX + SECTION_5_SIZE)
#define SECTION_6_SIZE  (sizeof(section_6_t))
#define SECTION_7_IDX   (SECTION_6_IDX + SECTION_6_SIZE)
#define SECTION_7_SIZE  (sizeof(section_7_t))
#define SECTION_8_IDX   (SECTION_7_IDX + SECTION_7_SIZE)
#define SECTION_8_SIZE  (0)
#define SECTION_9_IDX   (SECTION_8_IDX + SECTION_8_SIZE)
#define SECTION_9_SIZE  (0)
#define SECTION_10_IDX  (SECTION_9_IDX + SECTION_9_SIZE)
#define SECTION_10_SIZE (0)
#define SECTION_11_IDX  (SECTION_10_IDX + SECTION_10_SIZE)
#define SECTION_11_SIZE (0)

//! Mask value to look for most significant bit for huffman encoding
#define HUFFMAN_MASK_VALUE 0x00800000UL
#define HUFFMAN_MASK_BITS  (24)

//! Mask for checking if a huffman encoded byte is negative
#define NEGATIVE_MASK (0x20)

#define NOISE_FREE_BITS (19)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//==============================================================================
typedef struct scp_internal_t {
  uint8_t bit_index[SCP_CHANNELS];
  uint32_t p1_value[SCP_CHANNELS];
  uint32_t p2_value[SCP_CHANNELS];
  uint16_t lead_bytes[SCP_CHANNELS];
} scp_internal_t;

#pragma pack(push, sections, 1)

//==============================================================================
typedef struct section_header_t {
  uint16_t crc;
  uint16_t section_id;
  uint32_t length;
  uint8_t section_version;
  uint8_t protocol_version;
  char format_id[6];
} section_header_t;

//==============================================================================
typedef struct section_0_entry_t {
  uint16_t section_id;
  uint32_t length;
  uint32_t index;
} section_0_entry_t;

//==============================================================================
typedef struct section_0_t {
  section_header_t section_header;
  section_0_entry_t section_0_entries[SCP_SECTION_COUNT];
} section_0_t;

//==============================================================================
typedef struct tag_14_t {
  uint16_t institution_number;
  uint16_t department_number;
  uint16_t device_id_number;
  uint8_t device_type;
  uint8_t mfg_byte;
  char model_desc[6];
  uint8_t protocol_revision;
  uint8_t protocol_compat;
  uint8_t language;
  uint8_t capabilities;
  uint8_t mains_frequency;
  char reserved[16];
  uint8_t analysis_rev_length;
  char analysis_rev_string[ANALYSIS_REV_LENGTH];
  uint8_t serial_number_length;
  char serial_number_string[SERIAL_NUMBER_LENGTH + 1];
  uint8_t firmware_version_length;
  char firmware_version_string[FVERSION_STRING_LENGTH];
  uint8_t scp_software_length;
  char scp_software_string[SCP_SOFTWARE_LENGTH];
  uint8_t mfg_length;
  char mfg_string[MFG_STRING_LENGTH];
} tag_14_t;

//==============================================================================
typedef struct section_1_t {
  section_header_t section_header;

  uint8_t patient_id_tag;
  uint16_t patient_id_length;
  char patient_id[PATIENT_ID_LENGTH];

  uint8_t device_id_tag;
  uint16_t device_id_length;
  tag_14_t tag_14_data;

  uint8_t date_tag;
  uint16_t date_length;
  uint16_t date_year;
  uint8_t date_month;
  uint8_t date_day;

  uint8_t time_tag;
  uint16_t time_length;
  uint8_t time_hour;
  uint8_t time_minute;
  uint8_t time_second;

  uint8_t time_zone_tag;
  uint16_t time_zone_length;
  int16_t time_zone_offset;
  uint16_t time_zone_table;
  char time_zone_desc[TIME_ZONE_DESC_LENGTH];

  uint8_t hp_filter_tag;
  uint16_t hp_filter_length;
  uint16_t hp_filter_value;

  uint8_t lp_filter_tag;
  uint16_t lp_filter_length;
  uint16_t lp_filter_value;

  uint8_t sequence_tag;
  uint16_t sequence_length;
  char sequence_string[SEQUENCE_LENGTH];

  uint8_t terminator_tag;
  uint16_t terminator_length;

  // Needed because of differences between __attribute__ and #pragma packing
  uint8_t padding;
} section_1_t;

//==============================================================================
typedef struct section_2_entry_t {
  uint8_t prefix_bits;
  uint8_t total_bits;
  uint8_t table_mode;
  uint16_t base_value;
  uint32_t base_code;
} section_2_entry_t;

//==============================================================================
typedef struct section_2_t {
  section_header_t section_header;

  uint16_t num_tables;
  uint16_t num_codes;

  section_2_entry_t codes[SECTION_2_NUM_CODES];
} section_2_t;

//==============================================================================
typedef struct section_3_entry_t {
  uint32_t sample_start;
  uint32_t sample_end;
  uint8_t lead_id;
} section_3_entry_t;

//==============================================================================
typedef struct section_3_t {
  section_header_t section_header;

  uint8_t lead_count;
  uint8_t group_1_flags;
  section_3_entry_t group_1_details[SCP_CHANNELS];
} section_3_t;

//==============================================================================
typedef struct section_6_t {
  section_header_t section_header;

  uint16_t avm;
  uint16_t sample_period;
  uint8_t difference_encoding;
  uint8_t bimodal_compression;
  uint16_t lead_lengths[SCP_CHANNELS];
  union {
    uint8_t lead_data_max[SCP_CHANNELS][SCP_TOTAL_DATA_SIZE / SCP_CHANNELS];
    uint8_t lead_data_7[7][SCP_TOTAL_DATA_SIZE / 7];
    uint8_t lead_data_6[6][SCP_TOTAL_DATA_SIZE / 6];
    uint8_t lead_data_5[5][SCP_TOTAL_DATA_SIZE / 5];
    uint8_t lead_data_4[4][SCP_TOTAL_DATA_SIZE / 4];
    uint8_t lead_data_3[3][SCP_TOTAL_DATA_SIZE / 3];
    uint8_t lead_data_2[2][SCP_TOTAL_DATA_SIZE / 2];
    uint8_t lead_data_1[1][SCP_TOTAL_DATA_SIZE / 1];
  };
} section_6_t;

//==============================================================================
typedef struct section_7_pace_list_t {
  uint16_t time;
  uint16_t amplitude;
} section_7_pace_list_t;

//==============================================================================
typedef struct section_7_pace_details_t {
  uint8_t type;
  uint8_t source;
  uint16_t qrs_index;
  uint16_t width;
} section_7_pace_details_t;

//==============================================================================
typedef struct section_7_t {
  section_header_t section_header;

  uint8_t reference_beats;
  uint8_t pacemaker_pulses;
  uint16_t mean_rr_interval;
  uint16_t mean_pp_interval;

  section_7_pace_list_t pace_list[SCP_PACE_COUNT];
  section_7_pace_details_t pace_details[SCP_PACE_COUNT];
} section_7_t;

#pragma pack(pop, sections)

#pragma pack(push, file, 2)
//==============================================================================
typedef struct scp_file_t {
  uint16_t file_crc;
  uint32_t file_length;
  section_0_t section_0;
  section_1_t section_1;
  section_2_t section_2;
  section_3_t section_3;
  section_6_t section_6;
  section_7_t section_7;

  scp_internal_t scp_internal;
} scp_file_t;

#pragma pack(pop, file)

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

#pragma pack(pop, scp_section)
#endif /* SCP_SECTION_H */
