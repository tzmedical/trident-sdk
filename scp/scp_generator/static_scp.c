#include "scp_section.h"
#include "mrepeat.h"
#include "static_scp.h"

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//! The default values for the scp file header information.
static const scp_file_t
  const_scp_file =
    {
      .file_crc = 0,
      .file_length = sizeof(scp_file_t) - sizeof(scp_internal_t),

      //! The static portion of section 0, listing the default lengths for each
      //! subsection.
      .section_0 =
        {
          .section_header =
            {
              .crc = 0,
              .section_id = 0,
              .length = SECTION_0_SIZE,
              .section_version = SCP_VERSION,
              .protocol_version = SCP_VERSION,
              .format_id = {'S', 'C', 'P', 'E', 'C', 'G'},
            },

          .section_0_entries =
            {
              {.section_id = 0, .length = SECTION_0_SIZE, .index = SECTION_0_IDX},
              {.section_id = 1, .length = SECTION_1_SIZE, .index = SECTION_1_IDX},
              {.section_id = 2, .length = SECTION_2_SIZE, .index = SECTION_2_IDX},
              {.section_id = 3, .length = SECTION_3_SIZE, .index = SECTION_3_IDX},
              {.section_id = 4, .length = SECTION_4_SIZE, .index = SECTION_4_IDX},
              {.section_id = 5, .length = SECTION_5_SIZE, .index = SECTION_5_IDX},
              {.section_id = 6, .length = SECTION_6_SIZE, .index = SECTION_6_IDX},
              {.section_id = 7, .length = SECTION_7_SIZE, .index = SECTION_7_IDX},
              {.section_id = 8, .length = SECTION_8_SIZE, .index = SECTION_8_IDX},
              {.section_id = 9, .length = SECTION_9_SIZE, .index = SECTION_9_IDX},
              {.section_id = 10, .length = SECTION_10_SIZE, .index = SECTION_10_IDX},
              {.section_id = 11, .length = SECTION_11_SIZE, .index = SECTION_11_IDX},
            },
        },

      //! The static portion of section 1. We will use pre-allocated string lengths
      //! for all the dynamic strings to make processing easier. Strings will be NULL
      //! padded to size automatically by this buffer.
      .section_1 =
        {
          .section_header =
            {
              .crc = 0,
              .section_id = 1,
              .length = SECTION_1_SIZE,
              .section_version = SCP_VERSION,
              .protocol_version = SCP_VERSION,
              .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'},
            },

          .patient_id_tag = 2,
          .patient_id_length = PATIENT_ID_LENGTH,
          .patient_id = {MREPEAT(PATIENT_ID_LENGTH, MACRO_FILL, '\0')},

          .device_id_tag = 14,
          .device_id_length = sizeof(tag_14_t),
          .tag_14_data =
            {
              .institution_number = 0,
              .department_number = 0,
              .device_id_number = 0,
              .device_type = 0,
              .mfg_byte = 255,  // (check string)
              .model_desc = {0},
              .protocol_revision = SCP_VERSION,
              .protocol_compat = PROTOCOL_COMPAT,
              .language = 0,  // (ASCII only)
              .capabilities = ECG_CAPABILITIES,
              .mains_frequency = 0,  // (undefined)
              .reserved =
                {
                  MREPEAT(16, MACRO_FILL, '\0')  // RESERVED
                },
              .analysis_rev_length = ANALYSIS_REV_LENGTH,
              .analysis_rev_string =
                {
                  MREPEAT(ANALYSIS_REV_LENGTH, MACRO_FILL, '\0')  // UNUSED
                },
              .serial_number_length = SERIAL_NUMBER_LENGTH + 1,
              .serial_number_string = {MREPEAT(SERIAL_NUMBER_LENGTH, MACRO_FILL, '\0')},
              .firmware_version_length = FVERSION_STRING_LENGTH,
              .firmware_version_string = {0},
              .scp_software_length = SCP_SOFTWARE_LENGTH,
              .scp_software_string =
                {
                  MREPEAT(SCP_SOFTWARE_LENGTH, MACRO_FILL, '\0')  // UNUSED
                },
              .mfg_length = MFG_STRING_LENGTH,
              .mfg_string =
                {'T', 'Z', ' ', 'M', 'e', 'd', 'i', 'c', 'a', 'l', ',', ' ', 'I', 'n', 'c', '.'},
            },

          .date_tag = 25,
          .date_length = 4,
          .date_year = 0,
          .date_month = 0,
          .date_day = 0,

          .time_tag = 26,
          .time_length = 3,
          .time_hour = 0,
          .time_minute = 0,
          .time_second = 0,

          .time_zone_tag = 34,
          .time_zone_length = 4 + TIME_ZONE_DESC_LENGTH,
          .time_zone_offset = 0,
          .time_zone_table = 0,  // UNUSED
          .time_zone_desc = {MREPEAT(TIME_ZONE_DESC_LENGTH, MACRO_FILL, '\0')},

          .hp_filter_tag = 27,
          .hp_filter_length = 2,
          .hp_filter_value = 0,

          .lp_filter_tag = 28,
          .lp_filter_length = 2,
          .lp_filter_value = 1000,

          .sequence_tag = 31,
          .sequence_length = SEQUENCE_LENGTH,
          .sequence_string = {MREPEAT(SEQUENCE_LENGTH, MACRO_FILL, '\0')},

          .terminator_tag = 255,
          .terminator_length = 0,
        },

      //! This section defines the Huffman Table used for encoding data. This table
      //! has a minimum sample size of 5 bits and a maximum sample size of 27 bits.
      //! Ideally, this will be able to store 10 seconds of data at 1024 Hz in a 32
      //! kB file.
      .section_2 =
        {
          .section_header =
            {
              .crc = 0,
              .section_id = 2,
              .length = SECTION_2_SIZE,
              .section_version = SCP_VERSION,
              .protocol_version = SCP_VERSION,
              .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'},
            },

          .num_tables = 1,
          .num_codes = SECTION_2_NUM_CODES,

          .codes =
            {
              // CODE 0
              {
                .prefix_bits = 2,
                .total_bits = 8,
                .table_mode = 1,
                .base_value = 0,
                .base_code = 0  // Base Code 0b00
              },

              // CODE 1
              {
                .prefix_bits = 2,
                .total_bits = 16,
                .table_mode = 1,
                .base_value = 0,
                .base_code = 2  // Base Code 0b01
              },

              // CODE 2
              {
                .prefix_bits = 2,
                .total_bits = 24,
                .table_mode = 1,
                .base_value = 0,
                .base_code = 1  // Base Code 0b10
              },

              // CODE 3
              {
                .prefix_bits = 2,
                .total_bits = 32,
                .table_mode = 1,
                .base_value = 0,
                .base_code = 3  // Base Code 0b11
              },
            },
        },

      //! The static portion of section 3, listing SCP_CHANNELS channels of data,
      //! all recorded simultaneously. Lists sample range of 1 to 1
      .section_3 =
        {
          .section_header =
            {
              .crc = 0,
              .section_id = 3,
              .length = SECTION_3_SIZE,
              .section_version = SCP_VERSION,
              .protocol_version = SCP_VERSION,
              .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'},
            },

          .lead_count = SCP_CHANNELS,
          .group_1_flags = (SCP_CHANNELS << 3) | 0x04,  // Flags: no ref beat,
                                                        // simultaneous recording

          .group_1_details =
            {
#define SECTION_3_MACRO_REPEAT(unused1, unused2) \
  {.sample_start = 1, .sample_end = 0, .lead_id = LEAD_ID_UNSPECIFIED},
              MREPEAT(SCP_CHANNELS, SECTION_3_MACRO_REPEAT, ~)
#undef SECTION_3_MACRO_REPEAT
            },
        },

      //! The static portion of section 6, with the following values:
      //! ---AVM: 0
      //! ---Sample Period: 0
      //! ---Difference Encoding: 1
      //! ---Bimodal Compression: 0
      //! ---Lead Lengths: 0
      .section_6 =
        {
          .section_header =
            {
              .crc = 0,
              .section_id = 6,
              .length = SECTION_6_SIZE,
              .section_version = SCP_VERSION,
              .protocol_version = SCP_VERSION,
              .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'},
            },

          .avm = 0,
          .sample_period = 0,
          .difference_encoding = 1,
          .bimodal_compression = 0,
          .lead_lengths = {MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)},
        },

      //! The static portion of Section 7, listing 0 pacemaker detections
      .section_7 =
        {
          .section_header =
            {
              .crc = 0,
              .section_id = 7,
              .length = SECTION_7_SIZE,
              .section_version = SCP_VERSION,
              .protocol_version = SCP_VERSION,
              .format_id = {'\0', '\0', '\0', '\0', '\0', '\0'},
            },

          .reference_beats = 0,
          .pacemaker_pulses = 0,
          .mean_rr_interval = 29999,  // Average R-R Interval: 29999 means not computed
          .mean_pp_interval = 29999   // Average P-P Interval: 29999 means not computed
        },

      .scp_internal =
        {
          .bit_index = {MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)},
          .p1_value = {MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)},
          .p2_value = {MREPEAT(SCP_CHANNELS, MACRO_FILL, 0)},
        },
};

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

const scp_file_t* get_base_scp()
{
  return &const_scp_file;
}