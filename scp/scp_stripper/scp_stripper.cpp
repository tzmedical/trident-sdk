/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_stripper.cpp
 *          - This program locates every *.scp file within a specified
 *             directory, parses it, and then saves the raw data to a *.txt
 *             file with a name matching that of the *.scp file.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./scp_stripper /scp/directory/path/
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <ctime>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#include "util/win_dirent.h"
#else
#include <dirent.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

/***   Feature Defines   ***/
#define CHECK_CRC  // Comment this line to disable CRC Checking

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int32_t sectionHeader(uint8_t *pData, uint32_t *length)
{
  uint32_t i = 0;
  uint16_t fileCrcValue = binary_read16(pData, &i);  // CRC for section
  uint32_t id = binary_read16(pData, &i);            // Section ID for Section
  *length = binary_read32(pData, &i);                // Length for Section
  uint32_t sVersion = binary_read8(pData, &i);       // Section Version (2.2)
  uint32_t pVersion = binary_read8(pData, &i);       // Protocol Version (2.2)

  clog << "\n****** Section " << id << " Header ******" << endl;

  uint16_t calculatedCrcValue = 0xffff;
  crcBlock(&pData[2], (*length) - 2, &calculatedCrcValue);

#ifdef CHECK_CRC
  if (calculatedCrcValue != fileCrcValue) {
    cerr << "ERROR: CRC error! file: 0x" << hex << fileCrcValue << " - calculated: 0x"
         << calculatedCrcValue << dec << endl;
    return -1;
  }
#else
  clog << "Ignoring CRC (0x" << hex << fileCrcValue << " : 0x" << calculatedCrcValue << ")" << dec
       << endl;
#endif
  clog << "Section Length: " << *length << endl;
  clog << "Section Version: " << sVersion / 10 << "." << sVersion % 10 << endl;
  clog << "Protocol Version: " << pVersion / 10 << "." << pVersion % 10 << endl;

  return 0;
}

//==============================================================================
static uint8_t *get_bits(
  int32_t *out_data, uint32_t bits, uint8_t *in_byte, uint8_t *bit_num, uint32_t sign_extend,
  uint32_t debug)
{
  uint32_t sign_bit = bits - 1;
  *out_data = 0;

  while (bits) {
    if (bits <= *bit_num) {
      *out_data |= ((*in_byte) >> (*bit_num - bits)) & (0xff >> (8 - bits));
      *bit_num -= bits;
      bits = 0;
    }
    else {
      *out_data |= ((int32_t)*in_byte & (0x00ff >> (8 - *bit_num))) << (bits - *bit_num);
      bits -= *bit_num;
      *bit_num = 0;
    }

    if (0 == *bit_num) {
      *bit_num += 8;
      in_byte++;
    }
  }

  // Sign extend the value we read
  if (sign_extend && (*out_data & (1 << sign_bit))) {
    *out_data |= (~0ULL) << sign_bit;
  }

  return in_byte;
}

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

//==============================================================================
class subSection {
 private:
  uint16_t id;
  uint32_t length;
  uint32_t index;
  uint8_t *pData;

 public:
  //========================================================================
  subSection()
  {
    id = 0;
    length = 0;
    index = 0;
    pData = new uint8_t[2];
  }

  //========================================================================
  ~subSection()
  {
    delete[] pData;
  }

  //========================================================================
  void init(uint16_t i, uint32_t len, uint32_t ind, uint8_t *pD)
  {
    id = i;
    length = len;
    index = ind;
    delete[] pData;
    pData = new uint8_t[length];
    uint32_t j;
    for (j = 0; j < length; j++) {
      pData[j] = pD[j + index - 1];
    }
  }

  //========================================================================
  unsigned read_8(uint32_t *offset)
  {
    if (*offset > (length - 1)) {
      cerr << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read8(pData, offset);
  }

  //========================================================================
  unsigned read_16(uint32_t *offset)
  {
    if (*offset > (length - 2)) {
      cerr << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read16(pData, offset);
  }

  //========================================================================
  unsigned read_32(uint32_t *offset)
  {
    if (*offset > (length - 4)) {
      cerr << "Section Read Overflow!" << endl;
      return 0;
    }
    return binary_read32(pData, offset);
  }

  //========================================================================
  void readString(uint32_t *offset, string *str, uint32_t len)
  {
    if (*offset > (length - len)) {
      cerr << "Section Read Overflow!" << endl;
      return;
    }
    str->assign((const char *)&(pData[*offset]), (int32_t)len);
    *offset += len;
  }

  //========================================================================
  char *getPointer(uint32_t *offset)
  {
    return (char *)&pData[*offset];
  }

  //========================================================================
  int32_t readHeader(uint32_t *length)
  {
    return sectionHeader(pData, length);
  }

  //========================================================================
  bool exists()
  {
    return length ? 1 : 0;
  }
};

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
//    main()
//
//    This function processes each *.scp in the directory and outputs a
//    corresponding *.txt file with the ECG data for each valid file found.
//==============================================================================
int32_t main(int32_t argc, char *argv[])
{
  string dirName = "./";

  if (argc >= 2) {
    dirName.assign(argv[1]);
  }

  DIR *pDir = NULL;
  pDir = opendir(dirName.c_str());

  if (pDir == NULL) {
    cerr << "ERROR: Could not open Directory" << endl;
    return 0;
  }

  // DISABLE CLOG DEBUG OUTPUT
  clog.rdbuf(NULL);

  struct dirent *pEnt = NULL;

  while ((pEnt = readdir(pDir))) {
    if (pEnt == NULL) {
      cerr << "ERROR: Could not read directory entry" << endl;
    }

    string fileName;
    fileName.assign(dirName);
    fileName.append((const char *)pEnt->d_name);

    if ((fileName.find(".scp", 1) != string::npos) || (fileName.find(".SCP", 1) != string::npos)) {
      cout << endl << "Opening File: " << fileName << endl;

      uint16_t calculatedCrcValue;  // The CRC we calculate
      uint16_t fileCrcValue;        // The CRC from the file
      uint32_t length;              // The length read from the file

      uint8_t *pRead = NULL;  // Pointer used for the read command

      uint32_t i;

      uint8_t firstBlock[32];

      fstream inFile(fileName.c_str(), ios::in | ios::binary);
      inFile.seekg(0, ios::end);
      uint32_t fileLength = inFile.tellg();
      inFile.seekg(0, ios::beg);

      // Make sure the file is not too big for the format
      if (fileLength > MAX_SCP_FILE_SIZE) {
        cerr << "File size is SIGNIFICANTLY larger than expected. Aborting." << endl;
        inFile.close();
        continue;
      }

      // Make sure the file is not too small for the format
      if (fileLength < MIN_SCP_FILE_SIZE) {
        cerr << "File size is too small for an SCP file. Aborting." << endl;
        inFile.close();
        continue;
      }

      // Read in the first TWO 16 byte blocks to see if the file has been encrypted
      for (i = 0; i < 32; i++) {
        firstBlock[i] = 0;
      }
      inFile.read((char *)firstBlock, 32);  // Read the first block into RAM

      string str;                                    // Check that we are reading an SCP file
      str.assign((const char *)&firstBlock[16], 6);  // Copy what should be the "SCPECG" string
      if (str.compare(0, 6, SCP_ID_STRING)) {
        cerr << "ERROR: File corrupted! Aborting." << endl;
        return -1;
      }
      else {
        uint32_t j = 0;
        fileCrcValue = binary_read16(firstBlock, &j);
        length = binary_read32(firstBlock, &j);

        pRead = new uint8_t[fileLength];  // Allocate enough space to read in the whole file
        for (i = 0; i < 32; i++) {
          pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
        }
        inFile.read(
          (char *)&pRead[32],
          fileLength - 32);  // Store the remainder of the file in memory

        calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff
        crcBlock(
          &pRead[2], length - 2,
          &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
      }

      inFile.close();

#ifdef CHECK_CRC
      if (calculatedCrcValue == fileCrcValue) {
#endif
        uint32_t sI = 6;
        uint32_t j;

        /**********************************************************************************************
         *          Section 0
         **********************************************************************************************/
        if (sectionHeader(&pRead[sI], &length)) {  // Parse the header for section 0
          delete[] pRead;                          // Return value -> CRC Error
          continue;
        }

        subSection sections[12];  // Array of sections to store file data

        for (j = 16; j < length;) {
          uint16_t id = binary_read16(&pRead[sI], &j);   // Section Number: 0 - 11
          uint32_t len = binary_read32(&pRead[sI], &j);  // Section Length
          uint32_t ind = binary_read32(&pRead[sI], &j);  // Section Start Index
          if (id < 12) {
            // Copy data into section classes for later access
            sections[id].init(id, len, ind, pRead);
          }
          else {  // We've hit some sort of error
            cerr << "ERROR: Unexpected section index(" << id << ")! Aborting." << endl;
            delete[] pRead;
            continue;
          }
        }

        delete[] pRead;  // Free the buffer we used for file input

        /**********************************************************************************************
         *          Section 2
         **********************************************************************************************/

        int32_t **prefixBits = NULL;
        uint32_t **totalBits = NULL;
        uint32_t **switchByte = NULL;
        uint32_t **baseValue = NULL;
        int32_t **baseCode = NULL;
        uint32_t tableCount = 0;
        uint32_t *codeCount = NULL;
        uint8_t prefix_min_bits = 0xff;
        uint8_t prefix_max_bits = 0;
        if (sections[2].exists()) {
          if (sections[2].readHeader(&length)) {
            continue;
          }
          j = 16;
          tableCount = sections[2].read_16(&j);
          uint32_t l;
          prefixBits = new int32_t *[tableCount];
          totalBits = new uint32_t *[tableCount];
          switchByte = new uint32_t *[tableCount];
          baseValue = new uint32_t *[tableCount];
          baseCode = new int32_t *[tableCount];

          codeCount = new uint32_t[tableCount];

          for (l = 0; l < tableCount; l++) {
            codeCount[l] = sections[2].read_16(&j);
            prefixBits[l] = new int32_t[codeCount[l]];
            totalBits[l] = new uint32_t[codeCount[l]];
            switchByte[l] = new uint32_t[codeCount[l]];
            baseValue[l] = new uint32_t[codeCount[l]];
            baseCode[l] = new int32_t[codeCount[l]];
            uint32_t m;
            for (m = 0; m < codeCount[l]; m++) {
              prefixBits[l][m] = sections[2].read_8(&j);
              if (prefixBits[l][m] < prefix_min_bits) prefix_min_bits = prefixBits[l][m];
              if (prefixBits[l][m] > prefix_max_bits) prefix_max_bits = prefixBits[l][m];
              totalBits[l][m] = sections[2].read_8(&j);
              switchByte[l][m] = sections[2].read_8(&j);
              baseValue[l][m] = sections[2].read_16(&j);
              baseCode[l][m] = sections[2].read_32(&j);

              // Reverse the bit-order for ease of use later
              int32_t k;
              uint32_t temp = 0;
              for (k = 0; k < prefixBits[l][m]; k++) {
                temp = (temp << 1) | ((baseCode[l][m] & (1 << k)) ? 1 : 0);
              }
              baseCode[l][m] = temp;
            }
          }
        }

        /**********************************************************************************************
         *          Section 3
         **********************************************************************************************/

        uint32_t leadCount = 0;
        uint32_t *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
        if (sections[3].exists()) {
          if (sections[3].readHeader(&length)) continue;
          j = 16;
          leadCount = sections[3].read_8(&j);
          uint32_t flags = sections[3].read_8(&j);
          sampleStart = new uint32_t[leadCount];
          sampleEnd = new uint32_t[leadCount];
          leadID = new uint32_t[leadCount];
          for (i = 0; i < leadCount; i++) {
            sampleStart[i] = sections[3].read_32(&j);
            sampleEnd[i] = sections[3].read_32(&j);
            leadID[i] = sections[3].read_8(&j);
          }
          clog << "Number of Leads: " << leadCount << endl;
          clog << "Flags: 0x" << hex << flags << dec << endl;
          clog
            << ((flags & 0x01) ? "---Reference beat subtraction used for compression"
                               : "---Reference beat subtraction not used for compression")
            << endl;
          clog
            << ((flags & 0x04) ? "---Leads recorded simultaneously"
                               : "---Leads not recorded simultaneously")
            << endl;
          clog << "---Number of simultaneously recorded leads: " << (flags / 8) << endl;

          for (i = 0; i < leadCount; i++) {
            clog << "Details for Lead #" << i << endl;
            clog << "---Starting Sample Number: " << sampleStart[i] << endl;
            clog << "---Ending Sample Number: " << sampleEnd[i] << endl;
            clog << "---Lead Identification Code: " << leadID[i];
            if (leadID[i] == 131)
              clog << " (ES)" << endl;
            else if (leadID[i] == 132)
              clog << " (AS)" << endl;
            else if (leadID[i] == 133)
              clog << " (AI)" << endl;
            else
              clog << endl;
          }
        }

        /**********************************************************************************************
         *          Section 6
         **********************************************************************************************/

        uint32_t *leadLength = NULL;
        int32_t **dataArrays = NULL;
        if (sections[6].exists()) {
          if (sections[6].readHeader(&length)) continue;
          j = 16;
          uint32_t ampMult = sections[6].read_16(&j);
          uint32_t samplePeriod = sections[6].read_16(&j);
          uint32_t encoding = sections[6].read_8(&j);
          uint32_t compression = sections[6].read_8(&j);
          leadLength = new uint32_t[leadCount];
          dataArrays = new int32_t *[leadCount];
          for (i = 0; i < leadCount; i++) {
            leadLength[i] = sections[6].read_16(&j);
            dataArrays[i] = new int32_t[sampleEnd[i] - sampleStart[i] + 1];
          }
          for (i = 0; i < leadCount; i++) {
            uint8_t *rawData = new uint8_t[leadLength[i]];
            uint32_t l;
            for (l = 0; l < leadLength[i]; l++) {
              rawData[l] = sections[6].read_8(&j);
            }

            if (!sections[2].exists()) {
              int32_t *caster = (int32_t *)rawData;
              for (l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++) {
                if (l < (leadLength[i] / 2)) {
                  dataArrays[i][l] = caster[l];
                  if (dataArrays[i][l] & (1 << (16 - 1))) {
                    dataArrays[i][l] |= (~0ULL) << (16);
                  }
                }
                else
                  dataArrays[i][l] = 0;
              }
            }
            else {
              uint8_t currentBit = 8;
              uint8_t *currentByte = rawData;
              uint32_t currentTable = 0;
              for (l = 0; l < (sampleEnd[i] - sampleStart[i] + 1); l++) {
                uint32_t k = 0;
                int32_t prefixValue = 0;
                int32_t bits = 0;
                bool matchFound = 0;
                // Match the next code in the bitstream to our Huffman
                // table from Section 2
                while (!matchFound) {
                  if (prefix_min_bits == prefix_max_bits) {
                    if (!bits) {
                      currentByte =
                        get_bits(&prefixValue, prefix_max_bits, currentByte, &currentBit, 0, 0);
                      bits += prefix_max_bits;
                    }
                    else
                      break;
                  }
                  else {
                    currentByte = get_bits(&prefixValue, 1, currentByte, &currentBit, 0, 0);
                    bits += 1;
                    if (bits > prefix_max_bits) break;
                  }

                  if ((bits >= prefix_min_bits) && (bits <= prefix_max_bits)) {
                    for (k = 0; k < codeCount[currentTable]; k++) {
                      if (!prefixBits[currentTable][k]) {
                        matchFound = 1;
                        break;
                      }
                      else if (prefixBits[currentTable][k] == bits) {
                        if (baseCode[currentTable][k] == prefixValue) {
                          matchFound = 1;
                          break;
                        }
                      }
                    }
                  }

                  if ((currentByte - rawData) > leadLength[i]) {
                    sampleEnd[i] = l + sampleStart[i];
                    break;
                  }
                }

                // SwitchByte == 1 means we decode a value
                if (switchByte[currentTable][k]) {
                  currentByte = get_bits(
                    &dataArrays[i][l], totalBits[currentTable][k] - prefixBits[currentTable][k],
                    currentByte, &currentBit, 1, l < 3);
                }
                // SwitchByte == 0 means we switch to a different
                // table. (We don't use this on the Aera CT).
                else {
                  // This is where we would switch tables
                }
              }
            }

            delete[] rawData;
          }

          clog << "Amplitude multiplier: " << ampMult << " (nV/count)" << endl;
          if (ampMult) {
            clog << "---ADC Gain: " << (1000000. / (float)ampMult) << " (counts/mV)" << endl;
          }
          clog << "Sample Period: " << samplePeriod << " (us)" << endl;
          if (samplePeriod) {
            clog << "---Sample Rate: " << (1000000 / samplePeriod) << " (Hz)" << endl;
          }
          clog << "Data encoding: " << encoding << endl;
          if (encoding == 0)
            clog << "---Real data" << endl;
          else if (encoding == 1)
            clog << "---First difference data" << endl;
          else if (encoding == 2)
            clog << "---Second difference data" << endl;
          else
            clog << "---Unknown data" << endl;
          clog << "Compression: " << compression
               << (compression ? " (Bimodal compression)" : " (No compression)") << endl;
          ofstream outFile;
          size_t found = fileName.find(".scp");
          if (string::npos == found) {
            found = fileName.find(".SCP");
          }
          if (string::npos != found) {
            fileName.replace(found, 4, ".txt");

            int start_clock = clock();

            cout << "Creating File: " << fileName << endl;
            outFile.open(fileName.c_str());
            outFile << "Sample, CH1, CH2, CH3," << endl;
            uint32_t maxSamples = 0, l;
            for (i = 0; i < leadCount; i++) {
              if (sampleEnd[i] > maxSamples) maxSamples = sampleEnd[i];
            }
            int32_t *D1 = new int32_t[leadCount];
            int32_t *D2 = new int32_t[leadCount];
            for (l = 0; l < maxSamples; l++) {
              outFile << l << ", ";
              for (i = 0; i < leadCount; i++) {
                if (0 == leadLength[i]) continue;
                uint32_t relSample = 0;
                if ((sampleStart[i] - 1) <= l) relSample = 1 + l - sampleStart[i];
                if (((sampleStart[i] - 1) <= l) && ((sampleEnd[i] - 1) >= l)) {
                  int32_t tempData = dataArrays[i][l + sampleStart[i] - 1];
                  int32_t outputData = 0;
                  if (encoding == 1) {
                    // First differences
                    if (relSample == 0) {
                      outputData = tempData;
                    }
                    else {
                      outputData = D1[i] + tempData;
                    }
                    D1[i] = outputData;
                  }
                  else if (encoding == 2) {
                    // Second Differences
                    if (relSample < 2) {
                      outputData = tempData;
                    }
                    else {
                      outputData = tempData + (2 * D1[i]) - D2[i];
                    }
                    D2[i] = D1[i];
                    D1[i] = outputData;
                  }
                  else {
                    outputData = tempData;
                  }

                  outFile << outputData;
                }
                outFile << ", ";
              }
              outFile << dec << setfill(' ') << endl;
            }

            delete[] D1;
            delete[] D2;

            outFile.close();

            int time_clock = clock() - start_clock;
            int microseconds = (time_clock * 1000) + (CLOCKS_PER_SEC >> 1);
            microseconds /= CLOCKS_PER_SEC;
            cerr << "milliseconds: " << microseconds << endl;
          }
        }

        if (sections[2].exists()) {
          uint32_t l;
          for (l = 0; l < tableCount; l++) {
            delete[] prefixBits[l];
            delete[] totalBits[l];
            delete[] switchByte[l];
            delete[] baseValue[l];
            delete[] baseCode[l];
          }
          delete[] codeCount;
          delete[] prefixBits;
          delete[] totalBits;
          delete[] switchByte;
          delete[] baseValue;
          delete[] baseCode;
        }

        if (sections[3].exists()) {
          delete[] sampleStart;
          delete[] sampleEnd;
          delete[] leadID;
        }
        if (sections[6].exists()) {
          delete[] leadLength;
          uint32_t l;
          for (l = 0; l < leadCount; l++) {
            delete[] dataArrays[l];
          }
          delete[] dataArrays;
        }

#ifdef CHECK_CRC
      }
      else {
        cerr << "ERROR: File CRC Invalid. Skipping";
        continue;
      }
#endif
    }
  }

  cout << "Done.\n" << endl;  // Signal completion of program
  closedir(pDir);
  return 0;
}
