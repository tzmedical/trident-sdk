# TRIDENT DEVICE SETTINGS FILE FORMAT

## Overview

Settings for the HPR can be controlled using a settings file copied onto the SD
card. The filename for the settings file will be the “config.tzs”. If this file
is not present, the device will search for other files named “\*.tzs” and copy
the contents of the first valid one it finds to “config.tzs”.

Whenever a settings file is parsed by the device, all settings are first reset
to the values stored in internal memory. These will be the listed default values
until they are changed. After parsing a settings file, the new values will be
saved to internal memory as the new reset values.

## Overall File Layout

| Item             | Bytes      | Description                                                      |
| ---------------- | ---------- | ---------------------------------------------------------------- |
| CRC              | 2          | CRC-CCITT of the entire file, excluding these two bytes          |
| Length           | 4          | Length of the file in bytes, including the CRC                   |
| Format String    | 6          | The ASCII string "TZSET" to identify the file as an setting file |
| Device String    | 6          | The ASCII string "HPR " or "H3R " to identify the target device  |
| Firmware Version | 1          | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc         |
| Serial Number    | 11         | ASCII String of the serial number of the device, NULL Terminated |
| File Identifier  | 2          | File ID # used to distinguish file versions                      |
| Setting 1        | _Variable_ | First setting entry. Format described below                      |
| ...              | ...        | ...                                                              |
| Setting N        | _Variable_ | Last setting entry. Format described below                       |
| Terminator       | 4          | Terminator entry. Format described below                         |

## Setting Entry Format

The settings file will adhere to the following format. Each settings file may
vary in length depending on the number of settings to be changed. Any setting
not explicitly specified will be set to the default value. Therefore, it is
advisable that every setting that could affect the performance of the device
should be included in the settings file.

| Item         | Bytes      | Description                                                                                                |
| ------------ | ---------- | ---------------------------------------------------------------------------------------------------------- |
| Settings Tag | 1          | This value identifies the setting associated with the data to follow. Possible values are described below. |
| Length       | 1          | This is the number of bytes of data associated with this setting                                           |
| Data         | <_length_> | This is the data to be entered for the setting. Numeric values are stored in Little-Endian format          |
| Reserved     | 1          | This is a '\0' used to identify the end of each entry                                                      |

## Settings Terminator Entry

The last entry in the settings file will be followed by a 4 byte terminator
entry with the following format:

| Item         | Bytes | Description |
| ------------ | ----- | ----------- |
| Settings Tag | 1     | 255         |
| Length       | 1     | 1           |
| Data         | 1     | 255         |
| Reserved     | 1     | 0           |

## Settings Tags

Below is a comprehensive list of available settings. The settings are grouped
according to types.

## Recording Settings

The settings in this section are used to control the ECG recording process and
the Event Marking process. This includes manipulating the file resolution and
sample rate.

| Setting Number | Setting Name                    | Availability          | Values (default)                                                                                 | Units   | Tool Tip                                                                                                                                                                                                                                                                                 |
| -------------- | ------------------------------- | --------------------- | ------------------------------------------------------------------------------------------------ | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **31**         | Detect Pacemaker Spikes         | H3R: v1.0+ HPR: v1.0+ | true/false (false)                                                                               | Boolean | When this value is set, the device will detect and record the width of pacemaker pulses.                                                                                                                                                                                                 |
| **41**         | Pedometer Period                | H3R: v1.1+ HPR: v1.0+ | 0–900 (30)                                                                                       | Seconds | When this is set to a non-zero value, the device will record the number of detected steps every <value> seconds in the interval files.                                                                                                                                                   |
| **42**         | Store Raw Acceleration          | HPR: v1.0+            | true/false (false)                                                                               | Boolean | When this is enabled, the device will record raw acceleration waveforms in \*.tzg files.                                                                                                                                                                                                 |
| **43**         | Report Battery Temperature      | H3R: v2.4+ HPR: v1.7+ | true/false (true)                                                                                | Boolean | When this is enabled, the device will report battery temperature in \*.tzr files.                                                                                                                                                                                                        |
| **95**         | Study Length is Calendar Days   | H3R: v1.9+ HPR: v1.3+ | true/false (false)                                                                               | Boolean | When this value is set, the device will include recording interruptions in the recording length calculation, resulting in the recording ending exactly Study Hours after it is started, regardless of the amount of data stored.                                                         |
| **96**         | One ECG File                    | H3R: v1.6+ HPR: v1.0+ | true/false (false)                                                                               | Boolean | When this value is set, the device will store all ECG data in a single binary file consisting of concatenated SCP files, instead of using the directory structure for individual SCP files.                                                                                              |
| **97**         | Study Hours                     | H3R: v1.0+ HPR: v1.0+ | H3R: v1.0-v2.4: 1–744 (24) & v2.5+: 1-1464 (24), HPR: v1.0-v1.6: 1–744 (24) & v1.7+: 1-1464 (24) | Hours   | This setting defines the number of hours of data that the device will record before stopping the recording. This value is also used to update the “time remaining” and progress bar on the device status screen.                                                                         |
| **101**        | Sample Rate                     | H3R: v1.0+ HPR: v1.0+ | H3R: [250,500,1000,2000,4000] (250) HPR: [200,400,800,1600] (200)                                | Hz      | The value in this field sets the sampling rate for each ECG channel in samples per second. Input values other than those listed will be rounded up based on the capabilities of the device. Check the SCP files for actual recording rate.                                               |
| **111**        | Digital High Pass Filter        | H3R: v1.0+ HPR: v1.0+ | 0–100 (5)                                                                                        | Hz/100  | When this is set to a non-zero value, the device will apply a 2nd order high pass digital filter to the ECG data before being stored to the SD Card. The cutoff frequency in Hz will be the value of this setting divided by 100, with an effective setting range of 0.01 Hz to 1.00 Hz. |
| **112**        | Recording Confirmation Pin Code | H3R: v1.0+            | 0-9999 (0)                                                                                       | -       | When this is set to a non-zero value, the device will wait to start a recording until this value is entered by the patient.                                                                                                                                                              |
| **113**        | Digital Low Pass Filter         | H3R: v1.0+ HPR: v1.0+ | 0–100 (40)                                                                                       | Hz      | When this is set to a non-zero value, the device will apply a 6th order low pass digital filter to the ECG data before being passed to the analysis algorithm or being stored to the SD Card.                                                                                            |
| **114**        | Digital Notch Filter            | H3R: v1.0+ HPR: v1.0+ | 0-100 (60)                                                                                       | Hz      | When this is set to a non-zero value, the device will apply a 2nd order notch digital filter to the ECG data before being passed to the analysis algorithm or being stored to the SD Card.                                                                                               |
| **253**        | Zymed Compatibility             | HPR: v1.3+            | true/false (false)                                                                               | Boolean | When this value is set, the device will store ECG data both in SCP files and in a <serialNumber>.rf file for import into Zymed software.                                                                                                                                                 |

## Operating Mode Settings

The settings in this section are used to control the various operating modes of the device.

| Setting Number | Setting Name                       | Availability          | Values (default)                | Units   | Tool Tip                                                                                                                                                                                                                                                                     |
| -------------- | ---------------------------------- | --------------------- | ------------------------------- | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **143**        | Ignore Haptics Errors              | H3R: v2.3+            | true/false (false)              | Boolean | When this is set to a non-zero value, the device will not report an error when the vibration motor fails initialization. Instead, the device will simply fail to emit vibrations.                                                                                            |
| **144**        | Replace Electrode Alert Period     | HPR: v1.3+            | HPR: 0-900 (0)                  | Seconds | When this is set to a non-zero value, the device will beep every <value> seconds when the patch electrode needs to be replaced.                                                                                                                                              |
| **145**        | Low Battery Alert Period           | H3R: v2.4+ HPR: v1.3+ | H3R: 0-900 (0) HPR: 0-900 (0)   | Seconds | When this is set to a non-zero value, the device will notify every <value> seconds when the battery drops below the recharge threshold.                                                                                                                                      |
| **146**        | Electrode Wear Period              | HPR: v1.3+            | HPR: 0-168 (0)                  | Hours   | When this is set to a non-zero value the device will alert the patient when the wear-time for the patch electrode has been exceeded.                                                                                                                                         |
| **147**        | Allow Low Battery Monitoring Start | H3R: v1.0+ HPR: v1.0+ | true/false (false)              | Boolean | When this is enabled, the device will allow the user to start a study even if the battery is insufficiently charged for the duration of the recording.                                                                                                                       |
| **148**        | Study Complete Sleep Period        | H3R: v1.0+ HPR: v1.3+ | H3R: 0-900 (300) HPR: 0-900 (0) | Seconds | When this is set to a non-zero value the device will shut off the screen once the recording is complete. If set to a value greater than 1, the device will wake up every <value> seconds and notify the patient that the recording is complete.                              |
| **149**        | Lead Off Alert Period              | H3R: v1.0+ HPR: v1.3+ | H3R: 0-900 (300) HPR: 0-900 (0) | Seconds | When this is set to a non-zero value, the device will vibrate (H3R) or beep (HPR) every <value> seconds when a lead is disconnected to alert the patient to reconnect the lead. When set to 0, H3R devices will shut off the screen.                                         |
| **159**        | Screen Sleep Delay                 | H3R: v1.0+            | 5-180 (20)                      | Seconds | The value in this field sets the number of seconds that the device status screen will display before the device goes back to sleep. This also controls how long each page of the Digital Patient Diary is displayed if the user does not interact with it.                   |
| **160**        | Demonstration Mode                 | H3R: v1.0+ HPR: v1.0+ | H3R: 0-2 (0) HPR: 0-10 (0)      | <N/A>   | When set to 0, the device operates normally. When set to 1, the device will automatically restart a recording on power-up. When set to 2+, it is impossible to start a recording and HPR devices will blink the Green LED with a period of 2.4 seconds + this setting value. |
| **165**        | Symptom Diary Entries              | H3R: v1.0+            | 0-10 (0)                        | Entries | When this value is set to a value greater than 0, the symptom diary will be enabled using the first <value> symptom entries settings.                                                                                                                                        |
| **166**        | Activity Level Diary Entries       | H3R: v1.0+            | 0-10 (0)                        | Entries | When this value is set to a value greater than 0, the activity level diary will be enabled using the first <value> activity level entries settings.                                                                                                                          |
| **251**        | Error Retry Count                  | H3R: v1.0+ HPR: v1.0+ | 0-250 (5)                       | Retries | This value defines how many times the device will attempt to automatically recover from an error, within a given time-period, before reporting the error to the user.                                                                                                        |
| **252**        | Error Retry Period                 | H3R: v1.0+ HPR: v1.0+ | 0-1400 (30)                     | Minutes | This value defines the period within which the Error Retry Count must not be exceeded.                                                                                                                                                                                       |

## Patient Diary Settings

The settings in this section are strings related to the Patient Symptom and
Activity Level Diary system.

| Setting Number | Setting Name            | Availability | Length | Tool Tip                                                                                                                   |
| -------------- | ----------------------- | ------------ | ------ | -------------------------------------------------------------------------------------------------------------------------- |
| **181**        | Symptom Entry 1         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 1.                |
| **182**        | Symptom Entry 2         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 2.                |
| **183**        | Symptom Entry 3         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 3.                |
| **184**        | Symptom Entry 4         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 4.                |
| **185**        | Symptom Entry 5         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 5.                |
| **186**        | Symptom Entry 6         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 6.                |
| **187**        | Symptom Entry 7         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 7.                |
| **188**        | Symptom Entry 8         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 8.                |
| **189**        | Symptom Entry 9         | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 9.                |
| **190**        | Symptom Entry 10        | H3R: v1.0+   | 22     | This string is used in the patient symptom diary when the Symptom Diary Entries value is set to at least 10.               |
| **191**        | Activity Level Entry 1  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 1.  |
| **192**        | Activity Level Entry 2  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 2.  |
| **193**        | Activity Level Entry 3  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 3.  |
| **194**        | Activity Level Entry 4  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 4.  |
| **195**        | Activity Level Entry 5  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 5.  |
| **196**        | Activity Level Entry 6  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 6.  |
| **197**        | Activity Level Entry 7  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 7.  |
| **198**        | Activity Level Entry 8  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 8.  |
| **199**        | Activity Level Entry 9  | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 9.  |
| **200**        | Activity Level Entry 10 | H3R: v1.0+   | 22     | This string is used in the patient activity level diary when the Activity Level Diary Entries value is set to at least 10. |

## Personalization Settings

The settings in this section are strings related to Patient Identification and
Center Identification information.

| Setting Number | Setting Name  | Availability          | Length | Tool Tip                                                                                                                                       |
| -------------- | ------------- | --------------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| **201**        | Patient ID    | H3R: v1.0+ HPR: v1.0+ | 40     | Up to 40 ASCII characters identifying the patient to the host software system. This ID will be embedded in each SCP-ECG data file.             |
| **211**        | Top Banner    | H3R: v1.0+            | 28     | This text will be displayed on the top of the display at all times. If left empty, the device will display its own serial number here instead. |
| **212**        | Bottom Banner | H3R: v1.9+            | 32     | This text will be displayed on the bottom of the display at all times. If left empty, the device will display the patient ID here instead.     |

## MCT Configuration Settings

The settings in this section are used to control what is transmitted, how often
to transmit data, and some error mitigation.

| Setting Number | Setting Name                  | Availability          | Values (default)   | Units        | Tool Tip                                                                                                                                                                                                                                                                                                      |
| -------------- | ----------------------------- | --------------------- | ------------------ | ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **1**          | Bradycardia Heart Rate        | H3R: v1.1+            | 30-100 (40)        | BPM          | When the patient’s average heart rate drops BELOW the value set by this field, the device will report that the patient has entered Bradycardia.                                                                                                                                                               |
| **4**          | Tachycardia Heart Rate        | H3R: v1.1+            | 60-300 (120)       | BPM          | When the patient’s average heart rate rises ABOVE the value set by this field, the device will report that the patient has entered Tachycardia.                                                                                                                                                               |
| **8**          | Pause Duration Threshold      | H3R: v1.1+            | 1500–15000 (3000)  | Milliseconds | When the patient’s R-R interval rises ABOVE the value set by this field, the device will report a Pause for each beat preceding such an interval.                                                                                                                                                             |
| **10**         | Bradycardia Rate Change       | H3R: v1.1+            | 0-100 (0)          | BPM          | When this is set to a non-zero value, the device will create and upload an event each time the heart rate drops by at least <value> BPM below the Bradycardia Heart Rate. If it drops again by at least <value>, another event will be reported. If it drops by twice <value>, only 1 event will be reported. |
| **11**         | Tachycardia Rate Change       | H3R: v1.1+            | 0-100 (0)          | BPM          | When this is set to a non-zero value, the device will create and upload an event each time the heart rate rises by at least <value> BPM above the Tachycardia Heart Rate. If it rises again by at least <value>, another event will be reported. If it rises by twice <value>, only 1 event will be reported. |
| **12**         | Arrhythmia Duration Threshold | H3R: v1.1+            | 0-900 (15)         | Seconds      | The value of this field sets the minimum arrhythmia duration that will be reported. In cases where several arrhythmias occur together in short succession, the algorithm will attempt to “combine” the results into a smaller number of sufficiently long arrhythmias for reporting.                          |
| **40**         | Summarize QRS Detections      | H3R: v1.1+            | 0-900 (0)          | Seconds      | When the value of this field is set greater than or equal to 10, the device will summarize the QRS detections as a number of normal QRS beats and a number of PVC beats detected within the last <value> seconds. This significantly reduces the size of interval files.                                      |
| **105**        | Report Pre-Time               | H3R: v1.1+            | 0-300 (30)         | Seconds      | The value of this field sets the minimum length of ECG data reported prior to a rhythm change event. Patient events are also controlled by this setting prior to v2.0. Setting this and Report Post-Time to 0 will disable upload of rhythm change events.                                                    |
| **106**        | Report Post-Time              | H3R: v1.1+            | 0-300 (30)         | Seconds      | The value of this field sets the minimum length of ECG data reported following a rhythm change event. Patient events are also controlled by this setting prior to v2.0. Setting this and Report Pre-Time to 0 will disable upload of rhythm change events.                                                    |
| **125**        | Minimum Time Between Events   | H3R: v1.6+            | 0-300 (0)          | Seconds      | When this is set to a non-zero value, Rhythm events occurring within this number of seconds of a previous event will only be logged in the interval file and not uploaded as a separate event.                                                                                                                |
| **126**        | Suppress NSR Events           | H3R: v1.6-v1.8        | true/false (false) | Boolean      | When this value is set, the device will not upload any Rhythm Change events for Normal Sinus Rhythm.                                                                                                                                                                                                          |
| **126**        | Suppress Rhythm Events        | H3R: v1.9+            | "nbtapU" ("")      | String       | When this value is set, the device will not upload any Rhythm Change events that match the characters in this setting.                                                                                                                                                                                        |
| **127**        | Patient Event Pre-Time        | H3R: v2.0+            | 0-300 (30)         | Seconds      | The value of this field sets the minimum length of ECG data reported prior to a manual patient event. Setting this and Patient Event Post-Time to 0 will disable upload of patient events.                                                                                                                    |
| **128**        | Patient Event Post-Time       | H3R: v2.0+            | 0-300 (30)         | Seconds      | The value of this field sets the minimum length of ECG data reported following a manual patient event. Patient events are also controlled by this setting. Setting this and Patient Event Pre-Time to 0 will disable upload of patient events.                                                                |
| **155**        | Bulk Upload                   | H3R: v1.1+            | true/false (false) | Boolean      | When this value is set, the device will upload all of the SCP files it has recorded each hour, along with the interval file.                                                                                                                                                                                  |
| **156**        | Transmit Interval Reports     | H3R: v1.1+            | true/false (true)  | Boolean      | When this value is set, the device will transmit interval reports every hour.                                                                                                                                                                                                                                 |
| **225**        | Connection Timeout            | H3R: v1.1+ HPR: v1.7+ | 0-240 (5)          | Minutes      | The time to wait for a modem to establish a connection before closing the connection and trying again.                                                                                                                                                                                                        |
| **254**        | TZMR Compatibility            | H3R: v1.2+            | true/false (false) | Boolean      | When this value is set, the device will emulate the connections of an Aera CT device.                                                                                                                                                                                                                         |

## MCT Connection Settings

| Setting Number | Setting Name      | Availability          | Length | Tool Tip                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| -------------- | ----------------- | --------------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **213**        | Server Address    | H3R: v1.1+ HPR: v1.7+ | 255    | This string is the address of the web server used for mobile uploading data and downloading actions files. It should omit the preceding “https://” and may be an IP address or a domain name. A custom port may be used at the end of the string. Here are some examples: “8.8.8.8” “8.8.8.8:12345” “www.google.com” “www.google.com:12345” “8.8.8.8/h3r-devices” “8.8.8.8:12345/h3r-devices” “google.com/h3r-devices” “google.com:12345/h3r-devices” |
| **214**        | TLS Identity      | H3R: v1.1+ HPR: v1.7+ | 127    | When this string is not empty (i.e. not “”), the device will use TLS for all connections. If using PSK, this string will be transmitted in the clear to the server as psk_identity. This should match the server’s configuration of key-identity pairs. If using ECDSA, this string should be set to something like “use_ecdsa” to force TLS mode.                                                                                                    |
| **215**        | TLS PSK           | H3R: v1.1+ HPR: v1.7+ | 127    | This string contains the pre-shared key for TLS PSK ciphers. This string should represent the binary value as a long hexadecimal number [0-9, a-e].                                                                                                                                                                                                                                                                                                   |
| **216**        | APN Address       | H3R: v1.1+            | 255    | This string is the address of the APN server in the carrier network used for wireless connections.                                                                                                                                                                                                                                                                                                                                                    |
| **217**        | APN Username      | H3R: v1.1+            | 127    | This string is the username used for authentication with the carrier network. It can be left blank (i.e. “”) if no APN authentication is required.                                                                                                                                                                                                                                                                                                    |
| **218**        | APN Password      | H3R: v1.1+            | 127    | This string is the password associated with the APN Username. It can be left blank (i.e. “”) if no APN authentication is required.                                                                                                                                                                                                                                                                                                                    |
| **219**        | HTTP Bearer Token | H3R: v1.1+ HPR: v1.7+ | 127    | If this string is set, the device will add an authorization line to the HTTP header for each server connection according to the format: “Authorization: Bearer %s”.                                                                                                                                                                                                                                                                                   |
