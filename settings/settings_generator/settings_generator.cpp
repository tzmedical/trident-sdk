/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       settings_generator.cpp
 *          - This program generates a settings file for the H3R based on the
 *                values in the input file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./settings_generator input.txt > output.tzs
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdint>

#include "util/binary.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/setting.h"
#include "util/setting_list.h"

#include "settings_generator.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int SettingsFromText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  uint16_t firmwareVersion = 0;
  string serialNumber("");
  string deviceID;
  uint16_t fileIdNumber = 0;
  bool checkInputData = 0;
  bool badAlignment = 0;
  bool badCRC = 0;
  bool badLength = 0;
  bool tls_id_set = 0;
  bool tls_psk_set = 0;
  bool tzmr_compat_set = 0;
  bool aes_key_set = 0, aes_iv_set = 0;

  SettingList settingList("");

  // Loop through the entire input
  while (!inStream->eof()) {
    // Get a single line from the input
    string argument;
    getline(*inStream, argument);

    // Trim any leading whitespace
    const size_t begStr = argument.find_first_not_of(" \t");
    if (begStr != string::npos) argument.erase(0, begStr);

    // Trim any trailing whitespace
    const size_t endStr = argument.find_last_not_of(" \n\r\t");
    if (endStr != string::npos) argument.resize(endStr + 1);

    // Skip the line if it is a comment
    if (argument.find("#") == 0) {
      continue;
    }

    // Only process lines that have more than 8 characters in them
    if (argument.length() > 8) {
      // Trim whitespace before or after the "="
      bool done = 0;
      while (!done) {
        size_t found;
        if ((found = argument.find(" =")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("\t=")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("= ")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else if ((found = argument.find("=\t")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else {
          done = 1;
        }
      }

      // Locate and parse any file-related parameters
      if (argument.find("file.") != string::npos) {
        // Make sure the file type is correct
        if (argument.find("type=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.find(SETTINGS_ID_STRING) == string::npos) {
            *errStream << "ERROR! Invalid File ID: " << tempStr << endl;
            return -1;
          }
        }
        // Sets the firmware version embedded in the header of the file
        else if (argument.find("firmware_version=") != string::npos) {
          stringstream argStream;
          string tempStr = argument.substr(argument.find("=") + 1);
          if (tempStr.length()) {
            argStream << tempStr;
            argStream >> firmwareVersion;
          }
        }
        // Sets the device is in the header of the file
        else if (argument.find("device_id=") != string::npos) {
          deviceID.assign(argument.substr(argument.find("=") + 1));
          if (deviceID.length() > 5) {
            *errStream << "ERROR! Invalid Device ID: " << argument << endl;
            return -1;
          }
          if (string::npos == deviceID.find("TZMR")) {
            deviceID.resize(5, ' ');
          }

          settingList = SettingList(deviceID);

          if (!settingList.isValid()) {
            *errStream << "ERROR! Unsupported device_id field (" << deviceID << ")." << endl;
            return -1;
          }
        }
        // Sets the serial number embedded in the header of the file
        else if (argument.find("serial_number=") != string::npos) {
          serialNumber.assign(argument.substr(argument.find("=") + 1));
          if (
            serialNumber.length()
            && ((serialNumber.length() < 7) || (serialNumber.length() > 10))) {
            *errStream << "ERROR! Invalid Serial Number: " << argument << endl;
            return -1;
          }
        }
        // Sets the file ID embedded in the header of the file
        else if (argument.find("file_id=") != string::npos) {
          string tempStr = argument.substr(argument.find("=") + 1);
          if (tempStr.length()) {
            stringstream argStream(tempStr);
            argStream >> fileIdNumber;
          }
        }
        // Enables verification that settings are within valid ranges
        else if (argument.find("check_setting_ranges=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              checkInputData = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              checkInputData = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: file.check_setting_ranges=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Enables verification that settings are within valid ranges
        else if (argument.find("insert_bad_alignment=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badAlignment = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badAlignment = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: file.insert_bad_alignment=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Enables verification that settings are within valid ranges
        else if (argument.find("insert_bad_crc=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badCRC = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badCRC = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: file.insert_bad_crc=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Enables verification that settings are within valid ranges
        else if (argument.find("insert_bad_length=") != string::npos) {
          string tempStr(argument.substr(argument.find("=") + 1));
          if (tempStr.length()) {
            if (tempStr.find("true") != string::npos) {
              badLength = 1;
            }
            else if (tempStr.find("false") != string::npos) {
              badLength = 0;
            }
            else {
              *errStream << "ERROR! Invalid string: file.insert_bad_length=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Print warnings if trying to AES encrypt the file
        else if (argument.find("aes_key=") != string::npos) {
          *errStream << "WARNING: AES file encryption is not supported: " << argument << endl;
        }
        else if (argument.find("aes_iv=") != string::npos) {
          *errStream << "WARNING: AES file encryption is not supported: " << argument << endl;
        }
        // Abort if an invalid file setting is detected
        else {
          *errStream << "ERROR! Unrecognized input: " << argument << endl;
          return -1;
        }
      }
      // Locate and process any settings from the file
      else if (argument.find("setting.") != string::npos) {
        // Find any matches with the setting array defined at the beginning of
        // this source file.
        if (!settingList.isValid()) {
          *errStream << "ERROR! Please provide device_id prior to settings." << endl;
          return -1;
        }

        _setting *pSetting = settingList.locateSetting(argument, firmwareVersion, errStream);

        if (NULL == pSetting) {
          return -1;
        }
        if (pSetting->set(argument.substr(argument.find("=") + 1), checkInputData, errStream)) {
          return -1;
        }

        // flag TZMR compat mode potential conflicts
        if (TZMR_COMPAT == pSetting->getTag()) {
          tzmr_compat_set = pSetting->getDataVal();
        }
        else if (TLS_IDENTITY == pSetting->getTag()) {
          if (string::npos != pSetting->getParseStr().find("tls_identity")) {
            tls_id_set = 1;
          }
          else if (string::npos != pSetting->getParseStr().find("aes_iv")) {
            aes_iv_set = 1;
          }
        }
        else if (TLS_PSK == pSetting->getTag()) {
          if (string::npos != pSetting->getParseStr().find("tls_psk")) {
            tls_psk_set = 1;
          }
          else if (string::npos != pSetting->getParseStr().find("aes_key")) {
            aes_key_set = 1;
          }
        }
      }
      // Error on unrecognized settings - don't just skip them!
      else {
        *errStream << "ERROR! Unrecognized input: " << argument << endl;
        return -1;
      }
    }
  }

  // Sanity check
  if (tzmr_compat_set) {
    if (tls_psk_set || tls_id_set) {
      *errStream << "ERROR! TLS settings and TZMR compat are not allowed concurrently" << endl;
      return -1;
    }
  }
  else if (string::npos == deviceID.find("TZMR")) {
    if (aes_key_set || aes_iv_set) {
      *errStream << "ERROR! AES settings are not allowed without TZMR Compat set" << endl;
      return -1;
    }
  }

  // The first 6 bytes are length and CRC - add these at the end
  vector<uint8_t> buffer(6, 0);

  string formatID(SETTINGS_ID_STRING);
  binary_append(buffer, formatID, 6);         // The format identifier string
  binary_append(buffer, deviceID, 6);         // The device ID string
  binary_append(buffer, firmwareVersion, 1);  // This byte is F. Vers. (*10)
  if (string::npos != deviceID.find("TZMR")) {
    binary_append(buffer, serialNumber, 8);  // The next 8 bytes are the serial number
  }
  else if (string::npos != deviceID.find("H3R")) {
    binary_append(buffer, serialNumber, 11);  // The next 11 bytes are the serial number
  }
  else if (string::npos != deviceID.find("HPR")) {
    binary_append(buffer, serialNumber, 11);  // The next 11 bytes are the serial number
  }
  else {
    *errStream << "Unrecognized Device ID" << endl;
    return -1;
  }
  binary_append(buffer, fileIdNumber, 2);  // uint16_t fileId

  settingList.writeBinary(buffer, firmwareVersion, badAlignment, &retVal, errStream);
  binary_append(buffer, TERM_TAG, 1);
  binary_append(buffer, 1, 1);
  binary_append(buffer, 255, 1);
  binary_append(buffer, '\0', 1);

  // Finish the length/CRC in the header
  uint32_t length = buffer.size();
  uint32_t temp = length;
  if (badLength) temp += 255;
  binary_overwrite(buffer, 2, temp, 4);

  uint16_t crcValue = 0xffff;
  uint8_t *pBuf = buffer.data();
  crcBlock(&pBuf[2], length - 2, &crcValue);

  temp = crcValue;
  if (badCRC) temp++;
  binary_overwrite(buffer, 0, temp, 2);

  if (!retVal) {
    outStream->write((const char *)buffer.data(), length);
  }

  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  fstream argumentFile;
  istream *input;
  map<string, string> extraArgs;

  if (argc >= 2) {
    argumentFile.open(argv[1], ios::in);
    input = &argumentFile;
  }
  else {
    input = &cin;
  }

  return SettingsFromText(input, extraArgs, &cout, &cerr, &clog);
}
#endif