# Lines starting with "#" are comments
# This is an output file from settings_parser.exe
# This file may be used an an input file for settings_generator.exe

#CRC Valid: 0xfbbf
#File Length: 587 Bytes
file.type=TZSET
file.device_id=H3R  
file.firmware_version=23
file.serial_number=H3R4005401
file.file_id=0
file.check_setting_ranges=true

setting.tachy_bpm=150
setting.pause_duration=3000
setting.brady_rate_change=0
setting.tachy_rate_change=0
setting.arrhythmia_min_duration=30
setting.detect_pacemaker=false
setting.summarize_qrs_detection=0
setting.pedometer_period=30
setting.length_is_calendar_days=false
setting.one_ecg_file=false
setting.study_hours=720
setting.sample_rate=250
setting.report_pre_time=30
setting.report_post_time=30
setting.digital_hp_filter=5
setting.start_pin_code=0
setting.digital_lp_filter=50
setting.digital_notch_filter=60
setting.min_time_between_events=0
setting.suppress_rhythm_events=
setting.patient_event_pre_time=30
setting.patient_event_post_time=30
setting.ignore_haptics_errors=false
setting.allow_low_battery_start=true
setting.study_complete_sleep=300
setting.nag_on_lead_off=300
setting.bulk_upload=false
setting.transmit_interval_reports=true
setting.screen_sleep_time=20
setting.demo_mode=0
setting.symptom_diary_entries=10
setting.activity_diary_entries=10
setting.symptom_entry_1=1. None
setting.symptom_entry_2=2. Chest Pain
setting.symptom_entry_3=3. Dizziness
setting.symptom_entry_4=4. Skipped Beats
setting.symptom_entry_5=5. Tired or Fatigued
setting.symptom_entry_6=6. Rapid Heartbeat
setting.symptom_entry_7=7. Passed Out
setting.symptom_entry_8=8. Short of Breath
setting.symptom_entry_9=9. Lightheadedness
setting.symptom_entry_10=10. Irregular Rhythm
setting.activity_entry_1=1. None
setting.activity_entry_2=2. Sitting
setting.activity_entry_3=3. Laying Down
setting.activity_entry_4=4. Walking (Shopping)
setting.activity_entry_5=5. Cleaning
setting.activity_entry_6=6. Bending / Standing
setting.activity_entry_7=7. Exercise
setting.activity_entry_8=8. Climbing Stairs
setting.activity_entry_9=9. Driving
setting.activity_entry_10=10. Eating / Just Ate
setting.patient_id=
setting.top_banner=New Setting
setting.bottom_banner=
setting.http_bearer_token=
setting.connection_timeout=5
setting.error_retries=5
setting.error_period=30
setting.zymed_compat=0
setting.tzmr_compat=false
