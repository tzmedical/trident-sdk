/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       settings_parser.cpp
 *          - This program parses a *.tzs settings file from <cin> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./settings_parser inFile.tzs > outFile.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cstdint>

#include "settings_parser.h"
#include "util/common.h"
#include "util/crc.h"
#include "util/setting.h"
#include "util/setting_list.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

int SettingsToText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  int retVal = 0;

  uint16_t calculatedCrcValue;  // The CRC we calculate
  uint16_t fileCrcValue;        // The CRC from the file
  uint32_t length;              // The length read from the file

  uint8_t *pRead;  // Pointer used for the read command
  uint16_t *shortCaster;
  uint32_t *intCaster;

  uint8_t firstBlock[16];

  bool tzmr_compat_set = 0;
  uint32_t i;

  calculatedCrcValue = 0xffff;  // CRC is based off an initial value of 0xffff

  // Process the extra arguments
  bool printHeader = 1;
  bool skipCrc = 0;
  map<string, string>::iterator itr;
  itr = extraArgs.find("printHeader");
  if (itr != extraArgs.end()) {
    char *endptr;
    printHeader = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }
  itr = extraArgs.find("skipCrc");
  if (itr != extraArgs.end()) {
    char *endptr;
    skipCrc = (bool)strtoul(itr->second.c_str(), &endptr, 10);
  }

  for (i = 0; i < 16; i++) {
    firstBlock[i] = 0;
  }
  inStream->read((char *)firstBlock, 16);  // Read the first block into RAM

  *outStream << "# Lines starting with \"#\" are comments" << endl;
  *outStream << "# This is an output file from settings_parser.exe" << endl;
  *outStream << "# This file may be used an an input file for settings_generator.exe" << endl;
  *outStream << "" << endl;

  string str;
  str.assign((const char *)&firstBlock[6]);
  if (str.compare(0, 6, SETTINGS_ID_STRING)) {
    *errStream << "ERROR: File corrupted! (" << str << ") Aborting." << endl;
    return -1;
  }
  else {
    i = 0;
    shortCaster = (uint16_t *)&firstBlock[i];
    fileCrcValue = *shortCaster;
    i += 2;

    intCaster = (uint32_t *)&firstBlock[i];
    length = *intCaster;
    i += 4;

    pRead = new uint8_t[length];  // Allocate enough space to read in the whole file
    for (i = 0; i < 16; i++) {
      pRead[i] = firstBlock[i];  // Copy the first block into the file buffer
    }
    inStream->read((char *)&pRead[16],
                   length - 16);  // Store the remainder of the file in memory

    crcBlock(
      &pRead[2], length - 2,
      &calculatedCrcValue);  // Calculate the CRC for the remainder of the file
  }

  // If the CRC doesn't match, something has gone wrong
  if (skipCrc || calculatedCrcValue == fileCrcValue) {
    uint32_t j = 6;
    string format_id((const char *)&pRead[j]);
    j += 6;
    string device_id((const char *)&pRead[j]);
    j += 6;
    // Parse out the firmware version
    uint8_t firmwareVersion = pRead[j];
    j += 1;
    // Parse out the Serial Number String
    string serial_number((const char *)&pRead[j]);
    bool tzmr_flag = 0;
    if (string::npos != device_id.find("TZMR")) {
      j += 8;
      tzmr_flag = 1;
    }
    else if (string::npos != device_id.find("H3R  ")) {
      j += 11;
    }
    else if (string::npos != device_id.find("HPR  ")) {
      j += 11;
    }
    else {
      while ('\0' != pRead[j++]) {
        // Find the end of the string
      }
    }
    uint16_t fileID = pRead[j] + ((short)pRead[j + 1] << 8);
    j += 2;

    if (printHeader) {
      if (skipCrc) {
        *outStream << "#Ignoring file CRC." << endl;
      }
      else {
        *outStream << "#CRC Valid: 0x" << hex << fileCrcValue << dec << endl;
      }
      *outStream << "#File Length: " << length << " Bytes" << endl;  // Print out the file length
      *outStream << "file.type=" << format_id << endl;
      *outStream << "file.device_id=" << device_id << endl;
      *outStream << "file.firmware_version=" << dec << (int)firmwareVersion << endl;
      *outStream << "file.serial_number=" << serial_number << endl;

      *outStream << "file.file_id=" << fileID << endl;
      *outStream << "file.check_setting_ranges=true" << endl;

      *outStream << endl;
    }

    SettingList settingList(device_id);
    if (!settingList.isValid()) {
      *errStream << "ERROR! Unsupported device_id field (" << device_id << ")." << endl;
      return -1;
    }

    // Find if tzmr_compat is set
    uint32_t k = j;
    while (k < (length - 2)) {
      settingTags settingTag = (settingTags)pRead[k++];  // Setting Tag
      uint32_t settingLength = pRead[k++];               // Setting Length
      if (TZMR_COMPAT == settingTag) {
        _setting *pSetting = settingList.locateSetting(
          settingTag, firmwareVersion, tzmr_flag, tzmr_compat_set, errStream);

        // If not NULL, send the setting to the outstream, otherwise, skip the bad setting
        if (pSetting != NULL) {
          retVal = pSetting->setBinary(&pRead[k], settingLength, 1);
          if (retVal) return retVal;
          tzmr_compat_set = pSetting->getDataVal();
        }
        break;
      }

      k += settingLength;

      if (pRead[k++] != '\0') {
        *errStream << "Missing NULL detected! (tag: " << settingTag << ")" << endl;
      }
    }

    while (j < (length - 2)) {  // Parse the events until the end of the file
      settingTags settingTag = (settingTags)pRead[j++];  // Setting Tag
      uint32_t settingLength = pRead[j++];               // Setting Length

      _setting *pSetting = settingList.locateSetting(
        settingTag, firmwareVersion, tzmr_flag, tzmr_compat_set, errStream);

      // If not NULL, send the setting to the outstream, otherwise, skip the bad setting
      if (pSetting != NULL) {
        retVal = pSetting->setBinary(&pRead[j], settingLength, 1);
        if (retVal) return retVal;
        *outStream << pSetting->getSettingText() << endl;
      }

      j += settingLength;

      if (pRead[j++] != '\0') {
        *errStream << "Missing NULL detected! (tag: " << settingTag << ")" << endl;
        return -1;
      }
    }
  }

  delete[] pRead;  // Free the buffer we used for parsing

  if (calculatedCrcValue != fileCrcValue) {
    *errStream << "ERROR: File CRC Invalid." << endl;
    *errStream << "File CRC: 0x" << hex << fileCrcValue << dec << endl;
    *errStream << "Calculated CRC: 0x" << hex << calculatedCrcValue << dec << endl;
    *errStream << "File Length: " << length << endl;

    if (!skipCrc) {
      return -1;
    }
  }

  return retVal;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return SettingsToText(input, extraArgs, &cout, &cerr, &clog);
}
#endif