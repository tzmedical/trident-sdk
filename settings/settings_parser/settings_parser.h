#ifndef SETTING_PARSER_H
#define SETTING_PARSER_H

#include <iostream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to convert a settings file into a text file
//! \return 0 if successful
int SettingsToText(
  std::istream* inStream, std::map<std::string, std::string>& extraArgs, std::ostream* outStream,
  std::ostream* errStream, std::ostream* logStream);

#endif