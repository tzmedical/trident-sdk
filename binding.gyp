{
  "targets": [
    {
      "target_name": "trident-sdk",
      "cflags!": [
        "-fno-exceptions -Wall"
      ],
      "cflags_cc!": [
        "-fno-exceptions -Wall"
      ],
      "defines": [
        "GYP=1",
        "NAPI_DISABLE_CPP_EXCEPTIONS",
        "NAPI_VERSION=<(napi_build_version)"
      ],
      "sources": [
        "actions/actions_parser/actions_parser.cpp",
        "actions/actions_generator/actions_generator.cpp",
        "actions/actions_to_json/actions_to_json.cpp",
        "actions/actions_from_json/actions_from_json.cpp",
        "accel/accel_to_json/accel_to_json.cpp",
        "accel/accel_from_json/accel_from_json.cpp",
        "backup/backup_parser/backup_parser.cpp",
        "backup/backup_generator/backup_generator.cpp",
        "battery/battery_info_parser/battery_info_parser.cpp",
        "battery/battery_info_generator/battery_info_generator.cpp",
        "settings/settings_parser/settings_parser.cpp",
        "settings/settings_generator/settings_generator.cpp",
        "settings/settings_to_json/settings_to_json.cpp",
        "settings/settings_from_json/settings_from_json.cpp",
        "events/event_generator/event_generator.cpp",
        "events/event_parser/event_parser.cpp",
        "events/event_to_json/event_to_json.cpp",
        "events/event_finder/event_finder.cpp",
        "interval/interval_generator/interval_generator.cpp",
        "interval/interval_parser/interval_parser.cpp",
        "interval/interval_to_json/interval_to_json.cpp",
        "interval/interval_finder/interval_finder.cpp",
        "scp/scp_parser/scp_parser.cpp",
        "scp/scp_generator/scp_generator.cpp",
        "scp/scp_generator/static_scp.c",
        "scp/scp_to_json/scp_to_json.cpp",
        "queue/queue_to_json/queue_to_json.cpp",
        "util/action.cpp",
        "util/action_list.cpp",
        "util/binary.cpp",
        "util/common.cpp",
        "util/crc.cpp",
        "util/events.cpp",
        "util/json.cpp",
        "util/setting.cpp",
        "util/setting_list.cpp",
        "util/timestamp.cpp",
        "util/trident_sdk_addon.cpp"
      ],
      "conditions": [
        [
          "OS=='linux'",
          {
            "defines": [
              "OS_LINUX",
              "OS_UNIX"
            ],
            "sources": [
              "util/drive_eject_linux.cpp"
            ]
          }
        ],
        [
          "OS=='mac'",
          {
            "defines": [
              "OS_MAC",
              "OS_UNIX"
            ],
            "sources": [
              "util/drive_eject_mac.cpp"
            ]
          }
        ],
        [
          "OS=='win'",
          {
            "defines": [
              "OS_WIN"
            ],
            "sources": [
              "util/drive_eject_win.cpp"
            ]
          }
        ]
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "settings/settings_parser",
        "settings/settings_generator",
        "settings/settings_to_json",
        "settings/settings_from_json",
        "accel/accel_to_json",
        "accel/accel_from_json",
        "actions/actions_parser",
        "actions/actions_generator",
        "actions/actions_to_json",
        "actions/actions_from_json",
        "backup/backup_parser",
        "backup/backup_generator",
        "battery/battery_info_parser",
        "battery/battery_info_generator",
        "error/error_to_json",
        "events/event_generator",
        "events/event_parser",
        "events/event_to_json",
        "events/event_finder",
        "interval/interval_generator",
        "interval/interval_parser",
        "interval/interval_to_json",
        "interval/interval_finder",
        "scp/scp_parser",
        "scp/scp_generator",
        "scp/scp_to_json",
        "queue/queue_to_json",
        "node_addon",
        "rapid_json",
        "rapid_json/error",
        "rapid_json/internal",
        "rapid_json/msinttypes",
        "util",
        "."
      ],
      "libraries": [],
      "dependencies": [
        "<!(node -p \"require('node-addon-api').gyp\")"
      ]
    },
    {
      "target_name": "action_after_build",
      "type": "none",
      "dependencies": [
        "<(module_name)"
      ],
      "copies": [
        {
          "files": [
            "<(PRODUCT_DIR)/<(module_name).node"
          ],
          "destination": "<(module_path)"
        }
      ]
    }
  ]
}