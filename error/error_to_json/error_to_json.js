/* eslint-env node */
const { DateTime } = require('luxon')

const UTC_TIMEZONE = 'utc'
// set the parse format for 2020/12/18 02:33:44
const LUXON_PARSE_FORMAT = 'yyyy/MM/dd HH:mm:ss'

// Match the YYYY/MM/DD HH:MM:SS[.mmm] part
const TIMESTAMP_REGEX = /(\d{1,})[\\/-](\d{2})[\\/-](\d{2}) (\d{2}):(\d{2}):(\d{2})(.\d{3})?/

// Match the (-480) or (420) or (+420) or (32767) => -480, 420, +420, 32767
const TIMEZONE_REGEX = /(?<=\()[+-]?(\d*)(?=\))/

const LINE_SEPARATOR_REGEX = /\r?\n/
const LINE_SEGMENT_SEPARATOR = ' - '

const LUXON_DATE_OUTPUT_FORMAT = 'yyyy-MM-dd HH:mm:ss.SSSZZ'

/*
------------- CASES TO HANDLE ------------
1) empty lines are skipped

2) log objects are not created for lines without timestamps
    if a line does not have a timestamp, attach it to the
    previous line that has a timestamp in a text object

3) 2020/12/18 02:33:44 (-480) - v2.0 - HTTP Error Code(404): /actions/H3R3000039
  {
    "datetime": "2020-12-18 02:33:44.000+00:00",
    "timezoneOffset": "-480",
    "firmwareVersion": "v2.0",
    "message": "HTTP Error Code(404): /actions/H3R3000039",
    "error": {
      "httpCode": "404",
      "source": "/actions/H3R3000039"
    }
  },

4) 2021/02/16 23:11:02 (-480) - v2.0 - Cable Bad Config:
	   USB MODE: 128
	   ECG CHAN: 7
	   ECG MODE: 67
  {
    "datetime": "2021-02-16 23:11:02.000+00:00",
    "timezoneOffset": "-480",
    "firmwareVersion": "v2.0",
    "message": "Cable Bad Config: {USB MODE: 128, ECG CHAN: 7, ECG MODE: 67}"
  },

5) 2020/12/17 16:18:55 (-480) - v2.0 - 4202 - AT^SISO?
  {
    "datetime": "2020-12-17 16:18:55.000+00:00",
    "timezoneOffset": "-480",
    "firmwareVersion": "v2.0",
    "message": "4202 - AT^SISO?",
    "error": {
      "code": "4202",
      "source": "AT^SISO?"
    }
  },

6) 2020/12/15 01:12:00 (-480) - v2.0 - Device Booting (0) - Battery - aged(117) cycles(308) soc(3)
  {
    "datetime": "2020-12-15 01:12:00.000+00:00",
    "timezoneOffset": "-480",
    "firmwareVersion": "v2.0",
    "message": "Device Booting (0) - Battery - aged(117) cycles(308) soc(3)"
  },

7) 2020/12/15 01:09:28 (-480) - v2.0 - Error code 6007 from 1011 at line 709
  {
    "datetime": "2020-12-15 01:09:28.000+00:00",
    "timezoneOffset": "-480",
    "firmwareVersion": "v2.0",
    "message": "Error code 6007 from 1011 at line 709",
    "error": {
      "code": "6007",
      "source": "1011",
      "line": "709"
    }
  },

8) Need to accept local timezone offset parameter
   so we can update 32767 timezones with the local timezone
   Add the local timezone to the parsed datetime to get the actual utc timestamp
   2020/08/15 07:00:00 (32767) - v2.0 - Device Booting (0) - Battery - aged(117) cycles(308) soc(3)
  {
    "datetime": "2020-08-15 14:00:00.000+00:00",
    "timezoneOffset": "-420",
    "firmwareVersion": "v2.0",
    "message": "Device Booting (0) - Battery - aged(117) cycles(308) soc(3)"
  },

9) 0/00/00 00:00:00 (0) - v1.2 - User reset (0)
  This should be a valid log but we should send it as "Invalid date" in the JSON
  {
    "datetime": "Invalid date",
    "timezoneOffset": "0",
    "firmwareVersion": "v1.2",
    "message": "User reset (0)",
  },

10) 2020/10/29 16:39:54 (-240) - v1.9 - Cable error: 4004 at 632
  {
    "datetime": "2020-10-29 16:39:54.000+00:00",
    "timezoneOffset": "-240",
    "firmwareVersion": "v2.0",
    "message": "Cable error: 4004 at 632",
    "error": {
      "code": "4004",
      "source": "cable_monitor",
      "line": "632"
    }
  },

11) 2021/02/16 23:11:02 (-480) - v2.0 - Cable Bad Config

	USB MODE: 128
	ECG CHAN: 7
	ECG MODE: 67

	ANOTHER MODE: 223
  {
    "datetime": "2021-02-16 23:11:02.000+00:00",
    "timezoneOffset": "-480",
    "firmwareVersion": "v2.0",
    "message": "Cable Bad Config: {USB MODE: 128, ECG CHAN: 7, ECG MODE: 67, ANOTHER MODE: 223}"
  },
*/

/*
 --- OUTPUT TYPES (from Typescript)---
type ErrorLogError = {
  code?: string;
  line?: string;
  source?: string;
  httpCode?: string;
};
type ErrorLog = {
  datetime: string;
  timezoneOffset: number;
  firmwareVersion: string;
  message: string;
  error?: ErrorLogError;
};
*/

// try to match a regex for the message to form an error object
function determineErrorObject(message) {
  // HTTP Error Code(404): /actions/H3R3000039
  const httpErrorMatch = message.match(/^HTTP.*\((\d*)\): (.*)/)
  if (httpErrorMatch) {
    // groupMatches: 404, /actions/H3R000039
    const httpCode = httpErrorMatch[1]
    const source = httpErrorMatch[2]
    return { httpCode, source }
  }

  // Error code 6007 from 1011 at line 709
  // Error code 6007 from 1011 at 709
  const errorCodeMatch = message.match(/^Error code (\d+) from (\d+) at\D+(\d+)/)
  if (errorCodeMatch) {
    // groupMatches: 6007, 1011, 709
    const code = errorCodeMatch[1]
    const source = errorCodeMatch[2]
    const line = errorCodeMatch[3]
    return { code, source, line }
  }

  // Software reset (531) - H3R2801185 - aged(115) cycles(519) soc(74)
  const assertErrorCodeMatch = message.match(/^Software reset \((\d*)\)/)
  if (assertErrorCodeMatch) {
    // groupMatches: 531
    const code = assertErrorCodeMatch[1]
    if (code >= 2) {
      const source = 'assert'
      return { code, source }
    }
    return null
  }

  // 4202 - AT^SISO?
  const plainErrorCodeMatch = message.match(/^(\d+) - (.+)/)
  if (plainErrorCodeMatch) {
    // groupMatches: 4202, AT^SISO?
    const code = plainErrorCodeMatch[1]
    const source = plainErrorCodeMatch[2]
    return { code, source }
  }

  // Cable error: 4004 at 632
  const cableErrorMessageMatch = message.match(/^Cable error: (\d+) at (\d+)/)
  if (cableErrorMessageMatch) {
    // groupMatches: 4004, 632
    const code = cableErrorMessageMatch[1]
    const line = cableErrorMessageMatch[2]
    return { code, source: 'cable_monitor', line }
  }

  return null
}

function parseTimestamp(logLine) {
  // try to parse a date out of the line
  return logLine.match(TIMESTAMP_REGEX)
}

function skipEmptyLines(logLines, index) {
  let i = index
  while (i + 1 < logLines.length && !logLines[i + 1].trim()) {
    i += 1
  }
  return i
}

/**
 * Takes an error log file in a string format and parses each log line into its own object
 * @param {string} errorLog The errorLog string to parse
 * @param {string} zone The desired local timezone string
 *   https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
 * @returns {Array<object>} an array of json objects that contain the parsed logs
 */
function errorLogToJson(errorLog, zone = DateTime.local().zoneName) {
  const parsedLog = []

  // Handle numeric offset inputs
  let inputZone = zone
  if (inputZone.toString().match(/^[-+]?[0-9]+$/)) {
    const zoneNumber = Number(inputZone)
    const hours = Math.floor(Math.abs(zoneNumber) / 60)
    const minutes = Math.abs(zoneNumber) % 60
    if (zoneNumber >= 0) {
      inputZone = `UTC+${hours}:${minutes.toString().padStart(2, '0')}`
    } else {
      inputZone = `UTC-${hours}:${minutes.toString().padStart(2, '0')}`
    }
  }

  // Throw an error on invalid zone inputs
  const { offset } = DateTime.now().setZone(inputZone, { keepLocalTime: true })
  if (Number.isNaN(offset)) {
    throw new Error(`Invalid timezone value: ${zone}`)
  }

  // split file into lines
  const logLines = errorLog.split(LINE_SEPARATOR_REGEX)
  for (let i = 0; i < logLines.length; i += 1) {
    let logLine = logLines[i]

    // try to parse a date out of the line
    const timestampMatch = parseTimestamp(logLine)

    // if date was parsable and the next line does not have a timestamp
    // keep parsing lines adding them to a string object in the message
    // until a line has a parsable date
    // --- ORIGINAL ---
    // 2021/02/16 23:11:02 (-480) - v2.0 - Cable Bad Config:
    //   USB MODE: 128
    //   ECG CHAN: 7
    //   ECG MODE: 67
    // --- AFTER ---
    // 2021/02/16 23:11:02 (-480) - v2.0 - Cable Bad Config: {USB MODE: 128, ECG CHAN: 7, ECG MODE: 67}
    i = skipEmptyLines(logLines, i)
    if (
      timestampMatch !== null &&
      i + 1 < logLines.length &&
      logLines[i + 1] &&
      !parseTimestamp(logLines[i + 1])
    ) {
      logLine += ' {'
      let moreLines = true
      let firstParse = true
      while (moreLines) {
        i = skipEmptyLines(logLines, i)
        // if there are no more lines or the next line has a parsable timestamp
        // then we don't add any more contents to this line
        if (i + 1 >= logLines.length || !logLines[i + 1] || parseTimestamp(logLines[i + 1])) {
          moreLines = false
        }
        // if there are more lines that do not have a timestamp, add it to our current line
        else {
          if (!firstParse) {
            logLine += ', '
          } else {
            firstParse = false
          }

          logLine += logLines[i + 1].trim()
          // this will also skip lines in our overall iteration
          i += 1
        }
      }
      logLine += '}'
    }

    // if date was parsable, parse the individual pieces
    if (timestampMatch !== null) {
      const [timestamp] = timestampMatch
      const parts = logLine.split(LINE_SEGMENT_SEPARATOR)

      // if there are at least 3 parts then this is a valid log
      if (parts.length >= 3) {
        const [datePart, firmwarePart, ...messageParts] = parts
        const messagePart = messageParts.join(LINE_SEGMENT_SEPARATOR)
        const timezoneOffsetMatch = datePart.match(TIMEZONE_REGEX)

        // parse the timezoneOffset into a number
        if (timezoneOffsetMatch !== null) {
          let timezoneOffset = Number(timezoneOffsetMatch[0])

          // Parse the date-time string and figure out the timezone if it's reporting 32767
          const zoneName = timezoneOffset === 32767 ? zone : UTC_TIMEZONE
          const datetime = DateTime.fromFormat(timestamp, LUXON_PARSE_FORMAT).setZone(zoneName, {
            keepLocalTime: true,
          })
          if (timezoneOffset === 32767) {
            timezoneOffset = datetime.offset
          }

          // We want to output date-time strings in UTC for consistency, so do another setZone and format
          const formattedDateTime = datetime
            .setZone(UTC_TIMEZONE)
            .toFormat(LUXON_DATE_OUTPUT_FORMAT)

          const logObject = {
            datetime: formattedDateTime,
            timezoneOffset,
            firmwareVersion: firmwarePart,
            message: messagePart,
          }

          // Get additional information and attach it to final object
          const error = determineErrorObject(messagePart)
          if (error) {
            logObject.error = error
          }

          parsedLog.push(logObject)
        }
      }
    }
  }

  return parsedLog
}

module.exports = { errorLogToJson }
