/* eslint-env node, mocha */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const { tze, tzr, tza, tzs, tzg, scp, bak, batt, queue, error, utils } = require(`./index.js`)
const helper = require(`./util/specHelper.js`)
const chai = require('chai')

const { expect } = chai
const fs = require('fs')

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

chai.config.includeStack = true

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
function testJsonParser(File, inputData, outputJson, done, extraArgs) {
  File.toJson(inputData, extraArgs)
    .then((result) => {
      expect(result).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testJsonParserUtils(fileName, inputData, outputJson, done, extraArgs) {
  utils
    .toJson(fileName, inputData, extraArgs)
    .then((result) => {
      expect(result).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
async function testJsonParserError(File, inputData, expectedError, extraArgs) {
  try {
    await File.toJson(inputData, extraArgs)
    throw new Error('Should have thrown!')
  } catch (err) {
    expect(err.message).to.contain(expectedError)
  }
}

// ==============================================================================
function testJsonGeneratorSeparate(File, inputJson, outputJson, done, extraArgs) {
  File.fromJson(inputJson, extraArgs)
    .then((result) => File.toJson(result))
    .then((result) => {
      let expectedJson = outputJson
      if (Buffer.isBuffer(expectedJson)) {
        expectedJson = expectedJson.toString()
      }
      if (typeof expectedJson === 'string') {
        expectedJson = JSON.parse(expectedJson)
      }
      delete expectedJson.crc
      // eslint-disable-next-line no-param-reassign
      delete result.crc

      expect(result).deep.to.equal(expectedJson)
      done()
    })
    .catch((err) => {
      done(err)
    })
}

// ==============================================================================
function testJsonGenerator(File, inputJson, done, extraArgs) {
  testJsonGeneratorSeparate(File, inputJson, inputJson, done, extraArgs)
}

// ==============================================================================
async function testJsonGeneratorError(File, inputData, expectedError, extraArgs) {
  try {
    await File.fromJson(inputData, extraArgs)
    throw new Error('Should have thrown!')
  } catch (err) {
    expect(err.message).to.contain(expectedError)
  }
}

// ==============================================================================
function testJsonGeneratorUtils(File, inputJson, done, extraArgs) {
  utils
    .fromJson(inputJson, extraArgs)
    .then((result) => File.toJson(result))
    .then((result) => {
      let expectedJson = inputJson
      if (Buffer.isBuffer(expectedJson)) {
        expectedJson = expectedJson.toString()
      }
      if (typeof expectedJson === 'string') {
        expectedJson = JSON.parse(expectedJson)
      }
      delete expectedJson.crc
      // eslint-disable-next-line no-param-reassign
      delete result.crc

      expect(result).deep.to.equal(expectedJson)
      done()
    })
    .catch((err) => {
      done(err)
    })
}

// ==============================================================================
function testMetaJson(File, inputData, outputJson, done) {
  File.metaJson(inputData)
    .then((result) => {
      expect(result).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testTextParser(File, inputData, outputText, done, extraArgs) {
  const filteredOutput = helper.splitToFilteredArray(outputText)

  File.toText(inputData, extraArgs)
    .then((result) => {
      const filteredResult = helper.splitToFilteredArray(result)
      expect(filteredResult).deep.to.equal(filteredOutput)
      done()
    })
    .catch(done)
}

// ==============================================================================
async function testTextParserError(File, inputData, expectedError, extraArgs) {
  try {
    await File.toText(inputData, extraArgs)
    throw new Error('Should have thrown!')
  } catch (err) {
    expect(err.message).to.contain(expectedError)
  }
}

// ==============================================================================
function testTextGenerator(File, inputText, done, extraArgs, parserArgs) {
  const filteredInput = helper.splitToFilteredArray(inputText)

  File.fromText(inputText, extraArgs)
    .then((result) => File.toText(result, parserArgs))
    .then((result) => {
      const filteredResult = helper.splitToFilteredArray(result)
      expect(filteredResult).deep.to.equal(filteredInput)
      done()
    })
    .catch((err) => {
      done(err)
    })
}

// ==============================================================================
async function testTextGeneratorError(File, inputData, expectedError, extraArgs) {
  try {
    await File.fromText(inputData, extraArgs)
    throw new Error('Should have thrown!')
  } catch (err) {
    expect(err.message).to.contain(expectedError)
  }
}

// ==============================================================================
function testFinder(File, inputDir, inputTags, outputText, done) {
  const filteredOutput = helper.splitToFilteredArray(outputText)
  const cleanedOutput = helper.removeFilePaths(filteredOutput).sort()

  File.find(inputDir, { tag: inputTags })
    .then((result) => {
      const filteredResult = helper.splitToFilteredArray(result)
      const cleanedResult = helper.removeFilePaths(filteredResult).sort()
      expect(cleanedResult).deep.to.equal(cleanedOutput)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testParse(File, inputDir, serialNumber, tzmrCompat, outputJson, done) {
  const extraArgs = { serialNumber, tzmrCompat }

  File.parse(inputDir, extraArgs)
    .then((result) => {
      expect(result).deep.to.equal(outputJson)
      done()
    })
    .catch(done)
}

// ==============================================================================
function testRetrieve(File, inputDir, sequenceNumber, expected, done) {
  File.retrieveFile(inputDir, sequenceNumber)
    .then((result) => {
      expect(result).deep.to.equal(expected)
      done()
    })
    .catch(done)
}

//------------------------------------------------------------------------------
//     ___  __  __ ___  __
//      |  |_  (_   |  (_
//      |  |__ __)  |  __)
//
//------------------------------------------------------------------------------

// ==============================================================================
describe('tza.fromText()', () => {
  it('should generate a valid TZA file for an H3R device from a text input', (done) => {
    const inputText = fs.readFileSync(`./actions/actions_generator/h3r_input.txt`)

    testTextGenerator(tza, inputText, done)
  })

  it('should generate a valid TZA file for an HPR device from a text input', (done) => {
    const inputText = fs.readFileSync(`./actions/actions_generator/hpr_input.txt`)

    testTextGenerator(tza, inputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputText = fs.readFileSync(`./actions/actions_generator/h3r_input.txt`).toString()

    testTextGenerator(tza, inputText, done)
  })
})

// ==============================================================================
describe('tza.toText()', () => {
  it('should parse a TZA file for an H3R device into a text format', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_parser/h3r_input.tza`)
    const outputText = fs.readFileSync(`./actions/actions_parser/h3r_output.txt`)

    testTextParser(tza, inputData, outputText, done)
  })

  it('should parse a TZA file for an HPR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_parser/hpr_input.tza`)
    const outputText = fs.readFileSync(`./actions/actions_parser/hpr_output.txt`)

    testTextParser(tza, inputData, outputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_parser/h3r_input.tza`).toString('binary')
    const outputText = fs.readFileSync(`./actions/actions_parser/h3r_output.txt`)

    testTextParser(tza, inputData, outputText, done)
  })

  it('should throw an error for a non-TZA file', async () => {
    const inputData = fs.readFileSync(`./settings/settings_parser/h3r_input.tzs`)
    const expectedError = 'File corrupted! (TZSET) Aborting. (-1)'

    await testTextParserError(tza, inputData, expectedError)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./actions/actions_parser/bad_crc.tza`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testTextParserError(tza, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_parser/bad_crc.tza`)
    const outputText = fs.readFileSync(`./actions/actions_parser/bad_crc.txt`)

    testTextParser(tza, inputData, outputText, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tza.toJson()', () => {
  it('should parse a TZA file for an H3R device into a json format', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_to_json/h3r_input.tza`)
    const outputJson = require(`./actions/actions_to_json/h3r_output.json`)

    testJsonParser(tza, inputData, outputJson, done)
  })

  it('should parse a TZA file for an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_to_json/hpr_input.tza`)
    const outputJson = require(`./actions/actions_to_json/hpr_output.json`)

    testJsonParser(tza, inputData, outputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_to_json/h3r_input.tza`).toString('binary')
    const outputJson = require(`./actions/actions_to_json/h3r_output.json`)
    testJsonParser(tza, inputData, outputJson, done)
  })

  it('should throw an error for a non-TZA file', async () => {
    const inputData = fs.readFileSync(`./settings/settings_parser/h3r_input.tzs`)
    const expectedError = 'ERROR: File corrupted! (TZSET) Aborting. (-1)'

    await testJsonParserError(tza, inputData, expectedError)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./actions/actions_parser/bad_crc.tza`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testJsonParserError(tza, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_parser/bad_crc.tza`)
    const outputJson = require(`./actions/actions_parser/bad_crc.json`)

    testJsonParser(tza, inputData, outputJson, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tza.fromJson()', () => {
  it('should generate a TZA file for an H3R device from a JSON input', (done) => {
    const inputJson = require(`./actions/actions_from_json/h3r_input.json`)

    testJsonGenerator(tza, inputJson, done)
  })

  it('should generate a TZA file for an HPR device from a JSON input', (done) => {
    const inputJson = require(`./actions/actions_from_json/hpr_input.json`)

    testJsonGenerator(tza, inputJson, done)
  })

  it('should handle input data as a buffer', (done) => {
    const inputJson = fs.readFileSync(`./actions/actions_from_json/h3r_input.json`)

    testJsonGenerator(tza, inputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputJson = fs.readFileSync(`./actions/actions_from_json/h3r_input.json`).toString()

    testJsonGenerator(tza, inputJson, done)
  })
})

// ==============================================================================
describe('bak.fromText()', () => {
  it('should generate a v1 BAK file from a text input', (done) => {
    const inputText = fs.readFileSync(`./backup/backup_generator/v1_input.txt`)

    testTextGenerator(bak, inputText, done, {}, { deviceType: 'H3R' })
  })

  it('should generate a v2 BAK file from a text input', (done) => {
    const inputText = fs.readFileSync(`./backup/backup_generator/v2_input.txt`)

    testTextGenerator(bak, inputText, done, {}, { deviceType: 'H3R' })
  })

  it('should generate a v3 BAK file from a text input', (done) => {
    const inputText = fs.readFileSync(`${__dirname}/backup/backup_generator/v3_input.txt`)

    testTextGenerator(bak, inputText, done, {}, { deviceType: 'H3R' })
  })

  it('should generate a v4 BAK file from a text input', (done) => {
    const inputText = fs.readFileSync(`${__dirname}/backup/backup_generator/input.txt`)

    testTextGenerator(bak, inputText, done, {}, { deviceType: 'H3R' })
  })

  it('should handle input data as a string', (done) => {
    const inputText = fs.readFileSync(`./backup/backup_generator/input.txt`).toString()

    testTextGenerator(bak, inputText, done, {}, { deviceType: 'H3R' })
  })

  it('should throw an error if input is missing firmware version', async () => {
    const inputText = fs.readFileSync(`./backup/backup_generator/bad_input.txt`)
    const expectedError =
      'ERROR! Undefined firmware version. Provide version in file header with "file.firmware_version=" (-1)'

    await testTextGeneratorError(bak, inputText, expectedError)
  })
})

// ==============================================================================
describe('bak.toText()', () => {
  it('should parse a short v1 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`./backup/backup_parser/v1_short_input.tmp`)
    const outputText = fs.readFileSync(`./backup/backup_parser/v1_output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should parse a long v1 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`./backup/backup_parser/v1_long_input.tmp`)
    const outputText = fs.readFileSync(`./backup/backup_parser/v1_output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should parse a short v2 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`./backup/backup_parser/v2_short_input.tmp`)
    const outputText = fs.readFileSync(`./backup/backup_parser/v2_output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should parse a long v2 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`./backup/backup_parser/v2_long_input.tmp`)
    const outputText = fs.readFileSync(`./backup/backup_parser/v2_output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should parse a short v3 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`${__dirname}/backup/backup_parser/v3_short_input.tmp`)
    const outputText = fs.readFileSync(`${__dirname}/backup/backup_parser/v3_output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should parse a long v3 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`${__dirname}/backup/backup_parser/v3_long_input.tmp`)
    const outputText = fs.readFileSync(`${__dirname}/backup/backup_parser/v3_output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should parse a v4 BAK file into a text format', (done) => {
    const inputData = fs.readFileSync(`${__dirname}/backup/backup_parser/input.tmp`)
    const outputText = fs.readFileSync(`${__dirname}/backup/backup_parser/output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./backup/backup_parser/input.tmp`).toString('binary')
    const outputText = fs.readFileSync(`./backup/backup_parser/output.txt`)

    testTextParser(bak, inputData, outputText, done, { deviceType: 'H3R' })
  })
})

// ==============================================================================
describe('batt.fromText()', () => {
  it('should generate a BATT file from a text input', (done) => {
    const inputText = fs.readFileSync(`./battery/battery_info_generator/input.txt`)

    testTextGenerator(batt, inputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputText = fs.readFileSync(`./battery/battery_info_generator/input.txt`).toString()

    testTextGenerator(batt, inputText, done)
  })
})

// ==============================================================================
describe('batt.toText()', () => {
  it('should parse a BATT file into a text format', (done) => {
    const inputData = fs.readFileSync(`./battery/battery_info_parser/input.batt`)
    const outputText = fs.readFileSync(`./battery/battery_info_parser/output.txt`)

    testTextParser(batt, inputData, outputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./battery/battery_info_parser/input.batt`).toString('binary')
    const outputText = fs.readFileSync(`./battery/battery_info_parser/output.txt`)

    testTextParser(batt, inputData, outputText, done)
  })
})

// ==============================================================================
describe('queue.parse()', () => {
  it('should search a directory for wireless files and output in a json format accounting for the upload log', (done) => {
    const inputDir = `./queue/queue_to_json/tmp_upload`
    const serialNumber = 'H3R3000048'
    const tzmrCompat = '0'
    const outputJson = require(`./queue/queue_to_json/output_upload.json`)

    testParse(queue, inputDir, serialNumber, tzmrCompat, outputJson, done)
  })

  it('should search a directory for wireless files and output in a json format with no upload log', (done) => {
    const inputDir = `./queue/queue_to_json/tmp_noUpload`
    const serialNumber = 'H3R3000048'
    const tzmrCompat = '0'
    const outputJson = require(`./queue/queue_to_json/output_noUpload.json`)

    testParse(queue, inputDir, serialNumber, tzmrCompat, outputJson, done)
  })

  it('should search a directory for wireless files and output in a json format accounting for the upload log in tzmr compat mode', (done) => {
    const inputDir = `./queue/queue_to_json/tmp_upload`
    const serialNumber = 'H3R3000048'
    const tzmrCompat = '1'
    const outputJson = require(`./queue/queue_to_json/tzmrOutput_upload.json`)

    testParse(queue, inputDir, serialNumber, tzmrCompat, outputJson, done)
  })

  it('should search a directory for wireless files and output in a json format with no upload log in tzmr compat mode', (done) => {
    const inputDir = `./queue/queue_to_json/tmp_noUpload`
    const serialNumber = 'H3R3000048'
    const tzmrCompat = '1'
    const outputJson = require(`./queue/queue_to_json/tzmrOutput_noUpload.json`)

    testParse(queue, inputDir, serialNumber, tzmrCompat, outputJson, done)
  })

  it('should search a directory for wireless files and output in a json format including the error log routes', (done) => {
    const inputDir = `./queue/queue_to_json/tmp_errorLog`
    const serialNumber = 'H3R1234567'
    const tzmrCompat = '0'
    const outputJson = require(`./queue/queue_to_json/output_errorLog.json`)

    testParse(queue, inputDir, serialNumber, tzmrCompat, outputJson, done)
  })
})

// ==============================================================================
describe('error.toJson()', () => {
  it('should parse an error log file from an H3R device into a json format', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/h3r_error.log`)
    const outputJson = require(`./error/error_to_json/h3r_error.json`)

    testJsonParser(error, inputData, outputJson, done)
  })

  it('should handle multiline error entries in log files from H3R devices', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/h3r_multiline.log`)
    const outputJson = require(`./error/error_to_json/h3r_multiline.json`)

    testJsonParser(error, inputData, outputJson, done)
  })

  it('should detect assert codes in restart messages', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/h3r_asserts.log`)
    const outputJson = require(`./error/error_to_json/h3r_asserts.json`)

    testJsonParser(error, inputData, outputJson, done)
  })

  it('should parse an error log file from an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/hpr_error.log`)
    const outputJson = require(`./error/error_to_json/hpr_error.json`)

    // -05:00 or -04:00 timezone based on DST
    const zone = 'America/New_York'
    testJsonParser(error, inputData, outputJson, done, { zone })
  })

  it('should support numeric timezone inputs', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/hpr_error.log`)
    const outputJson = require(`./error/error_to_json/hpr_error_300.json`)

    // UTC-05:00 regardless of DST
    const zone = -300
    testJsonParser(error, inputData, outputJson, done, { zone })
  })

  it('should throw an error for a non-valid timezone input', async () => {
    const inputData = fs.readFileSync(`./error/error_to_json/hpr_error.log`)
    const expectedError = 'Invalid timezone value'

    const zone = 'Not/A_Zone'
    await testJsonParserError(error, inputData, expectedError, { zone })
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/h3r_error.log`).toString('binary')
    const outputJson = require(`./error/error_to_json/h3r_error.json`)

    testJsonParser(error, inputData, outputJson, done)
  })

  it('should throw an error for a non-error log file', async () => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_input.tze`)
    const expectedError = 'No parsable logs found!'

    await testJsonParserError(error, inputData, expectedError)
  })
})

// ==============================================================================
describe('tze.find()', () => {
  it('should search a directory for TZE files from an H3R device and output in a text format', (done) => {
    const inputDir = `./events/event_finder/h3r_dir`
    const inputTags = '0'
    const outputText = fs.readFileSync(`./events/event_finder/h3r_output.txt`)

    testFinder(tze, inputDir, inputTags, outputText, done)
  })

  it('should search a directory for TZE files from an HPR device and output in a text format', (done) => {
    const inputDir = `./events/event_finder/hpr_dir`
    const inputTags = '0'
    const outputText = fs.readFileSync(`./events/event_finder/hpr_output.txt`)

    testFinder(tze, inputDir, inputTags, outputText, done)
  })

  it('should search a directory for TZE files from a TZMR device and output in a text format', (done) => {
    const inputDir = `./events/event_finder/tzmr_dir`
    const inputTags = '0'
    const outputText = fs.readFileSync(`./events/event_finder/tzmr_output.txt`)

    testFinder(tze, inputDir, inputTags, outputText, done)
  })

  it('should search a directory for TZE files with specific tags from an H3R device and output in a text format', (done) => {
    const inputDir = `./events/event_finder/h3r_dir`
    const inputTags = '13'
    const outputText = fs.readFileSync(`./events/event_finder/h3r_output_rhythms.txt`)

    testFinder(tze, inputDir, inputTags, outputText, done)
  })
})

// ==============================================================================
describe('tze.fromJson()', () => {
  it('should generate a TZE file for an H3R device from a JSON input', (done) => {
    const inputJson = require(`./events/event_generator/h3r_input.json`)

    testJsonGenerator(tze, inputJson, done)
  })

  it('should generate a TZE file for an HPR device from a JSON input', (done) => {
    const inputJson = require(`./events/event_generator/hpr_input.json`)

    testJsonGenerator(tze, inputJson, done)
  })

  it('should generate a TZE file for a TZMR device from a JSON input', (done) => {
    const inputJson = require(`./events/event_generator/tzmr_input.json`)

    testJsonGenerator(tze, inputJson, done)
  })

  it('should handle input data as a buffer', (done) => {
    const inputJson = fs.readFileSync(`./events/event_generator/h3r_input.json`)

    testJsonGenerator(tze, inputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputJson = fs.readFileSync(`./events/event_generator/h3r_input.json`).toString()

    testJsonGenerator(tze, inputJson, done)
  })
})

// ==============================================================================
describe('tze.toText()', () => {
  it('should parse a TZE file from an H3R device into a text format', (done) => {
    const inputData = fs.readFileSync(`./events/event_parser/h3r_input.tze`)
    const outputText = fs.readFileSync(`./events/event_parser/h3r_output.txt`)

    testTextParser(tze, inputData, outputText, done)
  })

  it('should parse a TZE file from an HPR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./events/event_parser/hpr_input.tze`)
    const outputText = fs.readFileSync(`./events/event_parser/hpr_output.txt`)

    testTextParser(tze, inputData, outputText, done)
  })

  it('should parse a TZE file from a TZMR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./events/event_parser/tzmr_input.tze`)
    const outputText = fs.readFileSync(`./events/event_parser/tzmr_output.txt`)

    testTextParser(tze, inputData, outputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./events/event_parser/h3r_input.tze`).toString('binary')
    const outputText = fs.readFileSync(`./events/event_parser/h3r_output.txt`)

    testTextParser(tze, inputData, outputText, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./events/event_parser/bad_crc.tze`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testTextParserError(tze, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./events/event_parser/bad_crc.tze`)
    const outputText = fs.readFileSync(`./events/event_parser/bad_crc.txt`)

    testTextParser(tze, inputData, outputText, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tze.toJson()', () => {
  it('should parse a TZE file from an H3R device into a json format', (done) => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_input.tze`)
    const outputJson = require(`./events/event_to_json/h3r_output.json`)

    testJsonParser(tze, inputData, outputJson, done)
  })

  it('should parse a TZE file from an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./events/event_to_json/hpr_input.tze`)
    const outputJson = require(`./events/event_to_json/hpr_output.json`)

    testJsonParser(tze, inputData, outputJson, done)
  })

  it('should parse a TZE file from a TZMR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./events/event_to_json/tzmr_input.tze`)
    const outputJson = require(`./events/event_to_json/tzmr_output.json`)

    testJsonParser(tze, inputData, outputJson, done)
  })

  it('should parse a TZE file with a time zone offset of 32767 as local time', (done) => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_local_zone.tze`)
    const outputJson = require(`./events/event_to_json/h3r_local_zone.json`)

    const zone = 'UTC-04:00'
    testJsonParser(tze, inputData, outputJson, done, { zone })
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_input.tze`).toString('binary')
    const outputJson = require(`./events/event_to_json/h3r_output.json`)

    testJsonParser(tze, inputData, outputJson, done)
  })

  it('should throw an error for a non-TZE file', async () => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/h3r_input.tzr`)
    const expectedError = 'ERROR: File corrupted! Aborting. (-1)'

    await testJsonParserError(tze, inputData, expectedError)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./events/event_parser/bad_crc.tze`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testJsonParserError(tze, inputData, expectedError)
  })

  it('should throw an error for a non-valid timezone input', async () => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_input.tze`)
    const expectedError = 'Invalid timezone value'

    const zone = 'Not/A_Zone'
    await testJsonParserError(error, inputData, expectedError, { zone })
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./events/event_parser/bad_crc.tze`)
    const outputJson = require(`./events/event_parser/bad_crc.json`)

    testJsonParser(tze, inputData, outputJson, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tzr.find()', () => {
  it('should search a directory for TZR files from an H3R device and output in a text format', (done) => {
    const inputDir = `./interval/interval_finder/h3r_dir`
    const inputTags = '0'
    const outputText = fs.readFileSync(`./interval/interval_finder/h3r_output.txt`)

    testFinder(tzr, inputDir, inputTags, outputText, done)
  })

  it('should search a directory for TZR files from an HPR device and output in a text format', (done) => {
    const inputDir = `./interval/interval_finder/hpr_dir`
    const inputTags = '0'
    const outputText = fs.readFileSync(`./interval/interval_finder/hpr_output.txt`)

    testFinder(tzr, inputDir, inputTags, outputText, done)
  })

  it('should search a directory for TZR files from a TZMR device and output in a text format', (done) => {
    const inputDir = `./interval/interval_finder/tzmr_dir`
    const inputTags = '0'
    const outputText = fs.readFileSync(`./interval/interval_finder/tzmr_output.txt`)

    testFinder(tzr, inputDir, inputTags, outputText, done)
  })
})

// ==============================================================================
describe('tzr.fromJson()', () => {
  it('should generate a TZR file for an H3R device from a JSON input', (done) => {
    const inputJson = require(`./interval/interval_generator/h3r_input.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should generate a TZR file for an H3R device with summarize QRS detections enabled', (done) => {
    const inputJson = require(`./interval/interval_generator/h3r_summarize.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should generate a TZR file for an H3R device with temperature reporting enabled', (done) => {
    const inputJson = require(`./interval/interval_generator/h3r_temperature.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should generate a TZR file for an HPR device from a JSON input', (done) => {
    const inputJson = require(`./interval/interval_generator/hpr_input.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should generate a TZR file for an HPR device with temperature reporting enabled', (done) => {
    const inputJson = require(`./interval/interval_generator/hpr_temperature.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should generate a TZR file for a TZMR device from a JSON input', (done) => {
    const inputJson = require(`./interval/interval_generator/tzmr_input.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should handle input data as a buffer', (done) => {
    const inputJson = fs.readFileSync(`./interval/interval_generator/h3r_input.json`)

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputJson = fs.readFileSync(`./interval/interval_generator/h3r_input.json`).toString()

    testJsonGenerator(tzr, inputJson, done)
  })

  it('should generate a TZR file with an unknown tag', (done) => {
    // Cannot use the "testJsonGenerator()" as expectedJson != inputJson
    const inputJson = fs.readFileSync(`./interval/interval_generator/bad_tag_input.json`)
    const outputJson = fs.readFileSync(`./interval/interval_generator/bad_tag_output.json`)

    testJsonGeneratorSeparate(tzr, inputJson, outputJson, done)
  })
})

// ==============================================================================
describe('tzr.toText()', () => {
  it('should parse a TZR file from an H3R device into a text format', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_input.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_output.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should parse a TZR file from an H3R device with summarize QRS detections enabled', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_summarize.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_summarize.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should only output HR values from the TZR file if tag 19 is selected', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_summarize.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_summarize_t19.txt`)
    const extraArgs = { tag: '19' }

    testTextParser(tzr, inputData, outputText, done, extraArgs)
  })

  it('should only output battery values from the TZR file if tag 150 is selected', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_summarize.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_summarize_t150.txt`)
    const extraArgs = { tag: '150' }

    testTextParser(tzr, inputData, outputText, done, extraArgs)
  })

  it('should handle HR calculations properly when recording interruptions occur', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_summarize_2.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_summarize_2.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should handle HR calculations properly when negative delta-t values occur', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_summarize_3.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_summarize_3.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should handle HR calculations properly with starting offset', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_summarize.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_epoch_ms.txt`)

    const extraArgs = { epochMs: '1568174400000' }

    testTextParser(tzr, inputData, outputText, done, extraArgs)
  })

  it('should parse a TZR file from an HPR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/hpr_input.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/hpr_output.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should parse a TZR file from a TZMR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/tzmr_input.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/tzmr_output.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/h3r_input.tzr`).toString('binary')
    const outputText = fs.readFileSync(`./interval/interval_parser/h3r_output.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./interval/interval_parser/bad_crc.tzr`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testTextParserError(tzr, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/bad_crc.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/bad_crc.txt`)

    testTextParser(tzr, inputData, outputText, done, { skipCrc: '1' })
  })

  it('should parse a TZR file with an unknown tag', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/bad_tag_input.tzr`)
    const outputText = fs.readFileSync(`./interval/interval_parser/bad_tag_output.txt`)

    testTextParser(tzr, inputData, outputText, done)
  })
})

// ==============================================================================
describe('tzr.toJson()', () => {
  it('should parse a TZR file from an H3R device into a json format', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/h3r_input.tzr`)
    const outputJson = require(`./interval/interval_to_json/h3r_output.json`)

    testJsonParser(tzr, inputData, outputJson, done)
  })

  it('should parse a TZR file from an H3R device with summarize QRS detections enabled', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/h3r_summarize.tzr`)
    const outputJson = require(`./interval/interval_to_json/h3r_summarize.json`)

    testJsonParser(tzr, inputData, outputJson, done)
  })

  it(`should parse a TZR file from an H3R device with temperature reporting enabled`, (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/h3r_temperature.tzr`)
    const outputJson = require(`./interval/interval_to_json/h3r_temperature.json`)

    testJsonParser(tzr, inputData, outputJson, done)
  })

  it('should parse a TZR file from an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/hpr_input.tzr`)
    const outputJson = require(`./interval/interval_to_json/hpr_output.json`)

    testJsonParser(tzr, inputData, outputJson, done)
  })

  it(`should parse a TZR file from an HPR device with temperature reporting enabled`, (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/hpr_temperature.tzr`)
    const outputJson = require(`./interval/interval_to_json/hpr_temperature.json`)

    testJsonParser(tzr, inputData, outputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs
      .readFileSync(`./interval/interval_to_json/h3r_input.tzr`)
      .toString('binary')
    const outputJson = require(`./interval/interval_to_json/h3r_output.json`)

    testJsonParser(tzr, inputData, outputJson, done)
  })

  it('should throw an error for a non-TZR file', async () => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_input.tze`)
    const expectedError = 'ERROR: File corrupted! Aborting. (-1)'

    await testJsonParserError(tzr, inputData, expectedError)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./interval/interval_parser/bad_crc.tzr`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testJsonParserError(tzr, inputData, expectedError)
  })

  it('should throw an error for a non-valid timezone input', async () => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/h3r_input.tzr`)
    const expectedError = 'Invalid timezone value'

    const zone = 'Not/A_Zone'
    await testJsonParserError(error, inputData, expectedError, { zone })
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_parser/bad_crc.tzr`)
    const outputJson = require(`./interval/interval_parser/bad_crc.json`)

    testJsonParser(tzr, inputData, outputJson, done, { skipCrc: '1' })
  })

  it('should parse a TZR file with an unknown tag', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/bad_tag_input.tzr`)
    const outputJson = require(`./interval/interval_to_json/bad_tag_output.json`)
    testJsonParser(tzr, inputData, outputJson, done)
  })
})

// ==============================================================================
describe('tzs.fromText()', () => {
  it('should generate a TZS file for an H3R device from a text input', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/h3r_input.txt`)

    testTextGenerator(tzs, inputText, done)
  })

  it('should generate a TZS file for an H3R device in compat mode from a text input', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/h3r_compat.txt`)

    testTextGenerator(tzs, inputText, done)
  })

  it('should throw an error if an invalid sample rate for an H3R device is inputted', async () => {
    const inputText = fs.readFileSync(`./settings/settings_generator/h3r_bad_sample_rate.txt`)
    const expectedError =
      'ERROR! setting.sample_rate=300 is not in list of acceptable options: [250,500,1000,2000,4000] (-1)'

    await testTextGeneratorError(tzs, inputText, expectedError)
  })

  it('should throw an error if an invalid number of study hours for an H3R device running v2.4 from a text input', async () => {
    const inputText = fs.readFileSync(`./settings/settings_generator/h3r_bad_study_hours.txt`)
    const expectedError = 'ERROR! Invalid binary data: setting.study_hours=1464 (-1)'

    await testTextGeneratorError(tzs, inputText, expectedError)
  })

  it('should generate a TZS file for an HPR device from a text input', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/hpr_input.txt`)

    testTextGenerator(tzs, inputText, done)
  })

  it('should generate a TZS file for an HPRM device from a text input', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/hpr_mct_input.txt`)

    testTextGenerator(tzs, inputText, done)
  })

  it('should throw an error if an invalid sample rate for an HPR device is inputted', async () => {
    const inputText = fs.readFileSync(`./settings/settings_generator/hpr_bad_sample_rate.txt`)
    const expectedError =
      'ERROR! setting.sample_rate=250 is not in list of acceptable options: [200,400,800,1600] (-1)'

    await testTextGeneratorError(tzs, inputText, expectedError)
  })

  it('should throw an error if an invalid number of study hours for an HPR device running v1.6 from a text input', async () => {
    const inputText = fs.readFileSync(`./settings/settings_generator/hpr_bad_study_hours.txt`)
    const expectedError = 'ERROR! Invalid binary data: setting.study_hours=1464 (-1)'

    await testTextGeneratorError(tzs, inputText, expectedError)
  })

  it('should generate a TZS file for an HPR device with zymed_compat set to 2 from a text input', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/hpr_zymed_2.txt`)

    testTextGenerator(tzs, inputText, done)
  })

  it('should generate a TZS file for a TZMR device from a text input', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/tzmr_input.txt`)

    testTextGenerator(tzs, inputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputText = fs.readFileSync(`./settings/settings_generator/h3r_input.txt`).toString()

    testTextGenerator(tzs, inputText, done)
  })
})

// ==============================================================================
describe('tzs.toText()', () => {
  it('should parse a TZS file from an H3R device into a text format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/h3r_input.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/h3r_output.txt`)

    testTextParser(tzs, inputData, outputText, done)
  })

  it('should parse a TZS file from an H3R device in compat mode into a text format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/h3r_compat.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/h3r_compat.txt`)

    testTextParser(tzs, inputData, outputText, done)
  })

  it('should parse a TZS file from an HPR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/hpr_input.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/hpr_output.txt`)

    testTextParser(tzs, inputData, outputText, done)
  })

  it('should parse a TZS file from an HPRM device into a text format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/hpr_mct_input.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/hpr_mct_output.txt`)

    testTextParser(tzs, inputData, outputText, done)
  })

  it('should parse a TZS file from a TZMR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/tzmr_input.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/tzmr_output.txt`)

    testTextParser(tzs, inputData, outputText, done)
  })

  it('should parse a TZS file from an H3R device while ignoring bad tags (142) into a text format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/h3r_unknown_tag_142.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/h3r_unknown_tag_142.txt`)

    testTextParser(tzs, inputData, outputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/h3r_input.tzs`).toString('binary')
    const outputText = fs.readFileSync(`./settings/settings_parser/h3r_output.txt`)
    testTextParser(tzs, inputData, outputText, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./settings/settings_parser/bad_crc.tzs`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testTextParserError(tzs, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/bad_crc.tzs`)
    const outputText = fs.readFileSync(`./settings/settings_parser/bad_crc.txt`)

    testTextParser(tzs, inputData, outputText, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tzs.toJson()', () => {
  it('should parse a TZS file from an H3R device into a json format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/h3r_input.tzs`)
    const outputJson = require(`./settings/settings_to_json/h3r_output.json`)

    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should parse a TZS file from an H3R device in compat mode into a json format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/h3r_compat.tzs`)
    const outputJson = require(`./settings/settings_to_json/h3r_compat.json`)

    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should parse a TZS file from an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/hpr_input.tzs`)
    const outputJson = require(`./settings/settings_to_json/hpr_output.json`)

    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should parse a TZS file from an HPRM device into a json format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/hpr_mct_input.tzs`)
    const outputJson = require(`./settings/settings_to_json/hpr_mct_output.json`)

    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should parse a TZS file from a TZMR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/tzmr_input.tzs`)
    const outputJson = require(`./settings/settings_to_json/tzmr_output.json`)

    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should parse a TZS file from an H3R device while ignoring bad tags (142) into a json format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/h3r_unknown_tag_142.tzs`)
    const outputJson = require(`./settings/settings_to_json/h3r_unknown_tag_142.json`)

    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs
      .readFileSync(`./settings/settings_to_json/h3r_input.tzs`)
      .toString('binary')
    const outputJson = require(`./settings/settings_to_json/h3r_output.json`)
    testJsonParser(tzs, inputData, outputJson, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./settings/settings_parser/bad_crc.tzs`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testJsonParserError(tzs, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_parser/bad_crc.tzs`)
    const outputJson = require(`./settings/settings_parser/bad_crc.json`)

    testJsonParser(tzs, inputData, outputJson, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tzs.fromJson()', () => {
  it('should generate a TZS file for an H3R device from a JSON input', (done) => {
    const inputJson = require(`./settings/settings_from_json/h3r_input.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should generate a TZS file for an H3R device in compat mode from a JSON input', (done) => {
    const inputJson = require(`./settings/settings_from_json/h3r_compat.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should throw an error if an invalid sample rate for an H3R device is inputted', async () => {
    const inputJson = require(`./settings/settings_from_json/h3r_bad_sample_rate.json`)
    const expectedError =
      'ERROR! setting.sample_rate=300 is not in list of acceptable options: [250,500,1000,2000,4000] (-1)'

    await testJsonGeneratorError(tzs, inputJson, expectedError)
  })

  it('should throw an error if an invalid number of study hours for an H3R device running v2.4 is inputted from JSON input', async () => {
    const inputJson = require(`./settings/settings_from_json/h3r_bad_study_hours.json`)
    const expectedError = 'ERROR! Invalid binary data: setting.study_hours=1464 (-1)'

    await testJsonGeneratorError(tzs, inputJson, expectedError)
  })

  it('should generate a TZS file for an HPR device from a JSON input', (done) => {
    const inputJson = require(`./settings/settings_from_json/hpr_input.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should generate a TZS file for an HPRM device from a JSON input', (done) => {
    const inputJson = require(`./settings/settings_from_json/hpr_mct_input.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should throw an error if an invalid sample rate for an HPR device is inputted', async () => {
    const inputJson = require(`./settings/settings_from_json/hpr_bad_sample_rate.json`)
    const expectedError =
      'ERROR! setting.sample_rate=250 is not in list of acceptable options: [200,400,800,1600] (-1)'

    await testJsonGeneratorError(tzs, inputJson, expectedError)
  })

  it('should throw an error if an invalid number of study hours for an HPR device running v1.6 is inputted from JSON input', async () => {
    const inputJson = require(`./settings/settings_from_json/hpr_bad_study_hours.json`)
    const expectedError = 'ERROR! Invalid binary data: setting.study_hours=1464 (-1)'

    await testJsonGeneratorError(tzs, inputJson, expectedError)
  })

  it('should generate a TZS file for an HPR device with zymed_compat set to 2 from a JSON input', (done) => {
    const inputJson = require(`./settings/settings_from_json/hpr_zymed_2.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should generate a TZS file for a TZMR device from a JSON input', (done) => {
    const inputJson = require(`./settings/settings_from_json/tzmr_input.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should handle input data as a buffer', (done) => {
    const inputJson = fs.readFileSync(`./settings/settings_from_json/h3r_input.json`)

    testJsonGenerator(tzs, inputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputJson = fs.readFileSync(`./settings/settings_from_json/h3r_input.json`).toString()

    testJsonGenerator(tzs, inputJson, done)
  })
})

// ==============================================================================
describe('tzs.metaJson()', () => {
  it('should return meta-information for all H3R settings in a json format', (done) => {
    const inputData = 'H3R'
    const outputJson = require(`./settings/h3r_settings.json`)

    testMetaJson(tzs, inputData, outputJson, done)
  })

  it('should return meta-information for all HPR settings in a json format', (done) => {
    const inputData = 'HPR'
    const outputJson = require(`./settings/hpr_settings.json`)

    testMetaJson(tzs, inputData, outputJson, done)
  })

  it('should return meta-information for all TZMR settings in a json format', (done) => {
    const inputData = 'TZMR'
    const outputJson = require(`./settings/tzmr_settings.json`)

    testMetaJson(tzs, inputData, outputJson, done)
  })
})

// ==============================================================================
describe('tzg.toJson()', () => {
  it('should parse a TZG file from an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./accel/accel_to_json/hpr_input.tzg`)
    const outputJson = require(`./accel/accel_to_json/hpr_output.json`)

    testJsonParser(tzg, inputData, outputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./accel/accel_to_json/hpr_input.tzg`).toString('binary')
    const outputJson = require(`./accel/accel_to_json/hpr_output.json`)

    testJsonParser(tzg, inputData, outputJson, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./accel/accel_to_json/bad_crc.tzg`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testJsonParserError(tzg, inputData, expectedError)
  })

  it('should throw an error for a non-valid timezone input', async () => {
    const inputData = fs.readFileSync(`./accel/accel_to_json/hpr_input.tzg`)
    const expectedError = 'Invalid timezone value'

    const zone = 'Not/A_Zone'
    await testJsonParserError(error, inputData, expectedError, { zone })
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./accel/accel_to_json/bad_crc.tzg`)
    const outputJson = require(`./accel/accel_to_json/bad_crc.json`)

    testJsonParser(tzg, inputData, outputJson, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('tzg.fromJson()', () => {
  it('should generate a TZG file for an HPR device from a JSON input', (done) => {
    const inputJson = require(`./accel/accel_from_json/hpr_input.json`)

    testJsonGenerator(tzg, inputJson, done)
  })

  it('should handle input data as a buffer', (done) => {
    const inputJson = fs.readFileSync(`./accel/accel_from_json/hpr_input.json`)

    testJsonGenerator(tzg, inputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputJson = fs.readFileSync(`./accel/accel_from_json/hpr_input.json`).toString()

    testJsonGenerator(tzg, inputJson, done)
  })
})

// ==============================================================================
describe('scp.fromJson()', () => {
  it('should generate an SCP file for an H3R device from a JSON input', (done) => {
    const inputJson = require(`./scp/scp_generator/h3r_input.json`)

    testJsonGenerator(scp, inputJson, done)
  })

  it('should generate an SCP file for an HPR device from a JSON input', (done) => {
    const inputJson = require(`./scp/scp_generator/hpr_input.json`)

    testJsonGenerator(scp, inputJson, done)
  })

  it('should handle input data as a buffer', (done) => {
    const inputJson = fs.readFileSync(`./scp/scp_generator/h3r_input.json`)

    testJsonGenerator(scp, inputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputJson = fs.readFileSync(`./scp/scp_generator/h3r_input.json`).toString()

    testJsonGenerator(scp, inputJson, done)
  })
})

// ==============================================================================
describe('scp.toText()', () => {
  it('should parse an SCP file from an H3R device into a text format', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_parser/h3r_input.scp`)
    const outputText = fs.readFileSync(`./scp/scp_parser/h3r_output.txt`)

    testTextParser(scp, inputData, outputText, done)
  })

  it('should parse an SCP file from an HPR device into a text format', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_parser/hpr_input.scp`)
    const outputText = fs.readFileSync(`./scp/scp_parser/hpr_output.txt`)

    testTextParser(scp, inputData, outputText, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_parser/h3r_input.scp`).toString('binary')
    const outputText = fs.readFileSync(`./scp/scp_parser/h3r_output.txt`)

    testTextParser(scp, inputData, outputText, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./scp/scp_parser/bad_crc.scp`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testTextParserError(scp, inputData, expectedError)
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_parser/bad_crc.scp`)
    const outputText = fs.readFileSync(`./scp/scp_parser/bad_crc.txt`)

    testTextParser(scp, inputData, outputText, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('scp.toJson()', () => {
  it('should parse an SCP file from an H3R device into a json format', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_to_json/h3r_input.scp`)
    const outputJson = require(`./scp/scp_to_json/h3r_output.json`)

    testJsonParser(scp, inputData, outputJson, done)
  })

  it('should parse an SCP file from an HPR device into a json format', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_to_json/hpr_input.scp`)
    const outputJson = require(`./scp/scp_to_json/hpr_output.json`)

    testJsonParser(scp, inputData, outputJson, done)
  })

  it('should handle input data as a string', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_to_json/h3r_input.scp`).toString('binary')
    const outputJson = require(`./scp/scp_to_json/h3r_output.json`)

    testJsonParser(scp, inputData, outputJson, done)
  })

  it('should throw an error for a bad CRC', async () => {
    const inputData = fs.readFileSync(`./scp/scp_parser/bad_crc.scp`)
    const expectedError = 'ERROR: File CRC Invalid.'

    await testJsonParserError(scp, inputData, expectedError)
  })

  it('should throw an error for a non-valid timezone input', async () => {
    const inputData = fs.readFileSync(`./scp/scp_to_json/h3r_input.scp`)
    const expectedError = 'Invalid timezone value'

    const zone = 'Not/A_Zone'
    await testJsonParserError(error, inputData, expectedError, { zone })
  })

  it('should support skipping the CRC check', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_parser/bad_crc.scp`)
    const outputJson = require(`./scp/scp_parser/bad_crc.json`)

    testJsonParser(scp, inputData, outputJson, done, { skipCrc: '1' })
  })
})

// ==============================================================================
describe('scp.retrieveFile()', () => {
  it('should retrieve an SCP file from a given directory with one ecg disabled', (done) => {
    const expected = fs.readFileSync(`./test_drives/multi_ecg/ecgs/00/00/00/42.scp`)
    testRetrieve(scp, `./test_drives/multi_ecg`, 42, expected, done)
  })

  it('should retrieve an SCP file when passed the path to a single scp file, regardless of the index', (done) => {
    const expected = fs.readFileSync(`./test_drives/multi_ecg/ecgs/00/00/00/37.scp`)
    testRetrieve(scp, `./test_drives/multi_ecg/ecgs/00/00/00/37.scp`, 42, expected, done)
  })

  it('should retrieve an SCP file from a given directory with one ecg enabled', (done) => {
    const bufSize = 32768
    const expected = Buffer.alloc(bufSize)
    fs.readSync(
      fs.openSync(`./test_drives/one_ecg/ecgs/scpecg.bin`),
      expected,
      0,
      bufSize,
      bufSize * 42
    )
    testRetrieve(scp, `./test_drives/one_ecg`, 42, expected, done)
  })

  it('should retrieve an SCP file when given a direct path to the scpecg.bin file', (done) => {
    const bufSize = 32768
    const expected = Buffer.alloc(bufSize)
    fs.readSync(
      fs.openSync(`./test_drives/one_ecg/ecgs/scpecg.bin`),
      expected,
      0,
      bufSize,
      bufSize * 42
    )
    testRetrieve(scp, `./test_drives/one_ecg/ecgs/scpecg.bin`, 42, expected, done)
  })
})

// ==============================================================================
describe('utils.toJson', () => {
  it('should detect and parse SCP files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./scp/scp_to_json/h3r_input.scp`)
    const outputJson = require(`./scp/scp_to_json/h3r_output.json`)

    testJsonParserUtils('h3r_input.scp', inputData, outputJson, done)
  })

  it('should detect and parse TZR files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./interval/interval_to_json/h3r_input.tzr`)
    const outputJson = require(`./interval/interval_to_json/h3r_output.json`)

    testJsonParserUtils('h3r_input.tzr', inputData, outputJson, done)
  })

  it('should detect and parse TZE files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./events/event_to_json/h3r_input.tze`)
    const outputJson = require(`./events/event_to_json/h3r_output.json`)

    testJsonParserUtils('h3r_input.tze', inputData, outputJson, done)
  })

  it('should detect and parse TZS files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./settings/settings_to_json/h3r_input.tzs`)
    const outputJson = require(`./settings/settings_to_json/h3r_output.json`)

    testJsonParserUtils('h3r_input.tzs', inputData, outputJson, done)
  })

  it('should detect and parse TZA files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./actions/actions_to_json/h3r_input.tza`)
    const outputJson = require(`./actions/actions_to_json/h3r_output.json`)

    testJsonParserUtils('h3r_input.tza', inputData, outputJson, done)
  })

  it('should detect and parse TZG files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./accel/accel_to_json/hpr_input.tzg`)
    const outputJson = require(`./accel/accel_to_json/hpr_output.json`)

    testJsonParserUtils('hpr_input.tzg', inputData, outputJson, done)
  })

  it('should detect and parse error log files into a JSON format', (done) => {
    const inputData = fs.readFileSync(`./error/error_to_json/h3r_error.log`)
    const outputJson = require(`./error/error_to_json/h3r_error.json`)

    testJsonParserUtils('h3r_error.log', inputData, outputJson, done)
  })

  it('should throw an error if no filename is provided', (done) => {
    const fileName = ''
    const inputData = fs.readFileSync(`./scp/scp_to_json/h3r_input.scp`)

    try {
      utils.toJson(fileName, inputData)
      done('utils.toJson failed to throw!')
    } catch (err) {
      expect(err.message).to.equal('A filename MUST be specified to use utils.toJson()!')
      done()
    }
  })

  it('should throw an error if given unknown file type', (done) => {
    const inputData = fs.readFileSync(`./README.md`)

    try {
      utils.toJson('README.md', inputData)
      done('utils.toJson failed to throw!')
    } catch (err) {
      expect(err.message).to.equal('Unsupported file type!')
      done()
    }
  })
})

// ==============================================================================
describe('utils.fromJson', () => {
  it('should detect and generate SCP files from JSON inputs', (done) => {
    const inputJson = require(`./scp/scp_generator/h3r_input.json`)

    testJsonGeneratorUtils(scp, inputJson, done)
  })

  it('should detect and generate TZR files from JSON inputs', (done) => {
    const inputJson = require(`./interval/interval_generator/h3r_input.json`)

    testJsonGeneratorUtils(tzr, inputJson, done)
  })

  it('should detect and generate TZE files from JSON inputs', (done) => {
    const inputJson = require(`./events/event_generator/h3r_input.json`)

    testJsonGeneratorUtils(tze, inputJson, done)
  })

  it('should detect and generate TZS files from JSON inputs', (done) => {
    const inputJson = require(`./settings/settings_from_json/h3r_input.json`)

    testJsonGeneratorUtils(tzs, inputJson, done)
  })

  it('should detect and generate TZA files from JSON inputs', (done) => {
    const inputJson = require(`./actions/actions_from_json/h3r_input.json`)

    testJsonGeneratorUtils(tza, inputJson, done)
  })

  it('should detect and generate TZG files from JSON inputs', (done) => {
    const inputJson = require(`./accel/accel_from_json/hpr_input.json`)

    testJsonGeneratorUtils(tzg, inputJson, done)
  })

  it('should throw an error if given an unsupported file type', (done) => {
    const inputData = fs.readFileSync(`./README.md`)

    try {
      utils.fromJson(inputData)
      done('utils.fromJson failed to throw!')
    } catch (err) {
      expect(err.message).to.equal('Unsupported file type!')
      done()
    }
  })
})

// ==============================================================================
describe('utils', () => {
  it('should return an error on invalid drive identifiers', (done) => {
    utils
      .ejectDevice('0://')
      .then(() => {
        done(new Error('Should have thrown!'))
      })
      .catch((err) => {
        try {
          expect(err.message).to.contain('no such file or directory')
          done()
        } catch (e) {
          done(e)
        }
      })
  })

  it('should return an error on non trident device drives', (done) => {
    const testDrive = process.platform === 'win32' ? 'C:/' : '/'

    utils
      .ejectDevice(testDrive)
      .then(() => {
        done(new Error('Should have thrown!'))
      })
      .catch((err) => {
        try {
          expect(err.message).to.contain('No configuration found')
          done()
        } catch (e) {
          done(e)
        }
      })
  })

  it('should return an error on non-ejectable device drives', (done) => {
    const testDrive = process.platform === 'win32' ? 'C:/' : '/'
    const expectedError =
      process.platform === 'win32' ? 'Failed with result: 23' : 'Command failed:'
    utils
      .ejectDevice(testDrive, false)
      .then(() => {
        done(new Error('Should have thrown!'))
      })
      .catch((err) => {
        try {
          expect(err.message).to.contain(expectedError)
          done()
        } catch (e) {
          done(e)
        }
      })
  })
})
