# rtc_generator

Use this program to create a \*.rtc file to be placed on the Trident device to
update the device's system time.

The file created will include the current UTC offset in minutes as a 2-byte
signed integer. The device will use this offset, along with the timestamp of the
file, to set its own system clock. Because the system time is dependent on the
file timestamp, the device drive should be ejected as soon after the file is
written as possible.

## NOTE

Since the Trident device uses the "Date modified" field set by the PC when it
creates the file on the device, the UTC offset _must_ be entered as the time
zone of the PC that is copying the file onto the device, NOT the timezone of the
system that is creating the file (if these two entities are not the same).

If the time zone of the PC is not known, the UTC offset should be set to the
special value 32767 (0x7FFF) and the device will track the _local_ time of the
machine that set its clock, rather than tracking UTC.
