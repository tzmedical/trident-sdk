#ifndef RTC_GENERATOR_H
#define RTC_GENERATOR_H

#include <iostream>
#include <map>

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//! Uses internal memory to generate an rtc file
//! \return 0 if successful
int GenerateRtc(
  std::istream* inStream, std::ostream* outStream, std::ostream* errStream,
  std::ostream* logStream);

#endif