/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       battery_info_generator.cpp
 *          - This program generates a info.batt battery info file based on the
 *                values in the input file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                clang
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./battery_info_generator input.txt > info.batt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <cstdint>
#include <queue>

#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define MAX_TYPES (1)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint16_t type;
  uint16_t full_cap;
  uint16_t cycles;
  uint16_t rcomp_0;
  uint16_t temp_co;
  uint16_t q_residual_00;
  uint16_t q_residual_10;
  uint16_t q_residual_20;
  uint16_t q_residual_30;
  uint16_t d_qacc;
  uint16_t d_pacc;
} battery_info_t;

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

// This is here so we can add more type names if we use new fuel gauges
static char type_names[MAX_TYPES][10] = {"MAX17047"};

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

int generate_battery_info(battery_info_t *battery_info, queue<string> arguments, ostream *errStream)
{
  while (!arguments.empty()) {
    string argument = arguments.front();
    arguments.pop();

    if (argument.find("fuel_gauge_type=") != string::npos) {
      string tempStr(argument.substr(argument.find("=") + 1));
      if (tempStr.length()) {
        battery_info->type = 0;
        // Determine the fuel gauge type
        while ((battery_info->type < MAX_TYPES)
               && (tempStr.find(type_names[battery_info->type]) == string::npos)) {
          battery_info->type++;
        }

        if (battery_info->type >= MAX_TYPES) {
          *errStream << "ERROR! Invalid string: fuel_gauge_type=" << tempStr << endl;
          return -1;
        }
      }
    }
    else if (argument.find("full_cap=") != string::npos) {
      battery_info->full_cap = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("cycles=") != string::npos) {
      battery_info->cycles = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("rcomp_0=") != string::npos) {
      battery_info->rcomp_0 = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("temp_co=") != string::npos) {
      battery_info->temp_co = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("q_residual_00=") != string::npos) {
      battery_info->q_residual_00 = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("q_residual_10=") != string::npos) {
      battery_info->q_residual_10 = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("q_residual_20=") != string::npos) {
      battery_info->q_residual_20 = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("q_residual_30=") != string::npos) {
      battery_info->q_residual_30 = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("d_qacc=") != string::npos) {
      battery_info->d_qacc = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
    else if (argument.find("d_pacc=") != string::npos) {
      battery_info->d_pacc = stoi(argument.substr(argument.find_first_of('=') + 1));
    }
  }

  battery_info->length = sizeof(battery_info_t);

  uint16_t crcValue = 0xffff;
  crcBlock(((uint8_t *)battery_info) + 2, battery_info->length - 2, &crcValue);
  battery_info->crc_value = crcValue;

  return 0;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

int BatteryInfoFromText(
  istream *inStream, map<string, string> &extraArgs, ostream *outStream, ostream *errStream,
  ostream *logStream)
{
  battery_info_t battery_info = {0};

  int batteryInfoLength;
  int generatorStatus = -1;
  queue<string> arguments;

  // Loop through the entire input to get all the arguments
  while (!inStream->eof()) {
    // Get a single line from the input
    string argument;
    getline(*inStream, argument);

    string value;
    value = "";

    // Trim any leading whitespace
    const size_t begStr = argument.find_first_not_of(" \t");
    if (begStr != string::npos) argument.erase(0, begStr);

    // Trim any trailing whitespace
    const size_t endStr = argument.find_last_not_of(" \n\r\t");
    if (endStr != string::npos) argument.resize(endStr + 1);

    // Skip the line if it is a comment
    if (argument.find("#") == 0) {
      continue;
    }

    // Only process lines that have more than 8 characters in them
    if (argument.length() > 4) {
      // Trim whitespace before or after the "="
      bool done = 0;
      while (!done) {
        size_t found;
        if ((found = argument.find(" =")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("\t=")) != string::npos) {
          argument.erase(found, 1);
        }
        else if ((found = argument.find("= ")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else if ((found = argument.find("=\t")) != string::npos) {
          argument.erase(found + 1, 1);
        }
        else {
          done = 1;
        }
      }

      arguments.push(argument);
    }
  }

  generatorStatus = generate_battery_info(&battery_info, arguments, errStream);
  batteryInfoLength = sizeof(battery_info_t);

  if (!generatorStatus) {
    outStream->write((const char *)&battery_info, batteryInfoLength);
  }
  else {
    cout << "Could not generate battery info file: (" << generatorStatus << ")" << endl;
    return generatorStatus;
  }

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads in a file from cin and parses out the events.
//==============================================================================
int main(int argc, char *argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdout), O_BINARY);
#endif

  istream *input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return BatteryInfoFromText(input, extraArgs, &cout, &cerr, &clog);
}
#endif