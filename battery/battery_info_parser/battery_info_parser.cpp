/******************************************************************************
 *       Copyright (c) 2020, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       battery_info_parser.cpp
 *          - This program parses a info.batt battery info file from <inFile> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./battery_info_parser inFile.batt > outFile.txt
 *
 *
 *
 *****************************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <cstdint>
#include <map>

#include "util/crc.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define MAX_FILE_SIZE (512)
#define MAX_TYPES     (1)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

typedef struct {
  uint16_t crc_value;
  uint16_t length;
  uint16_t type;
  uint16_t full_cap;
  uint16_t cycles;
  uint16_t rcomp_0;
  uint16_t temp_co;
  uint16_t q_residual_00;
  uint16_t q_residual_10;
  uint16_t q_residual_20;
  uint16_t q_residual_30;
  uint16_t d_qacc;
  uint16_t d_pacc;
} battery_info_t;

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

// This is here so we can add more type names if we use new fuel gauges
static char type_names[MAX_TYPES][10] = {"MAX17047"};

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//==============================================================================
static int battery_info_from_buffer(
  uint8_t* buffer, battery_info_t* dest, uint16_t offset, uint16_t max_offset, ostream* errStream)
{
  uint16_t reported_crc = ((uint16_t*)buffer)[offset];
  uint16_t reported_length = ((uint16_t*)buffer)[offset + 1];

  if (!reported_length) reported_length = sizeof(battery_info_t);
  if (offset + reported_length > max_offset) {
    *errStream << "Reported length (" << reported_length << ") is greater than the max offset ("
               << max_offset << "). Aborting" << endl;
    return -1;
  }

  uint16_t calculated_crc = 0xffff;
  crcBlock(&buffer[offset] + 2, reported_length - 2, &calculated_crc);

  if (reported_crc != calculated_crc) {
    *errStream << "Reported CRC (" << reported_crc << ") does not match calculated CRC ("
               << calculated_crc << "). Aborting" << endl;
    return -1;
  }

  memcpy(dest, &buffer[offset], sizeof(battery_info_t));
  return 0;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//==============================================================================
int BatteryInfoToText(
  istream* inStream, map<string, string>& extraArgs, ostream* outStream, ostream* errStream,
  ostream* logStream)
{
  uint16_t i = 0;

  battery_info_t battery_info_struct;

  const int min_input = sizeof(battery_info_t);
  const int max_input = MAX_FILE_SIZE;

  int battery_info_found = 1;

  uint8_t input_buffer[max_input] = {};

  // Read in the file
  while (!inStream->eof() && i < max_input) {
    input_buffer[i++] = inStream->get();
  }

  // Eat the rest of the input
  while (!inStream->eof()) {
    i++;
    inStream->get();
  }

  // The read that set input->eof() to true incremented i past the Calculated file size
  i--;

  // Report any errors with input size here
  if (i > max_input) {
    *errStream << "File Size (" << i << ") is larger than expected (" << max_input << "). Aborting."
               << endl;
    return -1;
  }
  else if (i < min_input) {
    *errStream << "File Size (" << i << ") is smaller than expected (" << min_input
               << "). Aborting." << endl;
    return -1;
  }

  if (battery_info_from_buffer(input_buffer, &battery_info_struct, 0, MAX_FILE_SIZE, errStream)) {
    return -1;
  }

  if (battery_info_struct.type >= MAX_TYPES) {
    *errStream << "Unknown fuel gauge type: (" << battery_info_struct.type << ")" << endl;
    return -1;
  }

  // Output the appropriate struct elements
  *outStream << "file.type=TZBATT" << endl;
  *outStream << "fuel_gauge_type=" << type_names[battery_info_struct.type] << endl;
  *outStream << "full_cap=" << battery_info_struct.full_cap << endl;
  *outStream << "cycles=" << battery_info_struct.cycles << endl;
  *outStream << "rcomp_0=" << battery_info_struct.rcomp_0 << endl;
  *outStream << "temp_co=" << battery_info_struct.temp_co << endl;
  *outStream << "q_residual_00=" << battery_info_struct.q_residual_00 << endl;
  *outStream << "q_residual_10=" << battery_info_struct.q_residual_10 << endl;
  *outStream << "q_residual_20=" << battery_info_struct.q_residual_20 << endl;
  *outStream << "q_residual_30=" << battery_info_struct.q_residual_30 << endl;
  *outStream << "d_qacc=" << battery_info_struct.d_qacc << endl;
  *outStream << "d_pacc=" << battery_info_struct.d_pacc << endl;

  return 0;
}

#ifdef USE_MAIN
//==============================================================================
//    main()
//
//    This function reads a backup file from cin and parses it
//==============================================================================
int main(int argc, char* argv[])
{
#ifdef _WIN32
  _setmode(_fileno(stdin), O_BINARY);
#endif

  istream* input;
  fstream inFile;
  map<string, string> extraArgs;

  if (argc >= 2) {
    // Open the file and check the file length
    inFile.open(argv[1], ios::in | ios::binary);
    input = &inFile;
  }
  else {
    // Otherwise use standard input
    input = &cin;
  }

  return BatteryInfoToText(input, extraArgs, &cout, &cerr, &clog);
}
#endif