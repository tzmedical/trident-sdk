#!/usr/bin/env node
/* eslint-env node */
/* eslint-disable no-console */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const { resolve } = require('path')
const { program } = require('commander')
const fs = require('fs')
const { promisify } = require('util')

const readdir = promisify(fs.readdir)
const stat = promisify(fs.stat)
const readFile = promisify(fs.readFile)
const { version: appVersion } = require('./package.json')
const { tze, tzr, tza, tzs, tzg, scp, bak, batt, error, queue } = require('./index')

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const SUFFIX_REGEX_LOOKUP = {
  tze: /\.tze$/,
  tzr: /\.tzr$/,
  tza: /\.tza$/,
  tzs: /\.tzs$/,
  tzg: /\.tzg$/,
  scp: /\.scp$/,
  error: /\.log$/,
  bak: /\.tmp$/,
  batt: /\.batt$/,
}

const SUFFIX_LOOKUP = {
  tze: '.tze',
  tzr: '.tzr',
  tza: '.tza',
  tzs: '.tzs',
  tzg: '.tzg',
  scp: '.scp',
  error: '.log',
  bak: '.tmp',
  batt: '.batt',
}

const PARSER_LOOKUP = {
  tze,
  tzr,
  tza,
  tzs,
  tzg,
  scp,
  error,
  bak,
  batt,
}

const FORMAT_ID_LOOKUP = {
  tze: 'TZEVT',
  tzr: 'TZINT',
  tza: 'TZACT',
  tzs: 'TZSET',
  tzg: 'TZACC',
  scp: 'SCPECG',
  error: '- Device Booting (',
  bak: 'TZBAK',
  batt: 'TZBATT',
}

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
function getKeyByValue(object, value) {
  return Object.keys(object).find((key) => object[key] === value)
}

// ==============================================================================
async function getFiles(path) {
  let files
  if ((await stat(path)).isDirectory()) {
    const subDirs = await readdir(path)
    files = await Promise.all(
      subDirs.map(async (subDir) => {
        const res = resolve(path, subDir)
        return (await stat(res)).isDirectory() ? getFiles(res) : res
      })
    )
  } else {
    files = [resolve(path)]
  }
  return files.reduce((a, f) => a.concat(f), [])
}

// ==============================================================================
function checkFileSuffix(filename, formatOption, supportedFormats) {
  if (!formatOption) {
    const patterns = supportedFormats.map((format) => SUFFIX_REGEX_LOOKUP[format])
    return patterns.some((pattern) => filename.match(pattern))
  }

  if (!supportedFormats.includes(formatOption)) {
    return false
  }

  return filename.match(SUFFIX_REGEX_LOOKUP[formatOption])
}

// ==============================================================================
function resolveFileParser(filename, formatOption, supportedFormats) {
  if (formatOption) {
    if (!supportedFormats.includes(formatOption)) {
      throw new Error('Unsupported file type!')
    }

    return PARSER_LOOKUP[formatOption]
  }

  const parser = supportedFormats.reduce(
    (prev, current) =>
      prev || (filename.match(SUFFIX_REGEX_LOOKUP[current]) ? PARSER_LOOKUP[current] : undefined),
    undefined
  )
  if (!parser) {
    throw new Error('Unsupported file type!')
  }
  return parser
}

// ==============================================================================
function resolveFileGenerator(filename, formatOption, supportedFormats, buffer) {
  try {
    return resolveFileParser(filename, formatOption, supportedFormats)
  } catch (err) {
    const generator = supportedFormats.reduce(
      (prev, current) =>
        prev ||
        (buffer.toString().includes(FORMAT_ID_LOOKUP[current])
          ? PARSER_LOOKUP[current]
          : undefined),
      undefined
    )
    if (!generator) {
      throw new Error('Unsupported file type!')
    }
    return generator
  }
}

// ==============================================================================
function appendSuffix(filename, parser) {
  const key = getKeyByValue(PARSER_LOOKUP, parser)
  const suffix = SUFFIX_LOOKUP[key]
  if (!filename.endsWith(suffix)) {
    return filename.concat(suffix)
  }
  return filename
}

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

// ==============================================================================
program.name('trident-sdk').version(appVersion)

// ==============================================================================
program
  .command('toText <inPath> [outPath]')
  .usage('')
  .option(
    '-f, --format <type>',
    'the format of file to parse: tze, tzr, tza, tzs, scp, bak, or batt'
  )
  .option('-e, --epoch-ms <offset>', 'the starting ms epoch for HR calculations in tzr files')
  .option('-t, --tag <tag>', 'event tag number to search for in tzr or tze files')
  .option('--skip-crc', 'ignore the crc value in the file and print the parsing results')
  .option('--no-file-header', "don't print out the file header, just the entries")
  .option('--device-type <device>', 'type of device that created this backup file')
  .description('Parse a binary file into a text format, based on the suffix of the file.')
  .action(async (inPath, outPath, options) => {
    const files = await getFiles(inPath)

    const inRes = resolve(inPath)
    const outRes = outPath ? resolve(outPath) : undefined
    const supportedFormats = ['tze', 'tzr', 'tza', 'tzs', 'scp', 'bak', 'batt']
    const supportedForDirectory = ['tze', 'tzr', 'tza', 'tzs', 'scp']

    await Promise.all(
      files.map(async (inFile) => {
        try {
          const outFile = outRes
            ? inFile.replace(inRes, outRes).replace(/\.[a-z]*$/, '.txt')
            : undefined

          // If we're processing a directory, we will only process files based
          // on the file suffix. This can be limited to a single filetype using
          // the `-f` flag.
          if (inFile !== resolve(inPath)) {
            if (!checkFileSuffix(inFile, options.format, supportedForDirectory)) {
              console.warn(`Skipping file based on suffix: ${inFile}`)
              return
            }
          }

          // Read the file into memory and figure out what parser we need to call
          const buffer = await readFile(inFile)
          const file = resolveFileParser(inFile, options.format, supportedFormats)

          // Convert the extra CLI options into function arguments
          const extraArgs = {
            tag: options.tag ? `${options.tag}` : '0',
            skipCrc: options.skipCrc ? '1' : '0',
            printHeader: options.fileHeader ? '1' : '0',
            ...(options.deviceType && { deviceType: `${options.deviceType}` }),
            ...(options.epochMs && { epochMs: `${options.epochMs}` }),
          }

          // Parse the file
          const result = await file.toText(buffer, extraArgs)
          if (inFile !== resolve(inPath)) {
            console.warn(`toText: ${inFile}${!outFile ? '' : ` --> ${outFile}`}`)
          }
          if (outFile) {
            fs.writeFileSync(outFile, result)
          } else {
            console.log(result)
          }
        } catch (err) {
          console.error(`ERROR: processing ${inFile}`)
          console.error(err)
          if (files.length === 1) {
            process.exit(1)
          }
        }
      })
    )
  })

// ==============================================================================
program
  .command('fromText <inPath> <outPath>')
  .usage('')
  .option('-f, --format <type>', 'the format of file to generate: tza, tzs, bak, or batt')
  .description('Generate a binary file from a text format, based on the suffix of the output file.')
  .action(async (inPath, outPath, options) => {
    const files = await getFiles(inPath)
    const inRes = resolve(inPath)
    const outRes = resolve(outPath)
    const supportedFormats = ['tza', 'tzs', 'bak', 'batt']

    await Promise.all(
      files.map(async (inFile) => {
        try {
          let outFile = resolve(outPath)
          if (inRes !== inFile) {
            outFile = inFile.replace(inRes, outRes).replace(/\.[a-zA-Z0-9]*$/, '')
          }

          // If we're processing a directory, we will only process files based
          // on the file suffix.
          let processSuffix = /.*/
          if (inFile !== resolve(inPath)) {
            processSuffix = /\.txt$/
          }
          if (!inFile.match(processSuffix)) {
            console.warn(`Skipping file based on suffix: ${inFile}`)
            return
          }

          // Read the file into memory and figure out what parser we need to call
          const buffer = await readFile(inFile)
          const file = resolveFileGenerator(outFile, options.format, supportedFormats, buffer)

          // If we're processing a directory, make sure to add the suffix
          // based on the format.
          if (inFile !== resolve(inPath)) {
            outFile = appendSuffix(outFile, file)
          }

          // Convert the extra CLI options into function arguments
          const extraArgs = {
            ...(options.deviceType && { deviceType: `${options.deviceType}` }),
            ...(options.firmwareVersion && { firmwareVersion: `${options.firmwareVersion}` }),
          }

          // Generate the file
          const result = await file.fromText(buffer.toString(), extraArgs)
          if (inFile !== resolve(inPath)) {
            console.warn(`fromText: ${inFile} --> ${outFile}`)
          }
          fs.writeFileSync(outFile, result)
        } catch (err) {
          console.error(`ERROR: processing ${inFile}`)
          console.error(err)
          process.exit(1)
        }
      })
    )
  })

// ==============================================================================
program
  .command('toJson <inPath> [outPath]')
  .usage('')
  .option(
    '-f, --format <type>',
    'the format of file to parse: tze, tzr, tza, tzs, tzg, error, or scp'
  )
  .option(
    '-z, --zone <zoneName>',
    'the time zone locale name or numeric offset (e.g. `America/New_York`, `-480`, `UTC+2:00`)'
  )
  .option('--skip-crc', 'ignore the crc value in the file and print the parsing results')
  .description('Parse a binary file into a JSON format, based on the suffix of the file.')
  .action(async (inPath, outPath, options) => {
    const files = await getFiles(inPath)
    const inRes = resolve(inPath)
    const outRes = outPath ? resolve(outPath) : undefined
    const supportedFormats = ['tze', 'tzr', 'tza', 'tzs', 'tzg', 'scp', 'error']

    await Promise.all(
      files.map(async (inFile) => {
        try {
          const outFile = outRes
            ? inFile.replace(inRes, outRes).replace(/\.[a-z]*$/, '.json')
            : undefined

          // If we're processing a directory, we will only process files based
          // on the file suffix. This can be limited to a single filetype using
          // the `-f` flag.
          if (inFile !== resolve(inPath)) {
            if (!checkFileSuffix(inFile, options.format, supportedFormats)) {
              console.warn(`Skipping file based on suffix: ${inFile}`)
              return
            }
          }

          // Read the file into memory and figure out what parser we need to call
          const buffer = await readFile(inFile)
          const file = resolveFileParser(inFile, options.format, supportedFormats)

          // Parse the file
          const extraArgs = {
            skipCrc: options.skipCrc ? '1' : '0',
          }
          if (options.zone) {
            extraArgs.zone = `${options.zone}`
          }
          const result = await file.toJson(buffer, extraArgs)
          if (inFile !== resolve(inPath)) {
            console.warn(`toJson: ${inFile}${!outFile ? '' : ` --> ${outFile}`}`)
          }
          if (outFile) {
            fs.writeFileSync(outFile, JSON.stringify(result))
          } else {
            console.log(JSON.stringify(result))
          }
        } catch (err) {
          console.error(`ERROR: processing ${inFile}`)
          console.error(err)
          process.exit(1)
        }
      })
    )
  })

// ==============================================================================
program
  .command('fromJson <inPath> <outPath>')
  .usage('')
  .option('-f, --format <type>', 'the format of file to generate: tze, tzr, tza, tzs, or scp')
  .description('Generate a binary file from a JSON format, based on the suffix of the file.')
  .action(async (inPath, outPath, options) => {
    const files = await getFiles(inPath)
    const inRes = resolve(inPath)
    const outRes = resolve(outPath)
    const supportedFormats = ['tze', 'tzr', 'tza', 'tzs', 'tzg', 'scp']

    await Promise.all(
      files.map(async (inFile) => {
        try {
          let outFile = resolve(outPath)
          if (inRes !== inFile) {
            outFile = inFile.replace(inRes, outRes).replace(/\.[a-zA-Z0-9]*$/, '')
          }

          // If we're processing a directory, we will only process files based
          // on the file suffix.
          let processSuffix = /.*/
          if (inFile !== resolve(inPath)) {
            processSuffix = /\.json$/
          }
          if (!inFile.match(processSuffix)) {
            console.warn(`Skipping file based on suffix: ${inFile}`)
            return
          }

          // Read the file into memory and figure out what parser we need to call
          const buffer = await readFile(inFile)
          const file = resolveFileGenerator(outFile, options.format, supportedFormats, buffer)

          // If we're processing a directory, make sure to add the suffix
          // based on the format.
          if (inFile !== resolve(inPath)) {
            outFile = appendSuffix(outFile, file)
          }

          // Generate the file
          const result = await file.fromJson(buffer.toString())
          if (inFile !== resolve(inPath)) {
            console.warn(`fromJson: ${inFile} --> ${outFile}`)
          }
          fs.writeFileSync(outFile, result)
        } catch (err) {
          console.error(`ERROR: processing ${inFile}`)
          console.error(err)
          process.exit(1)
        }
      })
    )
  })

// ==============================================================================
program
  .command('metaJson <deviceType>')
  .usage('')
  .requiredOption('-f, --format <type>', 'the format of file to parse: tzs')
  .description('Output metadata for the filetype in a JSON format.')
  .action(async (deviceType, options) => {
    let file
    if (options.format === 'tzs') {
      file = tzs
    } else {
      console.error(`ERROR: ${options.format} - Unsupported file type!`)
      process.exit(1)
    }
    try {
      const result = await file.metaJson(deviceType)

      console.log(JSON.stringify(result))
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })

// ==============================================================================
program
  .command('find <directory> [outPath]')
  .usage('')
  .requiredOption('-f, --format <type>', 'the format of files to search: tzr or tze')
  .option('-t, --tag <tag>', 'event tag number to search for in tzr or tze files')
  .option('--skip-crc', 'ignore the crc value in the file and print the parsing results')
  .option('--no-file-header', "don't print out the file header, just the entries")
  .description('Search a directory.')
  .action(async (directory, outPath, options) => {
    let file
    if (options.format === 'tze') {
      file = tze
    } else if (options.format === 'tzr') {
      file = tzr
    } else {
      throw new Error('Unsupported file type!')
    }

    // Convert the extra CLI options into function arguments
    const extraArgs = {
      tag: options.tag ? `${options.tag}` : '0',
      skipCrc: options.skipCrc ? '1' : '0',
      printHeader: options.fileHeader ? '1' : '0',
    }

    try {
      const result = await file.find(directory, extraArgs)
      if (outPath) {
        fs.writeFileSync(outPath, result)
      } else {
        console.log(result)
      }
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })

// ==============================================================================
program
  .command('parse <directory> [outPath]')
  .usage('')
  .requiredOption('-s, --serial-number <serial-number>', 'the complete serial number of the device')
  .option('--tzmr-compat', 'set if the device is in tzmr compat mode')
  .description('Find and parse wireless files into a JSON format from the tmp folder.')
  .action(async (directory, outPath, options) => {
    const cliTzmrCompat = options.tzmrCompat ? '1' : '0'
    const cliSerialNumber = options.serialNumber || ''

    const args = { serialNumber: cliSerialNumber, tzmrCompat: cliTzmrCompat }

    try {
      const result = await queue.parse(directory, args)
      if (outPath) {
        fs.writeFileSync(outPath, JSON.stringify(result))
      } else {
        console.log(JSON.stringify(result))
      }
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })

// ==============================================================================
program
  .command('retrieveFile <directory> <outPath>')
  .usage('')
  .requiredOption('-s, --sequence-number <sequence>', 'the sequence number of the file')
  .requiredOption('-f, --format <type>', 'the format of files to search: scp')
  .description('Find an SCP file in a directory given a sequence number.')
  .action(async (directory, outPath, options) => {
    const cliSequenceNumber = options.sequenceNumber
    const cliFileType = options.format

    if (cliFileType !== 'scp') {
      throw new Error('Unsupported file type!')
    }

    if (cliSequenceNumber > 99999999) {
      throw new Error('Invalid sequence number!')
    }

    try {
      const result = await scp.retrieveFile(directory, cliSequenceNumber)
      fs.writeFileSync(outPath, result)
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })

program.parse(process.argv)
